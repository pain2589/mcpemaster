/*============================================================================
 PROJECT: SkinEditor
 FILE:    SEMapObject.m
 AUTHOR:  Nguyen Minh Khoai
 DATE:    10/18/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SEMapObject.h"

@interface SEMapObject()
@end

@implementation SEMapObject

+ (instancetype)newObject {
    return [[self alloc] init];
}

@end
