/*============================================================================
 PROJECT: McpeMaster
 FILE:    LeftMenuViewController.m
 AUTHOR:  Ngoc Tam Nguyen
 DATE:    12/28/14
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "LeftMenuViewController.h"
#import "CategoryLeftMenuCell.h"
#import "LeftMenuSectionHeaderView.h"
#import "BaseNavigationViewController.h"
#import "ClipViewController.h"
#import "TYMActivityIndicatorView.h"
#import "PRGlobalUtils.h"
#import "AboutViewController.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/
#define kMusicCategoryId    10
static NSString *leftMenuTableHeaderViewID = @"LeftMenuSectionHeaderView";

@interface LeftMenuViewController () <UITableViewDataSource, UITableViewDelegate, FBSDKLoginButtonDelegate>
@property(weak, nonatomic) IBOutlet UITableView *mainTableView;
@end

@implementation LeftMenuViewController{
    MBProgressHUD *HUD;
    BOOL loginErr;
}

- (void)startLoading {
    TYMActivityIndicatorView *loading = self.loadingIndicatorView;
    loading.center = CGPointMake(CGRectGetMidX(self.mainTableView.frame), loading.center.y);
    [loading startAnimating];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.loginButton = [[FBSDKLoginButton alloc] init];
    // Optional: Place the button in the center of your view.
    CGRect btnLoginFrame = CGRectMake(0, 0, self.loginView.frame.size.width, self.loginView.frame.size.height);
    self.loginButton.frame = btnLoginFrame;
    self.loginButton.delegate = self;
    [self.loginView addSubview:self.loginButton];
    
    if ([FBSDKAccessToken currentAccessToken]) {
        // User is logged in
        [FBSDKProfile loadCurrentProfileWithCompletion:
         ^(FBSDKProfile *profile, NSError *error) {
             if (profile) {
                 self.lbWelcome.text = [NSString stringWithFormat:@"Welcome, %@", profile.firstName];
             }
         }];
    } else {
        self.lbWelcome.text = @"Welcome Bro";
    }
    
    /* Hide navigation bar */
    self.navigationController.navigationBarHidden = YES;

    /* Remove extra sperator line below of table view */
    self.mainTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0.1f)];
    self.mainTableView.sectionFooterHeight = 0.1f;

    /* Register NIB for UITableView */
    UINib *nib = [UINib nibWithNibName:leftMenuTableHeaderViewID bundle:nil];
    [self.mainTableView registerNib:nib forHeaderFooterViewReuseIdentifier:leftMenuTableHeaderViewID];

    nib = [UINib nibWithNibName:@"LeftMenuSwitchCell" bundle:nil];
    [self.mainTableView registerNib:nib forCellReuseIdentifier:@"LeftMenuSwitchCell"];

    // Register all Notification
    NotifReg(self, @selector(reloadData), kNotificationLeftMenuShouldRefreshContent);
    
    //
//    self.selectedCategoryID = @"TopMap";
//    if ([AppDelegate sharedDelegate].mcpeType == AppFlag)
        self.selectedCategoryID = @"SkinEditor";

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t) (0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self reloadData];
    });
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (loginErr) {
        loginErr = NO;
        [UIAlertController showConfirmPopUpWithTitle:nil
                                             message:@"Login Failed. Please try again!"
                                        buttonTitles:@[@"OK"]
                                       completeBlock:nil];
    }
}

- (void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton {
    self.lbWelcome.text = @"Welcome Bro";
}

- (void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error {
    if (error) {
        //NSLog(@"Process error");
        self->loginErr = YES;
    } else if (result.isCancelled) {
        //NSLog(@"Cancelled");
    } else {
        //NSLog(@"Logged in");
        FBSDKGraphRequest *graphRequest = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"name"}];
        [graphRequest startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
            if (!error) {
                self.lbWelcome.text = [NSString stringWithFormat:@"Welcome, %@", result[@"name"]];
            }
        }];
    }
}

- (void)setSelectedCategoryID:(NSString *)selectedCategoryID {
    _selectedCategoryID = selectedCategoryID;
    _selectedPlaylistData = nil;
    _selectedIndexPath = nil;
}

- (void)setSelectedPlaylistData:(PRPlaylistData *)selectedPlaylistData {
    _selectedPlaylistData = selectedPlaylistData;
    _selectedCategoryID = nil;
    _selectedIndexPath = nil;
}

- (void)setSelectedIndexPath:(NSIndexPath *)selectedIndexPath {
    _selectedIndexPath = selectedIndexPath;
    _selectedCategoryID = nil;
    _selectedPlaylistData = nil;
}

#pragma mark - UITableView's delegates and datasources

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ([AppDelegate sharedDelegate].mcpeType == AppFlag){
        return 2;
    }
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kLeftMenuSectionHeaderHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    LeftMenuSectionHeaderView *view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"LeftMenuSectionHeaderView"];
    if ([AppDelegate sharedDelegate].mcpeType == AppFlag){
        if (section == 0){
            view.sectionTitleLabel.text = @"Master Tools";
        }else{
            view.sectionTitleLabel.text = @"About";
        }
    }else{
        if (section == 0) {
            view.sectionTitleLabel.text = @"Master Tools";
        }else if (section == 1) {
            view.sectionTitleLabel.text = @"Resources";
        }else{
            view.sectionTitleLabel.text = @"About";
        }
    }
    
    return view;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([AppDelegate sharedDelegate].mcpeType == AppFlag){
        if (section == 0) {
            return 1;
        } else if (section == 1) {
            return 3;
        }
    }
        
    if (section == 0) {
        return 2;
    } else if (section == 2) {
        return 3;
    } else {
        return 5;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kCategoryLeftMenuCellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CategoryLeftMenuCell *cell = (CategoryLeftMenuCell *) [self.mainTableView dequeueReusableCellWithIdentifier:@"CategoryLeftMenuCell"];
    if (!cell) {
        cell = (CategoryLeftMenuCell *) [IBHelper loadViewNib:@"CategoryLeftMenuCell"];
        cell.backgroundColor = [UIColor clearColor];
    }
    
    PRServiceCategory *category;
    
    if ([AppDelegate sharedDelegate].mcpeType == AppFlag){
        if (indexPath.section == 0) {
            category = [PRGlobalUtils skinCategories][indexPath.row];
            cell.categoryTitleLabel.text = category.name;
            
            /* Selected cell */
            cell.isSelected = [category.value isEqualToString:self.selectedCategoryID];
            
            return cell;
        } else {
            if (indexPath.row == 0) {
                cell.categoryTitleLabel.text = @"About us";
                //cell.iconImageView.image = [UIImage imageNamed:@"btnQuestion3"];
                cell.iconImageView.image = nil;
                /* Selected cell */
                cell.isSelected = [self.selectedCategoryID isEqualToString:@"About us"];
                
                return cell;
            } else if (indexPath.row == 1) {
                cell.categoryTitleLabel.text = @"Terms of Use";
                cell.iconImageView.image = nil;
                /* Selected cell */
                cell.isSelected = NO;
                
                return cell;
            } else {
                cell.categoryTitleLabel.text = @"Privacy Policy";
                cell.iconImageView.image = nil;
                /* Selected cell */
                cell.isSelected = NO;
                
                return cell;
            }
        }
    }else{
        if (indexPath.section != 2){
            /* Now show list category from services */
            if (indexPath.section == 0) {
                category = [PRGlobalUtils skinCategories][indexPath.row];
            }else if (indexPath.section == 1) {
                if (indexPath.row == 0){
                    category = [PRGlobalUtils mapCategories][0];
                } else if (indexPath.row == 1){
                    category = [PRGlobalUtils addonCategories][0];
                } else if (indexPath.row == 2){
                    category = [PRGlobalUtils textureCategories][0];
                } else if (indexPath.row == 3){
                    category = [PRGlobalUtils serverCategories][0];
                } else if (indexPath.row == 4){
                    category = [PRGlobalUtils seedCategories][0];
                }
            }
            
            cell.categoryTitleLabel.text = category.name;
//            //cell.iconImageView.image = [UIImage imageNamed:category.extra];
            cell.iconImageView.image = nil;
            
            /* Selected cell */
            cell.isSelected = [category.value isEqualToString:self.selectedCategoryID];
            
            return cell;
        }else{
            if (indexPath.row == 0) {
                cell.categoryTitleLabel.text = @"About us";
                //cell.iconImageView.image = [UIImage imageNamed:@"btnQuestion3"];
                cell.iconImageView.image = nil;
                /* Selected cell */
                cell.isSelected = [self.selectedCategoryID isEqualToString:@"About us"];
                
                return cell;
            } else if (indexPath.row == 1) {
                cell.categoryTitleLabel.text = @"Terms of Use";
                cell.iconImageView.image = nil;
                /* Selected cell */
                cell.isSelected = NO;
                
                return cell;
            } else {
                cell.categoryTitleLabel.text = @"Privacy Policy";
                cell.iconImageView.image = nil;
                /* Selected cell */
                cell.isSelected = NO;
                
                return cell;
            }
        }
    }
}

- (void)tableView:(UITableView *)tableView
  willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell.contentView setBackgroundColor:[UIColor clearColor]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    PRServiceCategory *category;
    
    if ([AppDelegate sharedDelegate].mcpeType == AppFlag){
        /* Now show list category from services */
        if (indexPath.section == 0) {
            category = [PRGlobalUtils skinCategories][indexPath.row];
            if (![self.selectedCategoryID isEqualToString:category.value]) {
                self.selectedCategoryID = category.value;
                if  ([self.selectedCategoryID isEqualToString:@"SkinEditor"]){
                    [[PRAppDelegate rootViewController] showSkinEditorViewController];
                    
                    /* Set selected this category */
                    [tableView reloadData];
                }else{
                    [[PRAppDelegate rootViewController] showClipViewControllerWithSelectedCategory:category isFavoriteView:NO];
                    
                    /* Set selected this category */
                    [tableView reloadData];
                }
            }
        }else if (indexPath.section == 1) {
            if (indexPath.row == 0) {
                [[PRAppDelegate rootViewController] showAboutViewController];
                self.selectedCategoryID = @"About us";
                [tableView reloadData];
            } else if (indexPath.row == 1) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://mcpemaster.co/terms.html"] options:@{} completionHandler:nil];
            } else {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://mcpemaster.co/privacy.html"] options:@{} completionHandler:nil];
            }
        }
    }else{
        if (indexPath.section == 2){
            if (indexPath.row == 0) {
                [[PRAppDelegate rootViewController] showAboutViewController];
                self.selectedCategoryID = @"About us";
                [tableView reloadData];
            } else if (indexPath.row == 1) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://mcpemaster.co/terms.html"] options:@{} completionHandler:nil];
            } else {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://mcpemaster.co/privacy.html"] options:@{} completionHandler:nil];
            }
        }else{
            
            /* Now show list category from services */
            if (indexPath.section == 0) {
                category = [PRGlobalUtils skinCategories][indexPath.row];
            }else if (indexPath.section == 1) {
                if (indexPath.row == 0){
                    category = [PRGlobalUtils mapCategories][0];
                } else if (indexPath.row == 1){
                    category = [PRGlobalUtils addonCategories][0];
                } else if (indexPath.row == 2){
                    category = [PRGlobalUtils textureCategories][0];
                } else if (indexPath.row == 3){
                    category = [PRGlobalUtils serverCategories][0];
                } else if (indexPath.row == 4){
                    category = [PRGlobalUtils seedCategories][0];
                }
            }
            // If choose difference category, we will reload list videos
            if (![self.selectedCategoryID isEqualToString:category.value]) {
                self.selectedCategoryID = category.value;
                if  ([self.selectedCategoryID isEqualToString:@"SkinEditor"]){
                    [[PRAppDelegate rootViewController] showSkinEditorViewController];
                    
                    /* Set selected this category */
                    [tableView reloadData];
                }else{
                    [[PRAppDelegate rootViewController] showClipViewControllerWithSelectedCategory:category isFavoriteView:NO];
                    
                    /* Set selected this category */
                    [tableView reloadData];
                }
            }
        }
    }
}

#pragma mark - Methods

- (void)reloadData {
    // Reload table view
    [self.mainTableView reloadData];
}

//
- (IBAction)onTouchGoPro:(id)sender{
    [[AppDelegate sharedDelegate].storeService presentAvailablePurchasesFrom:self];
}

@end
