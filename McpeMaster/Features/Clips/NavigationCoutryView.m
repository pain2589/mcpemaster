//
//  NavigationCoutryView.m
//  McpeMaster
//
//  Created by Le Nguyen Nam Phong on 14/01/2015.
//  Copyright (c) Năm 2015 Khoai Nguyen. All rights reserved.
//

#import "NavigationCoutryView.h"

@interface NavigationCoutryView ()
@property(weak, nonatomic) IBOutlet UIImageView *countryImageView;
@property(weak, nonatomic) IBOutlet UILabel *countryLabel;
@end

@implementation NavigationCoutryView

- (void)setupCountryWithImage:(UIImage *)image category:(NSString *)category {
    [self.countryImageView setImage:image];
    self.countryLabel.text = category;
}

- (IBAction)didTouchOnCountryPicker:(id)sender {
    if (self.onChooseCountryCallBack != nil) {
        self.onChooseCountryCallBack();
    }
}

@end
