//
//  Head.m
//  GirlSkin
//
//  Created by apple on 2016. 7. 12..
//  Copyright (c) 2016年 SWORD. All rights reserved.
//

#import "Hat.h"


@implementation Hat
- (void) initCube:(float)height
{
	float tmphat_texcoords[] = {
			// front hat
            0.5f + 8.0f/64.0f,      16.0f/height, 		// 0
            0.5f + 8.0f/64.0f,      8.0f/height, 	    // 3
            0.5f + 16.0f /64.0f,	8.0f/height, 	  	// 2
            0.5f + 16.0f /64.0f, 	16.0f/height, 	  	// 1
			// top hat
            0.5f + 8.0f/64.0f,      8.0f /height,    	// 3
            0.5f + 8.0f/64.0f,      0.0f,    			// 7
            0.5f + 16.0f/64.0f, 	0.0f,   			// 6
            0.5f + 16.0f/64.0f, 	8.0f /height,		// 2
			// bottom hat
            0.5f + 16.0f/64.0f, 	8.0f /height,		// 4
            0.5f + 16.0f/64.0f, 	0.0f,				// 0
            0.5f + 24.0f/64.0f, 	0.0f,				// 1
            0.5f + 24.0f/64.0f, 	8.0f /height,	    // 5
			// left hat
            0.5f + 16.0f/64.0f, 	16.0f /height,		// 1
            0.5f + 16.0f/64.0f, 	8.0f /height,		// 2
            0.5f + 24.0f/64.0f, 	8.0f /height,     	// 6
            0.5f + 24.0f/64.0f, 	16.0f /height,    	// 5
			// right hat
            0.5f + 0.0f,  			16.0f /height,   	// 7
            0.5f + 0.0f,  			8.0f /height,		// 3
            0.5f + 8.0f/64.0f,      8.0f/height,   		// 0
            0.5f + 8.0f/64.0f,      16.0f/height,   	// 4
			// back hat
            0.5f + 24.0f/64.0f, 	16.0f/height,   	// 5
            0.5f + 24.0f/64.0f, 	8.0f/height,    	// 6
            0.5f + 32.0f/64.0f, 	8.0f/height,		// 7
            0.5f + 32.0f/64.0f, 	16.0f/height,   	// 4
			};
	memcpy(hat_texcoords, tmphat_texcoords, sizeof(tmphat_texcoords));

    [super initCube:9.0f    //chiều ngang
                 sy:9.0f    //chiều cao
                 sz:9.0f   //chiều sâu
            offsetx:0.0f
            offsety:12.0f
            offsetz:0.0f
        step_value1:0.25f
         rot_axis_x:0.0f
         rot_axis_y:1.0f
         rot_axis_z:0.0f
         max_anlge1:5.0f
         min_angle1:-5.0f];
    
    [super AddTextures: hat_texcoords nSize: sizeof(hat_texcoords) / sizeof(float)];
}
@end
