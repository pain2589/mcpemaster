/*============================================================================
 PROJECT: SkinEditor
 FILE:    SEDrawView+Mathematics.m
 AUTHOR:  Nguyen Minh Khoai
 DATE:    10/20/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SEDrawView+Mathematics.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@implementation SEDrawView (Mathematics)

+ (CGRect)calculateUnitRectWithRect:(CGRect)rect colorMatrix:(SEBYTEMatrix *)colorMatrix {
    
    MatrixDimension matrixDimension = colorMatrix.dimension;
    
    CGFloat verticalUnitSize = CGRectGetHeight(rect) / matrixDimension.rowSize;
    CGFloat horizontalUnitSize = CGRectGetWidth(rect) / matrixDimension.columnSize;
    CGFloat unitEdgeSize = 0;
    
    /* calculate frame basing on row & column */
    NSComparisonResult result = [colorMatrix isSquareDimension];
    
    /* row < column */
    if (result == NSOrderedAscending) {
        unitEdgeSize = horizontalUnitSize;
    }
    /* row > column */
    else if (result == NSOrderedDescending) {
        unitEdgeSize = verticalUnitSize;
    }
    /* row == column */
    else {
        unitEdgeSize = MIN(verticalUnitSize, horizontalUnitSize);
    }
    
    return (CGRect){.origin = {0, 0}, .size = {unitEdgeSize, unitEdgeSize}};
}

@end
