//
//  Head.h
//  GirlSkin
//
//  Created by apple on 2016. 7. 12..
//  Copyright (c) 2016年 SWORD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Cube.h"

@interface Head : Cube{
   float head_texcoords[2 * 4 * 6];
}

- (void) initCube:(float)height;
@end
