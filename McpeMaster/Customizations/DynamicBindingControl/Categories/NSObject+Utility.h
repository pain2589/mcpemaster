//
//  NSObject+Utility.h
//  BrandMate
//
//  Created by vu bui on 1/22/15.
//  Copyright (c) 2015 Nguyen Minh Khoai. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^callBackBlock)(id data);

@interface NSObject (Utility)

- (void)bindDataIntoSelf:(NSDictionary *)data;

- (BOOL)hasMethod:(NSString *)method;

- (BOOL)hasMethod:(NSString *)method inDelegate:(id)delegate;

- (BOOL)hasProperty:(NSString *)property;

- (BOOL)hasProperty:(NSString *)property inInstance:(id)instance;

- (id)executeMethod:(NSString *)method withParams:(NSMutableArray *)params;

- (BOOL)isKeyPath:(NSString *)keyPath;

@end
