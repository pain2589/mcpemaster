//
//  StoreService.swift
//  IAP-iOS
//
//  Created by Van Tiep Nguyen on 10/10/18.
//  Copyright © 2018 Black Pixel Luminance. All rights reserved.
//

import Foundation
import IAP

@objc class StoreService: NSObject {
    let service: Store = Store<IAPProduct>()
    
    @objc func presentAvailablePurchases(from viewController: IAPViewController) {
        service.presentAvailablePurchases(from: viewController) { (wasCancelled) in
            if wasCancelled {
                print("cancelled")
            } else {
                print("completed")
                if self.availabilityForLife() == .purchased || self.availabilityForWeekly() == .subscribedWillRenew {
                    UserDefaults.standard.set(true, forKey: "kIsPro")
                }
            }
        }
    }
    
    @objc func restorePurchase() {
        service.restorePurchases()
    }
    
    @objc func availabilityForWeekly() -> Availability {
        return service.availability(for: .weekly)
    }
    
    @objc func availabilityForLife() -> Availability {
        return service.availability(for: .life)
    }
    
    @objc func isAppPurchased() -> Bool {
        return service.isAppPurchased
    }
}
