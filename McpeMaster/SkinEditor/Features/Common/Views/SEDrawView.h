/*============================================================================
 PROJECT: SkinEditor
 FILE:    SEDrawView.h
 AUTHOR:  Nguyen Minh Khoai
 DATE:    10/5/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import <UIKit/UIKit.h>
#import "SEBodyPartObject.h"

/*============================================================================
 MACRO
 =============================================================================*/
typedef NS_ENUM(short, EditorToolBarButtonType) {
    EditorToolBarButtonTypePencil = 1,
    EditorToolBarButtonTypeEraser,
    EditorToolBarButtonTypePicker,
    EditorToolBarButtonTypeFill,
    EditorToolBarButtonTypeNose,
    EditorToolBarButtonTypeUndo
};

/*============================================================================
 Interface:   SEDrawView
 =============================================================================*/

@protocol SEDrawViewDelegate;
@interface SEDrawView : UIView

@property (nonatomic, strong) UIColor *fillColor;
@property (nonatomic, strong) UIColor *lineColor;

@property (nonatomic, strong) SEBodyPartObject *object;
@property (weak, nonatomic) id<SEDrawViewDelegate> delegate;

@property (nonatomic, assign) BOOL tapEnabled;
@property (nonatomic, assign) CGRect unitRect;

@end

/*============================================================================
 PROTOCOL
 =============================================================================*/
@protocol SEDrawViewDelegate <NSObject>
@optional

- (EditorToolBarButtonType)actionForDrawView:(SEDrawView *)drawView;
- (void)drawView:(SEDrawView *)drawView didPickColor:(UIColor *)color;
- (void)didTapOnDrawView:(SEDrawView *)drawView;
- (void)didChangedOnDrawView:(SEDrawView *)drawView;

@end
