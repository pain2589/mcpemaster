/*============================================================================
 PROJECT: SkinEditor
 FILE:    SEMapObject.h
 AUTHOR:  Nguyen Minh Khoai
 DATE:    10/18/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SEBaseObject.h"
/*============================================================================
 MACRO
 =============================================================================*/

@class RLMServerObject;
@interface SEServerObject : SEBaseObject
@property (nonatomic, strong) RLMServerObject *referencedRealmObject;

+ (instancetype)newObject;

@end
