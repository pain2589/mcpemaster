//
//  RLeg.m
//  GirlSkin
//
//  Created by apple on 2016. 7. 12..
//  Copyright (c) 2016年 SWORD. All rights reserved.
//

#import "RLeg.h"


@implementation RLeg
- (void) initCube:(float)height
{
    if (height == 32.0f) {
        float tmprleg_texcoords[] = {
            // front rightLeg
            0.0f/64.0f + 4.0f/64.0f,	1.0f,
            0.0f/64.0f + 4.0f/64.0f,	1.0f - 6.0f/32.0f,
            0.0f/64.0f + 8.0f/64.0f,	1.0f - 6.0f/32.0f,
            0.0f/64.0f + 8.0f/64.0f,	1.0f,
            // front rightLeg1
            0.0f/64.0f + 4.0f/64.0f,	1.0f - 6.0f/32.0f,
            0.0f/64.0f + 4.0f/64.0f,	1.0f - 12.0f/32.0f,
            0.0f/64.0f + 8.0f/64.0f,	1.0f - 12.0f/32.0f,
            0.0f/64.0f + 8.0f/64.0f,	1.0f - 6.0f/32.0f,
            // top rightLeg
            0.0f/64.0f + 4.0f/64.0f,	1.0f - 12.0f/32.0f,
            0.0f/64.0f + 4.0f/64.0f,	1.0f - 16.0f/32.0f,
            0.0f/64.0f + 8.0f/64.0f,	1.0f - 16.0f/32.0f,
            0.0f/64.0f + 8.0f/64.0f,	1.0f - 12.0f/32.0f,
            // bottom rightLeg
            0.0f/64.0f + 8.0f/64.0f,	1.0f - 12.0f/32.0f,
            0.0f/64.0f + 8.0f/64.0f,	1.0f - 16.0f/32.0f,
            0.0f/64.0f + 12.0f/64.0f,	1.0f - 16.0f/32.0f,
            0.0f/64.0f + 12.0f/64.0f,	1.0f - 12.0f/32.0f,
            // left rightLeg
            0.0f/64.0f + 8.0f/64.0f,	1.0f,
            0.0f/64.0f + 8.0f/64.0f,	1.0f - 6.0f/32.0f,
            0.0f/64.0f + 12.0f/64.0f,	1.0f - 6.0f/32.0f,
            0.0f/64.0f + 12.0f/64.0f,	1.0f,
            // left rightLeg1
            0.0f/64.0f + 8.0f/64.0f,	1.0f - 6.0f/32.0f,
            0.0f/64.0f + 8.0f/64.0f,	1.0f - 12.0f/32.0f,
            0.0f/64.0f + 12.0f/64.0f,	1.0f - 12.0f/32.0f,
            0.0f/64.0f + 12.0f/64.0f,	1.0f - 6.0f/32.0f,
            // right rightLeg
            -8.0f/64.0f + 8.0f/64.0f,	1.0f,
            -8.0f/64.0f + 8.0f/64.0f,	1.0f - 6.0f/32.0f,
            -8.0f/64.0f + 12.0f/64.0f,	1.0f - 6.0f/32.0f,
            -8.0f/64.0f + 12.0f/64.0f,	1.0f,
            // right rightLeg1
            -8.0f/64.0f + 8.0f/64.0f,	1.0f - 6.0f/32.0f,
            -8.0f/64.0f + 8.0f/64.0f,	1.0f - 12.0f/32.0f,
            -8.0f/64.0f + 12.0f/64.0f,	1.0f - 12.0f/32.0f,
            -8.0f/64.0f + 12.0f/64.0f,	1.0f - 6.0f/32.0f,
            // back rightLeg
            0.0f/64.0f + 12.0f/64.0f,	1.0f,
            0.0f/64.0f + 12.0f/64.0f,	1.0f - 6.0f/32.0f,
            0.0f/64.0f + 16.0f/64.0f,	1.0f - 6.0f/32.0f,
            0.0f/64.0f + 16.0f/64.0f,	1.0f,
            // back rightLeg1
            0.0f/64.0f + 12.0f/64.0f,	1.0f - 6.0f/32.0f,
            0.0f/64.0f + 12.0f/64.0f,	1.0f - 12.0f/32.0f,
            0.0f/64.0f + 16.0f/64.0f,	1.0f - 12.0f/32.0f,
            0.0f/64.0f + 16.0f/64.0f,	1.0f - 6.0f/32.0f,
        };
        
        memcpy(rleg_texcoords, tmprleg_texcoords, sizeof(tmprleg_texcoords));
        
        [super initCube:4.0f sy:12.0f sz:4.0f offsetx:-2.0f offsety:-10.0f offsetz:0.0f step_value:-60.f/40.0f rot_axis_x:1.0f rot_axis_y:0.0f rot_axis_z:0.0f max_anlge:30.0f min_angle:-30.0f step_value1:60.0f/40.0f max_angle1:30.0f min_angle:-30.0f isFixAngle:true fixDir:1.0f];
        
        [super AddTextures:rleg_texcoords nSize:sizeof(rleg_texcoords) / sizeof(float)];
    }else if (height == 64.0f){
        float tmprleg_texcoords[] = {
            // front rightLeg
            0.0f/64.0f + 4.0f/64.0f,	0.5f,				 		// 0
            0.0f/64.0f + 4.0f/64.0f,	0.5f - 6.0f/64.0f,		    // 3
            0.0f/64.0f + 8.0f/64.0f,	0.5f - 6.0f/64.0f,          // 2
            0.0f/64.0f + 8.0f/64.0f,	0.5f, 					  	// 1
            // front rightLeg1
            0.0f/64.0f + 4.0f/64.0f,	0.5f - 6.0f/64.0f,			// 0
            0.0f/64.0f + 4.0f/64.0f,	0.5f - 12.0f/64.0f,         // 3
            0.0f/64.0f + 8.0f/64.0f,	0.5f - 12.0f/64.0f, 	  	// 2
            0.0f/64.0f + 8.0f/64.0f,	0.5f - 6.0f/64.0f,
            // top rightLeg
            0.0f/64.0f + 4.0f/64.0f,	0.5f - 12.0f/64.0f,         // 3
            0.0f/64.0f + 4.0f/64.0f,	0.5f - 16.0f/64.0f, 		// 7
            0.0f/64.0f + 8.0f/64.0f,	0.5f - 16.0f/64.0f,         // 6
            0.0f/64.0f + 8.0f/64.0f,	0.5f - 12.0f/64.0f,         // 2
            // bottom rightLeg
            0.0f/64.0f + 8.0f/64.0f,	0.5f - 12.0f/64.0f,         // 4
            0.0f/64.0f + 8.0f/64.0f,	0.5f - 16.0f/64.0f,         // 0
            0.0f/64.0f + 12.0f/64.0f,	0.5f - 16.0f/64.0f,         // 1
            0.0f/64.0f + 12.0f/64.0f,	0.5f - 12.0f/64.0f,         // 5
            // left rightLeg
            -8.0f/64.0f + 8.0f/64.0f,	0.5f,						// 1
            -8.0f/64.0f + 8.0f/64.0f,	0.5f - 6.0f/64.0f,			// 2
            -8.0f/64.0f + 12.0f/64.0f,	0.5f - 6.0f/64.0f,          // 6
            -8.0f/64.0f + 12.0f/64.0f,	0.5f,	    				// 5
            // left rightLeg1
            -8.0f/64.0f + 8.0f/64.0f,	0.5f - 6.0f/64.0f,			// 1
            -8.0f/64.0f + 8.0f/64.0f,	0.5f - 12.0f/64.0f,         // 2
            -8.0f/64.0f + 12.0f/64.0f,	0.5f - 12.0f/64.0f,     	// 6
            -8.0f/64.0f + 12.0f/64.0f,	0.5f - 6.0f/64.0f,	    	// 5
            // right rightLeg
            -8.0f/64.0f + 8.0f/64.0f,	0.5f,						// 1
            -8.0f/64.0f + 8.0f/64.0f,	0.5f - 6.0f/64.0f,			// 2
            -8.0f/64.0f + 12.0f/64.0f,	0.5f - 6.0f/64.0f,          // 6
            -8.0f/64.0f + 12.0f/64.0f,	0.5f,	    		   		// 4
            // right rightLeg1
            -8.0f/64.0f + 8.0f/64.0f,	0.5f - 6.0f/64.0f,			// 1
            -8.0f/64.0f + 8.0f/64.0f,	0.5f - 12.0f/64.0f,         // 2
            -8.0f/64.0f + 12.0f/64.0f,	0.5f - 12.0f/64.0f,     	// 6
            -8.0f/64.0f + 12.0f/64.0f,	0.5f - 6.0f/64.0f,   		// 4
            // back rightLeg
            0.0f/64.0f + 12.0f/64.0f,	0.5f,   					// 5
            0.0f/64.0f + 12.0f/64.0f,	0.5f - 6.0f/64.0f,   		// 6
            0.0f/64.0f + 16.0f/64.0f,	0.5f - 6.0f/64.0f,			// 7
            0.0f/64.0f + 16.0f/64.0f,	0.5f,   					// 4
            // back rightLeg1
            0.0f/64.0f + 12.0f/64.0f,	0.5f - 6.0f/64.0f,   		// 5
            0.0f/64.0f + 12.0f/64.0f,	0.5f - 12.0f/64.0f,   		// 6
            0.0f/64.0f + 16.0f/64.0f,	0.5f - 12.0f/64.0f,         // 7
            0.0f/64.0f + 16.0f/64.0f,	0.5f - 6.0f/64.0f,
        };
        
        memcpy(rleg_texcoords, tmprleg_texcoords, sizeof(tmprleg_texcoords));
        float tmprleg2_texcoords[] = {
            // front rightLeg
            4.0f/64.0f,     32.0f/64.0f + 16.0f/64.0f,                      // 0
            4.0f/64.0f,     20.0f/64.0f + 6.0f/64.0f + 16.0f/64.0f,         // 3
            8.0f/64.0f,     20.0f/64.0f + 6.0f/64.0f + 16.0f/64.0f,         // 2
            8.0f/64.0f,     32.0f/64.0f + 16.0f/64.0f,                      // 1
            // front rightLeg1
            4.0f/64.0f,     32.0f/64.0f - 6.0f/64.0f + 16.0f/64.0f,         // 0
            4.0f/64.0f,     20.0f/64.0f + 16.0f/64.0f,                      // 3
            8.0f/64.0f,     20.0f/64.0f + 16.0f/64.0f,                      // 2
            8.0f/64.0f,     32.0f/64.0f  - 6.0f/64.0f + 16.0f/64.0f,        // 1
            // top rightLeg
            4.0f/64.0f,     20.0f/64.0f + 16.0f/64.0f ,                     // 3
            4.0f/64.0f,     16.0f/64.0f + 16.0f/64.0f,                      // 7
            8.0f/64.0f,     16.0f/64.0f + 16.0f/64.0f,                      // 6
            8.0f/64.0f,     20.0f/64.0f + 16.0f/64.0f,                      // 2
            // bottom rightLeg
            8.0f/64.0f,     20.0f/64.0f + 16.0f/64.0f,                      // 4
            8.0f/64.0f,     16.0f/64.0f + 16.0f/64.0f,                      // 0
            12.0f/64.0f,	16.0f/64.0f + 16.0f/64.0f,                      // 1
            12.0f/64.0f,	20.0f/64.0f + 16.0f/64.0f,                      // 5
            // left rightLeg
            8.0f/64.0f,     32.0f/64.0f + 16.0f/64.0f,                      // 1
            8.0f/64.0f,     20.0f/64.0f + 6.0f/64.0f + 16.0f/64.0f,         // 2
            12.0f/64.0f,	20.0f/64.0f + 6.0f/64.0f + 16.0f/64.0f,         // 6
            12.0f/64.0f,	32.0f/64.0f + 16.0f/64.0f,                      // 5
            // left rightLeg1
            8.0f/64.0f,     32.0f/64.0f - 6.0f/64.0f + 16.0f/64.0f,         // 1
            8.0f/64.0f,     20.0f/64.0f + 16.0f/64.0f,                      // 2
            12.0f/64.0f,	20.0f/64.0f + 16.0f/64.0f,                      // 6
            12.0f/64.0f,	32.0f/64.0f - 6.0f/64.0f + 16.0f/64.0f,         // 5
            // right rightLeg
            0.0f/64.0f,     32.0f/64.0f + 16.0f/64.0f,                      // 7
            0.0f/64.0f,     20.0f/64.0f + 6.0f/64.0f + 16.0f/64.0f,         // 3
            4.0f/64.0f,     20.0f/64.0f + 6.0f/64.0f + 16.0f/64.0f,         // 0
            4.0f/64.0f,     32.0f/64.0f + 16.0f/64.0f,                      // 4
            // right rightLeg1
            0.0f/64.0f,     32.0f/64.0f - 6.0f/64.0f + 16.0f/64.0f,         // 7
            0.0f/64.0f,     20.0f/64.0f + 16.0f/64.0f,                      // 3
            4.0f/64.0f,     20.0f/64.0f + 16.0f/64.0f,                      // 0
            4.0f/64.0f,     32.0f/64.0f - 6.0f/64.0f + 16.0f/64.0f,         // 4
            // back rightLeg
            12.0f/64.0f,	32.0f/64.0f + 16.0f/64.0f,                      // 5
            12.0f/64.0f,	20.0f/64.0f + 6.0f/64.0f + 16.0f/64.0f,         // 6
            16.0f/64.0f,	20.0f/64.0f + 6.0f/64.0f + 16.0f/64.0f,         // 7
            16.0f/64.0f,	32.0f/64.0f + 16.0f/64.0f,                      // 4
            // back rightLeg1
            12.0f/64.0f,	32.0f/64.0f - 6.0f/64.0f + 16.0f/64.0f,         // 5
            12.0f/64.0f,	20.0f/64.0f + 16.0f/64.0f,                      // 6
            16.0f/64.0f,	20.0f/64.0f + 16.0f/64.0f,                      // 7
            16.0f/64.0f,	32.0f/64.0f - 6.0f/64.0f + 16.0f/64.0f,         // 4
        };
        memcpy(rleg2_texcoords, tmprleg2_texcoords, sizeof(rleg2_texcoords));
        [super initCube:4.0f sy:12.0f sz:4.0f offsetx:-2.0f offsety:-10.0f offsetz:0.0f step_value:-60.f/40.0f rot_axis_x:1.0f rot_axis_y:0.0f rot_axis_z:0.0f max_anlge:30.0f min_angle:-30.0f step_value1:60.0f/40.0f max_angle1:30.0f min_angle:-30.0f isFixAngle:true fixDir:1.0f];
        
        [super AddTextures:rleg_texcoords nSize:sizeof(rleg_texcoords) / sizeof(float)];
        [super AddTextures:rleg2_texcoords nSize:sizeof(rleg2_texcoords) / sizeof(float)];
    }
}
@end
