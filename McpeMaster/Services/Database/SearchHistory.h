//
//  SearchHistory.h
//  McpeMaster
//
//  Created by Khoai Nguyen on 3/31/15.
//  Copyright (c) 2015 Khoai Nguyen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface SearchHistory : NSManagedObject

@property(nonatomic, retain) NSString *content;
@property(nonatomic, retain) NSNumber *dirtyFlag;
@end
