//
//  TextureViewController.h
//  McpeMaster
//
//  Created by kaka on 2/5/17.
//
//

#import <UIKit/UIKit.h>

@import GoogleMobileAds;
@interface AboutViewController : UIViewController<GADBannerViewDelegate>

@property(weak, nonatomic) IBOutlet UITextView *textViewDescription;
@property(weak, nonatomic) IBOutlet UIButton *btnFeedback;
@property(weak, nonatomic) IBOutlet UIButton *btnShare;
@property(weak, nonatomic) IBOutlet UIButton *btnRate;

@property (strong,nonatomic) GADBannerView *bannerView;

@end
