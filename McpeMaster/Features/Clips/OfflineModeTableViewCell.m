//
//  OfflineModeTableViewCell.m
//  McpeMaster
//
//  Created by Le Nguyen Nam Phong on 13/04/2015.
//  Copyright (c) Năm 2015 Khoai Nguyen. All rights reserved.
//

#import "OfflineModeTableViewCell.h"

@implementation OfflineModeTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)reloadButtonClick:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(reloadButtonClick)]) {
        [self.delegate reloadButtonClick];
    }
}

@end
