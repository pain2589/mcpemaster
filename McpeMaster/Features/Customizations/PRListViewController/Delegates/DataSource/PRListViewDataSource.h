/*============================================================================
 PROJECT: McpeMaster
 FILE:    PRListViewDataSource.h
 AUTHOR:  Khoai Nguyen
 DATE:    3/18/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import <Foundation/Foundation.h>
#import "MKCollectionViewControllerProtocols.h"
#import "MKListViewControllerLogics.h"

/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   PRListViewDataSource
 =============================================================================*/


@interface PRListViewDataSource : NSObject <MKCollectionViewControllerDataSource, MKCollectionViewControllerDelegate>
@property(nonatomic, weak) id <MKCollectionViewControllerLogicDelegate> logics;
@property(nonatomic, weak) id <MKCollectionViewControllerConfiguration> configs;
@end

@interface PRListVideoViewDataSource : PRListViewDataSource

@end

