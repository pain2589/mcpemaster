/*============================================================================
 PROJECT: McpeMaster
 FILE:    PlaylistManager.m
 AUTHOR:  Tam Nguyen Ngoc
 DATE:    3/8/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "PlaylistManager.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/

#define kLocalPlaylistUserDefaultKey                @"McpeMaster_local_playlist_user_default_key"
#define kLocalPlaylistUserDefaultNameKey            @"McpeMaster_local_playlist_user_default_name_key"
#define kLocalPlaylistUserDefaultVideosKey          @"McpeMaster_local_playlist_user_default_videos_key"

#define kPinnedYoutubePlaylistKey                   @"kPinnedYoutubePlaylistKey"

/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@implementation PRPlaylistData

- (instancetype)init {
    self = [super init];

    if (self) {
        _name = @"";
        _videos = [NSMutableArray array];
    }

    return self;
}

- (void)setName:(NSString *)name {
    if (!name) {
        _name = @"";
    } else {
        _name = name;
    }
}

- (NSString *)displayName {
    if ([self isRecentPlaylist]) {
        return @"Recents";
    } else {
        return self.name;
    }
}

- (int)indexOfVideo:(NSObject *)video {
    int index = -1;

    for (int i = 0; i < self.videos.count; i++) {
    }

    return index;
}

- (NSArray *)getShuffleVideosWithBeginVideo:(NSObject *)beginVideo {
    NSMutableArray *result = [NSMutableArray arrayWithArray:self.videos];

    if (result.count > 1) {
        static BOOL seeded = NO;

        if (!seeded) {
            seeded = YES;
            srandom((unsigned int) time(NULL));
        }

        NSUInteger count = [result count];
        for (NSUInteger i = 0; i < count; ++i) {
            // Select a random element between i and end of array to swap with.
            NSUInteger nElements = count - i;
            NSUInteger n = (random() % nElements) + i;
            [result exchangeObjectAtIndex:i withObjectAtIndex:n];
        }

        // Move begin object to video
        if (beginVideo) {
            for (int i = 0; i < result.count; i++) {
            }
        }
    }

    return result;
}

- (BOOL)isRecentPlaylist {
    return [self.name isEqualToString:kRecentPlaylistName];
}

@end
