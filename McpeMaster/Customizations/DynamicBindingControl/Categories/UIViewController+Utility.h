/*============================================================================
 PROJECT: McpeMaster
 FILE:    UIViewController+Utility.h
 AUTHOR:  Khoai Nguyen
 DATE:    2/8/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import <UIKit/UIKit.h>

/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   UIViewController_Utility
 =============================================================================*/

@interface UIViewController (Utility)

/* 
 Dynamic factory to create view controller with controller identifier
 */
- (UIViewController *)viewControllerByIdentifier:(NSString *)identifier;

/**
utility function, it is used to push a view into stack (Navigation or Tabbar).
@Param NSString    name    name of controller that defined in "FactoryViewController" class.
*/
- (void)pushToControllerName:(NSString *)name;

/**
utility function, it is used to push a view into stack (Navigation or Tabbar).
@Param NSString        name    name of controller that defined in "FactoryViewController" class.
@Param NSDictionary    data    data that you want to bind into creating controller. The data is a dictionary, and keys are property names of instance.
*/
- (void)pushToControllerName:(NSString *)name withData:(NSDictionary *)data;

- (UIViewController *)topViewController;

@end
