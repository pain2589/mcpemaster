/*============================================================================
 PROJECT: SuperCleaner
 FILE:    UIViewController+StoryBoardSupport.h
 AUTHOR:  Khoai Nguyen
 DATE:    8/7/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import <UIKit/UIKit.h>

/*============================================================================
 MACRO
 =============================================================================*/
static NSString *const  DefaultStoryBoard  = @"EditorMain";
static NSString *const  EditorStoryBoard   = @"Editor";

/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   UIViewController_StoryBoardSupport
 =============================================================================*/

@interface UIViewController (StoryBoardSupport)

+ (instancetype)controllerByStoryID:(NSString *)storyID
                       inStoryBoard:(NSString *)storyBoard
                      withArguments:(NSDictionary *)arguments;

+ (instancetype)controllerInDefaultStoryBoardByID:(NSString *)storyID
                                    withArguments:(NSDictionary *)arguments;

+ (instancetype)controllerInDefaultStoryBoardByID:(NSString *)storyID;

@end
