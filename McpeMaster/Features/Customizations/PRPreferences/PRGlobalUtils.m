/*============================================================================
 PROJECT: McpeMaster
 FILE:    PRGlobalUtils.m
 AUTHOR:  Khoai Nguyen
 DATE:    2/17/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "PRGlobalUtils.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
static NSString *kMapPlistFile = @"Maps.plist";
static NSString *kModPlistFile = @"Mods.plist";
static NSString *kSeedPlistFile = @"Seeds.plist";
static NSString *kServerPlistFile = @"Servers.plist";
static NSString *kSkinPlistFile = @"Skins.plist";
static NSString *kTexturePlistFile = @"Textures.plist";
static NSString *kAddonPlistFile = @"Add-ons.plist";

@implementation PRServiceCategory

+ (JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithDictionary:@{@"id" : @"value", @"icon" : @"extra"}];
}

@end

/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@interface PRGlobalUtils ()
@property(nonatomic, readwrite) NSArray *mapCategories;
@property(nonatomic, readwrite) NSArray *modCategories;
@property(nonatomic, readwrite) NSArray *seedCategories;
@property(nonatomic, readwrite) NSArray *serverCategories;
@property(nonatomic, readwrite) NSArray *skinCategories;
@property(nonatomic, readwrite) NSArray *textureCategories;
@property(nonatomic, readwrite) NSArray *addonCategories;
@property(nonatomic, strong) UIImageView *confirmImage;
@property(nonatomic, strong) NSString *countryCode;
@property(nonatomic, strong) NSString *countryName;
@property(nonatomic, strong) NSString *locationCountryCode;
@property(nonatomic, strong) NSString *locationCountryName;
@end

@implementation PRGlobalUtils

+ (PRGlobalUtils *)sharedUtils {
    static dispatch_once_t predicate;
    static PRGlobalUtils *instance = nil;
    dispatch_once(&predicate, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

+ (PRGlobalUtils *)instance {
    return [PRGlobalUtils sharedUtils];
}

#if (!__has_feature(objc_arc))

- (id)retain {
    
    return self;
}

- (unsigned)retainCount {
    return UINT_MAX;  //denotes an object that cannot be released
}

- (oneway void)release {
    //do nothing
}

- (id)autorelease {
    
    return self;
}
#endif

#pragma mark - Accessors

- (NSArray *)mapCategories {
    if (!_mapCategories) {
        _mapCategories = [self loadConfigurationsFromFile:kMapPlistFile parseToClass:PRServiceCategory.description];
    }
    return _mapCategories;
}

- (NSArray *)modCategories {
    if (!_modCategories) {
        _modCategories = [self loadConfigurationsFromFile:kModPlistFile parseToClass:PRServiceCategory.description];
    }
    return _modCategories;
}

- (NSArray *)seedCategories {
    if (!_seedCategories) {
        _seedCategories = [self loadConfigurationsFromFile:kSeedPlistFile parseToClass:PRServiceCategory.description];
    }
    return _seedCategories;
}

- (NSArray *)serverCategories {
    if (!_serverCategories) {
        _serverCategories = [self loadConfigurationsFromFile:kServerPlistFile parseToClass:PRServiceCategory.description];
    }
    return _serverCategories;
}

- (NSArray *)skinCategories {
    if (!_skinCategories) {
        _skinCategories = [self loadConfigurationsFromFile:kSkinPlistFile parseToClass:PRServiceCategory.description];
    }
    return _skinCategories;
}

- (NSArray *)textureCategories {
    if (!_textureCategories) {
        _textureCategories = [self loadConfigurationsFromFile:kTexturePlistFile parseToClass:PRServiceCategory.description];
    }
    return _textureCategories;
}

- (NSArray *)addonCategories {
    if (!_addonCategories) {
        _addonCategories = [self loadConfigurationsFromFile:kAddonPlistFile parseToClass:PRServiceCategory.description];
    }
    return _addonCategories;
}

#pragma mark - Public Interface APIs

+ (NSArray *)mapCategories {
    return self.instance.mapCategories;
}

+ (NSArray *)modCategories{
    return self.instance.modCategories;
}

+ (NSArray *)seedCategories{
    return self.instance.seedCategories;
}

+ (NSArray *)serverCategories{
    return self.instance.serverCategories;
}

+ (NSArray *)skinCategories{
    return self.instance.skinCategories;
}

+ (NSArray *)textureCategories{
    return self.instance.textureCategories;
}

+ (NSArray *)addonCategories{
    return self.instance.addonCategories;
}

@end
