/*============================================================================
 PROJECT: McpeMaster
 FILE:    RootViewController.m
 AUTHOR:  Khoai Nguyen
 DATE:    12/23/14
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "RootViewController.h"
#import "LeftMenuViewController.h"
#import "ClipViewController.h"
#import "BaseNavigationViewController.h"
#import "PRSettings.h"
#import "ClipViewController.h"
#import "AboutViewController.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
#define kAnchorLeftPeekAmount   (IS_IPAD ? SCREEN_WIDTH - 300 : 64)


/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@interface RootViewController () <ECSlidingViewControllerDelegate, ECSlidingViewControllerLayout>
@property(nonatomic, assign) CGFloat fixedScreenWidth;
@end

@implementation RootViewController

- (void)createSubviewControllers {
    
    PRServiceCategory *defaultCategory = [PRSettings defaultSettings].category;
    
    /* left view controller */
    id leftViewController = [self viewControllerByIdentifier:@"LeftMenuNavigationController"];
    self.underLeftViewController.edgesForExtendedLayout = UIRectEdgeAll;
    
    /* center view controller */
    
//    if ([AppDelegate sharedDelegate].mcpeType == AppFlag){
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"EditorMain" bundle:nil];
        UIViewController *editorVC = [sb instantiateViewControllerWithIdentifier:@"SkinEditorViewController"];
        UINavigationController *editorNavigationVC = [[UINavigationController alloc] initWithRootViewController:editorVC];
        self.topViewController = editorNavigationVC;
//    }else{
//        UIViewController *topViewController = [self viewControllerByIdentifier:@"ClipNavigationController"];
//        [topViewController bindDataIntoSelf:@{@"topViewController.selectedCategory" : defaultCategory}];
//         self.topViewController = topViewController;
//    }
    
    self.underLeftViewController = leftViewController;
   
    
    [self disableSwipeAndTapGesture:NO];
    
}

- (void)setTopViewController:(UIViewController *)topViewController {
    [super setTopViewController:topViewController];
    [self.topViewController.view addGestureRecognizer:self.panGesture];
}

- (void)viewDidLoad {
    [self createSubviewControllers];
    [super viewDidLoad];
    
//    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
//    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[IBHelper loadCommonImage:@"left_menu_bg"]];
    
    /* set delegate to register */
    self.delegate = self;
}

- (BOOL)shouldAutorotate {
#ifdef IPHONE_LANDSCAPE_HIDE
    return YES;
#else
    return [PRSettings defaultSettings].isLandScapeMode;
#endif
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - ECSlidingViewControllerDelegate, ECSlidingViewControllerLayout

- (CGRect)slidingViewController:(ECSlidingViewController *)slidingViewController
         frameForViewController:(UIViewController *)viewController
                topViewPosition:(ECSlidingViewControllerTopViewPosition)topViewPosition {
    
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (UIInterfaceOrientationIsLandscape(orientation)) {
        
        if (viewController == self.topViewController) {
            NSLog(@"[FRONT FRAME]: %@", NSStringFromCGRect(CGRectMake(SCREEN_HEIGHT - SCREEN_WIDTH, 20, SCREEN_WIDTH, SCREEN_WIDTH - 20)));
            return CGRectMake(SCREEN_HEIGHT - SCREEN_WIDTH, 20, SCREEN_WIDTH, SCREEN_WIDTH - 20);
        }
        else if (viewController == self.underLeftViewController) {
            NSLog(@"[LEFT FRAME]: %@", NSStringFromCGRect(CGRectMake(0, 0, SCREEN_HEIGHT - SCREEN_WIDTH, SCREEN_WIDTH)));
            return CGRectMake(0, 0, SCREEN_HEIGHT - SCREEN_WIDTH, SCREEN_WIDTH);
        }
    }
    else {
        if (viewController == self.topViewController) {
            CGFloat x = 0;
            if (topViewPosition != ECSlidingViewControllerTopViewPositionCentered) {
                x = SCREEN_WIDTH - kAnchorLeftPeekAmount;
            }
            //Tiep fix bug iphone X navigation bar Phuong an 2
            if (IS_IPHONE_X_XS || IS_IPHONE_XS_MAX || IS_IPHONE_XR){
                return CGRectMake(x, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
            }else{
                return CGRectMake(x, 20, SCREEN_WIDTH, SCREEN_HEIGHT - 20);
            }
            
        }
        else if (viewController == self.underLeftViewController) {
            return CGRectMake(0, 0, SCREEN_WIDTH - kAnchorLeftPeekAmount, SCREEN_HEIGHT);
        }
    }
    return CGRectInfinite;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
}

- (id <ECSlidingViewControllerLayout>)slidingViewController:(ECSlidingViewController *)slidingViewController
                         layoutControllerForTopViewPosition:(ECSlidingViewControllerTopViewPosition)topViewPosition {
    return self;
}

#pragma mark - Public

- (void)disableSwipeAndTapGesture:(BOOL)status {
    
    [self.topViewController.view removeGestureRecognizer:self.panGesture];
    
    if (!status) {
        [self.topViewController.view addGestureRecognizer:self.panGesture];
        self.topViewAnchoredGesture = ECSlidingViewControllerAnchoredGesturePanning | ECSlidingViewControllerAnchoredGestureTapping;
    }
    else {
        self.topViewAnchoredGesture = ECSlidingViewControllerAnchoredGestureDisabled;
    }
}

#pragma mark - Utitlity Methods
- (void)showClipViewControllerWithSelectedCategory:(PRServiceCategory *)category isFavoriteView:(BOOL)isFavorite{
    UIViewController *navigationController = [self viewControllerByIdentifier:@"ClipNavigationController"];
    [navigationController bindDataIntoSelf:@{@"topViewController.selectedCategory" : category}];
    self.topViewController = navigationController;
    [PRAppDelegate setIsFavoriteView:isFavorite];
    [self resetTopViewAnimated:YES];
}

- (void)showSkinEditorViewController {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"EditorMain" bundle:nil];
    UIViewController *editorVC = [sb instantiateViewControllerWithIdentifier:@"SkinEditorViewController"];
    UINavigationController *editorNavigationVC = [[UINavigationController alloc] initWithRootViewController:editorVC];
    self.topViewController = editorNavigationVC;
    [self resetTopViewAnimated:YES];
}

- (void)showAboutViewController {
    AboutViewController *aboutVC = [[AboutViewController alloc] initWithNibName:@"AboutViewController" bundle:nil];
    UINavigationController *aboutNavigationVC = [[UINavigationController alloc] initWithRootViewController:aboutVC];
    self.topViewController = aboutNavigationVC;
    [self resetTopViewAnimated:YES];
}

@end
