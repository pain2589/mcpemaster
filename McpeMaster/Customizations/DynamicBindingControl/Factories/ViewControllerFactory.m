//
//  FactoryViewController.m
//  BrandMate
//
//  Created by vu bui on 1/21/15.
//  Copyright (c) 2015 Nguyen Minh Khoai. All rights reserved.
//

#import "ViewControllerFactory.h"
#import "NSObject+Utility.h"
#import "IBHelper.h"
#import "GlobalUtils.h"

@interface ViewControllerFactory ()

@property(nonatomic, strong) NSMutableDictionary *controllers;

@end

@implementation ViewControllerFactory

+ (id)sharedInstance {
    static ViewControllerFactory *instance = nil;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        if (!instance)
            instance = [[ViewControllerFactory alloc] init];
    });
    return instance;
}

- (UIViewController *)controllerForName:(NSString *)name {
    UIViewController *controller = nil;
    if (name) {
        NSDictionary *controllerData = [self.controllers objectForKey:name];
        if (controllerData)
            controller = [IBHelper loadViewController:[controllerData objectForKey:kFactoryViewControllerName]
                                              inStory:[controllerData objectForKey:kFactoryViewControllerStoryboardName]];
    }
    return controller;
}

- (UIViewController *)controllerForName:(NSString *)name andBindDataIntoInstance:(NSDictionary *)data {
    UIViewController *controller = [self controllerForName:name];
    if (controller)
        [controller bindDataIntoSelf:data];

    return controller;
}

- (void)initComponentsFromFile:(NSString *)file {
    NSDictionary *components = [[GlobalUtils sharedUtils] loadConfigurationsFromFile:file parseToClass:nil];
    self.controllers = [NSMutableDictionary dictionaryWithDictionary:components];
}

#pragma mark - private functions -

- (NSDictionary *)controllerDataForName:(NSString *)name storyboardName:(NSString *)storyboardName {
    return @{kFactoryViewControllerName : name, kFactoryViewControllerStoryboardName : storyboardName};
}

- (void)initConfiguration {
    self.controllers = [NSMutableDictionary dictionary];
}

#pragma mark - override function-

- (id)init {
    self = [super init];
    if (self)
        [self initConfiguration];

    return self;
}

@end
