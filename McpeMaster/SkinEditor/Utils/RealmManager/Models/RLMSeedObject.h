/*============================================================================
 PROJECT: SkinEditor
 FILE:    RLMSeedObject.h
 AUTHOR:  Nguyen Van Tiep
 DATE:    11/10/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import <Realm/Realm.h>

/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   RLMSeedObject
 =============================================================================*/
@interface RLMSeedObject : RLMObject
@property NSString *mcpeId;
@property NSString *name;
@property NSString *type;
@property NSString *thumb;
@property NSString *link;
@property NSString *seedCode;
@property NSString *file;
@property NSString *mcpeDescription;
@property double logTime;
@property BOOL favorited;
@property BOOL downloaded;
@end
