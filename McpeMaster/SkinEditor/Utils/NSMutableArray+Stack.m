/*============================================================================
 PROJECT: SuperCleaner
 FILE:    NSMutableArray+Stack.m
 AUTHOR:  Khoai Nguyen
 DATE:    8/21/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "NSMutableArray+Stack.h"
#import <objc/runtime.h>
/*============================================================================
 PRIVATE MACRO
 =============================================================================*/

const char maximumCountKey = 0;

/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@implementation NSMutableArray (Stack)

#pragma mark -
#pragma mark - Accessors

- (void)setMaximumCount:(NSUInteger)maximumCount {
    objc_setAssociatedObject(self, &maximumCountKey, @(maximumCount), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSUInteger)maximumCount {
    NSNumber *number  = objc_getAssociatedObject(self, &maximumCountKey);
    if (number) {
        return [number unsignedIntegerValue];
    }
    return 0;
}

#pragma mark -
#pragma mark - Public Methods

- (void)pushStackObject:(id)object {
    if (!object) return;
    
    if ([self isFullStack]) {
        [self removeObjectAtIndex:0];
    }
    [self addObject:object];
}

- (void)popStackObject {
    [self removeLastObject];
}

- (BOOL)isFullStack {
    if (self.maximumCount == 0) {
        return NO;
    }
    return (self.count == self.maximumCount);
}

- (BOOL)isEmptyStack {
    return self.count == 0;
}

- (void)resetStack {
    [self removeAllObjects];
}

- (id)topStackObject {
    return [self lastObject];
}

@end
