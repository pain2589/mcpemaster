/*============================================================================
 PROJECT: SkinEditor
 FILE:    SEDrawView+Mathematics.h
 AUTHOR:  Nguyen Minh Khoai
 DATE:    10/20/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SEDrawView.h"

/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   SEDrawView_Mathematics
 =============================================================================*/

@interface SEDrawView (Mathematics)

+ (CGRect)calculateUnitRectWithRect:(CGRect)rect colorMatrix:(SEBYTEMatrix *)colorMatrix;

@end
