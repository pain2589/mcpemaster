//
//  RArm.h
//  GirlSkin
//
//  Created by apple on 2016. 7. 12..
//  Copyright (c) 2016年 SWORD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Cube1.h"

@interface RArm: Cube1{
    float rarm_texcoords[2 * 4 * 10];
    float rarm2_texcoords[2 * 4 * 10];
}
- (void) initCube:(float)height;
@end
