/*============================================================================
 PROJECT: McpeMaster
 FILE:    PlaylistManager.h
 AUTHOR:  Tam Nguyen Ngoc
 DATE:    3/8/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/

/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   PlaylistManager
 =============================================================================*/

#define kRecentPlaylistName             @"McpeMaster-app-recent-playlist-xyz"

@interface PRPlaylistData : NSObject

@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSMutableArray *videos;
@property(assign, nonatomic) BOOL isOnlinePlaylist;

// Addition data
@property(strong, nonatomic) id object;

- (NSString *)displayName;

- (int)indexOfVideo:(NSObject *)video;

// Return one video in shuffle mode
- (NSArray *)getShuffleVideosWithBeginVideo:(NSObject *)beginVideo;

// Return if this is recent playlits
- (BOOL)isRecentPlaylist;

@end
