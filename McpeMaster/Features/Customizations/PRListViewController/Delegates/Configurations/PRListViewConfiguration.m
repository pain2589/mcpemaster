/*============================================================================
 PROJECT: McpeMaster
 FILE:    PRListViewConfiguration.m
 AUTHOR:  Khoai Nguyen
 DATE:    3/19/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "PRListViewConfiguration.h"
#import "MKCollectionViewController.h"

#import "IBHelper.h"
#import "ClipListCell.h"
#import "OfflineConnectionView.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@implementation PRListViewConfiguration

- (NSString *)cellIdentifierForCollectionController:(MKCollectionViewController *)controller
                                        atIndexPath:(NSIndexPath *)indexPath {
    return ClipListCell.description;
}

- (NSString *)footerIDInController:(MKCollectionViewController *)controller atIndexPath:(NSIndexPath *)indexPath {
    return OfflineConnectionView.description;
}

- (UICollectionViewLayout *)collectionViewLayoutInCollectionController:(MKCollectionViewController *)controller {
    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *) controller.collectionViewLayout;
    layout.minimumInteritemSpacing = 0;
    layout.minimumLineSpacing = 0;
    return layout;
}

- (void)setupCollectionViewInController:(MKCollectionViewController *)controller {

    controller.collectionView.alwaysBounceVertical = YES;

    /* register cell xib view */
    NSString *identifier = [self cellIdentifierForCollectionController:controller atIndexPath:nil];
    UINib *nib = [IBHelper nibWithNibName:identifier];

    if (nib) {
        [controller.collectionView registerNib:nib forCellWithReuseIdentifier:identifier];
    }

    /* register footer xib */
    NSString *footerID = [self footerIDInController:controller atIndexPath:nil];
    nib = [IBHelper nibWithNibName:footerID];

    if (nib) {
        [controller.collectionView registerNib:nib
                    forSupplementaryViewOfKind:UICollectionElementKindSectionFooter
                           withReuseIdentifier:footerID];
    }

    /* and pull to reload */
    __weak typeof(controller) weakSelf = controller;
    [controller.collectionView addCustomizedPullToRefreshWithActionHandler:^{
        [weakSelf pullToReload];
    }];

    /* and infinite loading */
    [controller.collectionView addCustomizedInfiniteScrollingWithActionHandler:^{
        [weakSelf fetchMoreData];
    }];
}

@end

/*=============================================================================*/
// PRListVideoViewConfiguration
/*=============================================================================*/
@implementation PRListVideoViewConfiguration

@end

