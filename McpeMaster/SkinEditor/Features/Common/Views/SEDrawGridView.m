/*============================================================================
 PROJECT: SkinEditor
 FILE:    SEGridView.m
 AUTHOR:  Khoai Nguyen
 DATE:    10/11/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SEDrawGridView.h"
#import "UIView+CoreGraphicUtils.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/


@implementation SEDrawGridView

- (void)setGridLineColor:(UIColor *)gridLineColor {
    _gridLineColor = gridLineColor;
    [self setNeedsDisplay];
}

- (void)setMatrixDimension:(MatrixDimension)matrixDimension {
    _matrixDimension = matrixDimension;
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    [self drawGridLinesInRect:rect color:_gridLineColor matrixDimension:_matrixDimension];
}

@end
