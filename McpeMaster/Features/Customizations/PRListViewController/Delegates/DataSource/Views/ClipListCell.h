/*============================================================================
 PROJECT: McpeMaster
 FILE:    ClipListCell.h
 AUTHOR:  Khoai Nguyen
 DATE:    3/18/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import <Foundation/Foundation.h>
#import "MKCollectionViewCell.h"
#import "AFNetworking.h"
#import <StoreKit/StoreKit.h>

/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 PROTOCOL
 =============================================================================*/
typedef void (^TouchedOnOptionsBlock)(id sender);

/*============================================================================
 Interface:   ClipListCell
 =============================================================================*/


@interface ClipListCell : MKCollectionViewCell <UIActionSheetDelegate>

@property(copy, nonatomic) TouchedOnOptionsBlock touchedOnOptionsBlock;
@property (strong,nonatomic) UIViewController *parentViewController;
@property (strong,nonatomic) NSProgress *progressObject;

- (void)setTouchedOnOptionsBlock:(void (^)(id sender))block;

@end
