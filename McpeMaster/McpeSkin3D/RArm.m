//
//  RArm.m
//  GirlSkin
//
//  Created by apple on 2016. 7. 12..
//  Copyright (c) 2016年 SWORD. All rights reserved.
//

#import "RArm.h"


@implementation RArm
- (void) initCube:(float)height
{
    if (height == 32.0f) {
        float tmprarm_texcoords[] = {
            // front rightArm
            44.0f/64.0f,	1.0f,
            44.0f/64.0f,	1.0f - 6.0f/32.0f,
            48.0f/64.0f,	1.0f - 6.0f/32.0f,
            48.0f/64.0f,	1.0f,
            // front rightArm1
            44.0f/64.0f,	1.0f - 6.0f/32.0f,
            44.0f/64.0f,	1.0f - 12.0f/32.0f,
            48.0f/64.0f,	1.0f - 12.0f/32.0f,
            48.0f/64.0f,	1.0f - 6.0f/32.0f,
            // top rightArm
            44.0f/64.0f,	1.0f - 12.0f/32.0f,
            44.0f/64.0f,	1.0f - 16.0f/32.0f,
            48.0f/64.0f,	1.0f - 16.0f/32.0f,
            48.0f/64.0f,	1.0f - 12.0f/32.0f,
            // bottom rightArm
            48.0f/64.0f,	1.0f - 12.0f/32.0f,
            48.0f/64.0f,	1.0f - 16.0f/32.0f,
            52.0f/64.0f,	1.0f - 16.0f/32.0f,
            52.0f/64.0f,	1.0f - 12.0f/32.0f,
            // left rightArm
            48.0f/64.0f,	1.0f,
            48.0f/64.0f,	1.0f - 6.0f/32.0f,
            52.0f/64.0f,	1.0f - 6.0f/32.0f,
            52.0f/64.0f,	1.0f,
            // left rightArm1
            48.0f/64.0f,	1.0f - 6.0f/32.0f,
            48.0f/64.0f,	1.0f - 12.0f/32.0f,
            52.0f/64.0f,	1.0f - 12.0f/32.0f,
            52.0f/64.0f,	1.0f - 6.0f/32.0f,
            // right rightArm
            40.0f/64.0f,	1.0f,
            40.0f/64.0f,	1.0f - 6.0f/32.0f,
            44.0f/64.0f,	1.0f - 6.0f/32.0f,
            44.0f/64.0f,	1.0f,
            // right rightArm1
            40.0f/64.0f,	1.0f - 6.0f/32.0f,
            40.0f/64.0f,	1.0f - 12.0f/32.0f,
            44.0f/64.0f,	1.0f - 12.0f/32.0f,
            44.0f/64.0f,	1.0f - 6.0f/32.0f,
            // back rightArm
            52.0f/64.0f,	1.0f,
            52.0f/64.0f,	1.0f - 6.0f/32.0f,
            56.0f/64.0f,	1.0f - 6.0f/32.0f,
            56.0f/64.0f,	1.0f,
            // back rightArm1
            52.0f/64.0f,	1.0f - 6.0f/32.0f,
            52.0f/64.0f,	1.0f - 12.0f/32.0f,
            56.0f/64.0f,	1.0f - 12.0f/32.0f,
            56.0f/64.0f,	1.0f - 6.0f/32.0f,
        };
        memcpy(rarm_texcoords, tmprarm_texcoords, sizeof(tmprarm_texcoords));
       
        [super initCube:4.0f sy:12.0f sz:4.0f offsetx:-6.0f offsety:2.0f offsetz:0.0f step_value:20.0f/40.f rot_axis_x:1.0f rot_axis_y:0.0f rot_axis_z:0.0f max_anlge:10.0f min_angle:-10.0f step_value1:-40.0f/40.0f max_angle1:20.0f min_angle:-20.0f isFixAngle:true fixDir:-1.0f];
        
        [super AddTextures:rarm_texcoords nSize:sizeof(rarm_texcoords) / sizeof(float)];
    }else if (height == 64.0f){
        float tmprarm_texcoords[] = {
            // front rightArm
            44.0f/64.0f,	0.5f,
            44.0f/64.0f,	0.5f - 6.0f/64.0f,
            48.0f/64.0f,	0.5f - 6.0f/64.0f,
            48.0f/64.0f,	0.5f,
            // front rightArm1
            44.0f/64.0f,	0.5f - 6.0f/64.0f,
            44.0f/64.0f,	0.5f - 12.0f/64.0f,
            48.0f/64.0f,	0.5f - 12.0f/64.0f,
            48.0f/64.0f,	0.5f - 6.0f/64.0f,
            // top rightArm
            44.0f/64.0f,	0.5f - 12.0f/64.0f,
            44.0f/64.0f,	0.5f - 16.0f/64.0f,
            48.0f/64.0f,	0.5f - 16.0f/64.0f,
            48.0f/64.0f,	0.5f - 12.0f/64.0f,
            // bottom rightArm
            48.0f/64.0f,	0.5f - 12.0f/64.0f,
            48.0f/64.0f,	0.5f - 16.0f/64.0f,
            52.0f/64.0f,	0.5f - 16.0f/64.0f,
            52.0f/64.0f,	0.5f - 12.0f/64.0f,
            // left rightArm
            40.0f/64.0f,	0.5f,
            40.0f/64.0f,	0.5f - 6.0f/64.0f,
            44.0f/64.0f,	0.5f - 6.0f/64.0f,
            44.0f/64.0f,	0.5f,
            // left rightArm1
            40.0f/64.0f,	0.5f - 6.0f/64.0f,
            40.0f/64.0f,	0.5f - 12.0f/64.0f,
            44.0f/64.0f,	0.5f - 12.0f/64.0f,
            44.0f/64.0f,	0.5f - 6.0f/64.0f,
            // right rightArm
            40.0f/64.0f,	0.5f,
            40.0f/64.0f,	0.5f - 6.0f/64.0f,
            44.0f/64.0f,	0.5f - 6.0f/64.0f,
            44.0f/64.0f,	0.5f,
            // right rightArm1
            40.0f/64.0f,	0.5f - 6.0f/64.0f,
            40.0f/64.0f,	0.5f - 12.0f/64.0f,
            44.0f/64.0f,	0.5f - 12.0f/64.0f,
            44.0f/64.0f,	0.5f - 6.0f/64.0f,
            // back rightArm
            52.0f/64.0f,	0.5f,
            52.0f/64.0f,	0.5f - 6.0f/64.0f,
            56.0f/64.0f,	0.5f - 6.0f/64.0f,
            56.0f/64.0f,	0.5f,
            // back rightArm1
            52.0f/64.0f,	0.5f - 6.0f/64.0f,
            52.0f/64.0f,	0.5f - 12.0f/64.0f,
            56.0f/64.0f,	0.5f - 12.0f/64.0f,
            56.0f/64.0f,	0.5f - 6.0f/64.0f,
        };
        memcpy(rarm_texcoords, tmprarm_texcoords, sizeof(tmprarm_texcoords));
        float tmprarm2_texcoords[] = {
            // front rightArm
            44.0f/64.0f,	32.0f/64.0f + 16.0f/64.0f, 					// 0
            44.0f/64.0f,	20.0f/64.0f + 6.0f/64.0f + 16.0f/64.0f,	    // 3
            48.0f/64.0f,	20.0f/64.0f + 6.0f/64.0f + 16.0f/64.0f, 	// 2
            48.0f/64.0f,	32.0f/64.0f + 16.0f/64.0f, 	  				// 1
            // front rightArm1
            44.0f/64.0f,	32.0f/64.0f - 6.0f/64.0f + 16.0f/64.0f, 	// 0
            44.0f/64.0f,	20.0f/64.0f + 16.0f/64.0f,	    			// 3
            48.0f/64.0f,	20.0f/64.0f + 16.0f/64.0f, 	  				// 2
            48.0f/64.0f,	32.0f/64.0f - 6.0f/64.0f + 16.0f/64.0f, 	// 1
            // top rightArm
            44.0f/64.0f,	20.0f/64.0f + 16.0f/64.0f,    				// 3
            44.0f/64.0f,	16.0f/64.0f + 16.0f/64.0f, 					// 7
            48.0f/64.0f,	16.0f/64.0f + 16.0f/64.0f,					// 6
            48.0f/64.0f,	20.0f/64.0f + 16.0f/64.0f,					// 2
            // bottom rightArm
            48.0f/64.0f,	20.0f/64.0f + 16.0f/64.0f,					// 4
            48.0f/64.0f,	16.0f/64.0f + 16.0f/64.0f,					// 0
            52.0f/64.0f,	16.0f/64.0f + 16.0f/64.0f,					// 1
            52.0f/64.0f,	20.0f/64.0f + 16.0f/64.0f,	    			// 5
            // left rightArm
            48.0f/64.0f,	32.0f/64.0f + 16.0f/64.0f,					// 1
            48.0f/64.0f,	20.0f/64.0f + 6.0f/64.0f + 16.0f/64.0f,		// 2
            52.0f/64.0f,	20.0f/64.0f + 6.0f/64.0f + 16.0f/64.0f,     // 6
            52.0f/64.0f,	32.0f/64.0f + 16.0f/64.0f,    				// 5
            // left rightArm1
            48.0f/64.0f,	32.0f/64.0f - 6.0f/64.0f + 16.0f/64.0f,		// 1
            48.0f/64.0f,	20.0f/64.0f + 16.0f/64.0f,					// 2
            52.0f/64.0f,	20.0f/64.0f + 16.0f/64.0f,     				// 6
            52.0f/64.0f,	32.0f/64.0f - 6.0f/64.0f + 16.0f/64.0f,    	// 5
            // right rightArm
            40.0f/64.0f,	32.0f/64.0f + 16.0f/64.0f,	   				// 7
            40.0f/64.0f,	20.0f/64.0f + 6.0f/64.0f + 16.0f/64.0f,		// 3
            44.0f/64.0f,	20.0f/64.0f + 6.0f/64.0f + 16.0f/64.0f,   	// 0
            44.0f/64.0f,	32.0f/64.0f + 16.0f/64.0f,   				// 4
            // right rightArm1
            40.0f/64.0f,	32.0f/64.0f - 6.0f/64.0f + 16.0f/64.0f,	   	// 7
            40.0f/64.0f,	20.0f/64.0f + 16.0f/64.0f,					// 3
            44.0f/64.0f,	20.0f/64.0f + 16.0f/64.0f,   				// 0
            44.0f/64.0f,	32.0f/64.0f - 6.0f/64.0f + 16.0f/64.0f,   	// 4
            
            // back rightArm
            52.0f/64.0f,	32.0f/64.0f + 32.0f/64.0f,   				// 5
            52.0f/64.0f,	20.0f/64.0f + 6.0f/64.0f + 32.0f/64.0f,   	// 6
            56.0f/64.0f,	20.0f/64.0f + 6.0f/64.0f + 32.0f/64.0f,		// 7
            56.0f/64.0f,	32.0f/64.0f + 32.0f/64.0f,   				// 4
            // back rightArm1
            52.0f/64.0f,	32.0f/64.0f - 6.0f/64.0f + 32.0f/64.0f,   	// 5
            52.0f/64.0f,	20.0f/64.0f + 32.0f/64.0f,   				// 6
            56.0f/64.0f,	20.0f/64.0f + 32.0f/64.0f,					// 7
            56.0f/64.0f,	32.0f/64.0f - 6.0f/64.0f + 32.0f/64.0f		// 4
        };
        memcpy(rarm2_texcoords, tmprarm2_texcoords, sizeof(tmprarm2_texcoords));
        [super initCube:4.0f sy:12.0f sz:4.0f offsetx:-6.0f offsety:2.0f offsetz:0.0f step_value:20.0f/40.f rot_axis_x:1.0f rot_axis_y:0.0f rot_axis_z:0.0f max_anlge:10.0f min_angle:-10.0f step_value1:-40.0f/40.0f max_angle1:20.0f min_angle:-20.0f isFixAngle:true fixDir:-1.0f];
        
        [super AddTextures:rarm_texcoords nSize:sizeof(rarm_texcoords) / sizeof(float)];
        [super AddTextures:rarm2_texcoords nSize:sizeof(rarm2_texcoords) / sizeof(float)];
    }
}
@end
