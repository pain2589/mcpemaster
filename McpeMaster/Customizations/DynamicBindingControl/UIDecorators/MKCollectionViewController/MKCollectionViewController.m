/*============================================================================
 PROJECT: McpeMaster
 FILE:    MKCollectionViewController.m
 AUTHOR:  Khoai Nguyen
 DATE:    3/15/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "MKCollectionViewController.h"
#import <AdSupport/AdSupport.h>
/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@interface MKCollectionViewController () <UICollectionViewDelegateFlowLayout,GADBannerViewDelegate>
@end

@implementation MKCollectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self loadConfigurations];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self loadConfigurations];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([AppDelegate sharedDelegate].isConsent == 1) {
        [self setupConsentAdmob];
    }
    
    _index = 0;
    _offset = 20;
    _ads = [[NSMutableDictionary alloc] init];
    _adsToLoad = [[NSMutableArray alloc] init];
    _loadStateForAds = [[NSMutableDictionary alloc] init];
    
    // A banner ad is placed in the UITableView once per adInterval. iPads will have a
    // larger ad interval to avoid mutliple ads being on screen at the same time.
    _adInterval = [UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad ? 21 : 9;
    [self.collectionView registerNib:[UINib nibWithNibName:@"BannerAd" bundle:nil] forCellWithReuseIdentifier:@"GADBannerViewCell"];
    
    if ([AppDelegate sharedDelegate].isConsent == 0) {
        [self addBannerAds];
        [self preloadNextAd];
    }
    
    /* setup collection view */
    [self setUpCollectionView];
}

- (void)addBannerToTable {
    if ([self hasMethod:@"loadDataInCollectionViewControllerAtFirstTime:" inDelegate:_logics]) {
        [_logics loadDataInCollectionViewControllerAtFirstTime:self];
    }
    
    [self addBannerAds];
    [self preloadNextAd];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}


- (void)dealloc {
    self.dataSource = nil;
    self.delegate = nil;
    self.logics = nil;
    self.configuration = nil;
    self.indicatorDelegate = nil;
}

#pragma mark - Public Methods

- (void)loadConfigurations {
    /* load configurations */
}

- (void)waitForLoadingData {
    if ([self hasMethod:@"showOnView:atRect:" inDelegate:_indicatorDelegate]) {
        [_indicatorDelegate showOnView:self.view atRect:self.view.bounds];
    }
}

- (void)stopForLoadingData {
    if ([self hasMethod:@"hideFromView:" inDelegate:_indicatorDelegate]) {
        [_indicatorDelegate hideFromView:self.view];
    }
}

- (void)cleanData {
    if ([self hasMethod:@"clearDataInController:" inDelegate:_logics]) {
        [_logics clearDataInController:self];
    }
}

- (void)fetchData {
    if ([self hasMethod:@"reloadDataInCollectionViewController:" inDelegate:_logics]) {
        [_logics reloadDataInCollectionViewController:self];
    }
}

- (void)fetchMoreData {
    if ([self hasMethod:@"loadMoreDataInCollectionViewController:" inDelegate:_logics]) {
        _offset += 20;
        [self addBannerAds];
        [self preloadNextAd];
        [_logics loadMoreDataInCollectionViewController:self];
    }
}

- (void)reloadData {
    [self.collectionView reloadData];
}

- (void)pullToReload {
    if ([self hasMethod:@"forceReloadDataInCollectionViewController:" inDelegate:_logics]) {
        _index = 0;
        _offset = 20;
        [self addBannerAds];
        [self preloadNextAd];
        [_logics forceReloadDataInCollectionViewController:self];
    }
}

#pragma mark - Private Methods

- (void)setUpCollectionView {

    /* adjust collection view layout */
    if ([self hasMethod:@"collectionViewLayoutInCollectionController:" inDelegate:_configuration]) {
        [_configuration collectionViewLayoutInCollectionController:self];
    }

    /* apply more configurations for collection view */
    if ([self hasMethod:@"setupCollectionViewInController:" inDelegate:_configuration]) {
        [_configuration setupCollectionViewInController:self];
    }
}

#pragma mark - UICollectionViewDelegate && UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    if ([self hasMethod:@"numberOfSessionsInCollectionViewController:" inDelegate:_dataSource]) {
        return [_dataSource numberOfSessionsInCollectionViewController:self];
    }

    return 1; // by default number of sections = 1
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [_dataSource collectionController:self numberOfRowInSection:section];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([[_logics data] count] > 0){
        if ([[_logics data][indexPath.row] isKindOfClass:GADBannerView.class]) {
            UICollectionViewCell *reusableAdCell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"GADBannerViewCell" forIndexPath:indexPath];
            // Remove previous GADBannerView from the content view before adding a new one.
            for (UIView *subview in reusableAdCell.contentView.subviews) {
                [subview removeFromSuperview];
            }
            GADBannerView *adView = [_logics data][indexPath.row];
            [reusableAdCell.contentView addSubview:adView];
            return reusableAdCell;
        }
    }
    
    MKCollectionViewCell *cell = [_dataSource collectionController:self cellForItemAtIndexPath:indexPath];
    
    /* adjust cell after update information */
    if ([self hasMethod:@"collectionController:configureCell:atIndexPath:" inDelegate:_dataSource]) {
        [_dataSource collectionController:self configureCell:cell atIndexPath:indexPath];
    }

    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath {

    UICollectionReusableView *view = nil;

    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        if ([self hasMethod:@"headerViewInCollectionViewController:atIndexPath:" inDelegate:_dataSource]) {
            view = [_dataSource headerViewInCollectionViewController:self atIndexPath:indexPath];
        }
    }
    else if ([kind isEqualToString:UICollectionElementKindSectionFooter]) {
        if ([self hasMethod:@"footerViewInCollectionViewController:atIndexPath:" inDelegate:_dataSource]) {
            view = [_dataSource footerViewInCollectionViewController:self atIndexPath:indexPath];
        }
    }

    return view;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([[_logics data][indexPath.row] isKindOfClass:GADBannerView.class]) {
//        GADBannerView *adView = [_logics data][indexPath.row];
//        return adView.frame.size;
        if (IS_IPAD) {
            return CGSizeMake([UIScreen mainScreen].bounds.size.width, 90);
        } else {
            return CGSizeMake([UIScreen mainScreen].bounds.size.width, 50);
        }
        
    }else if ([self hasMethod:@"collectionController:layout:sizeForItemAtIndexPath:" inDelegate:_delegate]) {
        return [_delegate collectionController:self layout:collectionViewLayout sizeForItemAtIndexPath:indexPath];
    }

    return CGSizeMake([UIScreen mainScreen].bounds.size.width, 45); // by default height is 45
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                         layout:(UICollectionViewLayout *)collectionViewLayout
referenceSizeForFooterInSection:(NSInteger)section {

    if ([self hasMethod:@"collectionController:layout:referenceSizeForFooterInSection:" inDelegate:_delegate]) {
        return [_delegate collectionController:self layout:collectionViewLayout referenceSizeForFooterInSection:section];
    }

    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                         layout:(UICollectionViewLayout *)collectionViewLayout
referenceSizeForHeaderInSection:(NSInteger)section {

    if ([self hasMethod:@"collectionController:layout:referenceSizeForHeaderInSection:" inDelegate:_delegate]) {
        return [_delegate collectionController:self layout:collectionViewLayout referenceSizeForHeaderInSection:section];
    }

    return CGSizeZero;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {

    [collectionView deselectItemAtIndexPath:indexPath animated:NO];

    if ([self hasMethod:@"collectionController:didSelectItemAtIndexPath:" inDelegate:_delegate]) {
        return [_delegate collectionController:self didSelectItemAtIndexPath:indexPath];
    }
}

//Tiep De ra 1 khoang trong duoi bottom de show banner
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
//    if (IS_IPHONE_4_OR_LESS){
//        //Tiep Phan nay danh cho admob banner cũ
//        if (IS_IPAD) {
//            return UIEdgeInsetsMake(0, 0, 90, 0);
//        }else{
//            return UIEdgeInsetsMake(0, 0, 50, 0);
//        }
//    }else{
//        //Tiep them Phan nay danh cho admob native express banner
//        if (IS_IPAD) {
//            return UIEdgeInsetsMake(0, 0, 132, 0);
//        }else{
//            return UIEdgeInsetsMake(0, 0, 132, 0);
//        }
//    }
    
    //Tiep Phan nay danh cho admob banner cũ
    if (IS_IPAD) {
        return UIEdgeInsetsMake(0, 0, 90, 0);
    }else{
        return UIEdgeInsetsMake(0, 0, 50, 0);
    }
}

//insert adView to data list
- (void)insertAdView{
    NSArray *sortedKeys = [[_ads allKeys] sortedArrayUsingComparator:^NSComparisonResult(NSString *a, NSString *b){
        return a.integerValue > b.integerValue;
    }];
    for (NSString* key in sortedKeys) {
        if (key.integerValue < [[_logics data] count]){
            [[_logics data] insertObject:_ads[key] atIndex:key.integerValue];
            
            [_ads removeObjectForKey:key];
        }
    }
}

// Return string containing memory address location of a GADBannerView to be used to
// uniquely identify the object.
- (NSString *)referenceKeyForAdView:(GADBannerView *)adView {
    return [[NSString alloc] initWithFormat:@"%p", adView];
}

// MARK: - GADBannerView delegate methods

- (void)adViewDidReceiveAd:(GADBannerView *)bannerView {
    // Mark banner ad as succesfully loaded.
    _loadStateForAds[[self referenceKeyForAdView:bannerView]] = @YES;
    // Load the next ad in the adsToLoad list.
    [self preloadNextAd];
}

- (void)adView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"Failed to receive ad: %@", error.localizedDescription);
    // Load the next ad in the adsToLoad list.
    [self preloadNextAd];
}

// MARK: - UITableView source data generation

/// Adds banner ads to the tableViewItems list.
- (void)addBannerAds {
    while (_index < _offset) {
//        GADBannerView *adView = [[GADBannerView alloc] initWithAdSize:GADAdSizeFullWidthPortraitWithHeight(100)];
//        GADBannerView *adView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeLargeBanner];
        GADBannerView *adView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerPortrait];
        
        adView.adUnitID = ADMOB_BANNER_ID;
        adView.rootViewController = self;
        adView.delegate = self;
        
        _ads[[NSString stringWithFormat:@"%ld",(long)_index]] = adView;
        //[_tableViewItems insertObject:adView atIndex:_index];
        
        [_adsToLoad addObject:adView];
        _loadStateForAds[[self referenceKeyForAdView:adView]] = @NO;
        
        _index += _adInterval;
    }
}

/// Preloads banner ads sequentially. Dequeues and loads next ad from adsToLoad list.
- (void)preloadNextAd {
    if (!_adsToLoad.count) {
        return;
    }
    GADBannerView *adView = _adsToLoad.firstObject;
    [_adsToLoad removeObjectAtIndex:0];
    GADRequest *request = [GADRequest request];
    //request.testDevices = @[ kGADSimulatorID ];
    
    //Forward user consent choice
    if (PACConsentInformation.sharedInstance.requestLocationInEEAOrUnknown &&
        PACConsentInformation.sharedInstance.consentStatus == PACConsentStatusNonPersonalized) {
        GADExtras *extras = [[GADExtras alloc] init];
        extras.additionalParameters = @{@"npa": @"1"};
        [request registerAdNetworkExtras:extras];
        NSLog(@"Ads is non-personalized now!!!");
    }
    
    [adView loadRequest:request];
}

//
- (void)setupConsentAdmob {
    //[self doTestingConsentAdmob];
    //
    
    [PACConsentInformation.sharedInstance
     requestConsentInfoUpdateForPublisherIdentifiers:@[ADMOB_PUBLISHER_ID]
     completionHandler:^(NSError *_Nullable error) {
         if (error) {
             // Consent info update failed.
             NSLog(@"Consent info update failed.");
             [self addBannerToTable];
         } else {
             // Consent info update succeeded. The shared PACConsentInformationinstance has been updated.
             if (PACConsentInformation.sharedInstance.requestLocationInEEAOrUnknown &&
                 PACConsentInformation.sharedInstance.consentStatus == PACConsentStatusUnknown) {
                 NSURL *privacyURL = [NSURL URLWithString:@"https://mcpemaster.co/privacy.html"];
                 PACConsentForm *form = [[PACConsentForm alloc] initWithApplicationPrivacyPolicyURL:privacyURL];
                 form.shouldOfferPersonalizedAds = YES;
                 form.shouldOfferNonPersonalizedAds = YES;
                 form.shouldOfferAdFree = YES;
                 
                 [form loadWithCompletionHandler:^(NSError *_Nullable error) {
                     if (error) {
                         // Handle error.
                         NSLog(@"Load form consent Error: %@", error);
                         [self addBannerToTable];
                     } else {
                         [form presentFromViewController:self
                                       dismissCompletion:^(NSError *_Nullable error, BOOL userPrefersAdFree) {
                                           if (error) {
                                               NSLog(@"Present form consent Error: %@", error);
                                               [self addBannerToTable];
                                           } else if (userPrefersAdFree) {
                                               // The user prefers to use a paid version of the app.
                                               [[AppDelegate sharedDelegate].storeService presentAvailablePurchasesFrom:self];
                                           } else {
                                               // Check the user's consent choice.
                                               PACConsentStatus status = PACConsentInformation.sharedInstance.consentStatus;
                                               PACConsentInformation.sharedInstance.consentStatus = status;
                                               [self addBannerToTable];
                                           }
                                       }];
                     }
                 }];
             } else {
                 //not in EU or select personalized ads
                 [self addBannerToTable];
             }
         }
     }];
}

- (void)doTestingConsentAdmob {
    //Debug Consent, whitelist debug device
    //NSLog(@"Advertising ID: %@", ASIdentifierManager.sharedManager.advertisingIdentifier.UUIDString);
    //73885AEE-F69A-41EA-879B-C75F5F46300D
    //PACConsentInformation.sharedInstance.debugIdentifiers = @[ @"73885AEE-F69A-41EA-879B-C75F5F46300D" ];
    //PACConsentInformation.sharedInstance.debugIdentifiers = @[ @"C14481C3-F410-4486-A355-7C93F3A8B267" ];
    
    // Geography appears as in EEA for debug devices.
    //PACConsentInformation.sharedInstance.debugGeography = PACDebugGeographyEEA;
    
    //Reset user choice
    //PACConsentInformation.sharedInstance.consentStatus = PACConsentStatusUnknown;
}

@end
