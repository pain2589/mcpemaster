/*============================================================================
 PROJECT: McpeMaster
 FILE:    PRUtils.m
 AUTHOR:  Khoai Nguyen
 DATE:    1/14/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "PRUtils.h"
#import "DTAlertView.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@implementation PRUtils

+ (NSString *)localizedText:(NSString *)text forCount:(NSNumber *)count {
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    //    formatter.decimalSeparator = [[NSLocale currentLocale] objectForKey:NSLocaleDecimalSeparator];
    //    formatter.groupingSeparator = [[NSLocale currentLocale] objectForKey:NSLocaleGroupingSeparator];
    formatter.groupingSeparator = @".";
    formatter.decimalSeparator = @",";
    
    if (text && text.length > 0) {
        NSString *subffix = [count integerValue] > 1 ? [text stringByAppendingString:@"s"] : text;
        return [NSString stringWithFormat:@"%@ %@", [formatter stringFromNumber:count], subffix];
    } else {
        return [formatter stringFromNumber:count];
    }
}

+ (NSString *)youtubeDuration:(NSString *)durationText {
    
    //    NSMutableArray *arrayTime = [self scannerDuationTime:durationText];
    //    if ([arrayTime count] == 3) {
    //        return [NSString stringWithFormat:@"%@:%@:%@", [arrayTime objectAtIndex:0], [arrayTime objectAtIndex:1], [arrayTime objectAtIndex:2]];
    //    }
    //    else if ([arrayTime count] == 2) {
    //        return [NSString stringWithFormat:@"00:%@:%@", [arrayTime objectAtIndex:0], [arrayTime objectAtIndex:1]];
    //    }
    //    else {
    //        return [NSString stringWithFormat:@"00:%@", [arrayTime objectAtIndex:0]];
    //    }
    
    return [self parseISO8601Time:durationText];
}

+ (NSString *)formatStringForNumber:(NSInteger)number {
    if (number == 0) {
        return [NSString stringWithFormat:@"%02ld", (long)number];
    }
    return [NSString stringWithFormat:@"%ld", (long)number];
}

+ (NSString *)parseISO8601Time:(NSString*)duration {
    NSInteger hours = 0;
    NSInteger minutes = 0;
    NSInteger seconds = 0;
    
    //Get Time part from ISO 8601 formatted duration http://en.wikipedia.org/wiki/ISO_8601#Durations
    duration = [duration substringFromIndex:[duration rangeOfString:@"T"].location];
    
    while ([duration length] > 1) { //only one letter remains after parsing
        duration = [duration substringFromIndex:1];
        
        NSScanner *scanner = [[NSScanner alloc] initWithString:duration];
        
        NSString *durationPart = [[NSString alloc] init];
        [scanner scanCharactersFromSet:[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] intoString:&durationPart];
        
        NSRange rangeOfDurationPart = [duration rangeOfString:durationPart];
        
        duration = [duration substringFromIndex:rangeOfDurationPart.location + rangeOfDurationPart.length];
        
        if ([[duration substringToIndex:1] isEqualToString:@"H"]) {
            hours = [durationPart intValue];
        }
        if ([[duration substringToIndex:1] isEqualToString:@"M"]) {
            minutes = [durationPart intValue];
        }
        if ([[duration substringToIndex:1] isEqualToString:@"S"]) {
            seconds = [durationPart intValue];
        }
    }
    
    if (hours > 0) {
        return [NSString stringWithFormat:@"%@:%@:%@",
                [self formatStringForNumber:(long)hours],
                [self formatStringForNumber:(long)minutes],
                [self formatStringForNumber:(long)seconds]];
    }
    else {
        return [NSString stringWithFormat:@"%@:%@",
                [self formatStringForNumber:(long)minutes],
                [self formatStringForNumber:(long)seconds]];
    }
}

+ (CGRect)boundingRectForLabel:(UILabel *)label {
    return [label.text boundingRectWithSize:CGSizeMake(label.frame.size.width, CGFLOAT_MAX)
                                    options:NSStringDrawingUsesLineFragmentOrigin
                                 attributes:@{NSFontAttributeName : label.font}
                                    context:nil];
}

+ (void)showErrorMessage:(NSString *)errorMessage {
    DTAlertView *alertView = [DTAlertView alertViewWithTitle:nil
                                                     message:errorMessage
                                                    delegate:nil
                                           cancelButtonTitle:@"OK"
                                         positiveButtonTitle:nil];
    [alertView show];
}

@end
