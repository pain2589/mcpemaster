/*============================================================================
 PROJECT: SportLocker
 FILE:    UIButton+SDWedCacheCropToSize.h
 AUTHOR:  Khoai Nguyen
 DATE:    12/27/14
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import <UIKit/UIKit.h>

/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   UIButton_SDWedCacheCropToSize
 =============================================================================*/

@interface UIButton (SDWedCacheCropToSize)

- (void)setImageWithURL:(NSURL *)url
       placeholderImage:(UIImage *)placeholder
        andCropToBounds:(CGRect)bounds;

- (void)setImageWithURL:(NSURL *)url
       placeholderImage:(UIImage *)placeholder
              andResize:(CGSize)size
        withContentMode:(UIViewContentMode)mode;

@end
