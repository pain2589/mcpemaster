//
//  AppDelegate.m
//  McpeMaster
//
//  Created by Khoai Nguyen on 12/22/14.
//  Copyright (c) 2014 Khoai Nguyen. All rights reserved.
//

#import "AppDelegate.h"
#import "PRSettings.h"
#import "MKPhotoAccessor.h"
#import "iRate.h"
#import "MKRealmManager.h"
#import "Firebase.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface AppDelegate ()

@end

@implementation AppDelegate

//If we call a method with null parameter, in other programming language it will cause a NullPointerException,
//but in Objective C, if the method returns an object, any pointer type, any integer scalar of size less than or equal to sizeof(void*),
//a float, a double, a long double, or a long long, then a message sent to nil returns 0.
//example: [string length] => because string = nil then [string count] return 0

//However, if we set the loop to run a set number of times, then we're first sending a message to anArray at [anArray objectAtIndex:i];
//This will also return 0, but since objectAtIndex: returns a pointer, and a pointer to 0 is nil/NULL,
//NSLog will be passed nil each time through the loop. (Although NSLog is a function and not a method, it prints out (null) if passed a nil NSString.

//In Objective-C, it instead just causes possibly incorrect run-time behavior.
//However, if you have a method that doesn't break if it returns 0/nil/NULL/a zeroed struct,
//then this saves you from having to check to make sure the object or parameters are nil.
//- (void)foo:(NSString*)string
//{
//    int i;
//    NSLog(@"%d",[string length]);
//    NSLog(@"%@",string);
//    //for(i = 0; i < [string length]; ++i){
//        NSLog(@"%@", [string characterAtIndex:0]);
//    //}
//}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
//    _storeService = [[StoreService alloc] init];
    
//    if (_storeService.availabilityForLife == AvailabilityPurchased ||
//        _storeService.availabilityForWeekly == AvailabilitySubscribedWillRenew) {
//        UsrDfltSetBool4Key(YES, kIsPro);
//    } else {
//        UsrDfltSetBool4Key(NO, kIsPro);
//    }
    
//    NSLog(_storeService.isAppPurchased ? @"YES" : @"NO");
//    NSLog(@"%ld", (long)_storeService.availabilityForWeekly);
//    NSLog(@"%ld", (long)_storeService.availabilityForLife);
    //
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    [self loginFBRegister];
    

    //[self foo:nil];
    _countClick = 0;
    _numOfClickToShowAds = 3;
    _mcpeSearchKeyword = @"";
    
    _mapCategories = [[NSMutableArray alloc] init];
    _addonCategories = [[NSMutableArray alloc] init];
    _textureCategories = [[NSMutableArray alloc] init];
    _skinCategories = [[NSMutableArray alloc] init];
    _serverCategories = [[NSMutableArray alloc] init];
    _seedCategories = [[NSMutableArray alloc] init];
    
    //disable the idleTimer
    [UIApplication sharedApplication].idleTimerDisabled = true;
    
    // Initialize the Google Mobile Ads SDK with the AdMob application ID.
    [GADMobileAds configureWithApplicationID:ADMOB_APP_ID];
    
    //Tiep Preloading FIRST interstitial at the FIRST logical break point
    //self.googleInterstitialView = [self createAndLoadInterstitial];
    
    //FireBase
    [FIRApp configure];
    
    //
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    _majorVersion = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    if (!UsrDfltObj4Key(_majorVersion)) {
        UsrDfltSetObjKey(@"0", _majorVersion);
    }
    
    _isConsent = 0;
    _mcpeType = 2508;
    _mcpeCode = 2508;
    _mcpeNum = _numOfClickToShowAds;
    _mcpeTitle = @"";
    _mcpeMess = @"";
    _mcpeOther = @"";
    _promoteAppMess = @"";
    _promoteAppUrl = @"";
    _currentVersion = _majorVersion;
    
    //iRate
    [iRate sharedInstance].appStoreID = [AppID integerValue];
    
    //migrate realm db
    RLMRealmConfiguration *config = [RLMRealmConfiguration defaultConfiguration];
    config.schemaVersion = 2;
    config.migrationBlock = ^(RLMMigration *migration, uint64_t  oldSchemaVersion) {
        if (oldSchemaVersion < 2) {
            
        }
    };
    [RLMRealmConfiguration setDefaultConfiguration:config];
    
    // now that we have updated the schema version and provided a migration block,
    // opening an outdated Realm will automatically perform the migration and
    // opening the Realm will succeed
    [RLMRealm defaultRealm];
    
    //Prevent iCloud backup
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    [self addSkipBackupAttributeToItemAtPath:documentsPath];
    
    // Override point for customization after application launch.
    [[MKPhotoAccessor sharedAccessor] requestPhotoAuthorization:^(BOOL granted) {
        
    }];
    
    //Get App info
    [self getInfo:^(BOOL success){
        if (success) {
            self->_isFinishGetInfo = YES;
        }
    }];
    
    while (!_isFinishGetInfo){
        //Stop here till finish get infor
        NSLog(@"loading...");
    }

    //Get Categories
    _isFinishGetInfo = NO;
    [self getCategories:^(BOOL success){
        if (success) {
            self->_isFinishGetInfo = YES;
        }
    }];
    
    while (!_isFinishGetInfo){
        //Stop here till finish get infor
        NSLog(@"loading...");
    }
    
    /* get fixed screen quickly */
    fixedScreenRect();
    
    /* load list of view controllers & their storyboards */
    [[ViewControllerFactory sharedInstance] initComponentsFromFile:@"ViewControllerFactory.plist"];
    
    /* show/hide status bar */
    NSError *errorActiveAudiosession = nil;
    
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&errorActiveAudiosession];
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
    
    if (errorActiveAudiosession) {
        NSLog(@"Error %@", errorActiveAudiosession.userInfo);
    }
    
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(beginReceivingRemoteControlEvents)]) {
        [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    }
    
    self.window.backgroundColor = [UIColor blackColor];
    if (IS_IPHONE_X_XS || IS_IPHONE_XS_MAX || IS_IPHONE_XR){
        //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault]; //black color
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    }else{
        //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent]; //white color
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    }
    
    if ([iRate sharedInstance].usesCount % 10 == 0){
        [SKStoreReviewController requestReview];
    }
    
    return YES;
}

- (void)setMcpeSearchKeyword:(NSString *)mcpeSearchKeyword{
    _mcpeSearchKeyword = mcpeSearchKeyword;
}

- (void)setIsFavoriteView:(BOOL)isFavoriteView{
    _isFavoriteView = isFavoriteView;
}

+ (AppDelegate *)sharedDelegate {
    return (AppDelegate *) [UIApplication sharedApplication].delegate;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    if (_glView){
        [_glView stopAnimation];
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    if (_glView){
         [_glView setAnimationInterval:1.0f/50.0f];
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
}

#pragma mark - Overrided methods

- (RootViewController *)rootViewController {
    return (RootViewController *) self.window.rootViewController;
}

//
- (void)getInfo:(void (^)(BOOL success))completionBlock{
    if ([self connected]) {
        NSString *urlString= [NSString stringWithFormat:@"https://mcpemaster.co/getMcpeData.php?mcpeId=%@",AppID];
        NSString *urlUTF8 = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSURL *url = [NSURL URLWithString:urlUTF8];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSURLResponse *response;
        
        NSData *GETReply = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];

        if (GETReply) {
            NSError *myError = nil;
            NSDictionary *res = [NSJSONSerialization JSONObjectWithData:GETReply options:NSJSONReadingMutableLeaves error:&myError];

            if (myError == nil) {
                if (res[@"mcpeType"] && ![res[@"mcpeType"] isKindOfClass:[NSNull class]]) {
                    _mcpeType = [res[@"mcpeType"] intValue];
                }else{
                    _mcpeType = 2508;
                }
                if (res[@"mcpeCode"] && ![res[@"mcpeCode"] isKindOfClass:[NSNull class]]) {
                    _mcpeCode = [res[@"mcpeCode"] intValue];
                }else{
                    _mcpeCode = 2508;
                }
                if (res[@"mcpeMess"] && ![res[@"mcpeMess"] isKindOfClass:[NSNull class]]) {
                    _mcpeMess = res[@"mcpeMess"];
                }else{

                }
                if (res[@"mcpeTitle"] && ![res[@"mcpeTitle"] isKindOfClass:[NSNull class]]) {
                    _mcpeTitle = res[@"mcpeTitle"];
                }else{

                }
                if (res[@"mcpeOther"] && ![res[@"mcpeOther"] isKindOfClass:[NSNull class]]) {
                    _mcpeOther = res[@"mcpeOther"];
                }else{

                }
                if (res[@"mcpeNum"] && ![res[@"mcpeNum"] isKindOfClass:[NSNull class]]) {
                    _mcpeNum = [res[@"mcpeNum"] intValue];
                }else{
                    _mcpeNum = _numOfClickToShowAds;
                }
                if (res[@"promoteAppMess"] && ![res[@"promoteAppMess"] isKindOfClass:[NSNull class]]) {
                    _promoteAppMess = res[@"promoteAppMess"];
                }else{

                }
                if (res[@"promoteAppUrl"] && ![res[@"promoteAppUrl"] isKindOfClass:[NSNull class]]) {
                    _promoteAppUrl = res[@"promoteAppUrl"];
                }else{

                }
                if (res[@"currentVersion"] && ![res[@"currentVersion"] isKindOfClass:[NSNull class]]) {
                    _currentVersion = res[@"currentVersion"];
                }else{

                }
                if (res[@"isConsent"] && ![res[@"isConsent"] isKindOfClass:[NSNull class]]) {
                    _isConsent = [res[@"isConsent"] intValue];
                }else{
                    _isConsent = 0;
                }
                completionBlock(YES);
            }else{
                NSLog(@"Parse JSON error!");
                completionBlock(YES);
            }
        }else{
            NSLog(@"Parse JSON error!");
            completionBlock(YES);
        }
    }else{
        completionBlock(YES);
    }
}

//
- (void)getCategories:(void (^)(BOOL success))completionBlock{
    if ([self connected]) {
        NSString *urlString= @"https://mcpemaster.co/mcpehub/getMcpeCategories.php";
        NSString *urlUTF8 = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSURL *url = [NSURL URLWithString:urlUTF8];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSURLResponse *response;
        NSData *GETReply = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
        
        if (GETReply) {
            NSError *myError = nil;
            NSArray *arr = [NSJSONSerialization JSONObjectWithData:GETReply options:NSJSONReadingMutableContainers error:&myError];
            if (myError == nil) {
                for (NSDictionary *res in arr) {
                    if ([res[@"type"]  isEqualToString: @"mapCategory"]) {
                        if (![res[@"category"] isEqualToString:@""])
                            [_mapCategories addObject:res[@"category"]];
                    }
                    if ([res[@"type"] isEqualToString:@"addonCategory"]) {
                        if (![res[@"category"] isEqualToString:@""])
                            [_addonCategories addObject:res[@"category"]];
                    }
                    if ([res[@"type"] isEqualToString:@"textureCategory"]) {
                        if (![res[@"category"] isEqualToString:@""])
                            [_textureCategories addObject:res[@"category"]];
                    }
                    if ([res[@"type"] isEqualToString:@"seedCategory"]) {
                        if (![res[@"category"] isEqualToString:@""])
                            [_seedCategories addObject:res[@"category"]];
                    }
                    if ([res[@"type"] isEqualToString:@"skinCategory"]) {
                        if (![res[@"category"] isEqualToString:@""])
                            [_skinCategories addObject:res[@"category"]];
                    }
                    if ([res[@"type"] isEqualToString:@"serverCategory"]) {
                        if (![res[@"category"] isEqualToString:@""])
                            [_serverCategories addObject:res[@"category"]];
                    }
                    completionBlock(YES);
                }
            }else{
                NSLog(@"Parse JSON error!");
                completionBlock(YES);
            }
        }else{
            NSLog(@"Parse JSON error!");
            completionBlock(YES);
        }
    }else{
        completionBlock(YES);
    }
}

//
- (void)loginFBRegister {
//    NSNotificationCenter * __weak center = [NSNotificationCenter defaultCenter];
//    id __block token = [[NSNotificationCenter defaultCenter] addObserverForName:FBSDKAccessTokenDidChangeNotification
//                                                                         object:nil
//                                                                          queue:[NSOperationQueue mainQueue]
//                                                                     usingBlock:
    [[NSNotificationCenter defaultCenter] addObserverForName:FBSDKAccessTokenDidChangeNotification
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:
                        ^(NSNotification *notification) {
                            if (notification.userInfo[FBSDKAccessTokenDidChangeUserID]) {
                                [FBSDKProfile loadCurrentProfileWithCompletion:
                                 ^(FBSDKProfile *profile, NSError *error) {
                                     if (profile) {
                                         if ([FBSDKAccessToken currentAccessToken]) {
                                             // User is logged in
                                             
                                             if (!UDBool4Key(profile.userID)) {
                                                 //Save user_id to server
                                                 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                                     NSString *urlString= [NSString stringWithFormat:@"https://mcpemaster.co/mcpehub/registerMcpe.php?userId=%@", profile.userID];
                                                     NSString *urlUTF8 = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
                                                     NSURL *url = [NSURL URLWithString:urlUTF8];
                                                     NSURLRequest *request = [NSURLRequest requestWithURL:url];
                                                     NSURLResponse *response;
                                                     NSError *err = nil;
                                                     [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
                                                     dispatch_async(dispatch_get_main_queue(), ^(void){
                                                         if (err) {
                                                             NSLog(@"ERROR --> %@", err.localizedDescription);
                                                         }else{
                                                             NSLog(@"Register Succeffully userId = %@",profile.userID);
                                                         }
                                                     });
                                                 });
                                                 
                                                 UDSetBool4Key(YES, profile.userID);
                                             }
                                             [self syncFavoriteToDB:profile.userID];
                                         } else {
                                             
                                         }
                                     }
                                 }];
                            }
//                            [center removeObserver:token];
                        }];
}

//
- (void)getFavoriteFromServer:(NSString*)userId
                completeBlock:(void (^)(NSError *err, NSData *dataResponse))completeBlock{
    //Tiep Load Data From Server
    NSString *urlString= [NSString stringWithFormat:@"https://mcpemaster.co/mcpehub/getFavoriteMcpe.php?userId=%@",userId];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *urlUTF8 = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSURL *url = [NSURL URLWithString:urlUTF8];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSURLResponse *response;
        NSError *err = nil;
        NSData *dataResponse = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            if (err) {
                NSLog(@"ERROR --> %@", err.localizedDescription);
                completeBlock(err,nil);
            }else{
                completeBlock(err,dataResponse);
            }
        });
    });
}

- (void)syncFavoriteToDB:(NSString*)userId {
    [self getFavoriteFromServer:userId completeBlock:^(NSError *err, NSData *dataResponse) {
        if (!err) {
            NSError *error = nil;
            id dicts = [NSJSONSerialization JSONObjectWithData:dataResponse options:NSJSONReadingAllowFragments error:&error];
            
            if (error) {
                NSLog(@"ERROR --> %@", error.localizedDescription);
            }
            else {
                // Insert favorites to DB
                for (NSDictionary *dict in dicts) {
                    [self insertFavoriteToDB:dict];
                }
            }
        }else{
            NSLog(@"ERROR --> %@", err.localizedDescription);
        }
    }];
}

- (void)insertFavoriteToDB:(id)dict {
    NSString *mcpeId;
    NSString *mcpeType;
    NSString *mcpeTitle;
    NSString *stringDownloadURL;
    NSString *stringThumbURL;
    NSString *mcpeDescription;
    NSString *seedCode;
    NSString *serverIP;
    NSString *serverPort;
    NSString *serverDescription;
    
    mcpeType = dict[kMcpeType];
    mcpeId = dict[kMcpeId];
    mcpeTitle = dict[kMcpeTitle];
    stringThumbURL = dict[kMcpeThumb];
    stringDownloadURL = dict[kMcpeFile];
    
    if ([mcpeType isEqualToString:kMcpeTypeServer]) {
        serverIP = dict[kMcpeServerIP];
        serverPort = dict[kMcpeServerPort];
        serverDescription = dict[kMcpeServerDescription];
    }else if ([mcpeType isEqualToString:kMcpeTypeSeed]){
        seedCode = dict[kMcpeSeedCode];
        mcpeDescription = dict[kMcpeDescription];
    }else{
        mcpeDescription = dict[kMcpeDescription];
    }
    if ([mcpeType isEqualToString:kMcpeTypeMap]) {
        [MKRealmManager transactionWithBlock:^(RLMRealm *realm) {
            RLMMapObject *realmMapObject = [self fetchMapFromDB:stringDownloadURL];
            
            RLMMapObject *object = [[SEMapObject alloc] realmObject];
            object.mcpeId = mcpeId;
            object.name = mcpeTitle;
            object.localFile = [NSURL URLWithString:stringDownloadURL].lastPathComponent;
            object.thumb = stringThumbURL;
            object.file = stringDownloadURL;
            object.link = [NSURL URLWithString:stringDownloadURL].lastPathComponent.stringByDeletingPathExtension;
            object.downloaded = realmMapObject.downloaded;
            object.mcpeDescription = mcpeDescription;
            object.favorited = YES;
            
            [self removeAllFavMapFromDB:object.link realm:realm];
            [realm addObject:(RLMObject *)object];
        }];
    }
    if ([mcpeType isEqualToString:kMcpeTypeAddon]) {
        [MKRealmManager transactionWithBlock:^(RLMRealm *realm) {
            RLMAddonObject *realmAddonObject = [self fetchAddonFromDB:stringDownloadURL];
            
            RLMAddonObject *object = [[SEAddonObject alloc] realmObject];
            object.mcpeId = mcpeId;
            object.name = mcpeTitle;
            object.localFile = [NSURL URLWithString:stringDownloadURL].lastPathComponent;
            object.thumb = stringThumbURL;
            object.file = stringDownloadURL;
            object.link = [NSURL URLWithString:stringDownloadURL].lastPathComponent.stringByDeletingPathExtension;
            object.downloaded = realmAddonObject.downloaded;
            object.mcpeDescription = mcpeDescription;
            object.favorited = YES;
            
            [self removeAllFavAddonFromDB:object.link realm:realm];
            [realm addObject:(RLMObject *)object];
        }];
    }
    if ([mcpeType isEqualToString:kMcpeTypeTexture]) {
        [MKRealmManager transactionWithBlock:^(RLMRealm *realm) {
            RLMTextureObject *realmTextureObject = [self fetchTextureFromDB:stringDownloadURL];
            
            RLMTextureObject *object = [[SETextureObject alloc] realmObject];
            object.mcpeId = mcpeId;
            object.name = mcpeTitle;
            object.localFile = [NSURL URLWithString:stringDownloadURL].lastPathComponent;
            object.thumb = stringThumbURL;
            object.file = stringDownloadURL;
            object.link = [NSURL URLWithString:stringDownloadURL].lastPathComponent.stringByDeletingPathExtension;
            object.downloaded = realmTextureObject.downloaded;
            object.mcpeDescription = mcpeDescription;
            object.favorited = YES;
            
            [self removeAllFavTextureFromDB:object.link realm:realm];
            [realm addObject:(RLMObject *)object];
        }];
    }
    if ([mcpeType isEqualToString:kMcpeTypeSkin]) {
        [MKRealmManager transactionWithBlock:^(RLMRealm *realm) {
            RLMSkinOnlineObject *object = [[SESkinOnlineObject alloc] realmObject];
            object.mcpeId = mcpeId;
            object.name = mcpeTitle;
            object.thumb = stringThumbURL;
            object.file = stringDownloadURL;
            object.link = [NSURL URLWithString:stringDownloadURL].lastPathComponent.stringByDeletingPathExtension;
            object.favorited = YES;
            
            [self removeAllFavSkinOnlineFromDB:object.link realm:realm];
            [realm addObject:(RLMObject *)object];
        }];
    }
    if ([mcpeType isEqualToString:kMcpeTypeServer]) {
        [MKRealmManager transactionWithBlock:^(RLMRealm *realm) {
            RLMServerObject *object = [[SEServerObject alloc] realmObject];
            object.mcpeId = mcpeId;
            object.name = mcpeTitle;
            object.thumb = stringThumbURL;
            object.link = [NSURL URLWithString:stringThumbURL].lastPathComponent.stringByDeletingPathExtension;
            object.serverIP = serverIP;
            object.serverPort = serverPort;
            object.serverDescription = serverDescription;
            object.favorited = YES;
            
            [self removeAllFavServerFromDB:object.link realm:realm];
            [realm addObject:(RLMObject *)object];
        }];
    }
    if ([mcpeType isEqualToString:kMcpeTypeSeed]) {
        [MKRealmManager transactionWithBlock:^(RLMRealm *realm) {
            RLMSeedObject *object = [[SESeedObject alloc] realmObject];
            object.mcpeId = mcpeId;
            object.name = mcpeTitle;
            object.thumb = stringThumbURL;
            object.link = [NSURL URLWithString:stringThumbURL].lastPathComponent.stringByDeletingPathExtension;
            object.seedCode = seedCode;
            object.mcpeDescription = mcpeDescription;
            object.favorited = YES;
            
            [self removeAllFavSeedFromDB:object.link realm:realm];
            [realm addObject:(RLMObject *)object];
        }];
    }
}

- (RLMMapObject*)fetchMapFromDB:(NSString*)downloadURL {
    RLMResults<RLMMapObject *> *objects = [[RLMMapObject objectsWhere:[NSString stringWithFormat:@"link = '%@'",[NSURL URLWithString:downloadURL].lastPathComponent.stringByDeletingPathExtension]] sortedResultsUsingProperty:@"logTime" ascending:NO];
    return objects.firstObject;
}

- (RLMAddonObject*)fetchAddonFromDB:(NSString*)downloadURL {
    RLMResults<RLMAddonObject *> *objects = [[RLMAddonObject objectsWhere:[NSString stringWithFormat:@"link = '%@'",[NSURL URLWithString:downloadURL].lastPathComponent.stringByDeletingPathExtension]] sortedResultsUsingProperty:@"logTime" ascending:NO];
    return objects.firstObject;
}

- (RLMTextureObject*)fetchTextureFromDB:(NSString*)downloadURL {
    RLMResults<RLMTextureObject *> *objects = [[RLMTextureObject objectsWhere:[NSString stringWithFormat:@"link = '%@'",[NSURL URLWithString:downloadURL].lastPathComponent.stringByDeletingPathExtension]] sortedResultsUsingProperty:@"logTime" ascending:NO];
    return objects.firstObject;
}

- (void)removeAllFavMapFromDB:(NSString*)link realm:(RLMRealm*)realm{
    RLMResults<RLMMapObject *> *objects = [[RLMMapObject objectsWhere:[NSString stringWithFormat:@"link = '%@'",link]] sortedResultsUsingProperty:@"logTime" ascending:NO];
    [realm deleteObjects:objects];
}
- (void)removeAllFavAddonFromDB:(NSString*)link realm:(RLMRealm*)realm {
    RLMResults<RLMAddonObject *> *objects = [[RLMAddonObject objectsWhere:[NSString stringWithFormat:@"link = '%@'",link]] sortedResultsUsingProperty:@"logTime" ascending:NO];
    [realm deleteObjects:objects];
}
- (void)removeAllFavTextureFromDB:(NSString*)link realm:(RLMRealm*)realm {
    RLMResults<RLMTextureObject *> *objects = [[RLMTextureObject objectsWhere:[NSString stringWithFormat:@"link = '%@'",link]] sortedResultsUsingProperty:@"logTime" ascending:NO];
    [realm deleteObjects:objects];
}
- (void)removeAllFavSeedFromDB:(NSString*)link realm:(RLMRealm*)realm {
    RLMResults<RLMSeedObject *> *objects = [[RLMSeedObject objectsWhere:[NSString stringWithFormat:@"link = '%@'",link]] sortedResultsUsingProperty:@"logTime" ascending:NO];
    [realm deleteObjects:objects];
}
- (void)removeAllFavServerFromDB:(NSString*)link realm:(RLMRealm*)realm {
    RLMResults<RLMServerObject *> *objects = [[RLMServerObject objectsWhere:[NSString stringWithFormat:@"link = '%@'",link]] sortedResultsUsingProperty:@"logTime" ascending:NO];
    [realm deleteObjects:objects];
}
- (void)removeAllFavSkinOnlineFromDB:(NSString*)link realm:(RLMRealm*)realm {
    RLMResults<RLMSkinOnlineObject *> *objects = [[RLMSkinOnlineObject objectsWhere:[NSString stringWithFormat:@"link = '%@'",link]] sortedResultsUsingProperty:@"logTime" ascending:NO];
    [realm deleteObjects:objects];
}

//
- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

//Prevent iCloud backup
- (BOOL)addSkipBackupAttributeToItemAtPath:(NSString *) filePathString
{
    NSURL* URL= [NSURL URLWithString:filePathString];
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    
    return success;
}

#pragma mark - Admob Intertitial

- (void)interstitialDidDismissScreen:(GADInterstitial *)ad{
    //Tiep Preloading ANOTHER interstitial immediately after the previous one is dismissed, app is prepared to show an interstitial again at the NEXT logical break point
    self.googleInterstitialView = [self createAndLoadInterstitial];
}

- (void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error{
    NSLog(@"%@",[error localizedDescription]);
    _isWillShowInterstitial = NO;
}

- (void)interstitialDidReceiveAd:(GADInterstitial *)interstitial {
    //    NSLog(@"Interstitital Ads Load");
    //    [interstitial presentFromRootViewController:self.window.rootViewController];
}

- (GADInterstitial *)createAndLoadInterstitial{
    GADInterstitial *interstitial = [[GADInterstitial alloc] initWithAdUnitID:ADMOB_INTERSTITIAL_ID];
    interstitial.delegate = self;
    
    GADRequest *request = [GADRequest request];
    
    //Forward user consent choice
    if (PACConsentInformation.sharedInstance.requestLocationInEEAOrUnknown &&
        PACConsentInformation.sharedInstance.consentStatus == PACConsentStatusNonPersonalized) {
        GADExtras *extras = [[GADExtras alloc] init];
        extras.additionalParameters = @{@"npa": @"1"};
        [request registerAdNetworkExtras:extras];
        NSLog(@"Ads is non-personalized now!!!");
    }
    
    [interstitial loadRequest: request];
    return interstitial;
}

- (void)loadInterstitial{
//    if (!UDBool4Key(kIsPro)) {
        if ([self.googleInterstitialView isReady]) {
            if ([_majorVersion isEqualToString:_currentVersion]){
                [self.googleInterstitialView presentFromRootViewController:self.window.rootViewController];
            }
        }else{
            NSLog(@"Interstitial Ad wasn't ready");
            _isWillShowInterstitial = NO;
        }
//    }
}

/// Called just before presenting an interstitial. After this method finishes the interstitial will
/// animate onto the screen. Use this opportunity to stop animations and save the state of your
/// application in case the user leaves while the interstitial is on screen (e.g. to visit the App
/// Store from a link on the interstitial).
- (void)interstitialWillPresentScreen:(GADInterstitial *)ad{
    NSLog(@"interstitial will Present");
    _isWillShowInterstitial = YES;
}

/// Called when |ad| fails to present.
- (void)interstitialDidFailToPresentScreen:(GADInterstitial *)ad{
    NSLog(@"interstitial fail present");
    _isWillShowInterstitial = NO;
}

/// Called before the interstitial is to be animated off the screen.
- (void)interstitialWillDismissScreen:(GADInterstitial *)ad{
    NSLog(@"interstitial will dismiss");
    _isWillShowInterstitial = NO;
}

@end
