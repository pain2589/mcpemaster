/*============================================================================
 PROJECT: SkinEditor
 FILE:    SEPartViewController.m
 AUTHOR:  Nguyen Minh Khoai
 DATE:    10/19/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SEPartViewController.h"
#import "Firebase.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@interface SEPartViewController ()
@property (nonatomic, assign) CGFloat partSize;
@end

@implementation SEPartViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [FIRAnalytics setScreenName:@"Skin_Editor" screenClass:@"Skin_Editor"];
}

//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
//{
//    return (interfaceOrientation == UIInterfaceOrientationPortrait);
//}

- (BOOL)shouldAutorotate {
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    return (orientation == UIInterfaceOrientationPortrait);
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self layoutSubviews];
}

- (CGFloat)onePartSize {
    if (_partSize == 0) {
        CGRect rect = self.gridView.frame;
        _partSize = CGRectGetWidth(rect) / partCount;
    }
    return _partSize;
}

- (void)layoutSubviews {
    /* will be overridden in subclasses */
}

@end
