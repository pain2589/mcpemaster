/*============================================================================
 PROJECT: SuperCleaner
 FILE:    MKPhotoAccessor.m
 AUTHOR:  Khoai Nguyen
 DATE:    8/20/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "MKPhotoAccessor.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

void (^handler)(PHAuthorizationStatus);

@interface MKPhotoAccessor ()
@property(strong, nonatomic) PHCachingImageManager *imageManager;

@end

@implementation MKPhotoAccessor

+ (instancetype)sharedAccessor {
    static dispatch_once_t predicate;
    static MKPhotoAccessor *instance = nil;
    dispatch_once(&predicate, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

#if (!__has_feature(objc_arc))

- (id)retain {
    
    return self;
}

- (unsigned)retainCount {
    return UINT_MAX;  //denotes an object that cannot be released
}

- (oneway void)release {
    //do nothing
}

- (id)autorelease {
    
    return self;
}
#endif

- (PHCachingImageManager *)imageManager {
    if (!_imageManager) {
        _imageManager = [[PHCachingImageManager alloc] init];
    }
    return _imageManager;
}


#pragma mark -
#pragma mark - Support Methods


#pragma mark -
#pragma mark - Public Methods

- (void)requestImageForAsset:(PHAsset *)asset
                  targetSize:(CGSize)size
                 contentMode:(PHImageContentMode)contentMode
               completeBlock:(FetchAssetCompleteBlock)completeBlock {
    
    [self.imageManager requestImageForAsset:asset
                                 targetSize:size
                                contentMode:contentMode
                                    options:nil
                              resultHandler:^(UIImage *result, NSDictionary *info) {
                                  completeBlock(result, info);
                              }];
}

- (void)requestImageDataForAsset:(PHAsset *)asset
                   completeBlock:(FetchAssetCompleteBlock)completeBlock {
    
    [self.imageManager requestImageDataForAsset:asset
                                        options:nil
                                  resultHandler:^(NSData *_Nullable imageData, NSString *_Nullable dataUTI, UIImageOrientation orientation, NSDictionary *_Nullable info) {
                                      completeBlock(imageData, info);
                                  }];
}

- (void)requestVideoForAsset:(PHAsset *_Nonnull)asset
               completeBlock:(FetchAssetCompleteBlock _Nonnull)completeBlock {
    
    [self.imageManager requestAVAssetForVideo:asset
                                      options:nil
                                resultHandler:^(AVAsset *_Nullable asset, AVAudioMix *_Nullable audioMix, NSDictionary *_Nullable info) {
                                    completeBlock(asset, info);
                                }];
}

- (PHFetchOptions *)fetchOptionsForSortingCreateDate {
    
    /* create fetch options for all photos */
    PHFetchOptions *options = [[PHFetchOptions alloc] init];
    
    /* sort by creationDate ascending */
    options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
    return options;
}

- (PHFetchOptions *)fetchOptionsForMediaType:(PHAssetMediaType)mediaType {
    
    /* create fetch options for all photos */
    PHFetchOptions *options = [self fetchOptionsForSortingCreateDate];
    options.predicate = [NSPredicate predicateWithFormat:@"mediaType = %d", mediaType];
    return options;
}

- (PHFetchOptions *)fetchOptionsForCollections {
    
    /* create fetch options for all photos */
    PHFetchOptions *options = [self fetchOptionsForSortingCreateDate];
    options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"startDate" ascending:NO]];
    return options;
}

- (void)fetchPhotosAssetsCompleteBlock:(FetchResultCompleteBlock)completeBlock {
    
    /* create fetch options for all photos */
    PHFetchOptions *options = [self fetchOptionsForMediaType:PHAssetMediaTypeImage];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"mediaSubtypes = %d", PHAssetMediaSubtypePhotoScreenshot];
    options.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[options.predicate, predicate]];
    
    /* do fetch request */
    PHFetchResult *fetchResult = [PHAsset fetchAssetsWithOptions:options];
    
    /* get photos by asset */
    if (fetchResult) {
        completeBlock(fetchResult, YES);
    }
    else {
        completeBlock(nil, NO);
    }
}

- (void)fetchPhotosAssetsWithoutScreenShotsCompleteBlock:(FetchResultCompleteBlock _Nonnull)completeBlock {
    
    /* create fetch options for all photos */
    PHFetchOptions *options = [self fetchOptionsForSortingCreateDate];
    
    /* do fetch request */
    PHFetchResult *fetchResult = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:options];
    
    /* get photos by asset */
    if (fetchResult) {
        completeBlock(fetchResult, YES);
    }
    else {
        completeBlock(nil, NO);
    }
}

- (void)fetchScreenShotPhotosAssetsCompleteBlock:(FetchResultCompleteBlock _Nonnull)completeBlock {
    
    [self fetchPhotosAssetsInType:PHAssetCollectionTypeSmartAlbum
                       andSubtype:PHAssetCollectionSubtypeSmartAlbumScreenshots
                    completeBlock:^(NSArray<PHFetchResult *> *_Nullable results, BOOL success) {
                        completeBlock(results.firstObject, YES);
                    }];
}

- (void)fetchVideosAssetsCompleteBlock:(FetchResultCompleteBlock _Nonnull)completeBlock {
    
    [self fetchPhotosAssetsInType:PHAssetCollectionTypeSmartAlbum
                       andSubtype:PHAssetCollectionSubtypeSmartAlbumVideos
                    completeBlock:^(NSArray<PHFetchResult *> *_Nullable results, BOOL success) {
                        completeBlock(results.firstObject, YES);
                    }];
}

- (void)fetchPhotosAssetsInType:(PHAssetCollectionType)type
                     andSubtype:(PHAssetCollectionSubtype)subType
                  completeBlock:(FetchResultGroupsCompleteBlock _Nonnull)completeBlock {
    
    
    dispatch_async(dispatch_get_global_queue(0, DISPATCH_QUEUE_PRIORITY_DEFAULT), ^{
        
        /* create fetch options for all photos */
        PHFetchOptions *options = [self fetchOptionsForCollections];
        
        /* fetch smart albums */
        PHFetchResult *smartAlbums = [PHAssetCollection fetchAssetCollectionsWithType:type subtype:subType options:options];
        NSMutableArray *collections = [NSMutableArray new];
        
        for (PHCollection *collection in smartAlbums) {
            if (![collection isKindOfClass:[PHAssetCollection class]]) continue;
            
            PHAssetCollection *assetCollection = (PHAssetCollection *) collection;
            PHFetchResult *assetsFetchResult = [PHAsset fetchAssetsInAssetCollection:assetCollection options:nil];
            [collections addObject:assetsFetchResult];
        }
        
        if (collections.count > 0) {
            completeBlock(collections, YES);
        }
        else {
            completeBlock(nil, NO);
        }
    });
}

- (void)deleteAssets:(NSArray *_Nonnull)assets completeBlock:(DeleteAssetsCompleteBlock _Nonnull)completeBlock {
    
    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
        
        // Create a change request from the asset to be modified.
        [PHAssetChangeRequest deleteAssets:assets];
        
    }                                 completionHandler:^(BOOL success, NSError *error) {
        NSLog(@"Finished updating asset. %@", (success ? @"Success." : error));
        completeBlock(success);
    }];
}

- (void)requestPhotoAuthorization:(void (^)(BOOL granted))granted {
    handler = ^(PHAuthorizationStatus status) {
        if (status == PHAuthorizationStatusAuthorized) granted(YES);
        else if (status == PHAuthorizationStatusNotDetermined) [PHPhotoLibrary requestAuthorization:handler];
        else granted(NO);
    };
    handler([PHPhotoLibrary authorizationStatus]);
}

- (void)saveImage:(nonnull UIImage *)image
  completeHandler:(nullable void(^)(BOOL success, NSError * _Nullable error))completeHandler {
    
    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
        PHAssetChangeRequest *changeRequest = [PHAssetChangeRequest creationRequestForAssetFromImage:image];
        changeRequest.creationDate          = [NSDate date];
    } completionHandler:completeHandler];
}

- (void)saveImage:(nonnull UIImage *)image toAlbum:(nonnull NSString*)albumName
  completeHandler:(nullable void(^)(BOOL success, NSError * _Nullable error))completeHandler {
    //find album if exising
    PHAssetCollection *collection = [self fetchAssetCollectionWithAlbumName:albumName];
    
    if (collection) {
        [self insertImage:image intoAssetCollection:collection completeHandler:completeHandler];
    }else{
        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
            [PHAssetCollectionChangeRequest creationRequestForAssetCollectionWithTitle:albumName];
        } completionHandler:^(BOOL success, NSError *error) {
            if (success) {
                PHAssetCollection *newCollection = [self fetchAssetCollectionWithAlbumName:albumName];
                [self insertImage:image intoAssetCollection:newCollection completeHandler:completeHandler];
            } else {
                NSLog(@"Error creating album: %@", error);
            }
        }];
    }
}

- (void)insertImage:(UIImage*)image intoAssetCollection:(PHAssetCollection*)collection
completeHandler:(nullable void(^)(BOOL success, NSError * _Nullable error))completeHandler{
    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
        PHAssetChangeRequest *assetChangeRequest = [PHAssetChangeRequest creationRequestForAssetFromImage:image];
        assetChangeRequest.creationDate          = [NSDate date];
        PHAssetCollectionChangeRequest *assetCollectionChangeRequest = [PHAssetCollectionChangeRequest changeRequestForAssetCollection:collection];
        [assetCollectionChangeRequest addAssets:@[[assetChangeRequest placeholderForCreatedAsset]]];
    } completionHandler:completeHandler];
}

- (PHAssetCollection*)fetchAssetCollectionWithAlbumName:(NSString*)albumName {
    
    //Provide the predicate to match the title of the album.
    PHFetchOptions *fetchOption = [[PHFetchOptions alloc] init];
    fetchOption.predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"title == '%@'",albumName]];
    
    //Fetch the album using the fetch option
    PHFetchResult *fetchResult = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAlbumRegular options:fetchOption];
    //Assuming the album exists and no album shares it's name, it should be the only result fetched
    PHAssetCollection *collection = fetchResult.firstObject;
    
    return collection;
}

@end
