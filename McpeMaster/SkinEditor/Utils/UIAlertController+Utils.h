/*============================================================================
 PROJECT: SkinEditor
 FILE:    UIAlertController+Utils.h
 AUTHOR:  Khoai Nguyen
 DATE:    10/28/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import <UIKit/UIKit.h>

/*============================================================================
 MACRO
 =============================================================================*/
typedef void(^AlertActionCompleteBlock)(NSUInteger buttonIndex);

/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   UIAlertController_Utils
 =============================================================================*/

@interface UIAlertController (Utils)

+ (void)showConfirmPopUpWithTitle:(NSString *)title
                          message:(NSString *)message
                     buttonTitles:(NSArray<NSString *> *)buttonTitles
                    completeBlock:(AlertActionCompleteBlock)completeBlock;

@end
