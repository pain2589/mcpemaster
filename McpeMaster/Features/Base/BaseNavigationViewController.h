/*============================================================================
 PROJECT: McpeMaster
 FILE:    BaseNavigationViewController.h
 AUTHOR:  Khoai Nguyen
 DATE:    12/23/14
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import <UIKit/UIKit.h>

/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   BaseNavigationViewController
 =============================================================================*/

@import GoogleMobileAds;

@interface BaseNavigationViewController : UINavigationController <GADBannerViewDelegate>

@property(nonatomic, strong) NSNumber *topMargin;
@property(nonatomic, assign) NSNumber *cornerRadius;

@property (strong,nonatomic) GADBannerView *bannerView;

- (void)hideAdmobBanner;

@end
