/*============================================================================
 PROJECT: SkinEditor
 FILE:    SEHeadHatBodyPartViewController.m
 AUTHOR:  Nguyen Minh Khoai
 DATE:    10/19/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SEHeadHatBodyPartViewController.h"
#import "SEDrawView.h"
#import "SESkinObject.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@interface SEHeadHatBodyPartViewController ()

@end

@implementation SEHeadHatBodyPartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    /* layout subviews */
    NSUInteger midIndex = (partCount / 2) - 1;
    
    CGFloat partSize = self.onePartSize;
    
    CGSize labelSize= CGSizeMake(2 * partSize, partTitleLabelHeight);
    
    if (IS_IPHONE_4_OR_LESS) {
        partSize = self.onePartSize*0.9;
    }
    
    CGSize viewSize = CGSizeMake(2 * partSize, 2 * partSize);
    
    CGFloat y1      = topMargin;
    CGFloat y2      = y1 + viewSize.height + partTitleLabelHeight;
    CGFloat y3      = y2 + viewSize.height + partTitleLabelHeight;
    CGFloat leftX   = leftMargin;
    CGFloat rightX  = CGRectGetWidth(self.gridView.bounds) - (leftMargin + viewSize.width);
    CGFloat centerX = midIndex * partSize;
    CGFloat bottomX = 0.5 * midIndex * partSize;
    CGFloat backX   = 1.5 * midIndex * partSize;
    
    if (IS_IPHONE_4_OR_LESS) {
        y1 = y1*0.7;
        centerX = centerX/0.9;
        bottomX = bottomX/0.9;
        backX = backX/0.9;
    }
    
    /* TOP */
    /* top title */
    UILabel *label = [self.gridView viewWithTag:(SEBodyPartTop + SEBodyPartCount)];
    label.text = @"TOP";
    label.frame = (CGRect){.origin = {centerX, y1}, .size = labelSize};
    
    /* top part view */
    UIView *view = [self.gridView viewWithTag:SEBodyPartTop];
    view.frame = (CGRect){.origin = {centerX, y1 + partTitleLabelHeight}, .size = viewSize};
    label.center = CGPointMake(view.center.x, label.center.y);
    
    /* RIGHT */
    /* right label */
    label = [self.gridView viewWithTag:(SEBodyPartRight + SEBodyPartCount)];
    label.text = @"RIGHT";
    label.frame = (CGRect){.origin = {leftX, y2}, .size = labelSize};
    
    /* right part view */
    view = [self.gridView viewWithTag:SEBodyPartRight];
    view.frame = (CGRect){.origin = {leftX, y2 + partTitleLabelHeight}, .size = viewSize};
    label.center = CGPointMake(view.center.x, label.center.y);
    
    /* FRONT */
    /* front label */
    label = [self.gridView viewWithTag:(SEBodyPartFront + SEBodyPartCount)];
    label.text = @"FRONT";
    label.frame = (CGRect){.origin = {centerX, y2}, .size = labelSize};
    
    /* front part view */
    view = [self.gridView viewWithTag:SEBodyPartFront];
    view.frame = (CGRect){.origin = {centerX, y2 + partTitleLabelHeight}, .size = viewSize};
    label.center = CGPointMake(view.center.x, label.center.y);
    
    /* LEFT */
    /* left label */
    label = [self.gridView viewWithTag:(SEBodyPartLeft + SEBodyPartCount)];
    label.text = @"LEFT";
    label.frame = (CGRect){.origin = {rightX, y2}, .size = labelSize};
    
    /* left part view */
    view = [self.gridView viewWithTag:SEBodyPartLeft];
    view.frame = (CGRect){.origin = {rightX, y2 + partTitleLabelHeight}, .size = viewSize};
    label.center = CGPointMake(view.center.x, label.center.y);
    
    /* BOTTOM */
    /* Bottom label */
    label = [self.gridView viewWithTag:(SEBodyPartBottom + SEBodyPartCount)];
    label.text = @"BOTTOM";
    label.frame = (CGRect){.origin = {bottomX, y3}, .size = labelSize};
    
    /* Bottom part view */
    view = [self.gridView viewWithTag:SEBodyPartBottom];
    view.frame = (CGRect){.origin = {bottomX, y3 + partTitleLabelHeight}, .size = viewSize};
    label.center = CGPointMake(view.center.x, label.center.y);
    
    /* BACK */
    /* back label */
    label = [self.gridView viewWithTag:(SEBodyPartBack + SEBodyPartCount)];
    label.text = @"BACK";
    label.frame = (CGRect){.origin = {backX, y3}, .size = labelSize};
    
    /* back part view */
    view = [self.gridView viewWithTag:SEBodyPartBack];
    view.frame = (CGRect){.origin = {backX, y3 + partTitleLabelHeight}, .size = viewSize};
    label.center = CGPointMake(view.center.x, label.center.y);
}

@end
