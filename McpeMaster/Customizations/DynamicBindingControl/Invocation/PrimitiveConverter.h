//
//  PrimativeConverter.h
//  Test
//
//  Created by phinguyen on 2/28/14.
//  Copyright (c) 2014 TexoDesign. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PrimitiveConverter : NSObject

+ (int)convertToInt:(NSString *)stringValue;

+ (short)convertToShort:(NSString *)stringValue;

+ (long)convertToLong:(NSString *)stringValue;

+ (long long)convertToLongLong:(NSString *)stringValue;

+ (unsigned char)convertToUnsignedChar:(NSString *)stringValue;

+ (unsigned int)convertToUnsignedInt:(NSString *)stringValue;

+ (unsigned short)convertToUnsignedShort:(NSString *)stringValue;

+ (unsigned long)convertToUnsignedLong:(NSString *)stringValue;

+ (unsigned long long)convertToUnsignedLongLong:(NSString *)stringValue;

+ (float)convertToFloat:(NSString *)stringValue;

+ (double)convertToDouble:(NSString *)stringValue;

+ (BOOL)convertToBoolean:(NSString *)stringValue;

+ (const char *)convertToCString:(NSString *)stringValue;

+ (Class)convertToClass:(NSString *)stringValue;

+ (SEL)convertToSelector:(NSString *)stringValue;

+ (id)valueFromText:(NSString *)textValue withType:(const char *)type;

@end
