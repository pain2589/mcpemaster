/*============================================================================
 PROJECT: McpeMaster
 FILE:    PRIndicatorViewDelegate.m
 AUTHOR:  Khoai Nguyen
 DATE:    4/4/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "PRIndicatorViewDelegate.h"
#import "TYMActivityIndicatorView.h"
/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@interface PRIndicatorViewDelegate ()
@property(nonatomic, strong) TYMActivityIndicatorView *indicatorView;
@end

@implementation PRIndicatorViewDelegate

- (TYMActivityIndicatorView *)indicatorView {

    if (!_indicatorView) {
        _indicatorView = [[TYMActivityIndicatorView alloc] initWithActivityIndicatorStyle:TYMActivityIndicatorViewStyleNormal];
        _indicatorView.hidesWhenStopped = YES;
        _indicatorView.backgroundColor = nil;

        _indicatorView.translatesAutoresizingMaskIntoConstraints = YES;
        _indicatorView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;

    }
    return _indicatorView;
}

- (void)showOnView:(UIView *)view atRect:(CGRect)rect {

    if (self.indicatorView.superview) {
        [_indicatorView.superview bringSubviewToFront:_indicatorView];
    } else {
        [view addSubview:_indicatorView];
    }

    self.indicatorView.center = (CGPoint) {CGRectGetMidX(rect), CGRectGetMidY(rect)};
    [_indicatorView startAnimating];
}

- (void)hideFromView:(UIView *)view {
    [_indicatorView stopAnimating];
}

@end
