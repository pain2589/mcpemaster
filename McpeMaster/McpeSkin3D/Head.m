//
//  Head.m
//  GirlSkin
//
//  Created by apple on 2016. 7. 12..
//  Copyright (c) 2016年 SWORD. All rights reserved.
//

#import "Head.h"


@implementation Head
- (void) initCube:(float)height
{
   float tmphead_texcoords[] = {
			// front head
            8.0f/64.0f,     16.0f/height, 		// 0
            8.0f /64.0f,	8.0f/height, 	    // 3
            16.0f /64.0f,	8.0f/height, 	  	// 2
            16.0f /64.0f, 	16.0f/height, 	  	// 1
			// top head
            8.0f/64.0f, 	8.0f /height,    	// 3
            8.0f/64.0f, 	0.0f,    			// 7
            16.0f/64.0f, 	0.0f,   			// 6
            16.0f/64.0f, 	8.0f /height,		// 2
			// bottom head
            16.0f/64.0f, 	8.0f /height,		// 4
            16.0f/64.0f, 	0.0f,				// 0
            24.0f/64.0f, 	0.0f,				// 1
            24.0f/64.0f, 	8.0f /height,	    // 5
			// left head
            16.0f/64.0f, 	16.0f /height,		// 1
            16.0f/64.0f, 	8.0f /height,		// 2
            24.0f/64.0f, 	8.0f /height,     	// 6
            24.0f/64.0f, 	16.0f /height,    	// 5
			// right head
            0.0f,           16.0f /height,   	// 7
            0.0f,           8.0f /height,		// 3
            8.0f/64.0f, 	8.0f/height,   		// 0
            8.0f/64.0f, 	16.0f/height,   	// 4
            // back head
            24.0f/64.0f, 	16.0f/height,   	// 5
            24.0f/64.0f, 	8.0f/height,    	// 6
            32.0f/64.0f, 	8.0f/height,		// 7
            32.0f/64.0f, 	16.0f/height,   	// 4
			};
	memcpy(head_texcoords, tmphead_texcoords, sizeof(tmphead_texcoords));

    [super initCube:8.0f    //chiều ngang
                 sy:8.0f    //chiều cao
                 sz:8.0f   //chiều sâu
            offsetx:0.0f
            offsety:12.0f
            offsetz:0.0f
        step_value1:0.25f
         rot_axis_x:0.0f
         rot_axis_y:1.0f
         rot_axis_z:0.0f
         max_anlge1:5.0f
         min_angle1:-5.0f];
    
    [super AddTextures: head_texcoords nSize: sizeof(head_texcoords) / sizeof(float)];
}
@end
