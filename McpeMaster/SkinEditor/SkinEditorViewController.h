//
//  ViewController.h
//  SkinEditor
//
//  Created by Nguyen Minh Khoai on 10/4/16.
//  Copyright © 2016 Nguyen Minh Khoai. All rights reserved.
//

#import "SEBaseViewController.h"

@import GoogleMobileAds;

@interface SkinEditorViewController : SEBaseViewController<GADBannerViewDelegate>

@property (strong,nonatomic) GADBannerView *bannerView;

@end

