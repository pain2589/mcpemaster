//
//  Cube.h
//  GirlSkin
//
//  Created by apple on 2016. 7. 12..
//  Copyright (c) 2016年 SWORD. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <OpenGLES/ES1/gl.h>
#include <OpenGLES/ES1/glext.h>

@interface Cube : NSObject{
    float mScale[3];
    float mOffset[3];
    // Angle values for main rotate.
    float mAngle;
	float step_value;
	float max_angle;
	float min_angle;	
	float angle_axis[3];
	
	// vertex and normal buffers for cubic.
	float mVertexBuffer[3 * 4 * 6];
	float mNormalVertexBuffer[3 * 4 * 6];

	float face_vertices[3 * 4 * 6];
	float normal_vertices[3 * 4 * 6];

	NSMutableArray *mTextureBuffers;
}

- (void) initCube: (float) sx
               sy: (float) sy
               sz: (float) sz
          offsetx: (float) offsetx
          offsety: (float) offsety
          offsetz: (float) offsetz
      step_value1: (float) step_value1
       rot_axis_x: (float) rot_axis_x
       rot_axis_y: (float) rot_axis_y
       rot_axis_z: (float) rot_axis_z
       max_anlge1: (float) max_angle1
       min_angle1: (float) min_angle1;
- (float *) AddTextures: (float*) texCoords
                  nSize: (int) nSize;
- (void)draw:(BOOL)isRunning;
- (void) ClearAllTextures;
@end
