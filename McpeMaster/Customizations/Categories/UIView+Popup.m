/*============================================================================
 PROJECT: FileString
 FILE:    UIView+Popup.m
 AUTHOR:  Tam Nguyen
 DATE:    9/4/14
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "UIView+Popup.h"
#import <objc/runtime.h>
/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@interface UIView (PrivatePopup)

@property(nonatomic, strong) UIControl *overlayView;
@property(nonatomic, assign) BOOL visible;

@end

@implementation UIView (Popup)

//Runtime association key.
static NSString *visible = @"visible";
static NSString *overlayView = @"overlayView";

- (void)showPopupWithOpacityOverlay:(CGFloat)opacity {
    [self showPopupWithMainViewOpacity:1.0f opacityOverlay:opacity];
}

- (void)showPopupWithMainViewOpacity:(CGFloat)mainViewOpacity opacityOverlay:(CGFloat)opacity {
    /* get top window */
    UIWindow *topWindow = nil;
    NSArray *frontToBackWindows = [[UIApplication sharedApplication] windows];

    for (UIWindow *window in frontToBackWindows) {
        if (window.windowLevel == UIWindowLevelNormal) {
            topWindow = window;
            break;
        }
    }

    [self showPopupOnView:topWindow mainViewOpacity:mainViewOpacity opacityOverlay:opacity];
}

- (void)showPopupWithOpacityOverlay:(CGFloat)opacity hideWhenTouchOutSide:(BOOL)hideWhenTouchOutSide {
    [self showPopupWithOpacityOverlay:opacity];

    if (hideWhenTouchOutSide) {
        [self.overlayView addTarget:self action:@selector(hidePopUp) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)showPopupOnView:(UIView *)parentView opacityOverlay:(CGFloat)opacity {
    [self showPopupOnView:parentView mainViewOpacity:1.0f opacityOverlay:opacity];
}

- (void)showPopupOnView:(UIView *)parentView mainViewOpacity:(CGFloat)mainViewOpacity opacityOverlay:(CGFloat)opacity {
    if (!parentView) {
        return;
    }

    /* don't show if visible */
    if (self.visible) {
        return;
    }

    self.visible = YES;

    /* overlay view */
    if (!self.overlayView) {
        self.overlayView = [[UIControl alloc] initWithFrame:parentView.bounds];
        self.overlayView.backgroundColor = [UIColor blackColor];
        self.overlayView.layer.opacity = 0;
    } else {
        self.overlayView.frame = CGRectMake(0, 0, parentView.bounds.size.width, parentView.bounds.size.height);
    }

    /* show if hidden view */
    if (!self.superview) {

        [parentView addSubview:self.overlayView];
        self.center = CGPointMake(parentView.bounds.size.width / 2, parentView.bounds.size.height / 2);
        [parentView addSubview:self];

        // set first hiden if has animation
        self.layer.opacity = 0;
        self.overlayView.layer.opacity = 0;

        [UIView animateWithDuration:0.4f
                              delay:0.0f
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.layer.opacity = mainViewOpacity;
                             self.overlayView.layer.opacity = opacity;
                         }
                         completion:^(BOOL finished) {

                         }];

        self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.94, 0.94);
        [UIView animateWithDuration:0.2 delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);
                         }
                         completion:^(BOOL finished) {

                         }];
    }

    /* Hide keyboard when show popup */
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
}

- (void)showPopupOnView:(UIView *)parentView opacityOverlay:(CGFloat)opacity hideWhenTouchOutSide:(BOOL)hideWhenTouchOutSide {
    [self showPopupOnView:parentView opacityOverlay:opacity];

    if (hideWhenTouchOutSide) {
        [self.overlayView addTarget:self action:@selector(hidePopUp) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)hidePopUp {
    [self hidePopUpWithAnimated:YES];
}

- (void)hidePopUpWithAnimated:(BOOL)animated {
    /* don't hide if hidden */
    if (!self.visible) {
        return;
    }
    self.visible = NO;

    /* hide it */
    if (self.superview) {
        [self.layer removeAllAnimations];

        if (animated) {
            [UIView animateWithDuration:0.3f
                                  delay:0.0f
                                options:UIViewAnimationOptionCurveEaseInOut
                             animations:^{
                                 self.layer.opacity = 0;

                                 // hide overlay view
                                 if (self.overlayView) {
                                     self.overlayView.layer.opacity = 0;
                                 }
                             }
                             completion:^(BOOL finished) {
                                 // hide overlay view
                                 if (self.overlayView) {
                                     [self.overlayView removeFromSuperview];
                                 }

                                 [self removeFromSuperview];
                             }];
        } else {
            // hide overlay view
            if (self.overlayView) {
                [self.overlayView removeFromSuperview];
            }

            [self removeFromSuperview];
        }
    }
}

- (UIView *)overlayView {
    return objc_getAssociatedObject(self, (__bridge const void *) (overlayView));
}

- (void)setOverlayView:(UIView *)view {
    objc_setAssociatedObject(self, (__bridge const void *) overlayView, view, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (BOOL)visible {
    NSNumber *visibleBOOL = objc_getAssociatedObject(self, (__bridge const void *) (visible));
    return visibleBOOL.boolValue;
}

- (void)setVisible:(BOOL)isVisible {
    NSNumber *visibleBOOL = [NSNumber numberWithBool:isVisible];
    objc_setAssociatedObject(self, (__bridge const void *) visible, (id) visibleBOOL, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end
