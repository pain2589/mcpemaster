/*============================================================================
 PROJECT: McpeMaster
 FILE:    ContainerViewController.h
 AUTHOR:  Khoai Nguyen
 DATE:    12/28/14
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import <UIKit/UIKit.h>

/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   ContainerViewController
 =============================================================================*/


@interface ContainerViewController : UIViewController

@property(nonatomic, strong) NSArray *segueIdentifiers;
@property(nonatomic, readonly) id currentViewController;

- (void)showViewControllerWithSegueIdentifier:(NSString *)segueIdentifier;

- (void)showViewControllerAtIndex:(NSInteger)index;

@end
