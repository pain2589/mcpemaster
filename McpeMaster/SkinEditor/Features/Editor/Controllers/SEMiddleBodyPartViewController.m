/*============================================================================
 PROJECT: SkinEditor
 FILE:    SEMiddleBodyPartViewController.m
 AUTHOR:  Khoai Nguyen
 DATE:    10/20/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SEMiddleBodyPartViewController.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@interface SEMiddleBodyPartViewController ()

@end

@implementation SEMiddleBodyPartViewController

- (void)layoutSubviews {
    [super layoutSubviews];
    
    /* layout subviews */
    CGFloat partSize = self.onePartSize;
    
    CGSize labelSize = CGSizeMake(2 * partSize, partTitleLabelHeight);
    CGSize viewSize1 = CGSizeMake(2 * partSize, 1 * partSize);
    CGSize viewSize2 = CGSizeMake(2 * partSize, 3 * partSize);
    CGSize viewSize3 = CGSizeMake(1 * partSize, 3 * partSize);
    
    CGFloat y1          = topMargin;
    CGFloat y2          = y1 + viewSize1.height + partTitleLabelHeight;
    CGFloat y3          = y2 + viewSize2.height + partTitleLabelHeight;
    CGFloat leadingX    = leftMargin;
    CGFloat trailingX   = CGRectGetWidth(self.gridView.bounds) - (leftMargin + 2 * partSize);
    CGFloat centerX     = 1.75 * partSize;
    CGFloat leftX       = 2.25 * partSize + viewSize2.width;
    
    /* TOP */
    /* top title */
    UILabel *label = [self.gridView viewWithTag:(SEBodyPartTop + SEBodyPartCount)];
    label.text = @"TOP";
    label.frame = (CGRect){.origin = {centerX, y1}, .size = labelSize};
    
    /* top part view */
    UIView *view = [self.gridView viewWithTag:SEBodyPartTop];
    view.frame = (CGRect){.origin = {centerX, y1 + partTitleLabelHeight}, .size = viewSize1};
    
    /* RIGHT */
    /* right label */
    label = [self.gridView viewWithTag:(SEBodyPartRight + SEBodyPartCount)];
    label.textAlignment = NSTextAlignmentLeft;
    label.text = @"RIGHT";
    label.frame = (CGRect){.origin = {leadingX, y2}, .size = labelSize};
    
    /* right part view */
    view = [self.gridView viewWithTag:SEBodyPartRight];
    view.frame = (CGRect){.origin = {leadingX, y2 + partTitleLabelHeight}, .size = viewSize3};
    
    /* FRONT */
    /* front label */
    label = [self.gridView viewWithTag:(SEBodyPartFront + SEBodyPartCount)];
    label.text = @"FRONT";
    label.frame = (CGRect){.origin = {centerX, y2}, .size = labelSize};
    
    /* front part view */
    view = [self.gridView viewWithTag:SEBodyPartFront];
    view.frame = (CGRect){.origin = {centerX, y2 + partTitleLabelHeight}, .size = viewSize2};
    
    /* LEFT */
    /* left label */
    label = [self.gridView viewWithTag:(SEBodyPartLeft + SEBodyPartCount)];
    label.textAlignment = NSTextAlignmentLeft;
    label.text = @"LEFT";
    label.frame = (CGRect){.origin = {leftX, y2}, .size = labelSize};
    
    /* left part view */
    view = [self.gridView viewWithTag:SEBodyPartLeft];
    view.frame = (CGRect){.origin = {leftX, y2 + partTitleLabelHeight}, .size = viewSize3};
    
    /* BOTTOM */
    /* Bottom label */
    label = [self.gridView viewWithTag:(SEBodyPartBottom + SEBodyPartCount)];
    label.text = @"BOTTOM";
    label.frame = (CGRect){.origin = {centerX, y3}, .size = labelSize};
    
    /* Bottom part view */
    view = [self.gridView viewWithTag:SEBodyPartBottom];
    view.frame = (CGRect){.origin = {centerX, y3 + partTitleLabelHeight}, .size = viewSize1};
    label.center = CGPointMake(view.center.x, label.center.y);
    
    /* BACK */
    /* back label */
    label = [self.gridView viewWithTag:(SEBodyPartBack + SEBodyPartCount)];
    label.text = @"BACK";
    label.frame = (CGRect){.origin = {trailingX, y2}, .size = labelSize};
    
    /* back part view */
    view = [self.gridView viewWithTag:SEBodyPartBack];
    view.frame = (CGRect){.origin = {trailingX, y2 + partTitleLabelHeight}, .size = viewSize2};
}

@end
