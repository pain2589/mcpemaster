/*============================================================================
 PROJECT: McpeMaster
 FILE:    GlobalUtils.m
 AUTHOR:  Khoai Nguyen
 DATE:    2/17/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "GlobalUtils.h"
#import "NSArray+MKAdditions.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
#define kDefaultKeyName     @"value"

/*============================================================================
 Interface:   Optional Values
 =============================================================================*/

@implementation OptionalValue

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
    return YES;
}

@end


/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/
@implementation GlobalUtils

+ (instancetype)sharedUtils {
    static GlobalUtils *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

+ (id)objectForKey:(NSString *)key value:(id)value inArray:(NSArray *)array {
    return [array objectForKey:key value:value];
}

+ (id)objectForValue:(id)value inArray:(NSArray *)array {
    return [array objectForKey:kDefaultKeyName value:value];
}

#pragma mark - private functions


- (NSString *)filePathForName:(const NSString *)name {

    if (name && [name pathExtension]) {
        return [[NSBundle mainBundle] pathForResource:[name stringByDeletingPathExtension]
                                               ofType:[name pathExtension]];
    }
    return nil;
}

- (id)jsonObjectFromDictionary:(NSDictionary *)dictionary parseToClass:(NSString *)className {
    @autoreleasepool {
        NSError *error = nil;
        id ins = [[NSClassFromString(className) alloc] initWithDictionary:dictionary error:&error];

        if (ins && !error) {
            return ins;
        }
        return nil;
    }
}

- (id)loadConfigurationsFromFile:(const NSString *)file parseToClass:(NSString *)className {
    if (file) {
        NSString *filePath = [self filePathForName:file];
        if (filePath) {
            /* read file in array or dict format */
            NSData *data = [NSData dataWithContentsOfFile:filePath];
            id obj = [NSPropertyListSerialization propertyListWithData:data
                                                               options:NSPropertyListImmutable
                                                                format:NULL
                                                                 error:nil];

            if (className && obj) {
                if ([obj isKindOfClass:[NSArray class]]) {

                    NSMutableArray *array = [[NSMutableArray alloc] init];
                    for (id item in obj) {


                        id ins = [self jsonObjectFromDictionary:item parseToClass:className];
                        if (ins) {
                            [array addObject:ins];
                        }
                    }

                    return array;
                }
                else if ([obj isKindOfClass:[NSDictionary class]]) {
                    return [self jsonObjectFromDictionary:obj parseToClass:className];
                }
            }
            else {
                return obj;
            }
        }
    }
    return nil;
}

@end
