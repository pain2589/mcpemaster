/*============================================================================
 PROJECT: SkinEditor
 FILE:    SEBodyViewController.m
 AUTHOR:  Khoai Nguyen
 DATE:    10/17/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SESkinViewController.h"
#import "UIView+SEQuartzCore.h"
#import "UIView+Xib.h"
#import "SEDrawView.h"
#import "SEDescriptionPartView.h"

#import "SEHeadHatBodyPartViewController.h"
#import "SEMiddleBodyPartViewController.h"
#import "SEHandLegBodyPartViewController.h"

#import "MKPhotoAccessor.h"
#import "UIAlertController+Utils.h"
#import "NSMutableArray+SESkinObject.h"

#import "SESkinDrawView.h"
#import "MKRealmManager.h"
#import "SESkinObject+Realm.h"
#import "UIView+QuartzCoreUtils.h"
#import "ADUFileManager.h"

#import "OpenGLSkinViewController.h"
#import "Firebase.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@interface SESkinViewController ()<UITextFieldDelegate, SEDrawViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;

@property (weak, nonatomic) IBOutlet UIButton *preview3DButton;
@property (weak, nonatomic) IBOutlet UIButton *favoriteButton;
@property (weak, nonatomic) IBOutlet UIButton *exportButton;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (nonatomic, assign) BOOL favorited;
@property (nonatomic, assign) BOOL isCreateNew;
@property (nonatomic, assign) BOOL isImport;

@property (nonatomic, weak) SESkinDrawView *thumbnailView;

@end

@implementation SESkinViewController{
    OpenGLSkinViewController *skin3dViewController;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    //
    [self.view setBackgroundColor:UIColorFromRGB(0xE6E6E6)];
    //
    self.realmSkinObject = _skinObject.referencedRealmObject;
    if (_realmSkinObject) {
        _nameTextField.text = _realmSkinObject.name;
        _favorited = _realmSkinObject.favorited;
        [self updateFavoriteButton];
    }
    else {
        _preview3DButton.hidden = YES;
        _deleteButton.hidden = YES;
    }
    // Do any additional setup after loading the view.
    [_nameTextField makeDefaultGrayBorder];
    _nameTextField.delegate = self;
    
    [self.gridView makeDefaultGrayBorder];
    
    /* add thumbnail view */
    if (self.gridView.superview) {
        SESkinDrawView *thumbnailView = [[SESkinDrawView alloc] initWithFrame:CGRectZero];
        thumbnailView.backgroundColor = [UIColor colorWithHex:0x331940];
        [self.gridView.superview insertSubview:thumbnailView atIndex:0];
        self.thumbnailView = thumbnailView;
    }
    
    if (_isCreateNew || _isImport){
        _favoriteButton.hidden = YES;
    }
    
    [FIRAnalytics setScreenName:@"Skin_Editor" screenClass:@"Skin_Editor"];
}

//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
//{
//    return (interfaceOrientation == UIInterfaceOrientationPortrait);
//}

- (BOOL)shouldAutorotate {
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    return (orientation == UIInterfaceOrientationPortrait);
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self drawSkin];
}

- (SESkinObject *)skinObject {
    
    if (!_skinObject) {
        _skinObject = [SESkinObject newObject];
        _skinObject.isModified = YES;
    }
    return _skinObject;
}

- (void)updateFavoriteButton {
    if (_favorited){
        [_favoriteButton setImage:[UIImage imageNamed:@"icon_favorite"] forState:UIControlStateNormal];
    }else{
        [_favoriteButton setImage:[UIImage imageNamed:@"icon_unfavorite"] forState:UIControlStateNormal];
    }
}

- (void)drawSkin {
   
    /* update all draw views */
    if (self.gridView.subviews.count > 1) {
        
        for (NSUInteger i = 1; i <= SESkinBodyPartEditCount; i++) {
            SEDescriptionPartView *view = [self.gridView viewWithTag:i];
            SESkinBodyPartObject *object = [self.skinObject.parts partAtIndex:i];
            view.drawView.object = object.front;
            [view.drawView setNeedsDisplay];
        }
    }
    
    if (self.thumbnailView) {
        self.thumbnailView.skinObject = self.skinObject;
        [self.thumbnailView drawSkin];
    }
}

- (void)addLabelWithTag:(NSUInteger)tag {
   
    /* add label */
    UILabel *label = [UILabel new];
    label.font = [UIFont boldSystemFontOfSize:18];
    //label.textColor = [UIColor colorWithHex:@"#4ed2af" alpha:1];
    label.textColor = [UIColor colorWithHex:@"#A52A2A" alpha:1];
    label.textAlignment = NSTextAlignmentCenter;
    label.tag = (SESkinBodyPartEditCount + tag);
    [self.gridView addSubview:label];
}

- (void)layoutSubviews {
    
    /* add subviews */
    if (self.gridView.subviews.count <= 1) {
        for (NSUInteger i = 0; i < SESkinBodyPartEditCount; i++) {
            SEDescriptionPartView *view = [SEDescriptionPartView nibView];
            view.backgroundColor = [UIColor colorWithHex:@"0xf2f2f2" alpha:1];
            view.drawView.lineColor = [UIColor clearColor];
            view.drawView.delegate = self;
            view.drawView.tapEnabled = YES;
            [view makeDefaultGreenBorder];
            view.tag = (i + 1);
            view.drawView.tag = (i + 1);
            [self.gridView addSubview:view];
        }
        
        [self addLabelWithTag:1];
        [self addLabelWithTag:2];
    }
    
    /* update thumbnail frame */
    self.thumbnailView.frame = self.gridView.frame;
    
    /* layout subviews */
    CGFloat partSize = self.onePartSize * 0.8;
    
    CGSize labelSize = CGSizeMake(2 * partSize, partTitleLabelHeight);
    CGSize viewSize1 = CGSizeMake(2 * partSize, 2 * partSize);
    CGSize viewSize2 = CGSizeMake(2 * partSize, 3 * partSize);
    CGSize viewSize3 = CGSizeMake(partSize, 3 * partSize);
    
    CGFloat space = 2;
    CGFloat y1          = topMargin;
    CGFloat y2          = y1 + viewSize1.height + space;
    CGFloat y3          = y2 + viewSize2.height + space;
    CGFloat trailingX   = CGRectGetWidth(self.gridView.bounds) - (3 * partSize);
    CGFloat centerX     = 1.5 * partSize;
    
    /* HEAD */
    /* head title */
    UILabel *label = [self.gridView viewWithTag:(SESkinBodyPartEditCount + 1)];
    label.text = @"HAT";
    label.frame = (CGRect){.origin = {centerX, y1}, .size = labelSize};
    
    /* head part view */
    UIView *view = [self.gridView viewWithTag:SESkinBodyPartHead];
    view.frame = (CGRect){.origin = {centerX, y1 + partTitleLabelHeight}, .size = viewSize1};
    label.center = CGPointMake(view.center.x, label.center.y);
    
    /* hat part view */
    view = [self.gridView viewWithTag:SESkinBodyPartHat];
    view.frame = (CGRect){.origin = {trailingX, y1 + partTitleLabelHeight}, .size = viewSize1};
    label.center = CGPointMake(view.center.x, label.center.y);
    
    /* BODY */
    view = [self.gridView viewWithTag:SESkinBodyPartBody];
    view.frame = (CGRect){.origin = {centerX, y2 + partTitleLabelHeight}, .size = viewSize2};
    
    /* RIGHT HAND */
    view = [self.gridView viewWithTag:SESkinBodyPartRightArm];
    view.frame = (CGRect){.origin = {centerX - space - viewSize3.width, y2 + partTitleLabelHeight}, .size = viewSize3};
    
    /* LEFT HAND */
    view = [self.gridView viewWithTag:SESkinBodyPartLeftArm];
    view.frame = (CGRect){.origin = {centerX + space + viewSize2.width, y2 + partTitleLabelHeight}, .size = viewSize3};
    
    /* RIGHT LEG */
    view = [self.gridView viewWithTag:SESkinBodyPartRightLeg];
    view.frame = (CGRect){.origin = {centerX - space, y3 + partTitleLabelHeight}, .size = viewSize3};
    
    /* LEFT LEG */
    view = [self.gridView viewWithTag:SESkinBodyPartLeftLeg];
    view.frame = (CGRect){.origin = {centerX + viewSize3.width + space, y3 + partTitleLabelHeight}, .size = viewSize3};
}

#pragma mark -
#pragma mark - Support Methods

- (NSString *)saveImage:(UIImage *)image isThumbnail:(BOOL)isThumbnail {
    
    /* remove old file */
    NSString *oldImagePath = isThumbnail ? self.realmSkinObject.thumbnailPath : self.realmSkinObject.originalPath;
    if (oldImagePath.length == 0) {
        
        NSString *fileName = [NSString stringWithFormat:@"%f", [[NSDate date] timeIntervalSince1970]];
        NSString *finalName = [fileName stringByAppendingFormat:@"%@", isThumbnail ? @"thumbnail" : @""];
        oldImagePath = [[[ADUFileManager sharedManager] documentDirectoryPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"skins/%@",finalName]];
    }else{
        oldImagePath = [[[ADUFileManager sharedManager] documentDirectoryPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"skins/%@",oldImagePath]];
    }
    
    // Create the folder if necessary
    BOOL isDir = NO;
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSString *skinsDirectoryPath = [[[ADUFileManager sharedManager] documentDirectoryPath] stringByAppendingString:@"/skins/"];
    if (![fileManager fileExistsAtPath:skinsDirectoryPath
                           isDirectory:&isDir] && isDir == NO) {
        [fileManager createDirectoryAtPath:skinsDirectoryPath
               withIntermediateDirectories:NO
                                attributes:nil
                                     error:nil];
    }
    
    /* save file */
    NSData *data =  UIImagePNGRepresentation(image);
    if ([data writeToFile:oldImagePath atomically:NO]) {
        return oldImagePath;
    }
    
    NSLog(@"Failed to save image -> thumbnail: %@", isThumbnail ? @"YES" : @"NO");
    return nil;
}

- (UIImage *)generatedImage {
    
    UIImage *image = [self.skinObject image];
    NSData *data =  UIImagePNGRepresentation(image);
    UIImage *finalImage = [UIImage imageWithData:data];
    return finalImage;
}

- (UIImage *)generatedThumbnailImage {
    
    if (!_thumbnailView) return nil;
    
    self.thumbnailView.frame = [self.thumbnailView suggestedFrame];
    return [self.thumbnailView makeScreenShot];
}

#pragma mark -
#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark -
#pragma mark - SEDrawViewDelegate

- (void)didTapOnDrawView:(SEDrawView *)drawView {
    
    NSUInteger index = drawView.tag;
    SESkinBodyPartObject *object = [self.skinObject.parts partAtIndex: index];
    
    if (index == SESkinBodyPartHead || index == SESkinBodyPartHat) {
        
        NSString *title = nil;
        
        if (index == SESkinBodyPartHead) {
            title = @"HEAD";
        }
        else {
            title = @"HAT";
        }

        NSDictionary *arguments = @{@"partTitle": title, @"partObject": object, @"partImagedName": @"icon_head"};
        id controller = [SEHeadHatBodyPartViewController viewControllerWithNibName:@"SEBodyPartViewController" arguments:arguments];
        [self pushViewController:controller animated:YES];
    }
    else if (index == SESkinBodyPartLeftArm ||
             index == SESkinBodyPartRightArm ||
             index == SESkinBodyPartLeftLeg ||
             index == SESkinBodyPartRightLeg) {
        
        NSString *title = nil;
        NSString *image = nil;
        
        if (drawView.tag == SESkinBodyPartLeftArm) {
            title = @"LEFT HAND";
            image = @"icon_hand";
        }
        else if (drawView.tag == SESkinBodyPartRightArm) {
            title = @"RIGHT HAND";
            image = @"icon_hand";
        }
        else if (drawView.tag == SESkinBodyPartLeftLeg) {
            title = @"LEFT LEG";
            image = @"icon_leg";
        }
        else {
            title = @"RIGHT LEG";
            image = @"icon_leg";
        }
        
        NSDictionary *arguments = @{@"partTitle": title, @"partObject": object, @"partImagedName": image};
        id controller = [SEHandLegBodyPartViewController viewControllerWithNibName:@"SEBodyPartViewController" arguments:arguments];
        [self pushViewController:controller animated:YES];
    }
    else if (drawView.tag == SESkinBodyPartBody) {
        NSDictionary *arguments = @{@"partTitle": @"BODY", @"partObject": object, @"partImagedName": @"icon_body"};
        id controller = [SEMiddleBodyPartViewController viewControllerWithNibName:@"SEBodyPartViewController" arguments:arguments];
        [self pushViewController:controller animated:YES];
    }
}

- (void)addOrUpdateRealmSkinObject {
    
    [MKRealmManager transactionWithBlock:^(RLMRealm *realm) {
        
        /* create new one */
        RLMSkinObject *object = [self.skinObject realmObject];
        object.name = self.nameTextField.text.length > 0 ? self.nameTextField.text : @"SkinName";
        
        /* add new one */
        if (object) {
            object.favorited = self->_favorited;
        }
        
        /* update links */
        UIImage *originalImage = [self generatedImage];
        UIImage *thumbnailImage = [self generatedThumbnailImage];
        
        object.thumbnailPath = [[self saveImage:thumbnailImage isThumbnail:YES] lastPathComponent];
        object.originalPath = [[self saveImage:originalImage isThumbnail:NO] lastPathComponent];
        
        /* remove existing realm object */
        if (self.realmSkinObject) {
            object.logTime = self.realmSkinObject.logTime;
            [realm deleteObject:(RLMObject *)self.realmSkinObject];
        }
        
        [realm addObject:(RLMObject *)object];
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }];
}

- (void)onTouchBackButton:(id)sender {
    
    if (self.skinObject.isModified) {
        AlertActionCompleteBlock completeBlock = ^(NSUInteger buttonIndex) {
            
            if (buttonIndex == 0) {
                [self addOrUpdateRealmSkinObject];
            }
            else if (buttonIndex == 1) {
                [self.navigationController dismissViewControllerAnimated:YES completion:nil];
            }
        };
        
        [UIAlertController showConfirmPopUpWithTitle:nil
                                             message:@"Do you want to save editing skin?"
                                        buttonTitles:@[@"Yes", @"No"]
                                       completeBlock:completeBlock];
    }
    else {
        
        /* remove existing realm object */
        if (self.realmSkinObject) {
            [MKRealmManager transactionWithBlock:^(RLMRealm *realm) {
                self.realmSkinObject.favorited = self->_favorited;
                self.realmSkinObject.name = self.nameTextField.text.length > 0 ? self.nameTextField.text : @"SkinName";
            }];
        }
        
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)onTouchedDeleteButton:(UIButton *)sender {
    
    
}

- (void)onTouchedDeleteFavoriteButton:(UIButton *)sender {
    
    
}

- (IBAction)onPreview3D:(id)sender{
    skin3dViewController = [[OpenGLSkinViewController alloc] initWithNibName:@"OpenGLSkinViewController" bundle:nil];
    skin3dViewController.isOpenFromSkinEditor = YES;
    
    //Get image to show 3d
    NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
    NSURL *originalFilePath = [documentsDirectoryURL URLByAppendingPathComponent:[NSString stringWithFormat:@"skins/%@",self.realmSkinObject.originalPath]];
    skin3dViewController.skinImg = [UIImage imageWithData:[NSData dataWithContentsOfURL:originalFilePath]];
    
    //show 3d and present
    //[skin3dViewController show3DModel];
    [self.navigationController pushViewController:skin3dViewController animated:YES];
}

- (IBAction)onTouchedDelete:(id)sender {
    AlertActionCompleteBlock completeBlock = ^(NSUInteger buttonIndex) {
        
        if (buttonIndex == 0) {
            /* remove existing realm object */
            if (self.realmSkinObject) {
                //Delete file in device
                NSString *skinsDirectoryPath = [[[ADUFileManager sharedManager] documentDirectoryPath] stringByAppendingString:@"/skins/"];
                NSString *thumbnailPath = [skinsDirectoryPath stringByAppendingPathComponent:self.realmSkinObject.thumbnailPath];
                NSString *originalPath = [skinsDirectoryPath stringByAppendingPathComponent:self.realmSkinObject.originalPath];
                
                NSFileManager *fileManager = [NSFileManager defaultManager];
                
                if ([fileManager fileExistsAtPath:thumbnailPath]) {
                    NSError *error;
                    BOOL success = [fileManager removeItemAtPath:thumbnailPath error:&error];
                    if (success) {
                    }else
                    {
                        NSLog(@"Could not delete Skin Thumbnail file -:%@ ",[error localizedDescription]);
                    }
                }
                
                if ([fileManager fileExistsAtPath:originalPath]) {
                    NSError *error;
                    BOOL success = [fileManager removeItemAtPath:originalPath error:&error];
                    if (success) {
                    }else
                    {
                        NSLog(@"Could not delete Skin Original file -:%@ ",[error localizedDescription]);
                    }
                }
                [MKRealmManager transactionWithBlock:^(RLMRealm *realm) {
                    [realm deleteObject:(RLMObject *)self.realmSkinObject];
                    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
                }];
            }
        }
    };
    
    [UIAlertController showConfirmPopUpWithTitle:nil
                                         message:@"Are you sure want to delete this Skin from your Device?"
                                    buttonTitles:@[@"OK", @"Cancel"]
                                   completeBlock:completeBlock];
}

- (IBAction)onTouchedExport:(id)sender {
    AlertActionCompleteBlock completeBlock = ^(NSUInteger buttonIndex) {
        if (buttonIndex == 0) {
            //cancel
        } else {
            UIImage *pngImage = [self generatedImage];
            
            if (pngImage) {
                [[MKPhotoAccessor sharedAccessor] saveImage:pngImage toAlbum:@"MCPESkins" completeHandler:^(BOOL success, NSError * _Nullable error) {
                    if (success) {
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [UIAlertController showConfirmPopUpWithTitle:@"Success"
                                                                 message:@"Saved as new skin in MCPESkins Album"
                                                            buttonTitles:@[@"OK"]
                                                           completeBlock:nil];
                        });
                    }
                    else {
                        NSLog(@"Failed to save image (%@).", error.localizedDescription);
                    }
                }];
            }
        }
    };
    
    NSString *skinGuide = @"1. Saved Skin to Photos\n2. Open MineCraft App\n3. Go to Skin Setting\n4. Tap 'Choose New Skin' Button\n5. Choose Skin saved in Photos\n6. Tap 'Confirm Button'\n7. Show off new Skin to your friends ^_^";
    [UIAlertController showConfirmPopUpWithTitle:@"Installation Skin"
                                         message:skinGuide
                                    buttonTitles:@[@"Cancel",@"Save"]
                                   completeBlock:completeBlock];
}

- (IBAction)onTouchedFavorite:(id)sender {
    if (_favorited){
        AlertActionCompleteBlock completeBlock = ^(NSUInteger buttonIndex) {
            
            if (buttonIndex == 0) {
                if (self.realmSkinObject) {
                    self->_favorited = !self->_favorited;
                    [self updateFavoriteButton];
                    
                    [MKRealmManager transactionWithBlock:^(RLMRealm *realm) {
                        self.realmSkinObject.favorited = self->_favorited;
                    }];
                }
            }
        };
        
        [UIAlertController showConfirmPopUpWithTitle:nil
                                             message:@"Are you sure you want to remove this Skin from Favorites?"
                                        buttonTitles:@[@"OK", @"Cancel"]
                                       completeBlock:completeBlock];
    }else{
        _favorited = !_favorited;
        [self updateFavoriteButton];
        
        [MKRealmManager transactionWithBlock:^(RLMRealm *realm) {
            self.realmSkinObject.favorited = self->_favorited;
        }];
    }
}

@end
