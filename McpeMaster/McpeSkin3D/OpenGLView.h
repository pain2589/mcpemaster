//
//  OpenGLView.h
//  Tutorial01
//
//  Created by  on 12-11-24.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#include <OpenGLES/ES1/gl.h>
#include <OpenGLES/ES1/glext.h>
#include "GameCharacter.h"

@interface OpenGLView : UIView {
    CAEAGLLayer* _eaglLayer;
    
    // for mincraft
    EAGLContext* _context;
    GLuint _colorRenderBuffer;
    GLuint _frameBuffer;
    GLuint depthRenderbuffer;
    GLint backingWidth;
    GLint backingHeight;
    
    bool changeSkinImage;
    NSString* path;
    
    GLuint mBackTexData;
    GLuint mCharacterTexData;
    
    NSTimer *animationTimer;
    NSTimeInterval animationInterval;
    
    float mPlaneBuffer[3 * 4];
    float mPlaneTextureBuffer[2 * 4];
    
    float plane_vertices[3 * 4];
    float plane_texcords[2 * 4];
    float light0Position[4];
}

@property (nonatomic,strong) GameCharacter *mCharacter;

@property (nonatomic,assign) BOOL isAnimated;

- (id)initWithFrame:(CGRect)frame skinImage:(UIImage*)skinImg animated:(BOOL)animated;

- (void)loadTextureFromImg:(UIImage*)img;

- (void)stopAnimation;

- (void)clearTexture;

- (void)setAnimationInterval:(NSTimeInterval)interval;

@end
