/*============================================================================
 PROJECT: McpeMaster
 FILE:    PRECSlidingViewController.m
 AUTHOR:  Khoai Nguyen
 DATE:    4/15/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "PRECSlidingViewController.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@interface PRECSlidingViewController ()
@end

@implementation PRECSlidingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
#ifdef IPHONE_LANDSCAPE_HIDE
    if (IS_IPAD) {
        //Remove
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                [self forceShowLeftMenu:UIInterfaceOrientationLandscapeLeft];
//            });
    }
#else
    /* a little cheat to fix black left menu */
    UIDeviceOrientation deviceOrientation = [UIDevice currentDevice].orientation;
    if (UIDeviceOrientationIsLandscape(deviceOrientation)) {
        [self forceShowLeftMenu:UIInterfaceOrientationLandscapeLeft];
    }
#endif
    
    /* hotfix register UIDevice orientation to catch missing event at search option view */
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    NotifReg(self, @selector(handleOrientationChanged:), UIDeviceOrientationDidChangeNotification);
}

- (void)forceShowLeftMenu:(UIInterfaceOrientation)toInterfaceOrientation {
    if (UIInterfaceOrientationIsLandscape(toInterfaceOrientation) && !self.underLeftViewController.view.window) {
        ECSlidingViewControllerTopViewPosition currentPosition = self.currentTopViewPosition;
        [self anchorTopViewToRightAnimated:YES onComplete:^{
            if (currentPosition == ECSlidingViewControllerTopViewPositionCentered) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t) (0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self resetTopViewAnimated:NO onComplete:^{
                        
                    }];
                });
            }
        }];
    }
    else if (UIInterfaceOrientationIsPortrait(toInterfaceOrientation)) {
        /* force close menu */
        [self resetTopViewAnimated:YES];
    }
}

//- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
//    [self forceShowLeftMenu:toInterfaceOrientation];
//}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id)coordinator
{
    [coordinator animateAlongsideTransition:^(id context)
     {
         UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
         [self forceShowLeftMenu:orientation];
     } completion:^(id context)
     {
     }];
    
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
}

- (void)handleOrientationChanged:(NSNotification *)notification {
#ifndef IPHONE_LANDSCAPE_HIDE
    [self forceShowLeftMenu:(UIInterfaceOrientation) [UIDevice currentDevice].orientation];
#endif
}

@end
