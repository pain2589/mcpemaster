/*============================================================================
 PROJECT: SkinEditor
 FILE:    UIView+QuartzCoreUtils.m
 AUTHOR:  Khoai Nguyen
 DATE:    10/8/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "UIView+QuartzCoreUtils.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@implementation UIView (QuartzCoreUtils)

- (void)makeBorderWithWidth:(CGFloat)width color:(UIColor *)color {
    CALayer *layer = self.layer;
    layer.borderColor = color.CGColor;
    layer.borderWidth = width;
}

- (UIImage *)makeScreenShot {
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.opaque, 0.0);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}

@end
