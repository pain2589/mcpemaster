/*============================================================================
 PROJECT: McpeMaster
 FILE:    UIImageView+MKBindingData.m
 AUTHOR:  Khoai Nguyen
 DATE:    3/10/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "UIImageView+MKBindingData.h"
#import "NSObject+CheckTypes.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@implementation UIImageView (MKBindingData)

- (void)bindData:(id)data {

    /* only accept string type */
    if (!data) return;

    if ([data isImage]) {
        self.image = data;
    }
    else if ([data isString]) {
        [self sd_setImageWithURL:[NSURL URLWithString:data]];
    }
}

@end
