/*============================================================================
 PROJECT: SkinEditor
 FILE:    SEBodyPartViewController.m
 AUTHOR:  Nguyen Minh Khoai
 DATE:    10/19/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SEBodyPartViewController.h"
#import "UIView+Xib.h"
#import "SESkinObject.h"
#import "SEDrawView.h"
#import "UIView+SEQuartzCore.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@interface SEBodyPartViewController ()<SEDrawViewDelegate>
@end

@implementation SEBodyPartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [_headerView makeDefaultGrayBorder];
    [_headerView setBackgroundColor:[UIColor whiteColor]];
    
    self.gridView.backgroundColor = [UIColor colorWithHex:@"#ededed" alpha:1];
    self.gridView.gridLineColor = [UIColor colorWithHex:@"#dfdfdf" alpha:1];
    
    _titleLabel.text = _partTitle;
    _imageView.image = [UIImage imageNamed:_partImagedName];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    /* update all draw views */
    if (self.gridView.subviews.count > 1) {
        
        for (NSUInteger i = 0; i < SEBodyPartCount; i++) {
            SEDescriptionPartView *view = [self.gridView viewWithTag:(i + 1)];
            [view.drawView setNeedsDisplay];
        }
    }
}

- (void)setBodyPartObjectForView:(SEDrawView *)view {
    
    NSUInteger tag = view.tag;
    switch (tag) {
        case SEBodyPartTop:
            view.object = self.partObject.top;
            break;
            
        case SEBodyPartBottom:
            view.object = self.partObject.bottom;
            break;
            
        case SEBodyPartLeft:
            view.object = self.partObject.left;
            break;
            
        case SEBodyPartRight:
            view.object = self.partObject.right;
            break;
            
        case SEBodyPartFront:
            view.object = self.partObject.front;
            break;
            
        case SEBodyPartBack:
            view.object = self.partObject.back;
            break;
            
        default:
            break;
    }
}

- (void)layoutSubviews {

    /* draw grid view */
    CGFloat partSize = self.onePartSize;
    NSUInteger columnSize = CGRectGetWidth(self.gridView.bounds) / partSize;
    NSUInteger rowSize = CGRectGetHeight(self.gridView.bounds) / partSize;
    self.gridView.matrixDimension = MatrixDimensionMake(rowSize, columnSize);
    
    /* draw part views */
    if (self.gridView.subviews.count <= 1) {
        for (NSUInteger i = 0; i < SEBodyPartCount; i++) {
            
            /* create view */
            SEDescriptionPartView *view = [SEDescriptionPartView nibView];
            view.backgroundColor = [UIColor whiteColor];
            view.drawView.lineColor = [UIColor clearColor];
            view.drawView.delegate = self;
            view.drawView.tapEnabled = YES;
            [view makeDefaultGreenBorder];
            view.tag = (i + 1);
            view.drawView.tag = (i + 1);
            [self.gridView addSubview:view];
            
            /* update object */
            [self setBodyPartObjectForView:view.drawView];
            
            /* add label */
            UILabel *label = [UILabel new];
            label.font = [UIFont boldSystemFontOfSize:18];
            //label.textColor = [UIColor colorWithHex:@"#4ed2af" alpha:1];
            label.textColor = [UIColor colorWithHex:@"#A52A2A" alpha:1];
            label.textAlignment = NSTextAlignmentCenter;
            label.tag = (SEBodyPartCount + view.tag);
            [self.gridView addSubview:label];
        }
    }
}

#pragma mark -
#pragma mark - SEDrawViewDelegate

- (void)didTapOnDrawView:(SEDrawView *)drawView {
    NSDictionary *attributes = @{@"bodyPartObject": drawView.object};
    [self pushViewControllerWithStoryID:@"SEEditorViewController"
                           inStoryBoard:EditorStoryBoard
                              arguments:attributes
                               animated:YES];
}

@end
