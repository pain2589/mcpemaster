/*============================================================================
 PROJECT: McpeMaster
 FILE:    LeftMenuViewController.h
 AUTHOR:  Ngoc Tam Nguyen
 DATE:    12/28/14
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "PRBaseViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   LeftMenuViewController
 =============================================================================*/


@interface LeftMenuViewController : PRBaseViewController

@property(copy, nonatomic) NSString *selectedCategoryID;
@property(strong, nonatomic) PRPlaylistData *selectedPlaylistData;
@property(strong, nonatomic) NSIndexPath *selectedIndexPath;

@property(weak,nonatomic) IBOutlet UIButton *btnRestore;
@property(weak,nonatomic) IBOutlet UIButton *btnGoPro;

@property(weak,nonatomic) IBOutlet UIView *loginView;
@property(weak,nonatomic) IBOutlet UILabel *lbWelcome;

@property(strong, nonatomic) FBSDKLoginButton *loginButton;

#pragma mark - Support methods
- (void)reloadData;

- (IBAction)onTouchGoPro:(id)sender;

@end
