/*============================================================================
 PROJECT: SkinEditor
 FILE:    SEEditorViewController.m
 AUTHOR:  Khoai Nguyen
 DATE:    10/4/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SEEditorViewController.h"
#import "SEDrawView.h"
#import "UIView+SEQuartzCore.h"
#import "NSMutableArray+Stack.h"
#import "SEDrawView+Mathematics.h"
#import "QBImagePickerController.h"

#import "SESkinObject.h"
#import "MKPhotoAccessor.h"
#import "UIAlertController+Utils.h"
#import "Firebase.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/

#define TransparentColor    [UIColor colorWithPatternImage:[UIImage imageNamed:@"chess_pattern"]]
#define SEColor(color)      [color isEqual:[UIColor clearColor]] ? TransparentColor : color
static NSUInteger const     RecentColorPadding = 5;

/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@interface SEEditorViewController ()<SEDrawViewDelegate, QBImagePickerControllerDelegate>

/* UI components */
@property (weak, nonatomic) IBOutlet UIView *drawViewContainer;
@property (nonatomic, weak) SEDrawView *drawView;
@property (nonatomic, weak) UIImageView *backgroundView;

@property (weak, nonatomic) IBOutlet UIView *colorChooseView;
@property (weak, nonatomic) IBOutlet UIView *recentChosenColorView;
@property (weak, nonatomic) IBOutlet UIView *cameraView;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *toolBarButtons;
@property (weak, nonatomic) IBOutlet UIButton *pencilButton;
@property (weak, nonatomic) IBOutlet UIButton *undoButton;

/* Logical Components */
@property (nonatomic, strong) UIColor *color;
@property (nonatomic, strong) NSMutableArray *recentColors;
@property (nonatomic, assign) EditorToolBarButtonType actionType;

@property (nonatomic, strong) NSUndoManager *undoManager;
@property (nonatomic, strong) NSMutableArray<SEBYTEMatrix *> *colorMatrixes;

@property (nonatomic, assign) NSUInteger maximumColorCount;

@property (nonatomic, strong) SEBYTEMatrix *originalMatrix;
@property (nonatomic, assign) BOOL isModified;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *recentChooseColorLeadingConstraint;

@end

@implementation SEEditorViewController
@synthesize actionType;
@synthesize undoManager = _undoManager;

- (NSUndoManager *)undoManager {
    if (!_undoManager) {
        _undoManager = [NSUndoManager new];
    }
    return _undoManager;
}

- (NSMutableArray<SEBYTEMatrix *> *)colorMatrixes {
    if (!_colorMatrixes) {
        _colorMatrixes = [NSMutableArray<SEBYTEMatrix *> new];
    }
    return _colorMatrixes;
}

- (NSMutableArray *)recentColors {
    if (!_recentColors) {
        _recentColors = [[NSMutableArray alloc] init];
    }
    return _recentColors;
}

- (void)updateSelectedColor:(UIColor *)selectedColor andAddToRecentColorGroup:(BOOL)updateFlag {
    
    if (actionType == EditorToolBarButtonTypeEraser) {
        self.drawView.fillColor = [UIColor clearColor];
    }
    else {
        self.drawView.fillColor = selectedColor;
    }
    
    /* update color for chosen color view */
    _colorChooseView.backgroundColor = SEColor(selectedColor);
    
    /* update color */
    if (updateFlag) {
        
        /* update color for recent color view */
        [self.recentColors pushStackObject:selectedColor];
        [self updateColorsForRecentColorGroup];
    }
    
    [self saveRecentColorsArrayData:self.recentColors];
}

- (void)saveRecentColorsArrayData:(NSMutableArray*)recentColorsArray {
    NSMutableArray *archiveArray = [NSMutableArray arrayWithCapacity:recentColorsArray.count];
    for (UIColor *colorObject in recentColorsArray) {
        NSData *colorEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:colorObject];
        [archiveArray addObject:colorEncodedObject];
    }
    
    UDSetValue4Key(archiveArray, @"recentColors");
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    if (UDValue4Key(@"recentColors")) {
        //unarchive color data array
        NSMutableArray *archiveArray = UDValue4Key(@"recentColors");
        for (NSData *colorEncodedObject in archiveArray) {
            UIColor *colorObject = [NSKeyedUnarchiver unarchiveObjectWithData:colorEncodedObject];
            [self.recentColors addObject:colorObject];
        }
    }
    
    self.originalMatrix = [self.bodyPartObject.colorMatrix clonedMatrix];
    
    // Do any additional setup after loading the view.
    
    /* create draw view */
    UIImageView *backgroundView = [[UIImageView alloc] initWithFrame:CGRectZero];
    backgroundView.backgroundColor = TransparentColor;
    backgroundView.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addSubview:backgroundView];
    self.backgroundView = backgroundView;
    
    /* create draw view */
    SEDrawView *drawView = [[SEDrawView alloc] initWithFrame:CGRectZero];
    drawView.backgroundColor = [UIColor clearColor];
    drawView.delegate = self;
    drawView.object = self.bodyPartObject;
    [self.view addSubview:drawView];
    self.drawView = drawView;
    
    /* Some configurations */
    [_colorChooseView makeDefaultGrayBorder];
    [_cameraView makeDefaultGrayBorder];
    
    if (!self.recentColors || [self.recentColors count] == 0) {
        [self setSelectedColor: [UIColor orangeColor]];
    }else{
        self.color = [self.recentColors lastObject];
        _colorChooseView.backgroundColor = [self.recentColors lastObject];
    }
    
    [_pencilButton sendActionsForControlEvents:UIControlEventTouchUpInside];
    [self registerUndoObservers];
    [FIRAnalytics setScreenName:@"Skin_Editor" screenClass:@"Skin_Editor"];
}

//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
//    return (interfaceOrientation == UIInterfaceOrientationPortrait);
//}

- (BOOL)shouldAutorotate {
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    return (orientation == UIInterfaceOrientationPortrait);
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self resignFirstResponder];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];

    NSLog(@"recentChosenColorView.frame = %@", NSStringFromCGRect(self.recentChosenColorView.frame));
    [self layoutDrawView];
    [self layoutRecentColorGroup];
    [self updateColorsForRecentColorGroup];
}

- (void)dealloc {
    [self removeUndoObservers];
}

#pragma mark -
#pragma mark - Undo Manager

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)registerUndoObservers {
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self
               selector:@selector(handleUndoNotification:)
                   name:NSUndoManagerDidUndoChangeNotification
                 object:_undoManager];
}

- (void)removeUndoObservers {
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center removeObserver:self name:NSUndoManagerDidUndoChangeNotification object:_undoManager];
}

- (void)handleUndoNotification:(id)sender {
    
    _undoButton.enabled = self.undoManager.canUndo;
    _undoButton.userInteractionEnabled = self.undoManager.canUndo;
}

- (void)updateColorMatrix:(SEBYTEMatrix *)matrix {
    //    NSLog(@"Called update color matrix -> %p", matrix);
    [self.undoManager registerUndoWithTarget:self
                                    selector:@selector(revertToColorMatrix:)
                                      object:[matrix clonedMatrix]];
    [self handleUndoNotification:nil];
}

- (void)revertToColorMatrix:(SEBYTEMatrix *)matrix {
    NSLog(@"Called revert to color matrix -> %p", matrix);
    [_drawView.object updateColorMatrix:matrix];
    [_drawView setNeedsDisplay];
}

#pragma mark -
#pragma mark - Support Methods

- (void)addRecentColorGroup {
    
    for (UIView *view in self.recentChosenColorView.subviews) {
        [view removeFromSuperview];
    }
    
    for (NSUInteger i = 0; i < self.maximumColorCount; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [button addTarget:self
                   action:@selector(onTouchedRecentColorButton:)
         forControlEvents:UIControlEventTouchUpInside];
        
        [button makeBorderWithWidth:1 color:[UIColor colorWithHex:@"#AAAAAA" alpha:1]];
        button.tag = (i + 1);
        [self.recentChosenColorView addSubview:button];
    }
}

- (void)layoutRecentColorGroup {
    
    /* calculate number of recent color buttons */
    CGFloat width = CGRectGetWidth(self.recentChosenColorView.frame);
    CGFloat height = CGRectGetHeight(self.recentChosenColorView.frame);
    CGFloat distance = MIN(width, height);
    
    CGFloat unitSize = (distance - 2 * RecentColorPadding) / 3 ;
    NSUInteger countPerRow = width / (unitSize + RecentColorPadding);
    self.maximumColorCount = 3 * countPerRow;
    self.recentColors.maximumCount = self.maximumColorCount;
    
    /* add recent color buttons */
    [self addRecentColorGroup];
    
    /* layout recent color buttons */
    for (NSUInteger i = 0; i < self.maximumColorCount; i++) {
        
        UIButton *button = [self.recentChosenColorView viewWithTag:(i + 1)];
        
        NSUInteger column = (i % countPerRow);
        NSUInteger row = (i / countPerRow);
        
        CGFloat x = column * (unitSize + RecentColorPadding);
        CGFloat y = row * unitSize + row * RecentColorPadding;
        button.frame = (CGRect){.origin = {x, y}, .size = {unitSize, unitSize}};
    }
    
    self.recentChooseColorLeadingConstraint.constant = (width - unitSize * countPerRow - RecentColorPadding) / 2;
}

- (void)updateColorsForRecentColorGroup {
    
    for (NSUInteger i = 0; i < self.maximumColorCount; i++) {
        UIButton *button = [self.recentChosenColorView viewWithTag:(i + 1)];
        
        UIColor *color = nil;
        if (i < self.recentColors.count) {
            color = self.recentColors[i];
        }
        else {
            color = [UIColor clearColor];
        }
        [button setBackgroundColor:SEColor(color)];
    }
}

- (void)layoutDrawView {
    
    CGRect rect = CGRectInset(self.drawViewContainer.bounds, 20, 10);
    MatrixDimension matrixDimension = self.bodyPartObject.colorMatrix.dimension;
    
    /* update unit rect */
    CGRect unitRect = [SEDrawView calculateUnitRectWithRect:rect colorMatrix:self.bodyPartObject.colorMatrix];
    CGFloat unitEdgeSize = CGRectGetWidth(unitRect);
    CGFloat width = matrixDimension.columnSize * unitEdgeSize;
    CGFloat height = matrixDimension.rowSize * unitEdgeSize;
    CGRect drawRect = (CGRect){.origin = {0, 0}, .size = {width, height}};
    
    self.drawView.frame = drawRect;
    self.drawView.center = self.drawViewContainer.center;
    self.backgroundView.frame = self.drawView.frame;
}

#pragma mark -
#pragma mark - Events

- (void)setSelectedColor:(UIColor *)color {
    self.color = color;
    [self updateSelectedColor:color andAddToRecentColorGroup:YES];
}

- (IBAction)onTouchedColorChangedButton:(id)sender {
    
    NSDictionary *arguments = @{@"delegate": self, @"color": self.color};
    [self pushViewControllerWithStoryID:@"HRSampleColorPickerViewController"
                           inStoryBoard:EditorStoryBoard
                              arguments:arguments
                               animated:YES];
}

- (IBAction)onTouchedCameraButton:(id)sender {
    
    QBImagePickerController *imagePickerController = [QBImagePickerController new];
    imagePickerController.delegate = self;
    imagePickerController.allowsMultipleSelection = NO;
    imagePickerController.maximumNumberOfSelection = 1;
    imagePickerController.showsNumberOfSelectedAssets = YES;
    
    [self presentViewController:imagePickerController animated:YES completion:NULL];
}

- (IBAction)onTouchedToolBarButton:(id)sender {
    
    NSUInteger tag = [sender tag];
    if (tag != EditorToolBarButtonTypeNose) {
        for (UIButton *button in _toolBarButtons) {
            button.selected = NO;
        }
        [sender setSelected: YES];
    }
    actionType = tag;
    
    /* update color */
    if (actionType == EditorToolBarButtonTypeEraser) {
        self.drawView.fillColor = [UIColor clearColor];
    }
    else {
        self.drawView.fillColor = self.color;
    }
}

- (void)onTouchedRecentColorButton:(UIButton *)sender {
    if (sender.tag > self.recentColors.count) return;
    
    [self updateSelectedColor:[sender backgroundColor] andAddToRecentColorGroup:NO];
}

- (void)onTouchBackButton:(id)sender {
    
    if (_isModified) {
        AlertActionCompleteBlock completeBlock = ^(NSUInteger buttonIndex) {
            
            if (buttonIndex == 0) {
                [super onTouchBackButton:sender];
            }
            else if (buttonIndex == 1) {
                [self.bodyPartObject updateColorMatrix:self.originalMatrix];
                [super onTouchBackButton:sender];
            }
        };
        
        [UIAlertController showConfirmPopUpWithTitle:nil
                                             message:@"Save changes"
                                        buttonTitles:@[@"Yes", @"No", @"Cancel"]
                                       completeBlock:completeBlock];
    }
    else {
        [super onTouchBackButton:sender];
    }
}

#pragma mark -
#pragma mark - SEDrawViewDelegate

- (void)drawView:(SEDrawView *)drawView didPickColor:(UIColor *)color {
    [self setSelectedColor:color];
    [_pencilButton sendActionsForControlEvents:UIControlEventTouchUpInside];
}

- (EditorToolBarButtonType)actionForDrawView:(SEDrawView *)drawView {
    return self.actionType;
}

- (void)didChangedOnDrawView:(SEDrawView *)drawView {
    [self updateColorMatrix:drawView.object.colorMatrix];
    drawView.object.isModified = YES;
    self.isModified = YES;
}

#pragma mark -
#pragma mark - QBImagePickerControllerDelegate

- (void)qb_imagePickerControllerDidCancel:(QBImagePickerController *)imagePickerController {
    [imagePickerController dismissViewControllerAnimated:YES completion:nil];
}

- (void)qb_imagePickerController:(QBImagePickerController *)imagePickerController didSelectAsset:(PHAsset *)asset {
    
}

- (void)qb_imagePickerController:(QBImagePickerController *)imagePickerController didFinishPickingAssets:(NSArray *)assets {
    
    if (assets.count > 0) {
        
        id completeHandler = ^(id  _Nonnull data, NSDictionary * _Nonnull info) {
            
            if (data) {
                UIImage *image = [UIImage imageWithData:data];
                [self.bodyPartObject parseImagePixelsToBodyPart:image];
                [self.drawView setNeedsDisplay];
            }
        };
        
        [[MKPhotoAccessor sharedAccessor] requestImageDataForAsset:assets.firstObject
                                                     completeBlock:completeHandler];
    }
    
    [imagePickerController dismissViewControllerAnimated:YES completion:nil];
}

@end
