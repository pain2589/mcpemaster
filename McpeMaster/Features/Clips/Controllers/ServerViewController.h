//
//  ServerViewController.h
//  McpeMaster
//
//  Created by kaka on 2/5/17.
//
//

#import <UIKit/UIKit.h>
#import "ADUFileManager.h"
#import "AFNetworking.h"
#import "MKRealmManager.h"
#import "RLMServerObject.h"
#import "SEServerObject+Realm.h"
#import "UIAlertController+Utils.h"
#import "GRKGradientView.h"

@interface ServerViewController : UIViewController

@property(weak, nonatomic) IBOutlet UIImageView *thumbImg;
@property(weak, nonatomic) IBOutlet UIButton *btnCopyIp;
@property(weak, nonatomic) IBOutlet UIButton *btnCopyPort;
@property(weak, nonatomic) IBOutlet UIButton *btnFavorite;
@property(weak, nonatomic) IBOutlet UILabel *lbServerInfor;
@property(weak, nonatomic) IBOutlet UITextView *textViewDescription;
@property(weak, nonatomic) IBOutlet GRKGradientView *gradientView;
@property(weak, nonatomic) IBOutlet UILabel *statusLabel;
@property(weak, nonatomic) IBOutlet UILabel *playersLabel;

@property(strong, nonatomic) NSString *mcpeId;
@property(strong, nonatomic) NSString *strTitle;
@property(strong, nonatomic) NSString *strServerPort;
@property(strong, nonatomic) NSString *strServerIP;
@property(strong, nonatomic) NSString *strServerDescription;
@property (nonatomic, strong) NSString *serverMaxPlayers;
@property (nonatomic, strong) NSString *serverPlayers;
@property(strong, nonatomic) NSString *strThumbUrl;

@property (nonatomic, strong) SEServerObject *serverObject;
@property (nonatomic, strong) RLMServerObject *realmServerObject;

@end
