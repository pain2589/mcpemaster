//
//  AppDelegate.h
//  McpeMaster
//
//  Created by Khoai Nguyen on 12/22/14.
//  Copyright (c) 2014 Khoai Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "OpenGLView.h"
#import "Reachability.h"

#import "MKRealmManager.h"
#import "RLMMapObject.h"
#import "SEMapObject+Realm.h"
#import "RLMAddonObject.h"
#import "SEAddonObject+Realm.h"
#import "RLMTextureObject.h"
#import "SETextureObject+Realm.h"
#import "RLMSeedObject.h"
#import "SESeedObject+Realm.h"
#import "RLMServerObject.h"
#import "SEServerObject+Realm.h"
#import "RLMSkinOnlineObject.h"
#import "SESkinOnlineObject+Realm.h"

#import "MCPEMaster-Swift.h"

@import GoogleMobileAds;

#define PRAppDelegate (AppDelegate *)[UIApplication sharedApplication].delegate

@class RootViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate,GADInterstitialDelegate>

@property (strong, nonatomic) GADInterstitial *googleInterstitialView;
@property(nonatomic, assign) BOOL isWillShowInterstitial;

@property(strong, nonatomic) UIWindow *window;
@property(nonatomic, readonly) RootViewController *rootViewController;

@property (strong, nonatomic) OpenGLView *glView;

@property (strong, nonatomic) StoreService *storeService;

@property(nonatomic, assign) int mcpeCode;
@property(nonatomic, assign) int mcpeType;
@property(nonatomic, assign) int mcpeNum;
@property(nonatomic, assign) int isConsent;
@property (nonatomic, strong) NSString *mcpeMess;
@property (nonatomic, strong) NSString *mcpeTitle;
@property (nonatomic, strong) NSString *mcpeOther;
@property (nonatomic, strong) NSString *majorVersion;

@property (nonatomic, strong) NSString *promoteAppUrl;
@property (nonatomic, strong) NSString *promoteAppMess;
@property (nonatomic, strong) NSString *currentVersion;

@property (nonatomic, strong) NSString *mcpeSearchKeyword;
@property (nonatomic, strong) NSMutableArray *skinCategories;
@property (nonatomic, strong) NSMutableArray *mapCategories;
@property (nonatomic, strong) NSMutableArray *addonCategories;
@property (nonatomic, strong) NSMutableArray *textureCategories;
@property (nonatomic, strong) NSMutableArray *serverCategories;
@property (nonatomic, strong) NSMutableArray *seedCategories;

@property(nonatomic, assign) BOOL isFavoriteView;
@property(nonatomic, assign) BOOL isFinishGetInfo;
@property(nonatomic, assign) int countClick;
@property(nonatomic, assign) int numOfClickToShowAds;

+ (AppDelegate *)sharedDelegate;

- (void)loadInterstitial;
- (BOOL)connected;
- (void)setMcpeSearchKeyword:(NSString *)mcpeSearchKeyword;
- (void)setIsFavoriteView:(BOOL)isFavoriteView;
- (GADInterstitial *)createAndLoadInterstitial;
- (void)syncFavoriteToDB:(NSString*)userId;
- (void)loginFBRegister;

@end

