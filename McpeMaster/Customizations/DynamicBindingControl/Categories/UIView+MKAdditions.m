/*============================================================================
 PROJECT: GetThisNotThat
 FILE:    UIView+MKAdditions.m
 AUTHOR:  Khoai Nguyen
 DATE:    3/9/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "UIView+MKAdditions.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@implementation UIView (MKAdditions)

- (UIView *)childViewIsKindOfClass:(Class)className {

    for (UIView *subview in self.subviews) {
        if ([subview isKindOfClass:className]) {
            return subview;
        }
    }

    for (UIView *subview in self.subviews) {
        UIView *targetView = [subview childViewIsKindOfClass:className];
        if (targetView) {
            return targetView;
        }
    }

    return nil;
}

@end
