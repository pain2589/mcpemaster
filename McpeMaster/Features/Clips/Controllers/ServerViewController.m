//
//  ServerViewController.m
//  McpeMaster
//
//  Created by kaka on 2/5/17.
//
//

#import "ServerViewController.h"
#import "Firebase.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface ServerViewController ()

@end

@implementation ServerViewController {
    BOOL hasJustLoginFB;
    BOOL loginErr;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (![PRAppDelegate isFavoriteView]){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSString *urlString= [NSString stringWithFormat:@"https://mcpemaster.co/mcpehub/counterMcpe.php?type=server&id=%@",self->_mcpeId];
            NSString *urlUTF8 = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
            NSURL *url = [NSURL URLWithString:urlUTF8];
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            NSURLResponse *response;
            NSError *err = nil;
            [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
            dispatch_async(dispatch_get_main_queue(), ^(void){
                if (err) {
                    NSLog(@"ERROR --> %@", err.localizedDescription);
                }else{
                    NSLog(@"Counter Succeffully id = %@",self->_mcpeId);
                }
            });
        });
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (loginErr) {
        loginErr = NO;
        AlertActionCompleteBlock completeBlock = ^(NSUInteger buttonIndex) {
            if (buttonIndex == 1) {
                [self fbLogin];
            }
        };
        [UIAlertController showConfirmPopUpWithTitle:nil
                                             message:@"Login Failed. Please try again!"
                                        buttonTitles:@[@"Cancel", @"Login"]
                                       completeBlock:completeBlock];
    }
    
    if (hasJustLoginFB) {
        self.realmServerObject = [self fetchServerFromDB];
        [self updateUI];
        hasJustLoginFB = NO;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //
    [self.view setBackgroundColor:UIColorFromRGB(0xE6E6E6)];
    [self.textViewDescription setBackgroundColor:UIColorFromRGB(0xE6E6E6)];
    /* setup gradient view */
    self.gradientView.gradientOrientation = GRKGradientOrientationDown;
    self.gradientView.gradientColors = [NSArray arrayWithObjects:[UIColor clearColor], [UIColor blackColor], nil];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *urlString= [NSString stringWithFormat:@"https://mcpemaster.co/mcpehub/getServerStatus.php?serverIP=%@&serverPort=%@",self->_strServerIP,self->_strServerPort];
        NSString *urlUTF8 = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSURL *url = [NSURL URLWithString:urlUTF8];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSURLResponse *response;
        NSError *err = nil;
        NSData *dataResponse = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            if (err) {
                NSLog(@"ERROR --> %@", err.localizedDescription);
            }else{
                NSError *error = nil;
                NSDictionary *serverStatusDic = [NSJSONSerialization JSONObjectWithData:dataResponse options:NSJSONReadingAllowFragments error:&error];
                if (error) {
                    NSLog(@"ERROR --> %@", error.localizedDescription);
                    self.statusLabel.text = @"Offline";
                    [self.statusLabel setTextColor:[UIColor redColor]];
                }else {
                    if (serverStatusDic[kMcpeServerMaxPlayers] && ![serverStatusDic[kMcpeServerMaxPlayers] isKindOfClass:[NSNull class]]) {
                        self->_serverMaxPlayers = serverStatusDic[kMcpeServerMaxPlayers];
                    }else{
                        self->_serverMaxPlayers = @"";
                    }
                    if (serverStatusDic[kMcpeServerPlayers] && ![serverStatusDic[kMcpeServerPlayers] isKindOfClass:[NSNull class]]) {
                        self->_serverPlayers = serverStatusDic[kMcpeServerPlayers];
                    }else{
                        self->_serverPlayers = @"";
                    }
                    
                    if (serverStatusDic[kMcpeServerErr] && ![serverStatusDic[kMcpeServerErr] isKindOfClass:[NSNull class]]) {
                        self.statusLabel.text = @"Offline";
                        [self.statusLabel setTextColor:[UIColor redColor]];
                    }else{
                        self.statusLabel.text = @"Online";
                        [self.statusLabel setTextColor:[UIColor greenColor]];
                        
                        if (![self->_serverMaxPlayers isEqualToString:@""] && ![self->_serverPlayers isEqualToString:@""]) {
                            self.playersLabel.text = [NSString stringWithFormat:@"%@/%@",self->_serverPlayers,self->_serverMaxPlayers];
                        }
                    }
                }
            }
        });
    });
    //
    self.title = _strTitle;
    self.lbServerInfor.text = [NSString stringWithFormat:@"%@:%@",_strServerIP,_strServerPort];
    
    if (_strServerDescription == nil){
        _strServerDescription = @"";
    }
    NSData *data = [[NSData alloc] initWithBase64EncodedString:_strServerDescription options:0];
    NSString *htmlString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSAttributedString *attributedString = [[NSAttributedString alloc]
                                                initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                                options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                documentAttributes: nil
                                                error: nil
                                                ];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            self.textViewDescription.attributedText = attributedString;
        });
    });
    
    //Add BarButton
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithImage:[IBHelper loadImage:@"icon_back"]
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(didTouchOnBackButton:)];
    self.navigationItem.leftBarButtonItem = leftButton;
    
    //
    if (_strThumbUrl) {
        [self.thumbImg sd_setImageWithURL:[NSURL URLWithString:_strThumbUrl]
                               placeholderImage:[UIImage imageNamed:@"store_background"]
                                      completed:^(UIImage *img, NSError *err,SDImageCacheType type,NSURL *imgurl){
                                          
                                      }];
    }
    
    self.realmServerObject = [self fetchServerFromDB];
    //
    [self updateUI];
    
    //
    [(BaseNavigationViewController*)self.navigationController hideAdmobBanner];
    
    //
    [FIRAnalytics setScreenName:@"Server_View" screenClass:@"Server_View"];
}

- (void)updateUI{
    //
    if ([self isServerFavorite]){
        [self.btnFavorite setTitle:@"Remove Favorite" forState:UIControlStateNormal];
    }else{
        [self.btnFavorite setTitle:@"Add Favorite" forState:UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)didTouchOnBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

//
- (IBAction)copyIP:(id)sender{
   [UIPasteboard generalPasteboard].string = _strServerIP;
    
    [UIAlertController showConfirmPopUpWithTitle:@"Copy IP Successfully!"
                                         message:@"1. Open Minecraft App\n2. Enter Server IP\n3. Enter Sever Port\n4. Connect & Enjoy it :)"
                                    buttonTitles:@[@"OK"]
                                   completeBlock:nil];
}

- (IBAction)copyPort:(id)sender{
    [UIPasteboard generalPasteboard].string = _strServerPort;
    
    [UIAlertController showConfirmPopUpWithTitle:@"Copy Port Successfully!"
                                         message:@"1. Open Minecraft App\n2. Enter Server IP\n3. Enter Sever Port\n4. Connect & Enjoy it :)"
                                    buttonTitles:@[@"OK"]
                                   completeBlock:nil];
}

//
- (IBAction)onFavoriteButton:(id)sender {
    if ([FBSDKAccessToken currentAccessToken]) {
        // User is logged in
        [FBSDKProfile loadCurrentProfileWithCompletion:
         ^(FBSDKProfile *profile, NSError *error) {
             if (profile) {
                 [self addFavorite];
                 if ([self isServerFavorite]) {
                     [self syncFavorite:profile.userID action:@"add"];
                 } else {
                     [self syncFavorite:profile.userID action:@"remove"];
                 }
             }
         }];
    } else {
        AlertActionCompleteBlock completeBlock = ^(NSUInteger buttonIndex) {
            if (buttonIndex == 1) {
                [self fbLogin];
            }
        };
        
        [UIAlertController showConfirmPopUpWithTitle:nil
                                             message:@"Please login Facebook to use Favorite function!"
                                        buttonTitles:@[@"Cancel", @"Login"]
                                       completeBlock:completeBlock];
    }
}

- (void)addFavorite {
    self.realmServerObject = [self fetchServerFromDB];
    if (self.realmServerObject){
        [MKRealmManager transactionWithBlock:^(RLMRealm *realm) {
            RLMServerObject *object = [self.serverObject realmObject];
            object.mcpeId = self.realmServerObject.mcpeId;
            object.name = self.realmServerObject.name;
            object.thumb = self.realmServerObject.thumb;
            object.link = self.realmServerObject.link;
            object.serverIP = self.realmServerObject.serverIP;
            object.serverPort = self.realmServerObject.serverPort;
            object.serverDescription = self.realmServerObject.serverDescription;
            /* add new one */
            if (object) {
                if ([self isServerFavorite]){
                    object.favorited = NO;
                }else{
                    object.favorited = YES;
                }
            }
            
            object.logTime = self.realmServerObject.logTime;
            [realm deleteObject:(RLMObject *)self.realmServerObject];
            
            [realm addObject:(RLMObject *)object];
            self.realmServerObject = object;
            [self updateUI];
        }];
    }else{
        [MKRealmManager transactionWithBlock:^(RLMRealm *realm) {
            RLMServerObject *object = [self.serverObject realmObject];
            object.mcpeId = self->_mcpeId;
            object.name = self->_strTitle;
            object.thumb = self->_strThumbUrl;
            object.link = [NSURL URLWithString:self->_strThumbUrl].lastPathComponent.stringByDeletingPathExtension;
            object.serverIP = self->_strServerIP;
            object.serverPort = self->_strServerPort;
            object.serverDescription = self->_strServerDescription;
            /* add new one */
            if (object) {
                if ([self isServerFavorite]){
                    object.favorited = NO;
                }else{
                    object.favorited = YES;
                }
            }
            
            object.logTime = self.realmServerObject.logTime;
            
            [realm addObject:(RLMObject *)object];
            self.realmServerObject = object;
            [self updateUI];
        }];
    }
}

- (void)syncFavorite:(NSString*)userId action:(NSString*)action {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *urlString= [NSString stringWithFormat:@"https://mcpemaster.co/mcpehub/addFavoriteMcpe.php?type=server&userId=%@&id=%@&action=%@",userId,self->_mcpeId,action];
        NSString *urlUTF8 = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSURL *url = [NSURL URLWithString:urlUTF8];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSURLResponse *response;
        NSError *err = nil;
        [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            if (err) {
                NSLog(@"ERROR --> %@", err.localizedDescription);
            }else{
                NSLog(@"Report Map Succeffully id = %@",self->_mcpeId);
            }
        });
    });
}

- (void)fbLogin {
    FBSDKLoginManager *fbLogin = [[FBSDKLoginManager alloc] init];
    [fbLogin logInWithReadPermissions:@[@"email"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error != nil) {
            [fbLogin logOut];
            self->loginErr = YES;
        } else if (result.isCancelled) {
            //cancelled
            [fbLogin logOut];
        } else {
            //success
            self->hasJustLoginFB = YES;
        }
    }];
}

#pragma mark -
#pragma mark - DB Support

- (SEServerObject *)serverObject {
    
    if (!_serverObject) {
        _serverObject = [SEServerObject newObject];
    }
    return _serverObject;
}

- (BOOL)isServerFavorite{
    return self.realmServerObject.favorited;
}

- (RLMServerObject*)fetchServerFromDB {
    RLMResults<RLMServerObject *> *objects = [[RLMServerObject objectsWhere:[NSString stringWithFormat:@"link = '%@'",[NSURL URLWithString:_strThumbUrl].lastPathComponent.stringByDeletingPathExtension]] sortedResultsUsingProperty:@"logTime" ascending:NO];
    return objects.firstObject;
}

@end
