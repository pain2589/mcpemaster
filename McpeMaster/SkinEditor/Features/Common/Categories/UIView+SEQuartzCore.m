/*============================================================================
 PROJECT: SkinEditor
 FILE:    UIView+SEQuartzCore.m
 AUTHOR:  Khoai Nguyen
 DATE:    10/17/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "UIView+SEQuartzCore.h"
#import "UIColor+Extensions.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@implementation UIView (SEQuartzCore)

- (void)makeDefaultGrayBorder {
     [self makeBorderWithWidth:1 color:[UIColor colorWithHex:@"#AAAAAA" alpha:1]];
}

- (void)makeDefaultGreenBorder {
    [self makeBorderWithWidth:1 color:[UIColor colorWithHex:@"#A52A2A" alpha:1]];
//    [self makeBorderWithWidth:1 color:[UIColor colorWithHex:@"#4ed2af" alpha:1]];
}

@end
