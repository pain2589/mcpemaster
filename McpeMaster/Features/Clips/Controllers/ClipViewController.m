/*============================================================================
 PROJECT: McpeMaster
 FILE:    ClipViewController.m
 AUTHOR:  Khoai Nguyen
 DATE:    12/23/14
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "ClipViewController.h"
#import "PRIndicatorViewDelegate.h"
#import "MKCollectionViewControllerProtocols.h"
#import "PRListViewConfiguration.h"
#import "PRListViewDataSource.h"
#import "PRListViewLogicDelegate.h"
#import "PRNavigationBarDelegate.h"
#import "iRate.h"
#import "Firebase.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@implementation ClipViewController{
    UIBarButtonItem *topButton;
    UIBarButtonItem *latestButton;
    UIBarButtonItem *favoriteButton;
    UIBarButtonItem *searchButton;
    UIBarButtonItem *doneButton;
    UISearchBar *mcpeSearchBar;
    NSMutableArray *categoriesArr;
    UITableView *tblList;
    double keyboardHeight;
}

- (id)initSetupNavigationDelegate {

//    __weak typeof(self) weakSelf = self;
    return (id) [[PRClipNavigationBarDelegate alloc] initWithOwnViewController:self callBackHandler:^BOOL(NSUInteger barItemIndex) {

        if (barItemIndex == PRNavigationBarTypePickCountry) {
        }
        return YES;
    }];
}

- (void)setSelectedCategory:(PRServiceCategory *)selectedCategory {
    [(PRVideoListViewLogicDelegate *) self.logics setSelectedCategory:selectedCategory];
}

- (PRServiceCategory *)selectedCategory {
    return [(PRVideoListViewLogicDelegate *) self.logics selectedCategory];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setupUI];
    [self setUpAlert];
}

- (void)setupUI {
    //Tiep Preloading FIRST interstitial at the FIRST logical break point
//    if (!UDBool4Key(kIsPro)) {
        [PRAppDelegate setGoogleInterstitialView:[PRAppDelegate createAndLoadInterstitial]];
//    }
    
    __weak typeof(self) weakSelf = self;
    
    //Disable check country code
    [self refreshTitleView];
    [weakSelf.logics reloadDataInCollectionViewController:weakSelf];
    
    [self setUpNavBar];
    
//    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
//    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    
    //
    categoriesArr = [[NSMutableArray alloc] init];
    if ([self.selectedCategory.value isEqualToString:@"TopMap"] ||
        [self.selectedCategory.value isEqualToString:@"LatestMap"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteMap"] ||
        [self.selectedCategory.value isEqualToString:@"SearchMap"]){
        if ([PRAppDelegate mapCategories].count > 0){
            categoriesArr = [[PRAppDelegate mapCategories] copy];
        }
    }
    if ([self.selectedCategory.value isEqualToString:@"TopAddon"] ||
        [self.selectedCategory.value isEqualToString:@"LatestAddon"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteAddon"] ||
        [self.selectedCategory.value isEqualToString:@"SearchAddon"]){
        if ([PRAppDelegate addonCategories].count > 0){
            categoriesArr = [[PRAppDelegate addonCategories] copy];
        }
    }
    if ([self.selectedCategory.value isEqualToString:@"TopTexture"] ||
        [self.selectedCategory.value isEqualToString:@"LatestTexture"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteTexture"] ||
        [self.selectedCategory.value isEqualToString:@"SearchTexture"]){
        if ([PRAppDelegate textureCategories].count > 0){
            categoriesArr = [[PRAppDelegate textureCategories] copy];
        }
    }
    if ([self.selectedCategory.value isEqualToString:@"TopServer"] ||
        [self.selectedCategory.value isEqualToString:@"LatestServer"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteServer"] ||
        [self.selectedCategory.value isEqualToString:@"SearchServer"]){
        if ([PRAppDelegate serverCategories].count > 0){
            categoriesArr = [[PRAppDelegate serverCategories] copy];
        }
    }
    if ([self.selectedCategory.value isEqualToString:@"TopSeed"] ||
        [self.selectedCategory.value isEqualToString:@"LatestSeed"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteSeed"] ||
        [self.selectedCategory.value isEqualToString:@"SearchSeed"]){
        if ([PRAppDelegate seedCategories].count > 0){
            categoriesArr = [[PRAppDelegate seedCategories] copy];
        }
    }
    if ([self.selectedCategory.value isEqualToString:@"TopSkin"] ||
        [self.selectedCategory.value isEqualToString:@"LatestSkin"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteSkin"] ||
        [self.selectedCategory.value isEqualToString:@"SearchSkin"]){
        if ([PRAppDelegate skinCategories].count > 0){
            categoriesArr = [[PRAppDelegate skinCategories] copy];
        }
    }
    
    if (categoriesArr.count > 0){
        tblList = [[UITableView alloc]init];
        [tblList setDelegate:self];
        [tblList setDataSource:self];
        [self.view addSubview:tblList];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChange:) name:UIKeyboardWillChangeFrameNotification object:nil];
    [FIRAnalytics setScreenName:self.selectedCategory.value screenClass:self.selectedCategory.value];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return categoriesArr.count;
}


-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=[[UITableViewCell alloc]init];
    [cell setBackgroundColor:[UIColor whiteColor]];
    cell.textLabel.textColor=[UIColor blackColor];
    cell.textLabel.text=[categoriesArr objectAtIndex:indexPath.row];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell *cell=[tblList cellForRowAtIndexPath:indexPath];
    
    mcpeSearchBar.text = cell.textLabel.text;
}


-(void)collapseTableView
{
    CGRect frame = self.collectionView.frame;
    [tblList setFrame:CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height - keyboardHeight)];
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionTransitionNone
                     animations:^{
                         [self->tblList setFrame:CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, 0)];
                     }
                     completion:^(BOOL finished){
                         NSLog(@"collapsed");
                     }];
    
}

-(void)expandTableView
{
    CGRect frame = self.collectionView.frame;
    [tblList setFrame:CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, 0)];
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionTransitionNone
                     animations:^{
                         [self->tblList setFrame:CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height - self->keyboardHeight)];
                     }
                     completion:^(BOOL finished){
                        NSLog(@"expanded");
                     }];
}

- (void)keyboardWillChange:(NSNotification *)notification {
    CGRect keyboardRect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    keyboardHeight = keyboardRect.size.height;
}

- (void)setUpNavBar{
    //Add BarButton
    topButton = [[UIBarButtonItem alloc] initWithTitle:@"Top"
                                                 style:UIBarButtonItemStylePlain
                                                target:self
                                                action:@selector(loadTop)];
    topButton.tintColor = [UIColor darkTextColor];
    [topButton setTitleTextAttributes:@{NSFontAttributeName: FONT_GEOMETRIA(18)} forState:UIControlStateNormal];
    
    latestButton = [[UIBarButtonItem alloc] initWithTitle:@"Latest"
                                                    style:UIBarButtonItemStylePlain
                                                   target:self
                                                   action:@selector(loadLatest)];
    latestButton.tintColor = [UIColor darkTextColor];
    [latestButton setTitleTextAttributes:@{NSFontAttributeName: FONT_GEOMETRIA(18)} forState:UIControlStateNormal];
    
    favoriteButton = [[UIBarButtonItem alloc] initWithTitle:@"Favorites"
                                                      style:UIBarButtonItemStylePlain
                                                     target:self
                                                     action:@selector(loadFavorite)];
    favoriteButton.tintColor = [UIColor darkTextColor];
    [favoriteButton setTitleTextAttributes:@{NSFontAttributeName: FONT_GEOMETRIA(18)} forState:UIControlStateNormal];
    
    searchButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_search_unselected"]
                                                    style:UIBarButtonItemStylePlain
                                                   target:self
                                                   action:@selector(onTouchedSearchButton)];
    
    //
    doneButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                              target:self
                                                              action:@selector(cancelButtonPressed)];
    
    mcpeSearchBar = [[UISearchBar alloc] initWithFrame:self.navigationController.view.frame];
    mcpeSearchBar.delegate = self;
    mcpeSearchBar.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    //
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                          target:nil
                                                                          action:nil];
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    
    CGFloat searchBtnWidth = searchButton.image.size.width;
    CGFloat favoriteBtnWidth = [favoriteButton.title sizeWithAttributes:@{NSFontAttributeName: FONT_GEOMETRIA(18)}].width;
    CGFloat latestBtnWidth = [latestButton.title sizeWithAttributes:@{NSFontAttributeName: FONT_GEOMETRIA(18)}].width;
    CGFloat topBtnWidth = [topButton.title sizeWithAttributes:@{NSFontAttributeName: FONT_GEOMETRIA(18)}].width;
    
    UIView *leftBtnView = [self.navigationItem.leftBarButtonItem valueForKey:@"view"];
    CGFloat leftBtnWidth = leftBtnView? [leftBtnView frame].size.width : (CGFloat)0.0;
    
    CGFloat sumUIBarButtonsWidth = leftBtnWidth + latestBtnWidth + topBtnWidth + favoriteBtnWidth + searchBtnWidth + 100;
    
    fixedItem.width = (UIScreen.mainScreen.bounds.size.width - sumUIBarButtonsWidth)/2;
    
    if ([self.selectedCategory.value isEqualToString:@"TopMap"] ||
        [self.selectedCategory.value isEqualToString:@"TopSkin"] ||
        [self.selectedCategory.value isEqualToString:@"TopAddon"] ||
        [self.selectedCategory.value isEqualToString:@"TopTexture"] ||
        [self.selectedCategory.value isEqualToString:@"TopServer"] ||
        [self.selectedCategory.value isEqualToString:@"TopSeed"]){
        topButton.tintColor = [UIColor redColor];
        searchButton.tintColor = [UIColor blackColor];
    }
    
    if ([self.selectedCategory.value isEqualToString:@"LatestMap"] ||
        [self.selectedCategory.value isEqualToString:@"LatestSkin"] ||
        [self.selectedCategory.value isEqualToString:@"LatestAddon"] ||
        [self.selectedCategory.value isEqualToString:@"LatestTexture"] ||
        [self.selectedCategory.value isEqualToString:@"LatestServer"] ||
        [self.selectedCategory.value isEqualToString:@"LatestSeed"]){
        latestButton.tintColor = [UIColor redColor];
        searchButton.tintColor = [UIColor blackColor];
    }
    
    if ([self.selectedCategory.value isEqualToString:@"FavoriteMap"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteSkin"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteAddon"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteTexture"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteServer"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteSeed"]){
        favoriteButton.tintColor = [UIColor redColor];
        searchButton.tintColor = [UIColor blackColor];
    }
    self.navigationItem.rightBarButtonItems = @[searchButton,fixedItem,favoriteButton,item,topButton,item,latestButton];
}

- (void)loadTop{
    PRServiceCategory *category;
    if ([self.selectedCategory.value isEqualToString:@"TopMap"] ||
        [self.selectedCategory.value isEqualToString:@"LatestMap"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteMap"] ||
        [self.selectedCategory.value isEqualToString:@"SearchMap"]){
        category = [PRGlobalUtils mapCategories][0];
    }
    if ([self.selectedCategory.value isEqualToString:@"TopAddon"] ||
        [self.selectedCategory.value isEqualToString:@"LatestAddon"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteAddon"] ||
        [self.selectedCategory.value isEqualToString:@"SearchAddon"]){
        category = [PRGlobalUtils addonCategories][0];
    }
    if ([self.selectedCategory.value isEqualToString:@"TopTexture"] ||
        [self.selectedCategory.value isEqualToString:@"LatestTexture"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteTexture"] ||
        [self.selectedCategory.value isEqualToString:@"SearchTexture"]){
        category = [PRGlobalUtils textureCategories][0];
    }
    if ([self.selectedCategory.value isEqualToString:@"TopServer"] ||
        [self.selectedCategory.value isEqualToString:@"LatestServer"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteServer"] ||
        [self.selectedCategory.value isEqualToString:@"SearchServer"]){
        category = [PRGlobalUtils serverCategories][0];
    }
    if ([self.selectedCategory.value isEqualToString:@"TopSeed"] ||
        [self.selectedCategory.value isEqualToString:@"LatestSeed"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteSeed"] ||
        [self.selectedCategory.value isEqualToString:@"SearchSeed"]){
        category = [PRGlobalUtils seedCategories][0];
    }
    if ([self.selectedCategory.value isEqualToString:@"TopSkin"] ||
        [self.selectedCategory.value isEqualToString:@"LatestSkin"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteSkin"] ||
        [self.selectedCategory.value isEqualToString:@"SearchSkin"]){
        category = [PRGlobalUtils skinCategories][1];
    }
    
    [[PRAppDelegate rootViewController] showClipViewControllerWithSelectedCategory:category isFavoriteView:NO];
}

- (void)loadLatest{
    PRServiceCategory *category;
    if ([self.selectedCategory.value isEqualToString:@"TopMap"] ||
        [self.selectedCategory.value isEqualToString:@"LatestMap"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteMap"] ||
        [self.selectedCategory.value isEqualToString:@"SearchMap"]){
        category = [PRGlobalUtils mapCategories][1];
    }
    if ([self.selectedCategory.value isEqualToString:@"TopAddon"] ||
        [self.selectedCategory.value isEqualToString:@"LatestAddon"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteAddon"] ||
        [self.selectedCategory.value isEqualToString:@"SearchAddon"]){
        category = [PRGlobalUtils addonCategories][1];
    }
    if ([self.selectedCategory.value isEqualToString:@"TopTexture"] ||
        [self.selectedCategory.value isEqualToString:@"LatestTexture"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteTexture"] ||
        [self.selectedCategory.value isEqualToString:@"SearchTexture"]){
        category = [PRGlobalUtils textureCategories][1];
    }
    if ([self.selectedCategory.value isEqualToString:@"TopServer"] ||
        [self.selectedCategory.value isEqualToString:@"LatestServer"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteServer"] ||
        [self.selectedCategory.value isEqualToString:@"SearchServer"]){
        category = [PRGlobalUtils serverCategories][1];
    }
    if ([self.selectedCategory.value isEqualToString:@"TopSeed"] ||
        [self.selectedCategory.value isEqualToString:@"LatestSeed"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteSeed"] ||
        [self.selectedCategory.value isEqualToString:@"SearchSeed"]){
        category = [PRGlobalUtils seedCategories][1];
    }
    if ([self.selectedCategory.value isEqualToString:@"TopSkin"] ||
        [self.selectedCategory.value isEqualToString:@"LatestSkin"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteSkin"] ||
        [self.selectedCategory.value isEqualToString:@"SearchSkin"]){
        category = [PRGlobalUtils skinCategories][2];
    }
    [[PRAppDelegate rootViewController] showClipViewControllerWithSelectedCategory:category isFavoriteView:NO];
}

- (void)loadFavorite{
    PRServiceCategory *category;
    if ([self.selectedCategory.value isEqualToString:@"TopMap"] ||
        [self.selectedCategory.value isEqualToString:@"LatestMap"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteMap"] ||
        [self.selectedCategory.value isEqualToString:@"SearchMap"]){
        category = [PRGlobalUtils mapCategories][2];
    }
    if ([self.selectedCategory.value isEqualToString:@"TopAddon"] ||
        [self.selectedCategory.value isEqualToString:@"LatestAddon"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteAddon"] ||
        [self.selectedCategory.value isEqualToString:@"SearchAddon"]){
        category = [PRGlobalUtils addonCategories][2];
    }
    if ([self.selectedCategory.value isEqualToString:@"TopTexture"] ||
        [self.selectedCategory.value isEqualToString:@"LatestTexture"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteTexture"] ||
        [self.selectedCategory.value isEqualToString:@"SearchTexture"]){
        category = [PRGlobalUtils textureCategories][2];
    }
    if ([self.selectedCategory.value isEqualToString:@"TopServer"] ||
        [self.selectedCategory.value isEqualToString:@"LatestServer"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteServer"] ||
        [self.selectedCategory.value isEqualToString:@"SearchServer"]){
        category = [PRGlobalUtils serverCategories][2];
    }
    if ([self.selectedCategory.value isEqualToString:@"TopSeed"] ||
        [self.selectedCategory.value isEqualToString:@"LatestSeed"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteSeed"] ||
        [self.selectedCategory.value isEqualToString:@"SearchSeed"]){
        category = [PRGlobalUtils seedCategories][2];
    }
    if ([self.selectedCategory.value isEqualToString:@"TopSkin"] ||
        [self.selectedCategory.value isEqualToString:@"LatestSkin"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteSkin"] ||
        [self.selectedCategory.value isEqualToString:@"SearchSkin"]){
        category = [PRGlobalUtils skinCategories][3];
    }
    [[PRAppDelegate rootViewController] showClipViewControllerWithSelectedCategory:category isFavoriteView:YES];
}

- (void)onTouchedSearchButton{
    latestButton.tintColor = [UIColor blackColor];
    topButton.tintColor = [UIColor blackColor];
    favoriteButton.tintColor = [UIColor blackColor];
    
    self.navigationItem.titleView = mcpeSearchBar;
    self.navigationItem.rightBarButtonItems = @[doneButton];
    
    [mcpeSearchBar becomeFirstResponder];
    self.collectionView.userInteractionEnabled = NO;
    
    [self expandTableView];
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    self.collectionView.userInteractionEnabled = YES;
    PRServiceCategory *category;
    if ([self.selectedCategory.value isEqualToString:@"TopMap"] ||
        [self.selectedCategory.value isEqualToString:@"LatestMap"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteMap"] ||
        [self.selectedCategory.value isEqualToString:@"SearchMap"]){
        category = [PRGlobalUtils mapCategories][3];
    }
    if ([self.selectedCategory.value isEqualToString:@"TopAddon"] ||
        [self.selectedCategory.value isEqualToString:@"LatestAddon"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteAddon"] ||
        [self.selectedCategory.value isEqualToString:@"SearchAddon"]){
        category = [PRGlobalUtils addonCategories][3];
    }
    if ([self.selectedCategory.value isEqualToString:@"TopTexture"] ||
        [self.selectedCategory.value isEqualToString:@"LatestTexture"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteTexture"] ||
        [self.selectedCategory.value isEqualToString:@"SearchTexture"]){
        category = [PRGlobalUtils textureCategories][3];
    }
    if ([self.selectedCategory.value isEqualToString:@"TopServer"] ||
        [self.selectedCategory.value isEqualToString:@"LatestServer"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteServer"] ||
        [self.selectedCategory.value isEqualToString:@"SearchServer"]){
        category = [PRGlobalUtils serverCategories][3];
    }
    if ([self.selectedCategory.value isEqualToString:@"TopSeed"] ||
        [self.selectedCategory.value isEqualToString:@"LatestSeed"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteSeed"] ||
        [self.selectedCategory.value isEqualToString:@"SearchSeed"]){
        category = [PRGlobalUtils seedCategories][3];
    }
    if ([self.selectedCategory.value isEqualToString:@"TopSkin"] ||
        [self.selectedCategory.value isEqualToString:@"LatestSkin"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteSkin"] ||
        [self.selectedCategory.value isEqualToString:@"SearchSkin"]){
        category = [PRGlobalUtils skinCategories][4];
    }
    
    [self collapseTableView];
    [PRAppDelegate setMcpeSearchKeyword:searchBar.text];
    [[PRAppDelegate rootViewController] showClipViewControllerWithSelectedCategory:category isFavoriteView:NO];
}

- (void)cancelButtonPressed{
    self.collectionView.userInteractionEnabled = YES;
    [self collapseTableView];
    [self setUpNavBar];
    self.navigationItem.titleView = nil;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if ([self.selectedCategory.value isEqualToString:@"FavoriteMap"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteSkin"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteTexture"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteServer"] ||
        [self.selectedCategory.value isEqualToString:@"FavoriteSeed"]){
        [self pullToReload];
    }
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

#pragma mark - MKCollectionViewController Overridden

- (void)loadConfigurations {
    self.indicatorDelegate = [[PRIndicatorViewDelegate alloc] init];

    /* setup new configuration */
    PRVideoListViewLogicDelegate *searchVideoLogics = [[PRVideoListViewLogicDelegate alloc] init];
    PRListViewConfiguration *configuration = [[PRListViewConfiguration alloc] init];

    PRListVideoViewDataSource *control = [[PRListVideoViewDataSource alloc] init];
    control.logics = searchVideoLogics;
    control.configs = configuration;

    self.dataSource = control;
    self.delegate = control;
    self.logics = searchVideoLogics;
    self.configuration = configuration;
}

#pragma mark - Private

- (void)refreshTitleView {

    MKNavigationBarDelegateDecorator *decorator = self.navigationBarSetupDelegate;
    while (decorator) {

        if ([decorator hasMethod:@"refreshWithTitle:"]) {
            NSMutableArray *params = nil;
            if (self.selectedCategory) {
                params = [NSMutableArray arrayWithObject:self.selectedCategory.name];
            }
            [decorator executeMethod:@"refreshWithTitle:" withParams:params];
        }

        decorator = (MKNavigationBarDelegateDecorator *) [decorator delegate];
    }
}

- (void)setUpAlert {
    //Check app da bi xoa chua, neu bi xoa roi thi bat user down app khac cua minh
    if ([AppDelegate sharedDelegate].mcpeCode == 0) {
        AlertActionCompleteBlock completeBlock = ^(NSUInteger buttonIndex) {
            //App da bi xoa, yeu cau download app khac
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[AppDelegate sharedDelegate].mcpeTitle] options:@{} completionHandler:nil];
        };
        
        [UIAlertController showConfirmPopUpWithTitle:@"Important!"
                                             message:[AppDelegate sharedDelegate].mcpeMess
                                        buttonTitles:@[@"Go"]
                                       completeBlock:completeBlock];
    }
    
    //Check co quang cao app nao ko, neu co quang cao thi show len
    if(![[AppDelegate sharedDelegate].promoteAppMess isEqualToString:@""] &&
       ![[AppDelegate sharedDelegate].promoteAppUrl isEqualToString:@""] &&
       !UsrDfltGetBool4Key([AppDelegate sharedDelegate].promoteAppUrl)){
        
        AlertActionCompleteBlock completeBlock = ^(NSUInteger buttonIndex) {
            if (buttonIndex == 0) {
                //cancel
            } else {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[AppDelegate sharedDelegate].promoteAppUrl] options:@{} completionHandler:nil];
                UDSetBool4Key(YES, [AppDelegate sharedDelegate].promoteAppUrl);
            }
        };
        
        [UIAlertController showConfirmPopUpWithTitle:@"Important!"
                                             message:[AppDelegate sharedDelegate].promoteAppMess
                                        buttonTitles:@[@"Cancel",@"Go"]
                                       completeBlock:completeBlock];
    }
}

@end
