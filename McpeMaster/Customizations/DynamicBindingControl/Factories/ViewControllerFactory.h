//
//  FactoryViewController.h
//  BrandMate
//
//  Created by vu bui on 1/21/15.
//  Copyright (c) 2015 Nguyen Minh Khoai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define kFactoryViewControllerName              @"name"
#define kFactoryViewControllerStoryboardName    @"storyboard"

@interface ViewControllerFactory : NSObject

+ (id)sharedInstance;

- (void)initComponentsFromFile:(NSString *)file;

- (UIViewController *)controllerForName:(NSString *)name;

- (UIViewController *)controllerForName:(NSString *)name andBindDataIntoInstance:(NSDictionary *)data;

@end
