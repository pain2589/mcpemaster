/*============================================================================
 PROJECT: SkinEditor
 FILE:    MKRealmManager.m
 AUTHOR:  Nguyen Minh Khoai
 DATE:    11/10/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "MKRealmManager.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/
@interface MKRealmManager ()
@property (nonatomic, readonly) RLMRealm *realm;
@end

@implementation MKRealmManager

+ (instancetype)sharedInstance {
	static dispatch_once_t predicate;
	static MKRealmManager *instance = nil;
	dispatch_once(&predicate, ^{instance = [[self alloc] init];});
	return instance;
}
#if (!__has_feature(objc_arc))

- (id)retain {
    
    return self;
}

- (unsigned)retainCount {
    return UINT_MAX;  //denotes an object that cannot be released
}

- (oneway void)release {
    //do nothing
}

- (id)autorelease {
    
    return self;
}
#endif

- (RLMRealm *)realm {
    return [RLMRealm defaultRealm];
}

+ (void)transactionWithBlock:(RLMTransactionBlock)transactionBlock {
    RLMRealm *realm = [MKRealmManager sharedInstance].realm;
    [realm transactionWithBlock:^{ transactionBlock(realm); }];
}

@end
