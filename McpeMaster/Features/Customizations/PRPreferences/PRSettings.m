//
//  PRSettings.m
//  McpeMaster
//
//  Created by Le Nguyen Nam Phong on 10/02/2015.
//  Copyright (c) Năm 2015 Khoai Nguyen. All rights reserved.
//

#import "PRSettings.h"
/*=============================================================================*/
// Youtube Quality Keys
/*=============================================================================*/

#define kDefaultHomePage                        @"DefaultHomePage"
#define kSafeSearchSetting                      @"SafeSearchSetting"
#define kSettingPlayBack                        @"SettingPlayBack"
#define kOrientationMode                        @"OrientationMode"
#define kAutoPlayMode                           @"AutoPlayMode"
#define kFullScreenMode                         @"FullScreenMode"
#define kPlayBackMode                           @"PlayBackMode"

#define kPlaylistShuffleMode                    @"kPlaylistShuffleMode"
#define kPlaylistLoopMode                       @"kPlaylistLoopMode"

/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/
/*----------------------------------------------------------------------------
 Interface:   PRSettings
 -----------------------------------------------------------------------------*/
@implementation PRSettings

- (instancetype)init {

    self = [super init];
    if (self) {

        if (!UDIsValidKey(kSafeSearchSetting)) {
            self.safeSearch = @"none";
        }

        if (!UDIsValidKey(kSettingPlayBack)) {
            self.playBack = MKYoutubeQuality720p_MP4;
        }

        if (!UDIsValidKey(kOrientationMode)) {
            self.isLandScapeMode = YES;
        }
        
#if INCLUDE_LANDSCAPE_SETTINGS == 0
        self.isLandScapeMode = NO;
#endif
        if (!UDIsValidKey(kAutoPlayMode)) {
            self.isAutoPlay = YES;
        }

        if (!UDIsValidKey(kFullScreenMode)) {
            self.isAutoFullScreen = NO;
        }

        if (!UDIsValidKey(kPlayBackMode)) {
            self.isBackGroundPlayBack = YES;
        }
    }
    return self;
}

+ (PRSettings *)defaultSettings {
    static PRSettings *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[PRSettings alloc] init];
    });
    return instance;
}


- (void)setHomePage:(NSString *)homePage {
    UDSetValue4Key(homePage, kDefaultHomePage);
}

- (NSString *)homePage {
    return UDValue4Key(kDefaultHomePage);
}

- (void)setSafeSearch:(NSString *)safeSearch {
    UDSetValue4Key(safeSearch, kSafeSearchSetting);
}

- (NSString *)safeSearch {
    return UDValue4Key(kSafeSearchSetting);
}

- (void)setPlayBack:(NSUInteger)playBack {
    UDSetInteger4Key(playBack, kSettingPlayBack);
}

- (NSUInteger)playBack {
    return UDInteger4Key(kSettingPlayBack);
}

- (void)setIsLandScapeMode:(BOOL)isLandScapeMode {
    UDSetBool4Key(isLandScapeMode, kOrientationMode);
}

- (BOOL)isLandScapeMode {
    return UDBool4Key(kOrientationMode);
}

- (void)setIsAutoPlay:(BOOL)isAutoPlay {
    UDSetBool4Key(isAutoPlay, kAutoPlayMode);
}

- (BOOL)isAutoPlay {
    return UDBool4Key(kAutoPlayMode);
}

- (void)setIsAutoFullScreen:(BOOL)isAutoFullScreen {
    UDSetBool4Key(isAutoFullScreen, kFullScreenMode);
}

- (BOOL)isAutoFullScreen {
    return UDBool4Key(kFullScreenMode);
}

- (void)setIsBackGroundPlayBack:(BOOL)isBackGroundPlayBack {
    UDSetBool4Key(isBackGroundPlayBack, kPlayBackMode);
}

- (BOOL)isBackGroundPlayBack {
    return UDBool4Key(kPlayBackMode);
}

- (void)setPlaylistShuffleMode:(BOOL)playlistShuffleMode {
    UDSetBool4Key(playlistShuffleMode, kPlaylistShuffleMode);
}

- (BOOL)playlistShuffleMode {
    return UDBool4Key(kPlaylistShuffleMode);
}

- (void)setPlaylistLoopMode:(BOOL)playlistLoopMode {
    UDSetBool4Key(playlistLoopMode, kPlaylistLoopMode);
}

- (BOOL)playlistLoopMode {
    return UDBool4Key(kPlaylistLoopMode);
}

#pragma mark - Class Methods

- (PRServiceCategory *)category {
    //Tiep Add Default home page
//    if ([AppDelegate sharedDelegate].mcpeType == AppFlag){
        return [PRGlobalUtils objectForValue:self.homePage inArray:PRGlobalUtils.skinCategories];
//    }else{
//        return [PRGlobalUtils objectForValue:self.homePage inArray:PRGlobalUtils.mapCategories];
//    }
}

@end
