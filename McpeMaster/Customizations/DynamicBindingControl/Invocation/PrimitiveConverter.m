//
//  PrimativeConverter.m
//  Test
//
//  Created by phinguyen on 2/28/14.
//  Copyright (c) 2014 TexoDesign. All rights reserved.
//

#import "PrimitiveConverter.h"

#define CStringEquals(stringA, stringB) (stringA == stringB || strcmp(stringA, stringB) == 0)

@implementation PrimitiveConverter

+ (int)convertToInt:(NSString *)stringValue {
    return [stringValue intValue];
}

+ (short)convertToShort:(NSString *)stringValue {
    return (short) [stringValue intValue];
}

+ (long)convertToLong:(NSString *)stringValue {
    return (long) [stringValue longLongValue];
}

+ (long long)convertToLongLong:(NSString *)stringValue {
    return [stringValue longLongValue];
}

+ (unsigned char)convertToUnsignedChar:(NSString *)stringValue {
    return (unsigned char) [stringValue intValue];
}

+ (unsigned int)convertToUnsignedInt:(NSString *)stringValue {
    return (unsigned int) [stringValue longLongValue];
}

+ (unsigned short)convertToUnsignedShort:(NSString *)stringValue {
    return (unsigned short) [stringValue intValue];
}

+ (unsigned long)convertToUnsignedLong:(NSString *)stringValue {
    return (unsigned long) [stringValue longLongValue];
}

+ (unsigned long long)convertToUnsignedLongLong:(NSString *)stringValue {
    return strtoull([stringValue UTF8String], NULL, 0);
}

+ (float)convertToFloat:(NSString *)stringValue {
    return [stringValue floatValue];
}

+ (double)convertToDouble:(NSString *)stringValue {
    return [stringValue doubleValue];
}

+ (BOOL)convertToBoolean:(NSString *)stringValue {
    return [stringValue boolValue];
}

+ (const char *)convertToCString:(NSString *)stringValue {
    return [stringValue cStringUsingEncoding:NSUTF8StringEncoding];
}

+ (Class)convertToClass:(NSString *)stringValue {
    return NSClassFromString(stringValue);
}

+ (SEL)convertToSelector:(NSString *)stringValue {
    return NSSelectorFromString(stringValue);
}

+ (id)valueFromText:(NSString *)textValue withType:(const char *)type {
    id value = textValue;

    if (textValue && type) {
        if (CStringEquals(type, @encode(int))) {
            value = [NSNumber numberWithInt:[self convertToInt:textValue]];
        }
        else if (CStringEquals(type, @encode(unsigned int))) {
            value = [NSNumber numberWithUnsignedInt:[self convertToUnsignedInt:textValue]];
        }
        else if (CStringEquals(type, @encode(char))) {
            value = [NSNumber numberWithChar:[self convertToBoolean:textValue]];
        }
        else if (CStringEquals(type, @encode(unsigned char))) {
            value = [NSNumber numberWithUnsignedChar:[self convertToUnsignedChar:textValue]];
        }
        else if (CStringEquals(type, @encode(bool))) {
            value = [NSNumber numberWithBool:[self convertToBoolean:textValue]];
        }
        else if (CStringEquals(type, @encode(short))) {
            value = [NSNumber numberWithShort:[self convertToShort:textValue]];
        }
        else if (CStringEquals(type, @encode(unsigned short))) {
            value = [NSNumber numberWithUnsignedShort:[self convertToUnsignedShort:textValue]];
        }
        else if (CStringEquals(type, @encode(float))) {
            value = [NSNumber numberWithFloat:[self convertToFloat:textValue]];
        }
        else if (CStringEquals(type, @encode(double))) {
            value = [NSNumber numberWithDouble:[self convertToDouble:textValue]];
        }
        else if (CStringEquals(type, @encode(long))) {
            value = [NSNumber numberWithLong:[self convertToLong:textValue]];
        }
        else if (CStringEquals(type, @encode(unsigned long))) {
            value = [NSNumber numberWithUnsignedLong:[self convertToUnsignedLong:textValue]];
        }
        else if (CStringEquals(type, @encode(long long))) {
            value = [NSNumber numberWithLongLong:[self convertToLongLong:textValue]];
        }
        else if (CStringEquals(type, @encode(unsigned long long))) {
            value = [NSNumber numberWithUnsignedLongLong:[self convertToUnsignedLongLong:textValue]];
        }
        else if (CStringEquals(type, @encode(Class))) {
            value = [self convertToClass:textValue];
        }
    }

    return value;
}

@end
