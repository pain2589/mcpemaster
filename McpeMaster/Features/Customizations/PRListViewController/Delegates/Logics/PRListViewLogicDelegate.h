/*============================================================================
 PROJECT: McpeMaster
 FILE:    MKListViewDelegateDecorator.h
 AUTHOR:  Khoai Nguyen
 DATE:    3/16/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import <Foundation/Foundation.h>
#import "MKListViewControllerLogics.h"

/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 PROTOCOL
 =============================================================================*/
typedef void(^LoadDataCompleteBlock)(id data, id error);

/*============================================================================
 Interface:   PRListViewDelegate
 =============================================================================*/
@class MKCollectionViewController;

@interface PRListViewLogicDelegate : NSObject <MKCollectionViewControllerLogicDelegate>
@property(copy, nonatomic) NSString *nextPageToken;
@property(nonatomic, strong) NSString *errMsg;
@property(nonatomic, assign) BOOL hasNoResult;
@property(nonatomic, assign) BOOL hasNoConnection;
@property(nonatomic, assign) BOOL needToReload;

/* API Calls */
- (void)loadDataInCollectionController:(MKCollectionViewController *)controller
                         completeBlock:(LoadDataCompleteBlock)completeBlock;

@end

/*============================================================================
 Interface:   PRVideoListViewLogicDelegate
 =============================================================================*/

@interface PRVideoListViewLogicDelegate : PRListViewLogicDelegate
@property(copy, nonatomic) PRServiceCategory *selectedCategory;
@property(nonatomic, strong) NSMutableArray *favoriteMaps;
@property(nonatomic, strong) NSMutableArray *favoriteAddons;
@property(nonatomic, strong) NSMutableArray *favoriteTextures;
@property(nonatomic, strong) NSMutableArray *favoriteServers;
@property(nonatomic, strong) NSMutableArray *favoriteSeeds;
@property(nonatomic, strong) NSMutableArray *favoriteSkins;
@end

