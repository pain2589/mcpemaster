/*============================================================================
 PROJECT: McpeMaster
 FILE:    PRPreferences.h
 AUTHOR:  Khoai Nguyen
 DATE:    1/6/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import <Foundation/Foundation.h>

/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   PRPreferences
 =============================================================================*/


@interface PRPreferences : NSObject
@property(nonatomic, strong) NSMutableDictionary *options;

/* search options properties */
@property(nonatomic, strong) NSString *sortBy;
@property(nonatomic, strong) NSString *created;
@property(nonatomic, strong) NSString *duration;
@property(nonatomic, strong) NSString *onlyHD;
@property(nonatomic, strong) NSString *onlyClosedCaptioned;
@property(nonatomic, strong) NSString *onlyLiveStream;
@property(nonatomic, readonly, getter=isPreferenceChanged) BOOL optionsChanged;

+ (PRPreferences *)sharedPreferences;

+ (NSArray *)searchOptions;

/* public methods */
- (NSIndexPath *)indexPathForField:(NSString *)field atSection:(NSInteger)section;

- (void)save;

- (void)restore;

- (void)clearDirtyFlag;

@end
