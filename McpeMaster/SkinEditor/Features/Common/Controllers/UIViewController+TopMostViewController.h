/*============================================================================
 PROJECT: SkinEditor
 FILE:    UIViewController+TopMostViewController.h
 AUTHOR:  Nguyen Minh Khoai
 DATE:    11/16/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import <UIKit/UIKit.h>

/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   UIViewController_TopMostViewController
 =============================================================================*/

@interface UIViewController (TopMostViewController)
+ (UIViewController *)topViewController;
- (UIViewController *)topVisibleViewController;
@end
