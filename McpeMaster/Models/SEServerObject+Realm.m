/*============================================================================
 PROJECT: SkinEditor
 FILE:    SEMapObject+Realm.m
 AUTHOR:  Khoai Nguyen
 DATE:    11/13/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SEServerObject+Realm.h"
#import "RLMServerObject.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@implementation SEServerObject (Realm)

- (RLMServerObject *)realmObject {
    
    RLMServerObject *object = [[RLMServerObject alloc] init];
    object.type = @"server";
    object.logTime = [[NSDate date] timeIntervalSince1970];
    
    return object;
}

+ (SEServerObject *)fromRealmObject:(RLMServerObject *)realmObject {
    
    if (!realmObject) return nil;
    
    if (realmObject.name.length > 0) {
        SEServerObject *object = [SEServerObject newObject];
        return object;
    }
    
    return nil;
}

@end
