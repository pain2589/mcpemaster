/*============================================================================
 PROJECT: SkinEditor
 FILE:    SEPartViewController.h
 AUTHOR:  Nguyen Minh Khoai
 DATE:    10/19/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SEBaseViewController.h"
#import "SEDrawGridView.h"
#import "SEDescriptionPartView.h"

/*============================================================================
 MACRO
 =============================================================================*/
static NSUInteger const partCount = 8;

static CGFloat const partTitleLabelHeight = 35;
static CGFloat const topMargin = 10;
static CGFloat const leftMargin = 10;

/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   SEPartViewController
 =============================================================================*/


@interface SEPartViewController : SEBaseViewController
@property (nonatomic, weak) IBOutlet SEDrawGridView *gridView;
@property (nonatomic, readonly) CGFloat onePartSize;

- (void)layoutSubviews;

@end
