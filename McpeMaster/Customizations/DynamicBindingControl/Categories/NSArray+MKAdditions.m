/*============================================================================
 PROJECT: McpeMaster
 FILE:    NSArray+MKAdditions.m
 AUTHOR:  Khoai Nguyen
 DATE:    2/17/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "NSArray+MKAdditions.h"
#import "NSObject+Utility.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@implementation NSArray (MKAdditions)

- (id)objectForKey:(NSString *)key value:(id)value {
    __block id result = nil;
    if (self.count > 0) {
        [self enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if ([obj hasProperty:key]) {
                id localValue = [obj valueForKey:key];
                if ([value compare:localValue] == NSOrderedSame) {
                    result = obj;
                    *stop = YES;
                }
            }
        }];

        if (result == nil) {
            result = [self firstObject];
        }
    }
    return result;
}

@end
