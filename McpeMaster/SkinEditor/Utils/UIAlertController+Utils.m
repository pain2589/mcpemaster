/*============================================================================
 PROJECT: SkinEditor
 FILE:    UIAlertController+Utils.m
 AUTHOR:  Khoai Nguyen
 DATE:    10/28/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "UIAlertController+Utils.h"
#import "UIViewController+TopMostViewController.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@implementation UIAlertController (Utils)

+ (void)showConfirmPopUpWithTitle:(NSString *)title
                          message:(NSString *)message
                     buttonTitles:(NSArray<NSString *> *)buttonTitles
                    completeBlock:(AlertActionCompleteBlock)completeBlock {
    
    UIViewController *rootViewController = [UIViewController topViewController];
        
    /* create alert view controller */
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:title
                                                                        message:message
                                                                 preferredStyle:UIAlertControllerStyleAlert];
    
    /* create actions */
    id actionHandler = ^(UIAlertAction * _Nonnull action) {
        if (completeBlock) {
            completeBlock([buttonTitles indexOfObject:action.title]);
        }
    };
    
    for (NSString *buttonTitle in buttonTitles) {
        UIAlertAction *action = [UIAlertAction actionWithTitle:buttonTitle
                                                         style:UIAlertActionStyleDefault
                                                       handler:actionHandler];
        [controller addAction:action];
    }
    
    /* show alert controller */
    [rootViewController presentViewController:controller animated:YES completion:nil];
}

@end
