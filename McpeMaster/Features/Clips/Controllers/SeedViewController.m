//
//  MapViewController.m
//  McpeMaster
//
//  Created by kaka on 2/5/17.
//
//

#import "SeedViewController.h"
#import "Firebase.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface SeedViewController ()

@end

@implementation SeedViewController {
    BOOL hasJustLoginFB;
    BOOL loginErr;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initGoogleAdsBanner];
    if (![PRAppDelegate isFavoriteView]){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSString *urlString= [NSString stringWithFormat:@"https://mcpemaster.co/mcpehub/counterMcpe.php?type=seed&id=%@",self->_mcpeId];
            NSString *urlUTF8 = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
            NSURL *url = [NSURL URLWithString:urlUTF8];
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            NSURLResponse *response;
            NSError *err = nil;
            [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
            dispatch_async(dispatch_get_main_queue(), ^(void){
                if (err) {
                    NSLog(@"ERROR --> %@", err.localizedDescription);
                }else{
                    NSLog(@"Counter Succeffully id = %@",self->_mcpeId);
                }
            });
        });
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (loginErr) {
        loginErr = NO;
        AlertActionCompleteBlock completeBlock = ^(NSUInteger buttonIndex) {
            if (buttonIndex == 1) {
                [self fbLogin];
            }
        };
        [UIAlertController showConfirmPopUpWithTitle:nil
                                             message:@"Login Failed. Please try again!"
                                        buttonTitles:@[@"Cancel", @"Login"]
                                       completeBlock:completeBlock];
    }
    
    if (hasJustLoginFB) {
        self.realmSeedObject = [self fetchSeedFromDB];
        [self updateUI];
        hasJustLoginFB = NO;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //
    self.title = _strTitle;
    [self.view setBackgroundColor:UIColorFromRGB(0xE6E6E6)];
    [self.textViewDescription setBackgroundColor:UIColorFromRGB(0xE6E6E6)];
    
    self.lbSeedCode.text = _strSeedCode;
    //Add BarButton
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithImage:[IBHelper loadImage:@"icon_back"]
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(didTouchOnBackButton:)];
    self.navigationItem.leftBarButtonItem = leftButton;
    
    //
    if (_strThumbUrl) {
        [self.thumbImg sd_setImageWithURL:[NSURL URLWithString:_strThumbUrl]
                               placeholderImage:[UIImage imageNamed:@"store_background"]
                                      completed:^(UIImage *img, NSError *err,SDImageCacheType type,NSURL *imgurl){
                                          
                                      }];
    }
    
    if (_strMcpeDescription == nil){
        _strMcpeDescription = @"";
    }
    NSData *data = [[NSData alloc] initWithBase64EncodedString:_strMcpeDescription options:0];
    NSString *htmlString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSAttributedString *attributedString = [[NSAttributedString alloc]
                                                initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                                options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                documentAttributes: nil
                                                error: nil
                                                ];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            self.textViewDescription.attributedText = attributedString;
        });
    });
    
    self.realmSeedObject = [self fetchSeedFromDB];
    //
    [self updateUI];
    
    //
    [FIRAnalytics setScreenName:@"Seed_View" screenClass:@"Seed_View"];
}

- (void)updateUI{
    //
    if ([self isSeedFavorite]){
        [self.btnFavorite setTitle:@"Remove Favorite" forState:UIControlStateNormal];
    }else{
        [self.btnFavorite setTitle:@"Add Favorite" forState:UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)didTouchOnBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

//Download
- (IBAction)copySeedCode:(id)sender{
   [UIPasteboard generalPasteboard].string = _strSeedCode;
    
    [UIAlertController showConfirmPopUpWithTitle:@"Copy Successfully!"
                                         message:@"1. Open Minecraft App\n2. Tap 'Play Button'\n3. Go Create New World\n4. Enter Seed Code\n5. Enjoy it :)"
                                    buttonTitles:@[@"OK"]
                                   completeBlock:nil];
}

//
- (IBAction)onFavoriteButton:(id)sender {
    if ([FBSDKAccessToken currentAccessToken]) {
        // User is logged in
        [FBSDKProfile loadCurrentProfileWithCompletion:
         ^(FBSDKProfile *profile, NSError *error) {
             if (profile) {
                 [self addFavorite];
                 if ([self isSeedFavorite]) {
                     [self syncFavorite:profile.userID action:@"add"];
                 } else {
                     [self syncFavorite:profile.userID action:@"remove"];
                 }
             }
         }];
    } else {
        AlertActionCompleteBlock completeBlock = ^(NSUInteger buttonIndex) {
            if (buttonIndex == 1) {
                [self fbLogin];
            }
        };
        
        [UIAlertController showConfirmPopUpWithTitle:nil
                                             message:@"Please login Facebook to use Favorite function!"
                                        buttonTitles:@[@"Cancel", @"Login"]
                                       completeBlock:completeBlock];
    }
}

- (void)addFavorite {
    self.realmSeedObject = [self fetchSeedFromDB];
    if (self.realmSeedObject){
        [MKRealmManager transactionWithBlock:^(RLMRealm *realm) {
            RLMSeedObject *object = [self.seedObject realmObject];
            object.mcpeId = self.realmSeedObject.mcpeId;
            object.name = self.realmSeedObject.name;
            object.thumb = self.realmSeedObject.thumb;
            object.link = self.realmSeedObject.link;
            object.seedCode = self.realmSeedObject.seedCode;
            object.mcpeDescription = self.realmSeedObject.mcpeDescription;
            /* add new one */
            if (object) {
                if ([self isSeedFavorite]){
                    object.favorited = NO;
                }else{
                    object.favorited = YES;
                }
            }
            
            object.logTime = self.realmSeedObject.logTime;
            [realm deleteObject:(RLMObject *)self.realmSeedObject];
            
            [realm addObject:(RLMObject *)object];
            self.realmSeedObject = object;
            [self updateUI];
        }];
    }else{
        [MKRealmManager transactionWithBlock:^(RLMRealm *realm) {
            RLMSeedObject *object = [self.seedObject realmObject];
            object.mcpeId = self->_mcpeId;
            object.name = self->_strTitle;
            object.thumb = self->_strThumbUrl;
            object.link = [NSURL URLWithString:self->_strThumbUrl].lastPathComponent.stringByDeletingPathExtension;
            object.seedCode = self->_strSeedCode;
            object.mcpeDescription = self->_strMcpeDescription;
            /* add new one */
            if (object) {
                if ([self isSeedFavorite]){
                    object.favorited = NO;
                }else{
                    object.favorited = YES;
                }
            }
            
            object.logTime = self.realmSeedObject.logTime;
            
            [realm addObject:(RLMObject *)object];
            self.realmSeedObject = object;
            [self updateUI];
        }];
    }
}

- (void)syncFavorite:(NSString*)userId action:(NSString*)action {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *urlString= [NSString stringWithFormat:@"https://mcpemaster.co/mcpehub/addFavoriteMcpe.php?type=seed&userId=%@&id=%@&action=%@",userId,self->_mcpeId,action];
        NSString *urlUTF8 = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSURL *url = [NSURL URLWithString:urlUTF8];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSURLResponse *response;
        NSError *err = nil;
        [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            if (err) {
                NSLog(@"ERROR --> %@", err.localizedDescription);
            }else{
                NSLog(@"Report Map Succeffully id = %@",self->_mcpeId);
            }
        });
    });
}

- (void)fbLogin {
    FBSDKLoginManager *fbLogin = [[FBSDKLoginManager alloc] init];
    [fbLogin logInWithReadPermissions:@[@"email"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error != nil) {
            [fbLogin logOut];
            self->loginErr = YES;
        } else if (result.isCancelled) {
            //cancelled
            [fbLogin logOut];
        } else {
            //success
            self->hasJustLoginFB = YES;
        }
    }];
}

#pragma mark -
#pragma mark - DB Support

- (SESeedObject *)seedObject {
    
    if (!_seedObject) {
        _seedObject = [SESeedObject newObject];
    }
    return _seedObject;
}

- (BOOL)isSeedFavorite{
    return self.realmSeedObject.favorited;
}

- (RLMSeedObject*)fetchSeedFromDB {
    RLMResults<RLMSeedObject *> *objects = [[RLMSeedObject objectsWhere:[NSString stringWithFormat:@"link = '%@'",[NSURL URLWithString:_strThumbUrl].lastPathComponent.stringByDeletingPathExtension]] sortedResultsUsingProperty:@"logTime" ascending:NO];
    return objects.firstObject;
}

#pragma mark - GAD Banner Ads
- (void)initGoogleAdsBanner{
    if (!self.bannerView)
    {
        self.bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerPortrait];
        [self addBannerViewToView:self.bannerView];
        
        if (IS_IPAD) {
            CGRect frame = self.bannerView.frame;
            frame.origin.x = 20;
            self.bannerView.frame = frame;
        }
        
        self.bannerView.adUnitID = ADMOB_BANNER_ID;
        self.bannerView.rootViewController = self;
        self.bannerView.delegate = self;
        self.bannerView.hidden = TRUE;
        [self.view addSubview:self.bannerView];
        
        GADRequest *request = [GADRequest request];
        //request.testDevices = @[kGADSimulatorID];
        
        //Forward user consent choice
        if (PACConsentInformation.sharedInstance.requestLocationInEEAOrUnknown &&
            PACConsentInformation.sharedInstance.consentStatus == PACConsentStatusNonPersonalized) {
            GADExtras *extras = [[GADExtras alloc] init];
            extras.additionalParameters = @{@"npa": @"1"};
            [request registerAdNetworkExtras:extras];
            NSLog(@"Ads is non-personalized now!!!");
        }
        
        [self.bannerView loadRequest:request];
    }
}

- (void)addBannerViewToView:(UIView *)bannerView {
    bannerView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:bannerView];
    if (@available(iOS 11.0, *)) {
        // In iOS 11, we need to constrain the view to the safe area.
        [self positionBannerViewFullWidthAtBottomOfSafeArea:bannerView];
    } else {
        // In lower iOS versions, safe area is not available so we use
        // bottom layout guide and view edges.
        [self positionBannerViewFullWidthAtBottomOfView:bannerView];
    }
}

#pragma mark - view positioning

- (void)positionBannerViewFullWidthAtBottomOfSafeArea:(UIView *_Nonnull)bannerView NS_AVAILABLE_IOS(11.0) {
    // Position the banner. Stick it to the bottom of the Safe Area.
    // Make it constrained to the edges of the safe area.
    UILayoutGuide *guide = self.view.safeAreaLayoutGuide;
    
    [NSLayoutConstraint activateConstraints:@[
                                              [guide.leftAnchor constraintEqualToAnchor:bannerView.leftAnchor],
                                              [guide.rightAnchor constraintEqualToAnchor:bannerView.rightAnchor],
                                              [guide.bottomAnchor constraintEqualToAnchor:bannerView.bottomAnchor]
                                              ]];
}

- (void)positionBannerViewFullWidthAtBottomOfView:(UIView *_Nonnull)bannerView {
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:bannerView
                                                          attribute:NSLayoutAttributeLeading
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeLeading
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:bannerView
                                                          attribute:NSLayoutAttributeTrailing
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeTrailing
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:bannerView
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view.safeAreaLayoutGuide.bottomAnchor
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1
                                                           constant:0]];
}

#pragma mark - Banner hide and show -

// Hide the banner by sliding down
-(void)hideBanner:(UIView*)banner
{
    banner.hidden = TRUE;
}

// Show the banner by sliding up
-(void)showBanner:(UIView*)banner
{
    banner.hidden = FALSE;
}

#pragma mark - GADBanner delegate methods -

-(void)showAdmobBanner{
    self.bannerView.hidden = FALSE;
}

-(void)hideAdmobBanner{
    self.bannerView.hidden = TRUE;
}

// Called before ad is shown, good time to show the add
- (void)adViewDidReceiveAd:(GADBannerView *)view
{
    NSLog(@"Admob load");
    [self showBanner:self.bannerView];
}

// An error occured
- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error
{
    NSLog(@"Admob error: %@", error);
    [self hideBanner:self.bannerView];
}

@end
