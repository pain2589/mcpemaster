/*============================================================================
 PROJECT: McpeMaster
 FILE:    PRBaseViewController.m
 AUTHOR:  Khoai Nguyen
 DATE:    12/25/14
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "PRBaseViewController.h"
#import "TYMActivityIndicatorView.h"
#import "PRNavigationBarDelegate.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@interface PRBaseViewController () <UISearchBarDelegate, MKNavigationBarDelegate>
@end

@implementation PRBaseViewController

- (id)initSetupNavigationDelegate {
    __weak typeof(self) weakSelf = self;
    return (id) [[PRSearchAndBackNavigationBarDelegate alloc] initWithOwnViewController:self
                                                                        callBackHandler:^BOOL(NSUInteger barItemIndex) {
                                                                            if (barItemIndex == MKNavigationBarButtonTypeBack) {
                                                                                [weakSelf handleBackButtonEvent];
                                                                            }
                                                                            return YES;
                                                                        }];
}

- (id)setUpNavigationBarDelegate {
    if (_setUpNavigationBarDelegate == nil) {
        _setUpNavigationBarDelegate = [self initSetupNavigationDelegate];
    }
    return _setUpNavigationBarDelegate;
}


- (TYMActivityIndicatorView *)loadingIndicatorView {
    if (!_loadingIndicatorView) {
        _loadingIndicatorView = [[TYMActivityIndicatorView alloc] initWithActivityIndicatorStyle:TYMActivityIndicatorViewStyleNormal];
        _loadingIndicatorView.hidesWhenStopped = YES;
        _loadingIndicatorView.backgroundColor = nil;
        
        _loadingIndicatorView.translatesAutoresizingMaskIntoConstraints = YES;
        _loadingIndicatorView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
        
    }
    
    if (_loadingIndicatorView.superview) {
        [_loadingIndicatorView.superview bringSubviewToFront:_loadingIndicatorView];
    } else {
        [self.view addSubview:_loadingIndicatorView];
    }
    
    CGPoint originalCenter = CGPointApplyAffineTransform(self.view.center,
                                                         CGAffineTransformInvert(self.view.transform));
    _loadingIndicatorView.center = CGPointMake(fabs(originalCenter.x), fabs(originalCenter.y));
    return _loadingIndicatorView;
}

- (TYMActivityIndicatorView *)loadingIndicatorLarge {
    if (!_loadingIndicatorView) {
        _loadingIndicatorView = [[TYMActivityIndicatorView alloc] initWithActivityIndicatorStyle:TYMActivityIndicatorViewStyleLarge];
        _loadingIndicatorView.hidesWhenStopped = YES;
        _loadingIndicatorView.backgroundColor = nil;
        
        _loadingIndicatorView.translatesAutoresizingMaskIntoConstraints = YES;
        _loadingIndicatorView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
        
    }
    
    if (_loadingIndicatorView.superview) {
        [_loadingIndicatorView.superview bringSubviewToFront:_loadingIndicatorView];
    } else {
        [self.view addSubview:_loadingIndicatorView];
    }
    
    CGPoint originalCenter = CGPointApplyAffineTransform(self.view.center,
                                                         CGAffineTransformInvert(self.view.transform));
    _loadingIndicatorView.center = CGPointMake(fabs(originalCenter.x), fabs(originalCenter.y));
    return _loadingIndicatorView;
}

- (void)startLoadingLarge{
    [self.loadingIndicatorViewLarge startAnimating];
}

- (void)stopLoadingLarge{
    [self.loadingIndicatorViewLarge stopAnimating];
}

- (void)startLoading {
    [self.loadingIndicatorView startAnimating];
}

- (void)stopLoading {
    [self.loadingIndicatorView stopAnimating];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setUpNavigationBar];
    self.navigationItem.hidesBackButton = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)dealloc {
    
    // Remove all notification
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - MKNavigationBarDelegate

- (void)setUpNavigationBar {
    self.navigationItem.leftBarButtonItems = nil;
    self.navigationItem.rightBarButtonItems = nil;
    [self.setUpNavigationBarDelegate executeMethod:@"setUpNavigationBar" withParams:nil];
}

- (UIViewController *)ownedViewController {
    return self;
}

- (void)handleBackButtonEvent {
    
}

@end
