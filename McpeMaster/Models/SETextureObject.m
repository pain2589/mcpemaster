/*============================================================================
 PROJECT: SkinEditor
 FILE:    SETextureObject.m
 AUTHOR:  Nguyen Minh Khoai
 DATE:    10/18/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SETextureObject.h"

@interface SETextureObject()
@end

@implementation SETextureObject

+ (instancetype)newObject {
    return [[self alloc] init];
}

@end
