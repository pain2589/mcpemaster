/*============================================================================
 PROJECT: SkinEditor
 FILE:    SESKinCell.m
 AUTHOR:  Khoai Nguyen
 DATE:    11/11/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SESKinCell.h"
#import "UIView+SEQuartzCore.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/


@implementation SESKinCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _skinView.userInteractionEnabled = NO;
    [self makeDefaultGrayBorder];
}
@end
