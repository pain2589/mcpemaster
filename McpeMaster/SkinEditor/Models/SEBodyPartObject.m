/*============================================================================
 PROJECT: SkinEditor
 FILE:    SEBodyPartObject.m
 AUTHOR:  Nguyen Minh Khoai
 DATE:    10/4/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SEBodyPartObject.h"
#import "UIView+CoreGraphicUtils.h"

#import "SESkinObject.h"
#import <objc/runtime.h>

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
const RGBAColor RGBATransparentColor = {.red = 255, .blue = 255, .green = 255, .alpha = 0};
const MatrixDimension MaxtrixDimensionZero = {.rowSize = 0, .columnSize = 0};

/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/
@interface SEBYTEMatrix()
@property (nonatomic, readwrite) MatrixDimension dimension;
@property (nonatomic, strong) NSMutableArray<UIColor *> *matrix;
@end

@implementation SEBYTEMatrix : SEBaseObject
@synthesize dimension;
@synthesize matrix;

- (instancetype)initWithDimension:(MatrixDimension)matrixDimension {
    self = [super init];
    if (self) {
        
        self.dimension = matrixDimension;
        self.matrix = [NSMutableArray arrayWithCapacity:(dimension.rowSize * dimension.columnSize)];
        [self fillColor:[UIColor clearColor]];
    }
    return self;
}

+ (SEBYTEMatrix *)matrixOfTransparentColorsWithDimension:(MatrixDimension)dimension {
    return [[self alloc] initWithDimension:dimension];
}

- (void)setColor:(UIColor *)color forMaxtrixIndex:(MatrixIndex)matrixIndex {
    if (matrixIndex.row < dimension.rowSize && matrixIndex.column < dimension.columnSize) {
        self.matrix[matrixIndex.row * dimension.columnSize + matrixIndex.column] = color;
    }
}

- (void)setColor:(UIColor *)color forIndex:(NSUInteger)index {
    if (index <  self.matrix.count) {
        self.matrix[index] = color;
    }
}

- (void)fillColor:(UIColor *)color {
 
    for (NSUInteger i = 0; i < dimension.rowSize; i++) {
        for (NSUInteger j = 0; j < dimension.columnSize; j++) {
            self.matrix[i * dimension.columnSize + j] = color;
        }
    }
}

- (UIColor *)colorAtMatrixIndex:(MatrixIndex)matrixIndex {
    
    if (matrixIndex.row < dimension.rowSize && matrixIndex.column < dimension.columnSize) {
        return self.matrix[matrixIndex.row * dimension.columnSize + matrixIndex.column];
    }
    return  [UIColor clearColor];
}

- (UIColor *)colorAtIndex:(NSUInteger)index {
    if (index <  self.matrix.count) {
        return self.matrix[index];
    }
    return nil;
}

- (NSComparisonResult)isSquareDimension {
    
    NSUInteger rowCount = self.dimension.rowSize;
    NSUInteger columnCount = self.dimension.columnSize;
    
    NSComparisonResult result = NSOrderedSame;
    if (rowCount > columnCount) {
        result = NSOrderedDescending;
    }
    else if (rowCount < columnCount) {
        result = NSOrderedAscending;
    }
    return result;
}

- (instancetype)clonedMatrix {
    
    SEBYTEMatrix *instance = [SEBYTEMatrix matrixOfTransparentColorsWithDimension:self.dimension];
    
    for (NSUInteger i = 0; i < dimension.rowSize; i++) {
        for (NSUInteger j = 0; j < dimension.columnSize; j++) {
            instance.matrix[i * dimension.columnSize + j] = [self.matrix[i * dimension.columnSize + j] copy];
        }
    }
    return instance;
}

- (void)unionMatrix:(SEBYTEMatrix *)object {
    
    for (NSUInteger i = 0; i < dimension.rowSize; i++) {
        for (NSUInteger j = 0; j < dimension.columnSize; j++) {
            
            UIColor *color = object.matrix[i * dimension.columnSize + j];
            if (![color isEqual:[UIColor clearColor]] && ![color isEqual:SEClearColor]) {
                self.matrix[i * dimension.columnSize + j] = color;
            }
        }
    }
}

@end


@interface SEBodyPartObject()
@property (nonatomic, readwrite) SEBYTEMatrix *colorMatrix;
@end

@implementation SEBodyPartObject

- (instancetype)initWithDimension:(MatrixDimension)dimension startPoint:(CGPoint)startPoint {
    self = [super init];
    if (self) {
        self.colorMatrix = [SEBYTEMatrix matrixOfTransparentColorsWithDimension:dimension];
        self.startPoint = startPoint;
    }
    return self;
}

+ (SEBodyPartObject *)bodyPartWithDimension:(MatrixDimension)dimension startPoint:(CGPoint)startPoint {
    return [[self alloc] initWithDimension:dimension startPoint:startPoint];
}

+ (SEBodyPartObject *)reversedBodyPartWithDimension:(MatrixDimension)dimension startPoint:(CGPoint)startPoint {
    SEBodyPartObject *instance = [[self alloc] initWithDimension:dimension startPoint:startPoint];
    instance.isReversed = YES;
    return instance;
}

- (void)updateColorMatrix:(SEBYTEMatrix *)matrix {
    self.colorMatrix = matrix;
}


- (void)fillColorMatrixInContext:(CGContextRef)context {
    
    CGFloat width   = _colorMatrix.dimension.rowSize;
    CGFloat height  = _colorMatrix.dimension.columnSize;

    //draw pixels
    for (NSUInteger j = 0; j < height; j++) {
        for (NSUInteger i = 0; i < width; i++) {
            UIColor *color = [_colorMatrix colorAtMatrixIndex:MatrixIndexMake(i, j)];
            CGContextSetFillColorWithColor(context, color.CGColor);
            CGContextFillRect(context, CGRectMake(j + _startPoint.x, i + _startPoint.y, 1, 1));
        }
    }
}


- (void)getColorPixels:(unsigned char *)pixels
           bytesPerRow:(NSUInteger)bytesPerRow
         bytesPerPixel:(NSUInteger)bytesPerPixel {
    
    CGFloat width   = _colorMatrix.dimension.columnSize;
    CGFloat height  = _colorMatrix.dimension.rowSize;
    
    for (NSUInteger j = 0; j < height; j++) {
        for (NSUInteger i = 0; i < width; i++) {
            
            NSUInteger row = j;
            NSUInteger column = _isReversed ? (width - i - 1) : i;
            NSUInteger byteIndex = (bytesPerRow * (row + _startPoint.y)) + ((column + _startPoint.x) * bytesPerPixel);
            
            CGFloat alpha = ((CGFloat) pixels[byteIndex + 3] ) / 255.0f;
            CGFloat red   = ((CGFloat) pixels[byteIndex]     ) / 255.0f;
            CGFloat green = ((CGFloat) pixels[byteIndex + 1] ) / 255.0f;
            CGFloat blue  = ((CGFloat) pixels[byteIndex + 2] ) / 255.0f;
            
            UIColor *color = [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
            [_colorMatrix setColor:color forMaxtrixIndex:MatrixIndexMake(j, i)];
        }
    }
}

- (void)parseImagePixelsToBodyPart:(UIImage *)image {
    
    // First get the image into your data buffer
    CGImageRef imageRef = [image CGImage];
    NSUInteger width = CGImageGetWidth(imageRef);
    NSUInteger height = CGImageGetHeight(imageRef);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char *pixels = (unsigned char *) calloc(height * width * 4, sizeof(unsigned char));
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = bytesPerPixel * width;
    NSUInteger bitsPerComponent = 8;
    CGContextRef context = CGBitmapContextCreate(pixels, width, height,
                                                 bitsPerComponent, bytesPerRow, colorSpace,
                                                 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGColorSpaceRelease(colorSpace);
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
    CGContextRelease(context);
    
    /* parse colors */
    [self getColorPixels:pixels bytesPerRow:bytesPerRow bytesPerPixel:bytesPerPixel];
    free(pixels);
}

@end
