/*============================================================================
 PROJECT: SkinEditor
 FILE:    SESkinObject.m
 AUTHOR:  Nguyen Minh Khoai
 DATE:    10/18/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SESkinObject.h"
#import <UIKit/UIKit.h>
#import "NSMutableArray+SESkinObject.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
#define Square1Dimension    MatrixDimensionMake(8, 8)
#define Square2Dimension    MatrixDimensionMake(4, 4)

#define Rectangle1Dimension MatrixDimensionMake(12, 8)
#define Rectangle2Dimension MatrixDimensionMake(12, 4)
#define Rectangle3Dimension MatrixDimensionMake(4, 8)

#define NewSkinImageSize    CGSizeMake(64, 64)
#define OldSkinImageSize    CGSizeMake(64, 32)

/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/
@interface SESkinBodyPartObject ()
@end

@implementation SESkinBodyPartObject

+ (instancetype)head {
    
    SESkinBodyPartObject *instance = [SESkinBodyPartObject new];
    instance.top    = [SEBodyPartObject bodyPartWithDimension:Square1Dimension startPoint:CGPointMake(8, 0)];
    instance.bottom = [SEBodyPartObject bodyPartWithDimension:Square1Dimension startPoint:CGPointMake(16, 0)];
    instance.right  = [SEBodyPartObject bodyPartWithDimension:Square1Dimension startPoint:CGPointMake(0, 8)];
    instance.front  = [SEBodyPartObject bodyPartWithDimension:Square1Dimension startPoint:CGPointMake(8, 8)];
    instance.left   = [SEBodyPartObject bodyPartWithDimension:Square1Dimension startPoint:CGPointMake(16, 8)];
    instance.back   = [SEBodyPartObject bodyPartWithDimension:Square1Dimension startPoint:CGPointMake(24, 8)];
    return instance;
}

+ (instancetype)hat {
    SESkinBodyPartObject *instance = [SESkinBodyPartObject new];
    instance.top    = [SEBodyPartObject bodyPartWithDimension:Square1Dimension startPoint:CGPointMake(40, 0)];
    instance.bottom = [SEBodyPartObject bodyPartWithDimension:Square1Dimension startPoint:CGPointMake(48, 0)];
    instance.right  = [SEBodyPartObject bodyPartWithDimension:Square1Dimension startPoint:CGPointMake(32, 8)];
    instance.front  = [SEBodyPartObject bodyPartWithDimension:Square1Dimension startPoint:CGPointMake(40, 8)];
    instance.left   = [SEBodyPartObject bodyPartWithDimension:Square1Dimension startPoint:CGPointMake(48, 8)];
    instance.back   = [SEBodyPartObject bodyPartWithDimension:Square1Dimension startPoint:CGPointMake(55, 8)];
    return instance;
}

+ (instancetype)body {
    
    SESkinBodyPartObject *instance = [SESkinBodyPartObject new];
    instance.top    = [SEBodyPartObject bodyPartWithDimension:Rectangle3Dimension startPoint:CGPointMake(20, 16)];
    instance.bottom = [SEBodyPartObject bodyPartWithDimension:Rectangle3Dimension startPoint:CGPointMake(28, 16)];
    instance.right  = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(16, 20)];
    instance.front  = [SEBodyPartObject bodyPartWithDimension:Rectangle1Dimension startPoint:CGPointMake(20, 20)];
    instance.left   = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(28, 20)];
    instance.back   = [SEBodyPartObject bodyPartWithDimension:Rectangle1Dimension startPoint:CGPointMake(32, 20)];
    
    return instance;
}

+ (instancetype)rightLeg {
    SESkinBodyPartObject *instance = [SESkinBodyPartObject new];
    instance.top    = [SEBodyPartObject bodyPartWithDimension:Square2Dimension startPoint:CGPointMake(4, 16)];
    instance.bottom = [SEBodyPartObject bodyPartWithDimension:Square2Dimension startPoint:CGPointMake(8, 16)];
    instance.right  = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(0, 20)];
    instance.front  = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(4, 20)];
    instance.left   = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(8, 20)];
    instance.back   = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(12, 20)];
    return instance;
}


+ (instancetype)leftLeg {
    SESkinBodyPartObject *instance = [SESkinBodyPartObject new];
    instance.top    = [SEBodyPartObject bodyPartWithDimension:Square2Dimension startPoint:CGPointMake(20, 48)];
    instance.bottom = [SEBodyPartObject bodyPartWithDimension:Square2Dimension startPoint:CGPointMake(24, 48)];
    instance.right  = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(16, 52)];
    instance.front  = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(20, 52)];
    instance.left   = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(24, 52)];
    instance.back   = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(28, 52)];
    return instance;
}

+ (instancetype)leftArm {

    SESkinBodyPartObject *instance = [SESkinBodyPartObject new];
    instance.top    = [SEBodyPartObject bodyPartWithDimension:Square2Dimension startPoint:CGPointMake(36, 48)];
    instance.bottom = [SEBodyPartObject bodyPartWithDimension:Square2Dimension startPoint:CGPointMake(40, 48)];
    instance.right  = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(32, 52)];
    instance.front  = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(36, 52)];
    instance.left   = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(40, 52)];
    instance.back   = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(44, 52)];
    return instance;
}

+ (instancetype)rightArm {
    SESkinBodyPartObject *instance = [SESkinBodyPartObject new];
    instance.top    = [SEBodyPartObject bodyPartWithDimension:Square2Dimension startPoint:CGPointMake(44, 16)];
    instance.bottom = [SEBodyPartObject bodyPartWithDimension:Square2Dimension startPoint:CGPointMake(48, 16)];
    instance.right  = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(40, 20)];
    instance.front  = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(44, 20)];
    instance.left   = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(48, 20)];
    instance.back   = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(52, 20)];
    
    return instance;
}

+ (instancetype)jacket {
    SESkinBodyPartObject *instance = [SESkinBodyPartObject new];
    instance.top    = [SEBodyPartObject bodyPartWithDimension:Rectangle3Dimension startPoint:CGPointMake(20, 32)];
    instance.bottom = [SEBodyPartObject bodyPartWithDimension:Rectangle3Dimension startPoint:CGPointMake(28, 32)];
    instance.right  = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(16, 36)];
    instance.front  = [SEBodyPartObject bodyPartWithDimension:Rectangle1Dimension startPoint:CGPointMake(20, 36)];
    instance.left   = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(28, 36)];
    instance.back   = [SEBodyPartObject bodyPartWithDimension:Rectangle1Dimension startPoint:CGPointMake(32, 36)];
    return instance;

}

+ (instancetype)leftArm2 {
    
    SESkinBodyPartObject *instance = [SESkinBodyPartObject new];
    instance.top    = [SEBodyPartObject bodyPartWithDimension:Square2Dimension startPoint:CGPointMake(52, 48)];
    instance.bottom = [SEBodyPartObject bodyPartWithDimension:Square2Dimension startPoint:CGPointMake(56, 48)];
    instance.right  = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(48, 52)];
    instance.front  = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(52, 52)];
    instance.left   = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(56, 52)];
    instance.back   = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(60, 52)];
    return instance;
}

+ (instancetype)rightArm2 {
    
    SESkinBodyPartObject *instance = [SESkinBodyPartObject new];
    instance.top    = [SEBodyPartObject bodyPartWithDimension:Square2Dimension startPoint:CGPointMake(44, 32)];
    instance.bottom = [SEBodyPartObject bodyPartWithDimension:Square2Dimension startPoint:CGPointMake(48, 32)];
    instance.right  = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(40, 36)];
    instance.front  = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(44, 36)];
    instance.left   = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(48, 36)];
    instance.back   = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(52, 36)];
    
    return instance;
}

+ (instancetype)leftLeg2 {
    
    SESkinBodyPartObject *instance = [SESkinBodyPartObject new];
    instance.top    = [SEBodyPartObject bodyPartWithDimension:Square2Dimension startPoint:CGPointMake(4, 48)];
    instance.bottom = [SEBodyPartObject bodyPartWithDimension:Square2Dimension startPoint:CGPointMake(8, 48)];
    instance.right  = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(0, 52)];
    instance.front  = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(4, 52)];
    instance.left   = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(8, 52)];
    instance.back   = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(12, 52)];
    return instance;
}

+ (instancetype)rightLeg2 {
    
    SESkinBodyPartObject *instance = [SESkinBodyPartObject new];
    instance.top    = [SEBodyPartObject bodyPartWithDimension:Square2Dimension startPoint:CGPointMake(4, 32)];
    instance.bottom = [SEBodyPartObject bodyPartWithDimension:Square2Dimension startPoint:CGPointMake(8, 32)];
    instance.right  = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(0, 36)];
    instance.front  = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(4, 36)];
    instance.left   = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(8, 36)];
    instance.back   = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(12, 36)];
    return instance;
}


+ (instancetype)oldRightLeg {
    SESkinBodyPartObject *instance = [SESkinBodyPartObject new];
    instance.top    = [SEBodyPartObject bodyPartWithDimension:Square2Dimension startPoint:CGPointMake(4, 16)];
    instance.bottom = [SEBodyPartObject bodyPartWithDimension:Square2Dimension startPoint:CGPointMake(8, 16)];
    instance.right  = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(0, 20)];
    instance.front  = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(4, 20)];
    instance.left   = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(8, 20)];
    instance.back   = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(12, 20)];
    return instance;
}

+ (instancetype)oldLeftLeg {
    SESkinBodyPartObject *instance = [SESkinBodyPartObject new];
    instance.top    = [SEBodyPartObject reversedBodyPartWithDimension:Square2Dimension startPoint:CGPointMake(4, 16)];
    instance.bottom = [SEBodyPartObject reversedBodyPartWithDimension:Square2Dimension startPoint:CGPointMake(8, 16)];
    instance.right  = [SEBodyPartObject reversedBodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(0, 20)];
    instance.front  = [SEBodyPartObject reversedBodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(4, 20)];
    instance.left   = [SEBodyPartObject reversedBodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(8, 20)];
    instance.back   = [SEBodyPartObject reversedBodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(12, 20)];
    return instance;
}

+ (instancetype)oldRightArm {
    SESkinBodyPartObject *instance = [SESkinBodyPartObject new];
    instance.top    = [SEBodyPartObject bodyPartWithDimension:Square2Dimension startPoint:CGPointMake(44, 16)];
    instance.bottom = [SEBodyPartObject bodyPartWithDimension:Square2Dimension startPoint:CGPointMake(48, 16)];
    instance.right  = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(40, 20)];
    instance.front  = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(44, 20)];
    instance.left   = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(48, 20)];
    instance.back   = [SEBodyPartObject bodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(52, 20)];
    
    return instance;
}

+ (instancetype)oldLeftArm {
    SESkinBodyPartObject *instance = [SESkinBodyPartObject new];
    instance.top    = [SEBodyPartObject reversedBodyPartWithDimension:Square2Dimension startPoint:CGPointMake(44, 16)];
    instance.bottom = [SEBodyPartObject reversedBodyPartWithDimension:Square2Dimension startPoint:CGPointMake(48, 16)];
    instance.right  = [SEBodyPartObject reversedBodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(40, 20)];
    instance.front  = [SEBodyPartObject reversedBodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(44, 20)];
    instance.left   = [SEBodyPartObject reversedBodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(48, 20)];
    instance.back   = [SEBodyPartObject reversedBodyPartWithDimension:Rectangle2Dimension startPoint:CGPointMake(52, 20)];
    
    return instance;
}


- (void)fillColorMatrixInContext:(CGContextRef)context {
    [self.top       fillColorMatrixInContext:context];
    [self.bottom    fillColorMatrixInContext:context];
    [self.right     fillColorMatrixInContext:context];
    [self.front     fillColorMatrixInContext:context];
    [self.left      fillColorMatrixInContext:context];
    [self.back      fillColorMatrixInContext:context];
}

- (void)getColorPixels:(unsigned char *)pixels
           bytesPerRow:(NSUInteger)bytesPerRow
         bytesPerPixel:(NSUInteger)bytesPerPixel {
    
    [self.top       getColorPixels:pixels bytesPerRow:bytesPerRow bytesPerPixel:bytesPerPixel];
    [self.bottom    getColorPixels:pixels bytesPerRow:bytesPerRow bytesPerPixel:bytesPerPixel];
    [self.right     getColorPixels:pixels bytesPerRow:bytesPerRow bytesPerPixel:bytesPerPixel];
    [self.front     getColorPixels:pixels bytesPerRow:bytesPerRow bytesPerPixel:bytesPerPixel];
    [self.left      getColorPixels:pixels bytesPerRow:bytesPerRow bytesPerPixel:bytesPerPixel];
    [self.back      getColorPixels:pixels bytesPerRow:bytesPerRow bytesPerPixel:bytesPerPixel];
}

- (void)unionPart:(SESkinBodyPartObject *)object {
    [self.top.colorMatrix       unionMatrix:object.top.colorMatrix];
    [self.bottom.colorMatrix    unionMatrix:object.bottom.colorMatrix];
    [self.right.colorMatrix     unionMatrix:object.right.colorMatrix];
    [self.front.colorMatrix     unionMatrix:object.front.colorMatrix];
    [self.left.colorMatrix      unionMatrix:object.left.colorMatrix];
    [self.back.colorMatrix      unionMatrix:object.back.colorMatrix];
}

- (void)copyPart:(SESkinBodyPartObject *)object {
    
    [self.top       updateColorMatrix:object.top.colorMatrix];
    [self.bottom    updateColorMatrix:object.bottom.colorMatrix];
    [self.right     updateColorMatrix:object.right.colorMatrix];
    [self.front     updateColorMatrix:object.front.colorMatrix];
    [self.left      updateColorMatrix:object.left.colorMatrix];
    [self.back      updateColorMatrix:object.back.colorMatrix];
}

- (BOOL)isModified {
    if (self.top.isModified     ||
        self.bottom.isModified  ||
        self.left.isModified    ||
        self.right.isModified   ||
        self.front.isModified   ||
        self.back.isModified) {
        return YES;
    }
    return NO;
}

@end

@interface SESkinObject()
@end

@implementation SESkinObject

- (NSMutableArray *)createPartsForNewFormatSkin:(BOOL)isNewFormat {
    
    NSMutableArray *parts = [[NSMutableArray alloc] initWithCapacity:SESkinBodyPartCount];
    for (NSUInteger i = 0; i < SESkinBodyPartCount; i++) {
        [parts addObject:[NSNull null]];
    }
    
    
    if (isNewFormat) {
        [parts insertPart:[SESkinBodyPartObject head]       atIndex:SESkinBodyPartHead];
        [parts insertPart:[SESkinBodyPartObject hat]        atIndex:SESkinBodyPartHat];
        [parts insertPart:[SESkinBodyPartObject body]       atIndex:SESkinBodyPartBody];
        [parts insertPart:[SESkinBodyPartObject leftArm]    atIndex:SESkinBodyPartLeftArm];
        [parts insertPart:[SESkinBodyPartObject rightArm]   atIndex:SESkinBodyPartRightArm];
        [parts insertPart:[SESkinBodyPartObject leftLeg]    atIndex:SESkinBodyPartLeftLeg];
        [parts insertPart:[SESkinBodyPartObject rightLeg]   atIndex:SESkinBodyPartRightLeg];
        
        [parts insertPart:[SESkinBodyPartObject jacket]     atIndex:SESkinBodyPartJacket];
        [parts insertPart:[SESkinBodyPartObject leftArm2]   atIndex:SESkinBodyPartLeftArm2];
        [parts insertPart:[SESkinBodyPartObject rightArm2]  atIndex:SESkinBodyPartRightArm2];
        [parts insertPart:[SESkinBodyPartObject leftLeg2]   atIndex:SESkinBodyPartLeftLeg2];
        [parts insertPart:[SESkinBodyPartObject rightLeg2]  atIndex:SESkinBodyPartRightLeg2];
    }
    else {
        
        [parts insertPart:[SESkinBodyPartObject head] atIndex:SESkinBodyPartHead];
        [parts insertPart:[SESkinBodyPartObject hat] atIndex:SESkinBodyPartHat];
        [parts insertPart:[SESkinBodyPartObject body] atIndex:SESkinBodyPartBody];
        [parts insertPart:[SESkinBodyPartObject oldRightLeg] atIndex:SESkinBodyPartRightLeg];
        [parts insertPart:[SESkinBodyPartObject oldRightArm] atIndex:SESkinBodyPartRightArm];
        [parts insertPart:[SESkinBodyPartObject oldLeftLeg] atIndex:SESkinBodyPartLeftLeg];
        [parts insertPart:[SESkinBodyPartObject oldLeftArm] atIndex:SESkinBodyPartLeftArm];
    }
    
    return parts;
}

- (void)parseImagePixelsToSkin:(UIImage *)image {
    
    // First get the image into your data buffer
    CGImageRef imageRef = [image CGImage];
    NSUInteger width = CGImageGetWidth(imageRef);
    NSUInteger height = CGImageGetHeight(imageRef);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char *pixels = (unsigned char *) calloc(height * width * 4, sizeof(unsigned char));
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = bytesPerPixel * width;
    NSUInteger bitsPerComponent = 8;
    CGContextRef context = CGBitmapContextCreate(pixels, width, height,
                                                 bitsPerComponent, bytesPerRow, colorSpace,
                                                 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGColorSpaceRelease(colorSpace);
    
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
    CGContextRelease(context);
    
    if (width >= 64 && height >= 64) {
        
        /* update image size */
        self.imageSize = NewSkinImageSize;
        
        /* parse matrix color */
        for (SESkinBodyPartObject *part in self.parts) {
            [part getColorPixels:pixels bytesPerRow:bytesPerRow bytesPerPixel:bytesPerPixel];
        }
        
        /* union part by part */
        [[self.parts partAtIndex:SESkinBodyPartBody]     unionPart: [self.parts partAtIndex:SESkinBodyPartJacket]];
        [[self.parts partAtIndex:SESkinBodyPartLeftArm]  unionPart: [self.parts partAtIndex:SESkinBodyPartLeftArm2]];
        [[self.parts partAtIndex:SESkinBodyPartRightArm] unionPart: [self.parts partAtIndex:SESkinBodyPartRightArm2]];
        [[self.parts partAtIndex:SESkinBodyPartLeftLeg]  unionPart: [self.parts partAtIndex:SESkinBodyPartLeftLeg2]];
        [[self.parts partAtIndex:SESkinBodyPartRightLeg] unionPart: [self.parts partAtIndex:SESkinBodyPartRightLeg2]];
    }
    else {
        
        /* update image size */
        self.imageSize = NewSkinImageSize;
        
        /* parse matrix color */
        NSMutableArray *bodyParts = [self createPartsForNewFormatSkin:NO];
        for (id part in bodyParts) {
            
            if (part != [NSNull null]) {
                [part getColorPixels:pixels bytesPerRow:bytesPerRow bytesPerPixel:bytesPerPixel];
            }
        }
        
        /* union part by part */
        [[self.parts partAtIndex:SESkinBodyPartHead]     copyPart: [bodyParts partAtIndex:SESkinBodyPartHead]];
        [[self.parts partAtIndex:SESkinBodyPartHat]      copyPart: [bodyParts partAtIndex:SESkinBodyPartHat]];
        [[self.parts partAtIndex:SESkinBodyPartBody]     copyPart: [bodyParts partAtIndex:SESkinBodyPartBody]];
        [[self.parts partAtIndex:SESkinBodyPartLeftArm]  copyPart: [bodyParts partAtIndex:SESkinBodyPartLeftArm]];
        [[self.parts partAtIndex:SESkinBodyPartRightArm] copyPart: [bodyParts partAtIndex:SESkinBodyPartRightArm]];
        [[self.parts partAtIndex:SESkinBodyPartLeftLeg]  copyPart: [bodyParts partAtIndex:SESkinBodyPartLeftLeg]];
        [[self.parts partAtIndex:SESkinBodyPartRightLeg] copyPart: [bodyParts partAtIndex:SESkinBodyPartRightLeg]];
    }
    
    free(pixels);
}

#pragma mark -
#pragma mark - Public Methods

- (instancetype)init {
    self = [super init];
    if (self) {
        self.imageSize = NewSkinImageSize;
        self.parts = [self createPartsForNewFormatSkin:YES];
    }
    return self;
}

+ (instancetype)newObject {
    return [[self alloc] init];
}

+ (instancetype)objectFromImage:(UIImage *)image {
    
    SESkinObject *instance = [[self alloc] init];
    [instance parseImagePixelsToSkin:image];
    return instance;
}

- (BOOL)isOldSkinFormat {
    return (self.imageSize.width == 64 && self.imageSize.height == 32);
}

- (BOOL)isModified {
    
    if (_isModified) return _isModified;
    
    for (SESkinBodyPartObject *part in self.parts) {
        if (part.isModified) {
            return YES;
        }
    }
    return NO;
}

- (UIImage *)image {
    
    CGFloat width   = self.imageSize.width;
    CGFloat height  = self.imageSize.height;
    
    /* create drawing context */
    UIGraphicsBeginImageContext(CGSizeMake(width, height));
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    /* fill part by part */
    for (id part in self.parts) {
        
        if (part != [NSNull null]) {
            [part fillColorMatrixInContext:context];
        }
    }
    
    /* capture resultant image */
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

@end
