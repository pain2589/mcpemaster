/*============================================================================
 PROJECT: GetThisNotThat
 FILE:    NSObject+Swift.h
 AUTHOR:  Khoai Nguyen
 DATE:    2/2/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import <Foundation/Foundation.h>

/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   NSObject_Swift
 =============================================================================*/

@interface NSObject (Swift)

- (id)swift_performSelector:(SEL)selector;

- (id)swift_performSelector:(SEL)selector withObject:(id)object;

- (id)swift_performSelector:(SEL)selector withObject:(id)obj1 withObject:(id)obj2;

- (void)swift_performSelector:(SEL)selector withObject:(id)object afterDelay:(NSTimeInterval)delay;

@end
