/*============================================================================
 PROJECT: McpeMaster
 FILE:    MKCollectionViewControllerProtocols.h
 AUTHOR:  Khoai Nguyen
 DATE:    3/15/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import <Foundation/Foundation.h>

/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 PROTOCOL
 =============================================================================*/

#pragma mark - CollectionViewControllerDataSource
@class MKCollectionViewController, MKCollectionViewCell;

@protocol MKCollectionViewControllerDataSource <NSObject>

- (NSInteger)collectionController:(MKCollectionViewController *)controller numberOfRowInSection:(NSInteger)section;

- (MKCollectionViewCell *)collectionController:(MKCollectionViewController *)controller cellForItemAtIndexPath:(NSIndexPath *)indexPath;

@optional

- (void)collectionController:(MKCollectionViewController *)controller
               configureCell:(MKCollectionViewCell *)cell
                 atIndexPath:(NSIndexPath *)indexPath;

- (NSInteger)numberOfSessionsInCollectionViewController:(MKCollectionViewController *)controller;

- (UICollectionReusableView *)headerViewInCollectionViewController:(MKCollectionViewController *)controller
                                                       atIndexPath:(NSIndexPath *)indexPath;

- (UICollectionReusableView *)footerViewInCollectionViewController:(MKCollectionViewController *)controller
                                                       atIndexPath:(NSIndexPath *)indexPath;

@end

#pragma mark - MKCollectionViewControllerDelegate

@protocol MKCollectionViewControllerDelegate <NSObject>
@optional

- (CGSize)collectionController:(MKCollectionViewController *)controller
                        layout:(UICollectionViewLayout *)collectionViewLayout
        sizeForItemAtIndexPath:(NSIndexPath *)indexPath;

- (CGSize)collectionController:(MKCollectionViewController *)controller
                         layout:(UICollectionViewLayout *)collectionViewLayout
referenceSizeForFooterInSection:(NSInteger)section;

- (CGSize)collectionController:(MKCollectionViewController *)controller
                         layout:(UICollectionViewLayout *)collectionViewLayout
referenceSizeForHeaderInSection:(NSInteger)section;

- (void)collectionController:(MKCollectionViewController *)controller
    didSelectItemAtIndexPath:(NSIndexPath *)indexPath;

@end

#pragma mark - MKCollectionViewControllerConfiguration

@protocol MKCollectionViewControllerConfiguration <NSObject>
@optional

- (UICollectionViewLayout *)collectionViewLayoutInCollectionController:(MKCollectionViewController *)controller;

- (NSString *)cellIdentifierForCollectionController:(MKCollectionViewController *)controller atIndexPath:(NSIndexPath *)indexPath;

- (NSString *)headerIDInController:(MKCollectionViewController *)controller atIndexPath:(NSIndexPath *)indexPath;

- (NSString *)footerIDInController:(MKCollectionViewController *)controller atIndexPath:(NSIndexPath *)indexPath;

- (void)setupCollectionViewInController:(MKCollectionViewController *)controller;

@end
