/*============================================================================
 PROJECT: McpeMaster
 FILE:    PRBaseViewController.h
 AUTHOR:  Khoai Nguyen
 DATE:    12/25/14
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import <UIKit/UIKit.h>

/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   PRBaseViewController
 =============================================================================*/

@class TYMActivityIndicatorView;

@interface PRBaseViewController : UIViewController
@property(nonatomic, strong) TYMActivityIndicatorView *loadingIndicatorView;
@property(nonatomic, strong) TYMActivityIndicatorView *loadingIndicatorViewLarge;
@property(strong, nonatomic) id setUpNavigationBarDelegate;

- (id)initSetupNavigationDelegate;

- (void)startLoadingLarge;

- (void)startLoading;

- (void)stopLoading;

- (void)handleBackButtonEvent;

@end
