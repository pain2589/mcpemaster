/*============================================================================
 PROJECT: SkinEditor
 FILE:    UIViewController+NibSupport.m
 AUTHOR:  Khoai Nguyen
 DATE:    10/19/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "UIViewController+NibSupport.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@implementation UIViewController (NibSupport)

+ (instancetype)viewControllerWithNibName:(NSString *)nibName arguments:(NSDictionary *)arguments {
   
    id controller = [[self alloc] initWithNibName:nibName bundle:nil];
    
    /* binding data */
    if (arguments && controller) {
        
        NSArray *propertyKeys = arguments.allKeys;
        for (NSString *key in propertyKeys) {
            if ([controller respondsToSelector:NSSelectorFromString(key)] ||
                [UIViewController instancesRespondToSelector:NSSelectorFromString(key)]) {
                [controller setValue:arguments[key] forKey:key];
            }
        }
    }
    
    return controller;
}

+ (instancetype)viewControllerWithArguments:(NSDictionary *)arguments {
    return [self viewControllerWithNibName:NSStringFromClass(self) arguments:arguments];
}

@end
