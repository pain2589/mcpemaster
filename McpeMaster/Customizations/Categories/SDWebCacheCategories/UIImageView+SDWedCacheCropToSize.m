/*============================================================================
 PROJECT: SportLocker
 FILE:    UIImageView+SDWedCacheCropToSize.m
 AUTHOR:  Khoai Nguyen
 DATE:    12/27/14
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "UIImageView+SDWedCacheCropToSize.h"
#import "SDWebImageCompat.h"
#import "SDWebImageDownloader.h"
#import "SDWebImageManager.h"
#import "UIImage+FX.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@implementation UIImageView (SDWedCacheCropToSize)

#pragma mark - Public Methods

- (void)setImageWithURL:(NSURL *)url
       placeholderImage:(UIImage *)placeholder
        withContentMode:(UIViewContentMode)mode {

    UIImage *image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:[[url absoluteString] md5Encrypted]];

    if (image) {
        self.contentMode = mode;
        self.image = image;
    }
    else {
        [self sd_setImageWithURL:url
                placeholderImage:placeholder
                         options:SDWebImageCacheMemoryOnly
                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {

                           @autoreleasepool {

                               if (image) {
                                   /* reset mode */
                                   self.contentMode = mode;

                                   /* cache image */
                                   [[SDImageCache sharedImageCache] storeImage:image
                                                                        forKey:[[imageURL absoluteString] md5Encrypted]
                                                                        toDisk:YES];

                                   /* update UI */
                                   self.image = image;
                               }
                           }
                       }];
    }
}

- (void)setImageWithURL:(NSURL *)url
       placeholderImage:(UIImage *)placeholder
        andCropToBounds:(CGRect)bounds {

    UIImage *image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:[[url absoluteString] md5Encrypted]];

    if (image) {
        self.image = image;
    }
    else {
        [self sd_setImageWithURL:url
                placeholderImage:placeholder
                         options:SDWebImageCacheMemoryOnly
                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {

                           @autoreleasepool {

                               if (image) {

                                   /* update image */
                                   if (image) {
                                       image = [image imageCroppedAndScaledToSize:bounds.size
                                                                      contentMode:self.contentMode
                                                                         padToFit:YES];
                                   }

                                   /* cache image */
                                   [[SDImageCache sharedImageCache] storeImage:image
                                                                        forKey:[[imageURL absoluteString] md5Encrypted]
                                                                        toDisk:YES];

                                   /* update UI */
                                   self.image = image;

                               }
                           }
                       }];
    }
}

@end
