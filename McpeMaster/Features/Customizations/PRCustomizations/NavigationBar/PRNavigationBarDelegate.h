/*============================================================================
 PROJECT: McpeMaster
 FILE:    PRSearchNavigationBarDelegate.h
 AUTHOR:  Khoai Nguyen
 DATE:    4/5/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "MKNavigationBarDelegateDecorator.h"
#import "ConcreteNavigationBarDelegates.h"
#import "NavigationCoutryView.h"

/*============================================================================
 MACRO
 =============================================================================*/
typedef NS_ENUM(NSUInteger, PRNavigationBarType) {
    PRNavigationBarTypeSearch = 3,
    PRNavigationBarTypePickCountry,
};

/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   PRMenuNavigationBarDelegate
 =============================================================================*/
@interface PRMenuNavigationBarDelegate : MKLeftMenuNavigationBarDelegate
@end

/*============================================================================
 Interface:   PRSearchNavigationBarDelegate
 =============================================================================*/
@interface PRSearchNavigationBarDelegate : MKNavigationBarDelegateDecorator
- (void)resetSearchController;
@end

/*============================================================================
 Interface:   PRChooseCountryNavigationBarDelegate
 =============================================================================*/
@interface PRChooseCountryNavigationBarDelegate : MKNavigationBarDelegateDecorator
- (void)refreshWithTitle:(NSString *)title;
@end

/*============================================================================
 Interface:   PRClipNavigationBarDelegate
 =============================================================================*/
@interface PRClipNavigationBarDelegate : MKNavigationBarDelegateDecorator
@end

/*============================================================================
 Interface:   PRSettingNavigationBarDelegate
 =============================================================================*/
@interface PRSettingNavigationBarDelegate : MKNavigationBarDelegateDecorator
@end

/*============================================================================
 Interface:   PRSearchAndBackNavigationBarDelegate
 =============================================================================*/
@interface PRSearchAndBackNavigationBarDelegate : MKNavigationBarDelegateDecorator
@end

/*============================================================================
 Interface:   PRSearchAndMenuNavigationBarDelegate
 =============================================================================*/
@interface PRSearchAndMenuNavigationBarDelegate : MKNavigationBarDelegateDecorator
@end
