/*============================================================================
 PROJECT: SkinEditor
 FILE:    SETextureObject+Realm.m
 AUTHOR:  Khoai Nguyen
 DATE:    11/13/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SETextureObject+Realm.h"
#import "RLMTextureObject.h"
/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@implementation SETextureObject (Realm)

- (RLMTextureObject *)realmObject {
    
    RLMTextureObject *object = [[RLMTextureObject alloc] init];
    object.type = @"texture";
    object.logTime = [[NSDate date] timeIntervalSince1970];
    
    return object;
}

+ (SETextureObject *)fromRealmObject:(RLMTextureObject *)realmObject {
    
    if (!realmObject) return nil;
    
    if (realmObject.name.length > 0) {
        SETextureObject *object = [SETextureObject newObject];
        return object;
    }
    
    return nil;
}

@end
