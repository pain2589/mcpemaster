//
//  NSDate+Extension.h
//  McpeMaster
//
//  Created by Tai Huu Ho on 1/7/15.
//  Copyright (c) 2015 Khoai Nguyen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Extension)

- (NSString *)stringWithFormat:(NSString *)format;

- (NSString *)stringYouTubeTime;

@end
