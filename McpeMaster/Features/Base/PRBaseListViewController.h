/*============================================================================
 PROJECT: McpeMaster
 FILE:    PRBaseListViewController.h
 AUTHOR:  Khoai Nguyen
 DATE:    4/4/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "PRListViewController.h"

/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   PRBaseListViewController
 =============================================================================*/


@interface PRBaseListViewController : PRListViewController
@property(strong, nonatomic) id navigationBarSetupDelegate;

- (id)initSetupNavigationDelegate;

@end
