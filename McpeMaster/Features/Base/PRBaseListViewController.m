/*============================================================================
 PROJECT: McpeMaster
 FILE:    PRBaseListViewController.m
 AUTHOR:  Khoai Nguyen
 DATE:    4/4/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "PRBaseListViewController.h"
#import "PRIndicatorViewDelegate.h"
#import "PRNavigationBarDelegate.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/
@interface PRBaseListViewController () <UISearchBarDelegate, MKNavigationBarDelegate>
@end

@implementation PRBaseListViewController
@synthesize navigationBarSetupDelegate;

#pragma mark - Accessors

- (id)initSetupNavigationDelegate {
    return (id) [[PRSearchAndBackNavigationBarDelegate alloc] initWithDelegate:nil andOwnViewController:self];
}

- (id)navigationBarSetupDelegate {
    if (navigationBarSetupDelegate == nil) {
        navigationBarSetupDelegate = [self initSetupNavigationDelegate];
    }
    return navigationBarSetupDelegate;
}

#pragma mark - View life cycle

- (void)viewDidLoad {
    
    [self setUpNavigationBar];
//    if (@available(iOS 11, *)) {
//        UIEdgeInsets insets = [UIApplication sharedApplication].delegate.window.safeAreaInsets;
//        if (insets.top > 0) {
//            // We're running on an iPhone with a notch.
//        }
//    }
//    // Set up the large title bar on iPhone X
//    if (@available(iOS 11, *)) {
//        if ([UIApplication sharedApplication].delegate.window.safeAreaInsets.bottom > 0) {
//            self.navigationController.navigationBar.prefersLargeTitles = YES;
//            self.navigationItem.largeTitleDisplayMode = UINavigationItemLargeTitleDisplayModeNever;
//        }
//    }
//    if (@available(iOS 11, *)) {
//        self.navigationItem.largeTitleDisplayMode = UINavigationItemLargeTitleDisplayModeAlways;
//    }

    self.collectionView.backgroundColor = UIColorFromRGB(0xE6E6E6);
    [super viewDidLoad];
}

- (void)hideSearchSuggestionView {
    id decorator = self.navigationBarSetupDelegate;
    while (decorator) {
        if ([decorator hasMethod:@"resetSearchController"]) {
            [decorator executeMethod:@"resetSearchController" withParams:nil];
            break;
        }
        decorator = [decorator delegate];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    /* hide search if has it showing on screen */
    [self hideSearchSuggestionView];
}


- (void)dealloc {

    // Remove all notification
    self.navigationBarSetupDelegate = nil;

    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - MKCollectionViewController

- (void)loadConfigurations {
    self.indicatorDelegate = [[PRIndicatorViewDelegate alloc] init];
}

#pragma mark - MKNavigationBarDelegate

- (void)setUpNavigationBar {
    [self.navigationBarSetupDelegate executeMethod:@"setUpNavigationBar" withParams:nil];
}

@end
