/*============================================================================
 PROJECT: FileString
 FILE:    UIView+Popup.h
 AUTHOR:  Tam Nguyen
 DATE:    9/4/14
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import <UIKit/UIKit.h>

/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   UIView_Popup
 =============================================================================*/

@interface UIView (Popup)

- (void)showPopupWithOpacityOverlay:(CGFloat)opacity;

- (void)showPopupWithMainViewOpacity:(CGFloat)mainViewOpacity opacityOverlay:(CGFloat)opacity;

- (void)showPopupWithOpacityOverlay:(CGFloat)opacity hideWhenTouchOutSide:(BOOL)hideWhenTouchOutSide;

- (void)showPopupOnView:(UIView *)parentView opacityOverlay:(CGFloat)opacity;

- (void)showPopupOnView:(UIView *)parentView mainViewOpacity:(CGFloat)mainViewOpacity opacityOverlay:(CGFloat)opacity;

- (void)showPopupOnView:(UIView *)parentView opacityOverlay:(CGFloat)opacity hideWhenTouchOutSide:(BOOL)hideWhenTouchOutSide;

- (void)hidePopUpWithAnimated:(BOOL)animated;

@end
