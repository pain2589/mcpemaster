/*============================================================================
 PROJECT: McpeMaster
 FILE:    PRPreferences.m
 AUTHOR:  Khoai Nguyen
 DATE:    1/6/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "PRPreferences.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/

#define kSearchOptionsPlistFileName @"search.plist"

/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/
@interface PRPreferences ()
@property(nonatomic, readwrite) BOOL optionsChanged;
@end

@implementation PRPreferences

- (instancetype)init {
    self = [super init];
    if (self) {

        /* restore prefs */
        [self restore];

        /* set default values */
        if (!self.sortBy) {
            self.sortBy = @"relevance";
        }

        if (!self.created) {
            self.created = @"any";
        }

        if (!self.duration) {
            self.duration = @"any";
        }
    }
    return self;
}

+ (PRPreferences *)sharedPreferences {
    static PRPreferences *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[PRPreferences alloc] init];
    });
    return instance;
}

#pragma mark - static variables

+ (NSArray *)searchOptions {

    static NSArray *_searchOptions;
    static dispatch_once_t onceToken;

    dispatch_once(&onceToken, ^{
        _searchOptions = @[
                @[
                        @{NSLocalizedString(@"Date", nil) : @"date"},
                        @{NSLocalizedString(@"Rating", nil) : @"rating"},
                        @{NSLocalizedString(@"Relevance", nil) : @"relevance"},
                        @{NSLocalizedString(@"Title", nil) : @"title"},
                        @{NSLocalizedString(@"Video Count", nil) : @"videoCount"},
                        @{NSLocalizedString(@"Views", nil) : @"viewCount"}
                ],
                @[
                        @{NSLocalizedString(@"Any", nil) : @"any"},
                        @{NSLocalizedString(@"Last Hour", nil) : @"lastHour"},
                        @{NSLocalizedString(@"Today", nil) : @"today"},
                        @{NSLocalizedString(@"This Week", nil) : @"thisWeek"},
                        @{NSLocalizedString(@"This Month", nil) : @"thisMonth"},
                        @{NSLocalizedString(@"This Year", nil) : @"thisYear"}
                ],
                @[
                        @{NSLocalizedString(@"Any", nil) : @"any"},
                        @{NSLocalizedString(@"Short (< 4 Minutes)", nil) : @"short"},
                        @{NSLocalizedString(@"Medium (4-20 Minutes)", nil) : @"medium"},
                        @{NSLocalizedString(@"Long (> 20 Minutes)", nil) : @"long"}
                ],
                @[
                        @{NSLocalizedString(@"HD Only", nil) : @"high"},
                        @{NSLocalizedString(@"Closed Captioned Only", nil) : @"closedCaption"},
                        @{NSLocalizedString(@"Live Streams Only", nil) : @"live"}
                ]];

    });

    return _searchOptions;
}

#pragma mark - Accessors

- (BOOL)isPreferenceChanged {
    return _optionsChanged;
}

- (NSMutableDictionary *)options {
    if (!_options) {
        _options = [[NSMutableDictionary alloc] init];
    }
    return _options;
}

- (NSString *)sortBy {
    return self.options[@"sortBy"];
}

- (void)setSortBy:(NSString *)sortBy {
    self.options[@"sortBy"] = sortBy;
}

- (NSString *)created {
    return self.options[@"created"];
}

- (void)setCreated:(NSString *)created {
    self.options[@"created"] = created;
}

- (NSString *)duration {
    return self.options[@"duration"];
}

- (void)setDuration:(NSString *)duration {
    self.options[@"duration"] = duration;
}

- (NSString *)onlyHD {
    NSString *value = self.options[@"onlyHD"];
    if (!value) {
        value = @"any";
    }
    return value;
}

- (void)setOnlyHD:(NSString *)onlyHD {
    if (onlyHD) {
        self.options[@"onlyHD"] = onlyHD;
    } else {
        self.options[@"onlyHD"] = @"any";
    }
}

- (NSString *)onlyClosedCaptioned {
    NSString *value = self.options[@"onlyClosedCaptioned"];
    if (!value) {
        value = @"any";
    }
    return value;
}

- (void)setOnlyClosedCaptioned:(NSString *)onlyClosedCaptioned {
    if (onlyClosedCaptioned) {
        self.options[@"onlyClosedCaptioned"] = onlyClosedCaptioned;
    } else {
        self.options[@"onlyClosedCaptioned"] = @"any";
    }
}

- (NSString *)onlyLiveStream {
    return self.options[@"onlyLiveStream"];
}

- (void)setOnlyLiveStream:(NSString *)onlyLiveStream {
    if (onlyLiveStream) {
        self.options[@"onlyLiveStream"] = onlyLiveStream;
    }
}

- (NSString *)plistFilePath {

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:kSearchOptionsPlistFileName];

    return path;
}

#pragma mark - public methods

- (void)save {
    [self.options writeToFile:[self plistFilePath] atomically:YES];
    _optionsChanged = YES;
}

- (void)restore {
    self.options = [NSMutableDictionary dictionaryWithContentsOfFile:[self plistFilePath]];
}

- (void)clearDirtyFlag {
    _optionsChanged = NO;
}

- (NSIndexPath *)indexPathForField:(NSString *)field atSection:(NSInteger)_section {

    if (!field) return nil;

    NSInteger row = NSNotFound, section = _section;
    NSArray *options = [PRPreferences searchOptions];
    NSArray *arr = options[_section];

    for (NSInteger i = 0; i < arr.count; i++) {

        NSDictionary *dict = arr[i];
        if ([dict.allValues containsObject:field]) {
            row = i;
            break;
        }
    }

    return (row != NSNotFound && section != -1) ? [NSIndexPath indexPathForRow:row inSection:section] : nil;
}

@end
