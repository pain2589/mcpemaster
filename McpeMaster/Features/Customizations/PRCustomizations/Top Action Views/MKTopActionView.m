/*============================================================================
 PROJECT: McpeMaster
 FILE:    MKTopActionView.m
 AUTHOR:  Khoai Nguyen
 DATE:    3/23/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "MKTopActionView.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
#define kTitleLabelTag      99

/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/
@interface MKTopActionView () <UITableViewDataSource, UITableViewDelegate>
@property(nonatomic, copy) TopActionViewCompleteHandler completeHandler;
@property(weak, nonatomic) UIView *overlayView;
@property(weak, nonatomic) UITableView *tableView;
@end

@implementation MKTopActionView

- (void)setData:(NSArray *)data {
    _data = data;
    [self.tableView reloadData];
}

- (void)initComponents {

    /* add overlay view */
    UIView *overlayView = [[UIView alloc] initWithFrame:self.bounds];
    overlayView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    overlayView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTouchOnBackground:)];
    [overlayView addGestureRecognizer:tapGesture];
    [self addSubview:overlayView];
    self.overlayView = overlayView;

    /* add tableView */
    UITableView *tableView = [[UITableView alloc] initWithFrame:self.bounds style:UITableViewStylePlain];
    tableView.rowHeight = 44;
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.separatorColor = [UIColor clearColor];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.scrollEnabled = NO;
    tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth;

    [self addSubview:tableView];
    self.tableView = tableView;

    /* force hide */
    [self hide:NO];
}

- (void)dealloc {
    self.completeHandler = nil;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [self initComponents];
        self.data = kFilterActions;
    }
    return self;
}

+ (id)sharedMKTopActionView {

    static dispatch_once_t predicate;
    static MKTopActionView *instance = nil;

    dispatch_once(&predicate, ^{
        instance = [[self alloc] init];
    });

    return instance;
}

#if (!__has_feature(objc_arc))

- (id)retain {
    
    return self;
}

- (unsigned)retainCount {
    return UINT_MAX;  //denotes an object that cannot be released
}

- (oneway void)release {
    //do nothing
}

- (id)autorelease {
    
    return self;
}
#endif

- (BOOL)isShowing {
    return self.superview != nil;
}

- (void)showAtPoint:(CGPoint)point completeHandler:(TopActionViewCompleteHandler)completeHandler {
    [self showAtPoint:point onView:nil completeHandler:completeHandler];
}

- (void)showAtPoint:(CGPoint)point onView:(UIView *)view completeHandler:(TopActionViewCompleteHandler)completeHandler {
    self.completeHandler = completeHandler;

    if (view) {
        self.frame = view.frame;
        [view addSubview:self];
    } else {
        // Default add to UIWindow
        self.frame = [UIScreen mainScreen].bounds;

        UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
        if (!self.superview) {
            [keyWindow addSubview:self];
        }
        else {
            [self bringSubviewToFront:keyWindow];
        }
    }

    CGRect frame = self.frame;
    frame.origin.x = point.x;
    frame.origin.y = point.y;
    frame.size.height = CGRectGetHeight(view ? view.bounds : [UIScreen mainScreen].bounds) - point.y;
    self.frame = frame;

    CGRect scaleFrame = _tableView.frame;
    scaleFrame.size.height = _tableView.rowHeight * self.data.count;

    [UIView animateWithDuration:kAnimationDuration animations:^{
        self->_tableView.frame = scaleFrame;
        self.overlayView.alpha = 1;
    }                completion:^(BOOL finished) {
    }];
}

- (void)hide:(BOOL)animated {

    __weak typeof(self) weakSelf = self;
    if (animated) {

        CGRect scaleFrame = _tableView.frame;
        scaleFrame.size.height = 0;

        [UIView animateWithDuration:kAnimationDuration animations:^{
            self->_tableView.frame = scaleFrame;
            weakSelf.overlayView.alpha = 0;
        }                completion:^(BOOL finished) {
            if (weakSelf.superview) {
                [weakSelf removeFromSuperview];
            }
        }];
    }
    else {
        CGRect scaleFrame = _tableView.frame;
        scaleFrame.size.height = 0;
        _tableView.frame = scaleFrame;

        if (weakSelf.superview) {
            [weakSelf removeFromSuperview];
        }
    }
}

- (UILabel *)titleLabelInCell:(UITableViewCell *)cell {
    UILabel *titleLabel = (UILabel *) [cell viewWithTag:kTitleLabelTag];
    return titleLabel;
}

- (void)didTouchOnBackground:(id)sender {
    [self hide:YES];
}

#pragma mark - UITableViewDataSource & UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *ID = @"actionCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];

    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];

        /* add top line */
        CGRect rect = tableView.bounds;
        rect.size.height = 0.5;

        UIView *topLine = [[UIView alloc] initWithFrame:rect];
        topLine.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        topLine.backgroundColor = UIColorFromRGB(0xD62727);
        [cell.contentView addSubview:topLine];

        /* add label in center */
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:cell.bounds];
        titleLabel.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        titleLabel.tag = kTitleLabelTag;
        titleLabel.font = FONT_GEOMETRIA(14);
        titleLabel.textColor = UIColorFromRGB(0xD62727);
        titleLabel.textAlignment = NSTextAlignmentCenter;
        [cell.contentView addSubview:titleLabel];
    }

    UILabel *label = [self titleLabelInCell:cell];
    label.text = _data[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];

    if (_completeHandler != NULL) {
        _completeHandler(indexPath.row, _data[indexPath.row]);
    }

    [self hide:YES];
}

@end
