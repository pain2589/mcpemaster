/*============================================================================
 PROJECT: SkinEditor
 FILE:    SEBodyViewController.h
 AUTHOR:  Khoai Nguyen
 DATE:    10/17/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SEPartViewController.h"
#import "RLMSkinObject.h"

/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   SEBodyViewController
 =============================================================================*/

@class SESkinObject;
@interface SESkinViewController : SEPartViewController
@property (nonatomic, strong) SESkinObject *skinObject;
@property (nonatomic, strong) RLMSkinObject *realmSkinObject;
@end
