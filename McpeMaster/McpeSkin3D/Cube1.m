//
//  Cube1.m
//  GirlSkin
//
//  Created by apple on 2016. 7. 12..
//  Copyright (c) 2016년 SWORD. All rights reserved.
//

#import "Cube1.h"


@implementation Cube1
- (void) initCube: (float) sx
               sy: (float) sy
               sz: (float) sz
          offsetx: (float) offsetx
          offsety: (float) offsety
          offsetz: (float) offsetz
       step_value: (float) step_value
       rot_axis_x: (float) rot_axis_x
       rot_axis_y: (float) rot_axis_y
       rot_axis_z: (float) rot_axis_z
        max_anlge: (float) max_angle
        min_angle: (float) min_angle
      step_value1: (float) step_value1
       max_angle1: (float) max_angle1
        min_angle: (float) min_angle1
       isFixAngle: (bool) isFixAngle
           fixDir: (float) fixDir
{
    // mac init
    [self initCubePure:sx sy:sy sz:sz offsetx:offsetx offsety:offsety offsetz:offsetz];
    
    // second init
    main_step_value = step_value;
    main_angle_axis[0] = rot_axis_x;
    main_angle_axis[1] = rot_axis_y;
    main_angle_axis[2] = rot_axis_z;
    main_max_angle = max_angle;
    main_min_angle = min_angle;
    
    sub_step_value = step_value1;
    sub_max_angle = max_angle1;
    sub_min_angle = min_angle1;
    
    isFixOneAxis = isFixAngle;
    fixedDir = fixDir;
}

- (void) initCubePure: (float) sx
                   sy: (float) sy
                   sz: (float) sz
              offsetx: (float) offsetx
              offsety: (float) offsety
              offsetz: (float) offsetz
{
    // java init
    for(int i = 0; i < 3; i++) mScale[i] = mOffset[i] = main_angle_axis[i] = sub_angle_axis[i] = 0.0f;
    mMainAngle = 0.0f;
    main_step_value = -0.15f;
    main_max_angle = 3.0f;
    main_min_angle = -3.0f;
    
    isFixOneAxis = TRUE;
    fixedDir = 1.0f;
    
    float tmpVertices[] = {
        -1.0f, -1.0f,  1.0f, 	// 0
        1.0f, -1.0f,  1.0f, 	// 1
        1.0f,  1.0f,  1.0f, 	// 2
        -1.0f,  1.0f,  1.0f, 	// 3
        -1.0f, -1.0f, -1.0f, 	// 4
        1.0f, -1.0f, -1.0f, 	// 5
        1.0f,  1.0f, -1.0f, 	// 6
        -1.0f,  1.0f, -1.0f,  	// 7
        -1.0f,  0.0f,  1.0f,  	// 8
        1.0f,  0.0f, 1.0f,  	// 9
        1.0f,  0.0f, -1.0f,  	// 10
        -1.0f,  0.0f, -1.0f  	// 11
    };
    
    memcpy(vertices, tmpVertices, sizeof(tmpVertices));
    
    int tmpface_indecies[] = {
        0, 8, 9, 1, 	// front face.
        8, 3, 2, 9, 	// front face1.
        3, 7, 6, 2, 	// top face.
        1, 5, 4, 0, 	// bottom face.
        1, 9, 10, 5,	// left face.
        9, 2, 6, 10, 	// left face1.
        4, 11, 8, 0, 	// right face.
        11, 7, 3, 8, 	// right face1.
        5, 10, 11, 4, 	// back face.
        10, 6, 7, 11 	// back face1.
    };
    memcpy(face_indecies, tmpface_indecies, sizeof(tmpface_indecies));
    
    float tmpnormal_vertices[] = {
        // front face. -- 0
        0.0f, 0.0f, 1.0f,  // 0
        0.0f, 0.0f, 1.0f, 	// 3
        0.0f, 0.0f, 1.0f, 	// 2
        0.0f, 0.0f, 1.0f, 	// 1
        // front face1.
        0.0f, 0.0f, 1.0f,  // 0
        0.0f, 0.0f, 1.0f, 	// 3
        0.0f, 0.0f, 1.0f, 	// 2
        0.0f, 0.0f, 1.0f, 	// 1
        // top face.
        0.0f, 1.0f, 0.0f, 	// 3
        0.0f, 1.0f, 0.0f, 	// 7
        0.0f, 1.0f, 0.0f, 	// 6
        0.0f, 1.0f, 0.0f, 	// 2
        // bottom face. --- 3
        0.0f, -1.0f, 0.0f, // 4
        0.0f, -1.0f, 0.0f, // 0
        0.0f, -1.0f, 0.0f, // 1
        0.0f, -1.0f, 0.0f, // 5
        // left face --4
        1.0f, 0.0f, 0.0f, 	// 1
        1.0f, 0.0f, 0.0f, 	// 2
        1.0f, 0.0f, 0.0f,	// 6
        1.0f, 0.0f, 0.0f, 	// 5
        // left face1
        1.0f, 0.0f, 0.0f, 	// 1
        1.0f, 0.0f, 0.0f, 	// 2
        1.0f, 0.0f, 0.0f,	// 6
        1.0f, 0.0f, 0.0f, 	// 5
        // right face -- 6
        -1.0f, 0.0f, 0.0f, // 7
        -1.0f, 0.0f, 0.0f, // 3
        -1.0f, 0.0f, 0.0f, // 0
        -1.0f, 0.0f, 0.0f, // 4
        // right face1
        -1.0f, 0.0f, 0.0f, // 7
        -1.0f, 0.0f, 0.0f, // 3
        -1.0f, 0.0f, 0.0f, // 0
        -1.0f, 0.0f, 0.0f, // 4
        // back face -- 8
        0.0f, 0.0f, -1.0f, // 5
        0.0f, 0.0f, -1.0f, // 6
        0.0f, 0.0f, -1.0f, // 7
        0.0f, 0.0f, -1.0f, // 4
        // back face1
        0.0f, 0.0f, -1.0f, // 5
        0.0f, 0.0f, -1.0f, // 6
        0.0f, 0.0f, -1.0f, // 7
        0.0f, 0.0f, -1.0f, // 4
    };
    
    memcpy(normal_vertices, tmpnormal_vertices, sizeof(tmpnormal_vertices));
    memcpy(mNormalVertexBuffer, tmpnormal_vertices, sizeof(tmpnormal_vertices));
    
    mTextureBuffers = [NSMutableArray arrayWithCapacity:10];

    // mac init
    mScale[0] = sx; mScale[1] = sy; mScale[2] = sz;
    mOffset[0] = offsetx; mOffset[1] = offsety; mOffset[2] = offsetz;
}

- (float *) AddTextures: (float*) texCoords
					nSize: (int) nSize
{
	float *lpData = calloc(sizeof(float), nSize);
	memcpy(lpData, texCoords, sizeof(float) * nSize);

    [mTextureBuffers addObject:[NSNumber numberWithLongLong:(long long)lpData]];
    return lpData;
}


- (void) ClearAllTextures
{
	// all free
	for(int i = 0; i < [mTextureBuffers count]; i++){
        NSNumber *lpNItem = [mTextureBuffers objectAtIndex:i];
        float *lpData = (float*) [lpNItem longLongValue];
        free(lpData);
	}
	// array free
	[mTextureBuffers removeAllObjects];
}

- (void) draw: (bool) isRunning
{
		// drawing.
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		
		// change status.
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_NORMAL_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			
		glNormalPointer(GL_FLOAT, 0, mNormalVertexBuffer);

		for(int i = 0; i < sizeof(face_indecies) / sizeof(float); i++ ){
			int vertex_num = face_indecies[i];
            
			float y = vertices[vertex_num * 3 + 1] * mScale[1] / 2.0f;
			float z = vertices[vertex_num * 3 + 2] * mScale[2] / 2.0f; 	
			float y1, z1;
 			
 			if( isRunning ){
 				// when running, will rotate nodes.
 				if( vertex_num == 0 || vertex_num == 1 || vertex_num == 4 || vertex_num == 5 ){ 	 
 					
	 				y1 = (float) ( y * cos(mSubAngle / 180.0f * 2 * PI) + z * sin(mSubAngle / 180.0f * 2 * PI) );
 	 				z1 = (float) ( z * cos(mSubAngle / 180.0f * 2 * PI) - y * sin(mSubAngle / 180.0f * 2 * PI) );
 	 				
 					if(isFixOneAxis){
 						if( fixedDir > 0 && mSubAngle > 0 ){
 							y1 = y;
 							z1 = z;
 						}
 						else if(fixedDir < 0 && mSubAngle < 0 ){
 							y1 = y;
 							z1 = z;
 						}
 					}
 				}
 				else{
 					y1 = y;
 					z1 = z;
 				} 				
 			}
 			else{
 				y1 = y;
 				z1 = z;
 			}
 			
 			// put vertex buffer.
 			mVertexBuffer[i * 3] = vertices[vertex_num * 3] * mScale[0] / 2.0f;
 			mVertexBuffer[i * 3 + 1] = y1;
 			mVertexBuffer[i * 3 + 2] = z1;
		}
				
		glVertexPointer(3, GL_FLOAT, 0, mVertexBuffer);
		glPushMatrix();
		glTranslatef(mOffset[0], mOffset[1], mOffset[2]);
		
		if( isRunning ){
			glTranslatef(0.0f, mScale[1]/4.0f * 3.0f, 0.0f);
			glRotatef(mMainAngle, main_angle_axis[0], main_angle_axis[1], main_angle_axis[2]);
			glTranslatef(0.0f, -mScale[1]/4.0f * 3.0f, 0.0f);
			
			mMainAngle += main_step_value;
			if( mMainAngle >= main_max_angle ){
				main_step_value = main_step_value * -1.0f;
				mMainAngle = main_max_angle;
			}
			else if( mMainAngle <= main_min_angle ){
				main_step_value = main_step_value * -1.0f;
				mMainAngle = main_min_angle;
			}
			
			mSubAngle += sub_step_value;
			if( mSubAngle >= sub_max_angle ){
				sub_step_value = sub_step_value * -1.0f;
				mSubAngle = sub_max_angle;
			}
			else if( mSubAngle <= sub_min_angle ){
				sub_step_value = sub_step_value * -1.0f;
				mSubAngle = sub_min_angle;
			}
		}		
		
		// drawing.
		for( int j = 0; j < [mTextureBuffers count]; j++ ){
			float* tex_buf = (float *)[[mTextureBuffers objectAtIndex:j] longLongValue];
			glTexCoordPointer(2, GL_FLOAT,0, tex_buf);
			for(int i = 0; i <10; i++ ){				
				glDrawArrays(GL_TRIANGLE_FAN, i * 4, 4);
			}
		}				
		
		glPopMatrix();
		
		// disable.
		glDisable(GL_BLEND);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisableClientState(GL_VERTEX_ARRAY);
}
@end
