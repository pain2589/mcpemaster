/*============================================================================
 PROJECT: McpeMaster
 FILE:    NSString+MKAdditions.h
 AUTHOR:  Khoai Nguyen
 DATE:    2/8/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import <Foundation/Foundation.h>

/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   NSString_MKAdditions
 =============================================================================*/

@interface NSString (MKAdditions)
- (BOOL)isBlank;

- (NSString *)md5Encrypted;

- (NSString *)sha1Encrypted;
@end
