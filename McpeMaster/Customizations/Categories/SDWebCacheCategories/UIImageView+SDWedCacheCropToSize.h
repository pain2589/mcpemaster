/*============================================================================
 PROJECT: SportLocker
 FILE:    UIImageView+SDWedCacheCropToSize.h
 AUTHOR:  Khoai Nguyen
 DATE:    12/27/14
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import <UIKit/UIKit.h>

/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   UIImageView_SDWedCacheCropToSize
 =============================================================================*/

@interface UIImageView (SDWedCacheCropToSize)

- (void)setImageWithURL:(NSURL *)url
       placeholderImage:(UIImage *)placeholder
        withContentMode:(UIViewContentMode)mode;

- (void)setImageWithURL:(NSURL *)url
       placeholderImage:(UIImage *)placeholder
        andCropToBounds:(CGRect)bounds;

@end
