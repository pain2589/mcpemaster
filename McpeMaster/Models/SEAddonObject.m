/*============================================================================
 PROJECT: SkinEditor
 FILE:    SETextureObject.m
 AUTHOR:  Nguyen Minh Khoai
 DATE:    10/18/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SEAddonObject.h"

@interface SEAddonObject()
@end

@implementation SEAddonObject

+ (instancetype)newObject {
    return [[self alloc] init];
}

@end
