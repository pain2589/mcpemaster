/*============================================================================
 PROJECT: McpeMaster
 FILE:    PRListViewController.m
 AUTHOR:  Khoai Nguyen
 DATE:    3/16/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "PRListViewController.h"
#import "SVPullToRefresh.h"
#import "PRListViewLogicDelegate.h"
#import "PRListViewDataSource.h"
#import "PRListViewConfiguration.h"
#import "PRIndicatorViewDelegate.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@interface PRListViewController ()
@property(nonatomic, strong) UILabel *noResultView;
@end

@implementation PRListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (UILabel *)noResultView {
    if (!_noResultView) {
        _noResultView = [[UILabel alloc] initWithFrame:self.collectionView.bounds];
        _noResultView.text = @"No Result";
        _noResultView.textAlignment = NSTextAlignmentCenter;
    }
    else {
        _noResultView.frame = self.collectionView.bounds;
    }
    return _noResultView;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
}

- (void)onReloadedButtonHandler:(id)sender {
    [self fetchMoreData];
}

#pragma mark - Overridden

- (void)loadConfigurations {
    self.indicatorDelegate = [[PRIndicatorViewDelegate alloc] init];
}

- (void)stopForLoadingData {
    [super stopForLoadingData];

    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *) self.collectionViewLayout;
    PRListViewLogicDelegate *logicDelegate = (PRListViewLogicDelegate *) self.logics;

    /* check offline internet */
    if ([logicDelegate hasNoConnection]) {

        CGFloat height = 0;
        CGFloat width = CGRectGetWidth(self.collectionView.frame);

        if ([logicDelegate.data count] == 0) {
            height = CGRectGetHeight(self.collectionView.frame);
        }
        else {

            height = 80;
            if ([logicDelegate needToReload]) {
                /* Show no connection error message */
                //[PRUtils showAlertWithMessage:@"The Internet connection appears to be offline."];
                NSLog(@"The Internet connection appears to be offline.");

                if (logicDelegate.hasMoreData == NO) {
                    height = 0;
                }
            }
        }
        layout.footerReferenceSize = (CGSize) {width, height};
    }
    else {

        layout.footerReferenceSize = CGSizeZero;

        /* handle special case, show/hide "No Result" view */
        if ([logicDelegate hasNoResult]) {
            self.collectionView.backgroundView = self.noResultView;
        }
        else {
            self.collectionView.backgroundView = nil;
        }
    }
}
@end

