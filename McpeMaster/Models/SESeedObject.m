/*============================================================================
 PROJECT: SkinEditor
 FILE:    SEMapObject.m
 AUTHOR:  Nguyen Minh Khoai
 DATE:    10/18/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SESeedObject.h"

@interface SESeedObject()
@end

@implementation SESeedObject

+ (instancetype)newObject {
    return [[self alloc] init];
}

@end
