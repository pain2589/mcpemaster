/*============================================================================
 PROJECT: McpeMaster
 FILE:    CategoryLeftMenuCell.m
 AUTHOR:  Ngoc Tam Nguyen
 DATE:    12/28/14
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "CategoryLeftMenuCell.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/


@implementation CategoryLeftMenuCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    UIColor *color = self.iconImageView.backgroundColor;

    [super setSelected:selected animated:animated];

    self.iconImageView.backgroundColor = color;
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    UIColor *color = self.iconImageView.backgroundColor;
    [super setHighlighted:highlighted animated:animated];
    self.iconImageView.backgroundColor = color;
}

#pragma mark - Override methods

- (void)setIsSelected:(BOOL)isSelected {
    _isSelected = isSelected;

    // Configure the view for the selected state
    if (self.isSelected) {
        self.categoryTitleLabel.textColor = UIColorFromRGB(0xcc3131);
    } else {
        self.categoryTitleLabel.textColor = UIColorFromRGB(0xC9C8C8);
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];

    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

@end
