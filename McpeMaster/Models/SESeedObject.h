/*============================================================================
 PROJECT: SkinEditor
 FILE:    SESeedObject.h
 AUTHOR:  Nguyen Minh Khoai
 DATE:    10/18/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SEBaseObject.h"
/*============================================================================
 MACRO
 =============================================================================*/

@class RLMSeedObject;
@interface SESeedObject : SEBaseObject
@property (nonatomic, strong) RLMSeedObject *referencedRealmObject;

+ (instancetype)newObject;

@end
