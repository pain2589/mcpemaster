/*============================================================================
 PROJECT: SuperCleaner
 FILE:    MKAsset.m
 AUTHOR:  Khoai Nguyen
 DATE:    8/21/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "MKAsset.h"
//#import <CocoaImageHashing/OSImageHashing.h>
#import <Photos/Photos.h>
/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/
@interface MKAsset ()
@property(nonatomic, copy, readwrite) NSString *unit;
@end

@implementation MKAsset

- (unsigned long long)getVideoSizeOfAsset:(id)phAsset {
    if ([phAsset isKindOfClass:[AVURLAsset class]]) {
        AVURLAsset *urlAsset = (AVURLAsset *)phAsset;
        
        NSNumber *size;
        [urlAsset.URL getResourceValue:&size forKey:NSURLFileSizeKey error:nil];
        
        return [size unsignedLongLongValue];
    }
    return 0;
}

- (instancetype)initWithAsset:(PHAsset *)asset data:(NSData *)data type:(MKAssetType)type {
    self = [super init];
    if (self) {
        self.source = asset;
        self.data = data;
        self.type = type;
        
        if ([self.data isKindOfClass:[NSData class]]) {
            self.byteSize = [self.data length];
        }
        else {
            self.byteSize = [self getVideoSizeOfAsset:self.data];
        }
    }
    return self;
}

+ (instancetype)assetWithAsset:(PHAsset *)asset data:(NSData *)data type:(MKAssetType)type {
    return [[self alloc] initWithAsset:asset data:data type:type];
}

/*
 - (BOOL)isEqual:(id)object {
    
    if (!self.source || ![object source]) return NO;
    
    if (_type == MKAssetTypeVideo || _type == MKAssetTypeScreenShotPhoto) {
        NSDate *date1 = [self.source creationDate];
        NSDate *date2 = [[object source] creationDate];
        return [date1 isEqualToDateIgnoringTime:date2];
    }
    else if (_type == MKAssetTypeSimilarPhoto) {
        NSData *data1 = self.data;
        NSData *data2 = [object data];
        
        if (!data1 || !data2) return NO;
        return [[OSImageHashing sharedInstance] compareImageData:data1 to:data2];
    }
    return NO;
}
*/

@end
