//
//  MapViewController.h
//  McpeMaster
//
//  Created by kaka on 2/5/17.
//
//

#import <UIKit/UIKit.h>
#import "ADUFileManager.h"
#import "AFNetworking.h"
#import "MKRealmManager.h"
#import "RLMSeedObject.h"
#import "SESeedObject+Realm.h"
#import "UIAlertController+Utils.h"

@import GoogleMobileAds;
@interface SeedViewController : UIViewController<GADBannerViewDelegate>

@property (strong,nonatomic) GADBannerView *bannerView;

@property(weak, nonatomic) IBOutlet UIImageView *thumbImg;
@property(weak, nonatomic) IBOutlet UIButton *btnCopy;
@property(weak, nonatomic) IBOutlet UIButton *btnFavorite;
@property(weak, nonatomic) IBOutlet UILabel *lbSeedCode;
@property(weak, nonatomic) IBOutlet UITextView *textViewDescription;

@property(strong, nonatomic) NSString *mcpeId;
@property(strong, nonatomic) NSString *strTitle;
@property(strong, nonatomic) NSString *strSeedCode;
@property(strong, nonatomic) NSString *strThumbUrl;
@property(strong, nonatomic) NSString *strMcpeDescription;

@property (nonatomic, strong) SESeedObject *seedObject;
@property (nonatomic, strong) RLMSeedObject *realmSeedObject;

@end
