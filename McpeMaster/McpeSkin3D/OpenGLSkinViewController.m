//
//  OpenGLSkinViewController.m
//  mcpehub
//
//  Created by Kaka on 7/17/16.
//  Copyright © 2016 Kaka. All rights reserved.
//

/****************/
//Optimized Code for show 3D Minecraft Skin, but still have 1 issue: sometimes disappear some parts of Hat, - optimize later
//Đã optimize code show 3d skin, những vẫn bị lúc mất lúc hiện phần Mũ, để optimize sau
//1 số Skin trên server background ko trong suốt, gặp mấy hình này sẽ bị lỗi mất các bộ phận, khi nào phát hiện lỗi đó thì tải SKin về rồi dùng photoshop xóa background rồi upload lại lên server
/****************/

#import "OpenGLSkinViewController.h"
#import "iRate.h"
#import "MBProgressHUD.h"
#import <Photos/PHPhotoLibrary.h>
#import <Photos/PHObject.h>
#import <Photos/PHCollection.h>
#import <Photos/PHAssetCollectionChangeRequest.h>
#import <Photos/PHAssetChangeRequest.h>
#import "Firebase.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@implementation OpenGLSkinViewController{
    MBProgressHUD *HUD;
    UIBarButtonItem *favoriteButton;
    BOOL isReadyRewardVideo;
    BOOL hasJustLoginFB;
    BOOL loginErr;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //Admob Rewarded
    [GADRewardBasedVideoAd sharedInstance].delegate = self;
    if (![[GADRewardBasedVideoAd sharedInstance] isReady]) {
        [self requestRewardedVideo];
    }
    //
    [self initGoogleAdsBanner];
    //
    [FIRAnalytics setScreenName:@"3D_Skin_Preview" screenClass:@"3D_Skin_Preview"];
    // Count on server
    if (![PRAppDelegate isFavoriteView]){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSString *urlString= [NSString stringWithFormat:@"https://mcpemaster.co/mcpehub/counterMcpe.php?type=skin&id=%@",self->_mcpeId];
            NSString *urlUTF8 = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
            NSURL *url = [NSURL URLWithString:urlUTF8];
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            NSURLResponse *response;
            NSError *err = nil;
            [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
            dispatch_async(dispatch_get_main_queue(), ^(void){
                if (err) {
                    NSLog(@"ERROR --> %@", err.localizedDescription);
                }else{
                    NSLog(@"Counter Succeffully id = %@",self->_mcpeId);
                }
            });
        });
    }
    //
    [self setUp3DModel];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (loginErr) {
        loginErr = NO;
        AlertActionCompleteBlock completeBlock = ^(NSUInteger buttonIndex) {
            if (buttonIndex == 1) {
                [self fbLogin];
            }
        };
        [UIAlertController showConfirmPopUpWithTitle:nil
                                             message:@"Login Failed. Please try again!"
                                        buttonTitles:@[@"Cancel", @"Login"]
                                       completeBlock:completeBlock];
    }
    
    if (hasJustLoginFB) {
        self.realmSkinOnlineObject = [self fetchSkinOnlineFromDB];
        [self updateUI];
        hasJustLoginFB = NO;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:UIColorFromRGB(0xE6E6E6)];
    
    //Add BarButton
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithImage:[IBHelper loadImage:@"icon_back"]
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(didTouchOnBackButton:)];
    self.navigationItem.leftBarButtonItem = leftButton;
    
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btnDown"]
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(didTouchOnSaveButton:)];
    [saveButton setTintColor:UIColor.blackColor];
    
    favoriteButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_favorite"]
                                                      style:UIBarButtonItemStylePlain
                                                     target:self
                                                     action:@selector(onFavoriteButton)];
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                          target:nil
                                                                          action:nil];
    if (_isOpenFromSkinEditor) {
        self.navigationItem.rightBarButtonItem = saveButton;
    }else{
        self.navigationItem.rightBarButtonItems = @[saveButton,item,favoriteButton,item];
    }
}

- (void)setUp3DModel {
    self.realmSkinOnlineObject = [self fetchSkinOnlineFromDB];
    //
    [self updateUI];
    
    if (!_skinImg) {
        CGRect rect = [[UIScreen mainScreen] bounds];
        NSString *path32 = [[NSBundle mainBundle] pathForResource:@"64x32" ofType:@"png"];
        _skinImg32 = [[UIImage alloc] initWithContentsOfFile:path32];
        
        //Model 64x32
        _glView32 = [[OpenGLView alloc] initWithFrame:rect skinImage:_skinImg32 animated:NO];
        [self.view addSubview:_glView32];
        
        HUD = [[MBProgressHUD alloc] initWithView:self.parentViewController.view];
        [self.parentViewController.view addSubview:HUD];
        HUD.label.text = @"Loading...";
        [HUD removeFromSuperViewOnHide];
        [HUD showAnimated:YES];
    } else {
        [self show3DModel];
    }
}

//
- (void)onFavoriteButton{
    if ([FBSDKAccessToken currentAccessToken]) {
        // User is logged in
        [FBSDKProfile loadCurrentProfileWithCompletion:
         ^(FBSDKProfile *profile, NSError *error) {
             if (profile) {
                 [self addFavorite];
                 if ([self isSkinFavorite]) {
                     [self syncFavorite:profile.userID action:@"add"];
                 } else {
                     [self syncFavorite:profile.userID action:@"remove"];
                 }
             }
         }];
    } else {
        AlertActionCompleteBlock completeBlock = ^(NSUInteger buttonIndex) {
            if (buttonIndex == 1) {
                [self fbLogin];
            }
        };
        
        [UIAlertController showConfirmPopUpWithTitle:nil
                                             message:@"Please login Facebook to use Favorite function!"
                                        buttonTitles:@[@"Cancel", @"Login"]
                                       completeBlock:completeBlock];
    }
}

- (void)addFavorite {
    self.realmSkinOnlineObject = [self fetchSkinOnlineFromDB];
    if (self.realmSkinOnlineObject){
        [MKRealmManager transactionWithBlock:^(RLMRealm *realm) {
            RLMSkinOnlineObject *object = [self.skinOnlineObject realmObject];
            object.mcpeId = self.realmSkinOnlineObject.mcpeId;
            object.name = self.realmSkinOnlineObject.name;
            object.thumb = self.realmSkinOnlineObject.thumb;
            object.file = self.realmSkinOnlineObject.file;
            object.link = self.realmSkinOnlineObject.link;
            /* add new one */
            if (object) {
                if ([self isSkinFavorite]){
                    object.favorited = NO;
                }else{
                    object.favorited = YES;
                }
            }
            
            object.logTime = self.realmSkinOnlineObject.logTime;
            [realm deleteObject:(RLMObject *)self.realmSkinOnlineObject];
            
            [realm addObject:(RLMObject *)object];
            self.realmSkinOnlineObject = object;
            [self updateUI];
        }];
    }else{
        [MKRealmManager transactionWithBlock:^(RLMRealm *realm) {
            RLMSkinOnlineObject *object = [self.skinOnlineObject realmObject];
            object.mcpeId = self->_mcpeId;
            object.name = self->_mcpeTitle;
            object.thumb = self->_strThumbUrl;
            object.file = self->_strDownloadUrl;
            object.link = [NSURL URLWithString:self->_strDownloadUrl].lastPathComponent.stringByDeletingPathExtension;
            /* add new one */
            if (object) {
                if ([self isSkinFavorite]){
                    object.favorited = NO;
                }else{
                    object.favorited = YES;
                }
            }
            
            object.logTime = self.realmSkinOnlineObject.logTime;
            
            [realm addObject:(RLMObject *)object];
            self.realmSkinOnlineObject = object;
            [self updateUI];
        }];
    }
}

- (void)syncFavorite:(NSString*)userId action:(NSString*)action {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *urlString= [NSString stringWithFormat:@"https://mcpemaster.co/mcpehub/addFavoriteMcpe.php?type=skin&userId=%@&id=%@&action=%@",userId,self->_mcpeId,action];
        NSString *urlUTF8 = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSURL *url = [NSURL URLWithString:urlUTF8];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSURLResponse *response;
        NSError *err = nil;
        [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            if (err) {
                NSLog(@"ERROR --> %@", err.localizedDescription);
            }else{
                NSLog(@"Report Map Succeffully id = %@",self->_mcpeId);
            }
        });
    });
}

- (void)fbLogin {
    FBSDKLoginManager *fbLogin = [[FBSDKLoginManager alloc] init];
    [fbLogin logInWithReadPermissions:@[@"email"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error != nil) {
            [fbLogin logOut];
            self->loginErr = YES;
        } else if (result.isCancelled) {
            //cancelled
            [fbLogin logOut];
        } else {
            //success
            self->hasJustLoginFB = YES;
        }
    }];
}

- (void)updateUI{
    if ([self isSkinFavorite]){
        [favoriteButton setTintColor:[UIColor yellowColor]];
    }else{
        [favoriteButton setTintColor:[UIColor grayColor]];
    }
}

- (void)show3DModel{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        while (!self->_skinImg) {
            
        }
        dispatch_async(dispatch_get_main_queue(), ^(void){
            if (self.glView32) {
                [self->_glView32 stopAnimation];
                [self->_glView32 clearTexture];
                [self->_glView32 removeFromSuperview];
                self->_glView32 = nil;
            }
            CGRect rect = [[UIScreen mainScreen] bounds];
//            rect = CGRectMake(0, 0, 400, 667);
            self->_glView32 = [[OpenGLView alloc] initWithFrame:rect skinImage:self->_skinImg animated:YES];
            [self.view addSubview:self->_glView32];
            
            if (self->HUD) {
                [self->HUD hideAnimated:YES];
            }
        });
    });
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    [_glView32 stopAnimation];
    [_glView32 clearTexture];
    [_glView32 removeFromSuperview];
    _glView32 = nil;
    
    [(PRAppDelegate).glView stopAnimation];
    [(PRAppDelegate).glView clearTexture];
    [(PRAppDelegate).glView removeFromSuperview];
    (PRAppDelegate).glView = nil;
}

- (void)didTouchOnBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didTouchOnSaveButton:(id)sender {
    AlertActionCompleteBlock completeBlock = ^(NSUInteger buttonIndex) {
        if (buttonIndex == 0) {
            //cancel
        } else {
            //Show Alert when install after nth downloads
            int dlTimesInNewVersion = [UsrDfltObj4Key([PRAppDelegate majorVersion]) intValue];
            if ([AppDelegate sharedDelegate].mcpeType == 2508){
//                if (!UDBool4Key(kIsPro)){
//                    if (dlTimesInNewVersion >= [PRAppDelegate mcpeNum] && [PRAppDelegate mcpeNum] != 0){
//                        AlertActionCompleteBlock completeBlock = ^(NSUInteger buttonIndex) {
//                            if (buttonIndex == 0) {
//                                if (self->isReadyRewardVideo) {
//                                    [self showRewardedVideo];
//                                } else {
//                                    //Cancel
//                                }
//                            } else {
//                                [[AppDelegate sharedDelegate].storeService presentAvailablePurchasesFrom:self];
//                            }
//                        };
//                        self->isReadyRewardVideo = [[GADRewardBasedVideoAd sharedInstance] isReady];
//
//                        if (self->isReadyRewardVideo) {
//                            [UIAlertController showConfirmPopUpWithTitle:@"Premium Membership"
//                                                                 message:@"Upgrade Premium Membership for for Remove Ads, Unlimited Maps, Add-ons, Textures and Skins."
//                                                            buttonTitles:@[@"Watch Ads for 1 FREE download",@"Go Pro"]
//                                                           completeBlock:completeBlock];
//                        } else {
//                            [UIAlertController showConfirmPopUpWithTitle:@"Premium Membership"
//                                                                 message:@"Upgrade Premium Membership for for Remove Ads, Unlimited Maps, Add-ons, Textures and Skins."
//                                                            buttonTitles:@[@"Cancel",@"Go Pro"]
//                                                           completeBlock:completeBlock];
//                        }
//
//                        return;
//                    } else {
//                        [self saveSkin];
//                    }
//                } else {
//                    [self saveSkin];
//                }
                [self saveSkin];
            } else if ([AppDelegate sharedDelegate].mcpeType == 512) {
                if (![iRate sharedInstance].ratedAnyVersion) {
                    if (dlTimesInNewVersion >= [PRAppDelegate mcpeNum]  && (PRAppDelegate).mcpeNum != 0){
                        if (![[AppDelegate sharedDelegate].mcpeMess isEqualToString:@""]) {
                            AlertActionCompleteBlock completeBlock = ^(NSUInteger buttonIndex) {
                                if (buttonIndex == 0) {
                                    //cancel
                                } else {
                                    [iRate sharedInstance].ratedThisVersion = YES;
                                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[AppDelegate sharedDelegate].mcpeTitle] options:@{} completionHandler:nil];
                                }
                            };
                            
                            [UIAlertController showConfirmPopUpWithTitle:@"Important!"
                                                                 message:[AppDelegate sharedDelegate].mcpeMess
                                                            buttonTitles:@[@"No, Thanks",@"OK"]
                                                           completeBlock:completeBlock];
                            return;
                        }
                    }
                }
            } else {
                [self saveSkin];
            }
        }
    };
    
    NSString *skinGuide = @"1. Saved Skin to Photos\n2. Open MineCraft App\n3. Go to Skin Setting\n4. Tap 'Choose New Skin' Button\n5. Choose Skin saved in Photos\n6. Tap 'Confirm Button'\n7. Show off new Skin to your friends ^_^";
    [UIAlertController showConfirmPopUpWithTitle:@"Installation Skin"
                                         message:skinGuide
                                    buttonTitles:@[@"Cancel",@"Save"]
                                   completeBlock:completeBlock];
}

- (void)saveSkin {
    //UIImageWriteToSavedPhotosAlbum(_skinImg, nil, nil, nil);
    [[MKPhotoAccessor sharedAccessor] saveImage:self->_skinImg toAlbum:@"MCPESkins" completeHandler:^(BOOL success, NSError * _Nullable error) {
        if (success) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIAlertController showConfirmPopUpWithTitle:@"Success"
                                                     message:@"Saved as new skin in MCPESkins Album"
                                                buttonTitles:@[@"OK"]
                                               completeBlock:nil];
            });
        }
        else {
            NSLog(@"Failed to save image (%@).", error.localizedDescription);
        }
    }];
    int dlTimesInNewVersion = [UsrDfltObj4Key([PRAppDelegate majorVersion]) intValue];
    NSString *dlTimes = [NSString stringWithFormat:@"%d",dlTimesInNewVersion+1];
    UsrDfltSetObjKey(dlTimes, [PRAppDelegate majorVersion]);
}

#pragma mark -
#pragma mark - DB Support

- (SESkinOnlineObject *)skinOnlineObject {
    
    if (!_skinOnlineObject) {
        _skinOnlineObject = [SESkinOnlineObject newObject];
    }
    return _skinOnlineObject;
}

- (BOOL)isSkinFavorite{
    return self.realmSkinOnlineObject.favorited;
}

- (RLMSkinOnlineObject*)fetchSkinOnlineFromDB {
    RLMResults<RLMSkinOnlineObject *> *objects = [[RLMSkinOnlineObject objectsWhere:[NSString stringWithFormat:@"link = '%@'",[NSURL URLWithString:_strDownloadUrl].lastPathComponent.stringByDeletingPathExtension]] sortedResultsUsingProperty:@"logTime" ascending:NO];
    return objects.firstObject;
}

#pragma mark - GAD Banner Ads
- (void)initGoogleAdsBanner{
    if (!self.bannerView)
    {
        self.bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerPortrait];
        [self addBannerViewToView:self.bannerView];
        
        if (IS_IPAD) {
            CGRect frame = self.bannerView.frame;
            frame.origin.x = 20;
            self.bannerView.frame = frame;
        }
        
        self.bannerView.adUnitID = ADMOB_BANNER_ID;
        self.bannerView.rootViewController = self;
        self.bannerView.delegate = self;
        self.bannerView.hidden = TRUE;
        [self.view addSubview:self.bannerView];
        
        GADRequest *request = [GADRequest request];
        //request.testDevices = @[kGADSimulatorID];
        
        //Forward user consent choice
        if (PACConsentInformation.sharedInstance.requestLocationInEEAOrUnknown &&
            PACConsentInformation.sharedInstance.consentStatus == PACConsentStatusNonPersonalized) {
            GADExtras *extras = [[GADExtras alloc] init];
            extras.additionalParameters = @{@"npa": @"1"};
            [request registerAdNetworkExtras:extras];
            NSLog(@"Ads is non-personalized now!!!");
        }
        
        [self.bannerView loadRequest:request];
    }
}

- (void)addBannerViewToView:(UIView *)bannerView {
    bannerView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:bannerView];
    if (@available(iOS 11.0, *)) {
        // In iOS 11, we need to constrain the view to the safe area.
        [self positionBannerViewFullWidthAtBottomOfSafeArea:bannerView];
    } else {
        // In lower iOS versions, safe area is not available so we use
        // bottom layout guide and view edges.
        [self positionBannerViewFullWidthAtBottomOfView:bannerView];
    }
}

#pragma mark - view positioning

- (void)positionBannerViewFullWidthAtBottomOfSafeArea:(UIView *_Nonnull)bannerView NS_AVAILABLE_IOS(11.0) {
    // Position the banner. Stick it to the bottom of the Safe Area.
    // Make it constrained to the edges of the safe area.
    UILayoutGuide *guide = self.view.safeAreaLayoutGuide;
    
    [NSLayoutConstraint activateConstraints:@[
                                              [guide.leftAnchor constraintEqualToAnchor:bannerView.leftAnchor],
                                              [guide.rightAnchor constraintEqualToAnchor:bannerView.rightAnchor],
                                              [guide.bottomAnchor constraintEqualToAnchor:bannerView.bottomAnchor]
                                              ]];
}

- (void)positionBannerViewFullWidthAtBottomOfView:(UIView *_Nonnull)bannerView {
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:bannerView
                                                          attribute:NSLayoutAttributeLeading
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeLeading
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:bannerView
                                                          attribute:NSLayoutAttributeTrailing
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeTrailing
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:bannerView
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view.safeAreaLayoutGuide.bottomAnchor
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1
                                                           constant:0]];
}

#pragma mark - Banner hide and show -

// Hide the banner by sliding down
-(void)hideBanner:(UIView*)banner
{
    banner.hidden = TRUE;
}

// Show the banner by sliding up
-(void)showBanner:(UIView*)banner
{
    banner.hidden = FALSE;
}

#pragma mark - GADBanner delegate methods -

-(void)showAdmobBanner{
    self.bannerView.hidden = FALSE;
}

-(void)hideAdmobBanner{
    self.bannerView.hidden = TRUE;
}

// Called before ad is shown, good time to show the add
- (void)adViewDidReceiveAd:(GADBannerView *)view
{
    NSLog(@"Admob load");
    [self showBanner:self.bannerView];
}

// An error occured
- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error
{
    NSLog(@"Admob error: %@", error);
    [self hideBanner:self.bannerView];
}

//Admob Rewared
- (void)requestRewardedVideo {
    GADRequest *request = [GADRequest request];
    
    //Forward user consent choice
    if (PACConsentInformation.sharedInstance.requestLocationInEEAOrUnknown &&
        PACConsentInformation.sharedInstance.consentStatus == PACConsentStatusNonPersonalized) {
        GADExtras *extras = [[GADExtras alloc] init];
        extras.additionalParameters = @{@"npa": @"1"};
        [request registerAdNetworkExtras:extras];
        NSLog(@"Ads is non-personalized now!!!");
    }
    
    [[GADRewardBasedVideoAd sharedInstance] loadRequest:request
                                           withAdUnitID:ADMOB_REWARDED];
}

- (void)showRewardedVideo {
    [[GADRewardBasedVideoAd sharedInstance] presentFromRootViewController:self];
}

#pragma mark GADRewardBasedVideoAdDelegate implementation

- (void)rewardBasedVideoAdDidReceiveAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad is received.");
}

- (void)rewardBasedVideoAdDidOpen:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Opened reward based video ad.");
}

- (void)rewardBasedVideoAdDidStartPlaying:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad started playing.");
}

- (void)rewardBasedVideoAdDidClose:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad is closed.");
    //self.showVideoButton.hidden = YES;
}

- (void)rewardBasedVideoAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd
   didRewardUserWithReward:(GADAdReward *)reward {
    //NSString *rewardMessage =
    //[NSString stringWithFormat:@"Reward received with currency %@ , amount %lf", reward.type,
    //[reward.amount doubleValue]];
    //NSLog(@"%@", rewardMessage);
    // Reward the user for watching the video.
    //[self earnCoins:[reward.amount integerValue]];
    //self.showVideoButton.hidden = YES;
    
    [self requestRewardedVideo];
    NSLog(@"Rewarded successfully.");
    [self saveSkin];
}

- (void)rewardBasedVideoAdWillLeaveApplication:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad will leave application.");
}

- (void)rewardBasedVideoAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd
    didFailToLoadWithError:(NSError *)error {
    //NSLog(@"Reward based video ad failed to load.");
    //[self requestRewardedVideo];
}

@end
