/*============================================================================
 PROJECT: UpdateFirmwareManager
 FILE:    DRVFileManager.m
 AUTHOR:  Khoai Nguyen
 DATE:    8/28/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "ADUFileManager.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/

#if DEBUG
#define ADUFileManagerLog(fmt, ... ) NSLog( @"<%p %@:(%d)> %@", self, [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, [NSString stringWithFormat:(fmt), ##__VA_ARGS__] )
#else
#define ADUFileManagerLog(fmt, ... )
#endif

/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@interface ADUFileManager()
@property (nonatomic, readonly) NSFileManager *fileManager;
@end

@implementation ADUFileManager

+ (instancetype)sharedManager {
	static dispatch_once_t predicate;
	static ADUFileManager *instance = nil;
	dispatch_once(&predicate, ^{instance = [[self alloc] init];});
	return instance;
}
#if (!__has_feature(objc_arc))

- (id)retain {
    
    return self;
}

- (unsigned)retainCount {
    return UINT_MAX;  //denotes an object that cannot be released
}

- (oneway void)release {
    //do nothing
}

- (id)autorelease {
    
    return self;
}
#endif

- (NSFileManager *)fileManager {
    return [NSFileManager defaultManager];
}

#pragma mark - 
#pragma mark - Private Methods

- (NSString *)documentDirectoryPath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return paths.firstObject;
}

- (BOOL)isExistedFileOrDirectoryAtPath:(NSString *)path {
    if (path.length == 0) return NO; // invalid directory
    ADUFileManagerLog(@"file's directory:%@", path);
    return [self.fileManager fileExistsAtPath:path];
}

#pragma mark -
#pragma mark - Public methods

- (BOOL)isExistedDirectory:(NSString *)directory {
    return [self isExistedFileOrDirectoryAtPath:directory];
}

- (NSString *)createDirectory:(NSString *)path {
    
    if (![self isExistedDirectory:path]) {
        
        NSError *error = nil;
        BOOL isValid = [self.fileManager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:&error];
        
        if (!isValid || error) {
            ADUFileManagerLog(@"ERROR in create directory => %@", error.localizedDescription);
            return nil;
        }
    }
    
    return path;
}

- (NSString *)createDirectoryInDocumentDirectory:(NSString *)directory {
    
    /* get document directory */
    NSString *documentPath = [self documentDirectoryPath];
    
    /* try to create directory path */
    NSString *path = [documentPath stringByAppendingPathComponent:directory];
    
    return [self createDirectory:path];
}

- (BOOL)isExistedFilePath:(NSString *)filePath {
    return [self isExistedFileOrDirectoryAtPath:filePath];
}

- (BOOL)isExistedFile:(NSString *)file inDirectory:(NSString *)directory {
    
    if (![self isExistedDirectory:directory]) return NO;
    
    NSString *filePath = [directory stringByAppendingPathComponent:file];
    return [self isExistedFilePath:filePath];
}

- (BOOL)removeFilePath:(NSString *)filePath {
    
    NSError *error = nil;
    BOOL result = [self.fileManager removeItemAtPath:filePath error:&error];
    
    if (error) {
        ADUFileManagerLog(@"ERROR in remove file: %@", error.localizedDescription);
    }
    return result;
}

- (BOOL)removeFile:(NSString *)file inDirectory:(NSString *)directory {
    
    if (![self isExistedDirectory:directory]) return NO;

    NSString *filePath = [directory stringByAppendingPathComponent:file];
    return [self removeFilePath:filePath];
}

- (BOOL)removeAllFilesInDirectory:(NSString *)directory {
    
    BOOL result = YES;
    
    NSArray *filePaths = [self getFilesInDirectory:directory];
    if (filePaths) {
        for (NSString *filePath in filePaths) {
            
            if (![self removeFilePath:filePath]) {
                result = NO;
                break;
            }
        }
    }
    else {
        result = NO;
    }
    return result;
}

- (NSArray *)getFilePathsInDirectory:(NSString *)directory {
    
    if (![self isExistedDirectory:directory]) return nil;
    
    NSURL *directoryURL = [NSURL fileURLWithPath:directory];
    NSArray *keys = [NSArray arrayWithObject:NSURLIsDirectoryKey];
    
    NSDirectoryEnumerator *enumerator = [self.fileManager enumeratorAtURL:directoryURL
                                               includingPropertiesForKeys:keys
                                                                  options:0
                                                             errorHandler:^(NSURL *url, NSError *error) {
                                                                 // Handle the error.
                                                                 // Return YES if the enumeration should continue after the error.
                                                                 return YES;
                                                             }];
    
    NSMutableArray *filePaths = [NSMutableArray array];
    
    for (NSURL *url in enumerator) {
        
        NSError *error;
        NSNumber *isDirectory = nil;
        
        if (! [url getResourceValue:&isDirectory forKey:NSURLIsDirectoryKey error:&error]) {
            ADUFileManagerLog(@"ERROR in get files in direcory : %@", error.localizedDescription);
        }
        else if (![isDirectory boolValue]) {
            [filePaths addObject:url.path];
        }
    }
    
    return filePaths;
}

- (NSArray *)getFilesInDirectory:(NSString *)directory withExtension:(NSArray *)extensions {
    
    NSArray *files = [self getFilePathsInDirectory:directory];
    
    /* filter file types */
    NSMutableArray *filePaths = nil;
    if (extensions.count > 0) {
        
        NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(id  _Nonnull evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
            ADUFileManagerLog(@"Extension %@ in array: %@", [evaluatedObject pathExtension], extensions);
            return [extensions containsObject:[evaluatedObject pathExtension]];
        }];
        
        NSArray *filterFiles = [files filteredArrayUsingPredicate:predicate];
        filePaths = [NSMutableArray arrayWithArray:filterFiles];
    }
    else {
        filePaths = [NSMutableArray arrayWithArray:files];;
    }
    
    return filePaths;
}

- (NSArray *)getFilesInDirectory:(NSString *)directory {
    return [self getFilesInDirectory:directory withExtension:nil];
}

- (long long)getSizeOfFilePath:(NSString *)filePath {
    if (![self isExistedFilePath:filePath]) return 0;
    
    NSError *error;
    NSDictionary *dictionary = [self.fileManager attributesOfItemAtPath:filePath error:&error];
    if (!error) {
        NSNumber *size = [dictionary objectForKey:NSFileSize];
        return [size unsignedLongLongValue];
    }
    return 0;
}

- (long long)emptySpaceOnDisk {
    
    /* uint64_t totalSpace = 0; */
    uint64_t totalFreeSpace = 0;
    
    NSError *error = nil;
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[self documentDirectoryPath]
                                                                                       error:&error];
    
    if (dictionary) {
        NSNumber *fileSystemSizeInBytes = [dictionary objectForKey: NSFileSystemSize];
        NSNumber *freeFileSystemSizeInBytes = [dictionary objectForKey:NSFileSystemFreeSize];
       
        /* totalSpace = */ [fileSystemSizeInBytes unsignedLongLongValue];
        totalFreeSpace = [freeFileSystemSizeInBytes unsignedLongLongValue];
    } else {
        ADUFileManagerLog(@"Error Obtaining System Memory Info: Domain = %@, Code = %ld", [error domain], (long)[error code]);
    }
    
    return totalFreeSpace;
}

- (BOOL)archiveArray:(NSArray *)arr forKey:(NSString *)key {
    
    if (arr == nil || arr.count == 0 || key.length == 0) return NO;
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:arr];
    if (data) {
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:key];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    return YES;
}

- (NSArray *)unarchiveArrayForKey:(NSString *)key {
    
    if (key.length == 0) return nil;
    
    NSData *serialized = [[NSUserDefaults standardUserDefaults] objectForKey:key];
    return [NSKeyedUnarchiver unarchiveObjectWithData:serialized];
}

- (void)removeArchiveForKey:(NSString *)key {
    if (key.length == 0) return;
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
}

- (BOOL)copyFilePath:(NSString *)filePath toNewPath:(NSString *)newPath {
    if (filePath.length == 0 || newPath.length == 0) return NO;
    
    NSError *error = nil;
    BOOL result = [self.fileManager copyItemAtPath:filePath toPath:newPath error:&error];
    if (error) {
        ADUFileManagerLog(@"Error Copying File: Domain = %@, Code = %ld", [error domain], (long)[error code]);
    }
    return result;
}

- (NSInteger)daysBetweenDate:(NSDate *)fromDateTime andDate:(NSDate *)toDateTime {
    NSDate *fromDate;
    NSDate *toDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&fromDate
                 interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&toDate
                 interval:NULL forDate:toDateTime];
    
    NSDateComponents *difference = [calendar components:NSCalendarUnitDay
                                               fromDate:fromDate
                                                 toDate:toDate
                                                options:0];
    
    return [difference day];
}

- (BOOL)removeFilesInDirectory:(NSString *)directory withGreaterThanDays:(NSInteger)expiredDays {
    
    BOOL result = YES;
    NSArray *filePaths = [self getFilesInDirectory:directory];
    if (filePaths.count > 0) {
        for (NSString *filePath in filePaths) {
           
            NSError *error;
            NSDictionary *dictionary = [self.fileManager attributesOfItemAtPath:filePath error:&error];
            if (!error) {
               
                NSDate *modifiedDate = [dictionary objectForKey:NSFileModificationDate];
                NSInteger days = [self daysBetweenDate:modifiedDate andDate:[NSDate date]];
       
                if (days >= expiredDays) {
                    result = [self removeFilePath:filePath];
                }
            }
        }
    }
    else {
        result = NO;
    }
    return result;
}

@end
