/*============================================================================
 PROJECT: UpdateFirmwareManager
 FILE:    DRVFileManager.h
 AUTHOR:  Khoai Nguyen
 DATE:    8/28/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import <Foundation/Foundation.h>

/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   DRVFileManager
 =============================================================================*/


@interface ADUFileManager : NSObject

+ (instancetype)sharedManager;

/* Methods: 
 @Parameters
 @Description:
 @Return:
 */

- (NSString *)documentDirectoryPath;

- (NSString *)createDirectory:(NSString *)path;

- (NSString *)createDirectoryInDocumentDirectory:(NSString *)directory;

- (BOOL)isExistedDirectory:(NSString *)directory;

- (BOOL)isExistedFile:(NSString *)file inDirectory:(NSString *)directory;

- (BOOL)isExistedFilePath:(NSString *)filePath;

- (BOOL)removeFile:(NSString *)file inDirectory:(NSString *)directory;

- (BOOL)removeFilePath:(NSString *)filePath;

- (BOOL)removeAllFilesInDirectory:(NSString *)directory;

- (long long)getSizeOfFilePath:(NSString *)filePath;

- (long long)emptySpaceOnDisk;

- (BOOL)archiveArray:(NSArray *)arr forKey:(NSString *)key;

- (NSArray *)unarchiveArrayForKey:(NSString *)key;

- (void)removeArchiveForKey:(NSString *)key;

- (NSArray *)getFilesInDirectory:(NSString *)directory withExtension:(NSArray *)extensions;

- (NSArray *)getFilesInDirectory:(NSString *)directory;

- (BOOL)copyFilePath:(NSString *)filePath toNewPath:(NSString *)newPath;

- (BOOL)removeFilesInDirectory:(NSString *)directory withGreaterThanDays:(NSInteger)days;

@end
