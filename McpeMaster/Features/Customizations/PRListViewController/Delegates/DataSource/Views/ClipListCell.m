/*============================================================================
 PROJECT: McpeMaster
 FILE:    ClipListCell.m
 AUTHOR:  Khoai Nguyen
 DATE:    3/18/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "ClipListCell.h"
#import "GRKGradientView.h"
#import "UIButton+WebCache.h"
#import "iRate.h"
#import "OpenGLSkinViewController.h"
#import "SESkinDrawView.h"
#import "SESkinObject.h"
#import "ADUFileManager.h"
#import "MapViewController.h"
#import "TextureViewController.h"
#import "AddonViewController.h"
#import "SeedViewController.h"
#import "ServerViewController.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/

/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@interface ClipListCell ()
@property(weak, nonatomic) IBOutlet UIImageView *preloadSkinImg;
@property(weak, nonatomic) IBOutlet UIImageView *skinThumbView;
@property(weak, nonatomic) IBOutlet UIImageView *coverView;
@property(weak, nonatomic) IBOutlet UILabel *titleLabel;
@property(weak, nonatomic) IBOutlet UILabel *statusLabel;
@property(weak, nonatomic) IBOutlet UILabel *playersLabel;
@property (weak, nonatomic) IBOutlet SESkinDrawView *skinDrawView;

@property(weak, nonatomic) IBOutlet GRKGradientView *gradientView;

@property (nonatomic, strong) UIImage *skinImg;
@property (nonatomic, strong) NSString *mcpeId;
@property (nonatomic, strong) NSString *mcpeType;
@property (nonatomic, strong) NSString *mcpeTitle;
@property (nonatomic, strong) NSString *stringDownloadURL;
@property (nonatomic, strong) NSString *stringThumbURL;
@property (nonatomic, strong) NSString *mcpeDescription;
@property (nonatomic, strong) NSString *seedCode;
@property (nonatomic, strong) NSString *serverIP;
@property (nonatomic, strong) NSString *serverPort;
@property (nonatomic, strong) NSString *serverDescription;
@property (nonatomic, strong) NSString *serverMaxPlayers;
@property (nonatomic, strong) NSString *serverPlayers;
@property (nonatomic, strong) UILongPressGestureRecognizer *longPress;
@property (nonatomic, strong) UITapGestureRecognizer *tapGesture;

@end

@implementation ClipListCell{
    MBProgressHUD *HUD;
    OpenGLSkinViewController *skin3dViewController;
}

- (void)awakeFromNib {
    [super awakeFromNib];

    /* setup gradient view */
    self.gradientView.gradientOrientation = GRKGradientOrientationDown;
    self.gradientView.gradientColors = [NSArray arrayWithObjects:[UIColor clearColor], [UIColor blackColor], nil];
    
    self.longPress.enabled = YES;
    self.tapGes.enabled = YES;
}

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

- (UIImage *) imageWithView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

- (void)bindData:(id)dict {
    //Tiep bind data to cell
    _mcpeType = dict[kMcpeType];
    _mcpeId = dict[kMcpeId];
    _mcpeTitle = dict[kMcpeTitle];
    _stringThumbURL = dict[kMcpeThumb];
    _stringDownloadURL = dict[kMcpeFile];
    
    self.titleLabel.text = _mcpeTitle;
    
    if ([_mcpeType isEqualToString:kMcpeTypeSkin]) {
        self.coverView.hidden = YES;
        //self.skinThumbView.hidden = YES;
        self.gradientView.alpha = 0.3;
        
        _stringDownloadURL = dict[kMcpeFile];
        skin3dViewController = nil;
        _skinImg = nil;
        _skinDrawView.hidden = YES;
        if (dict[kMcpeThumb]) {
            [self.skinThumbView sd_setImageWithURL:[NSURL URLWithString:dict[kMcpeThumb]]
                                  placeholderImage:[UIImage imageNamed:@"store_background"]
                                         completed:^(UIImage *img, NSError *err,SDImageCacheType type,NSURL *imgurl){
                                             if (img) {
                                                 //Draw 2D thumbnail
//                                                 SESkinObject *skinObj = [SESkinObject objectFromImage:img];
//                                                 _skinDrawView.skinObject = skinObj;
//                                                 [_skinDrawView drawSkin];
//                                                 _skinDrawView.hidden = NO;
                                             }
                                             
                                             }];
        }
        if (_stringDownloadURL) {
            [self.preloadSkinImg sd_setImageWithURL:[NSURL URLWithString:_stringDownloadURL]
                                  placeholderImage:[UIImage imageNamed:@"store_background"]
                                         completed:^(UIImage *img, NSError *err,SDImageCacheType type,NSURL *imgurl){
                                             if (!self->skin3dViewController) {
                                                 self->skin3dViewController = [[OpenGLSkinViewController alloc] initWithNibName:@"OpenGLSkinViewController" bundle:nil];
                                             }
                                             if (img) {
                                                 self->skin3dViewController.skinImg = img;
                                             }
                                             }];
        }
    }else{
        if (dict[kMcpeThumb]) {
            [self.coverView sd_setImageWithURL:[NSURL URLWithString:dict[kMcpeThumb]]
                              placeholderImage:[UIImage imageNamed:@"store_background"]];
        }
        if ([_mcpeType isEqualToString:kMcpeTypeServer]) {
            self.statusLabel.text = @"";
            self.playersLabel.text = @"";
            self.statusLabel.hidden = NO;
            self.playersLabel.hidden = NO;
            
            _serverIP = dict[kMcpeServerIP];
            _serverPort = dict[kMcpeServerPort];
            _serverDescription = dict[kMcpeServerDescription];
        }else if ([_mcpeType isEqualToString:kMcpeTypeSeed]){
            _seedCode = dict[kMcpeSeedCode];
            _mcpeDescription = dict[kMcpeDescription];
        }else{
            _mcpeDescription = dict[kMcpeDescription];
        }
    }
}

//TapGesture & LongPressGesture
- (UITapGestureRecognizer *)tapGes{
    if (!_tapGesture) {
        _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
        [self addGestureRecognizer:_tapGesture];
    }
    return _tapGesture;
}

- (UILongPressGestureRecognizer *)longPress {
    
    if (!_longPress) {
        _longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
        [self addGestureRecognizer:_longPress];
    }
    return _longPress;
}

- (void)handleLongPress:(UILongPressGestureRecognizer*)sender {
    if (sender.state == UIGestureRecognizerStateBegan) {
        [self showMenu];
    }
}

- (void)handleTap:(UITapGestureRecognizer*)sender {
    [self showMenu];
}

//Show Menu
- (void)showMenu{
    if ((PRAppDelegate).mcpeNum != 0) {
        (PRAppDelegate).countClick++;
        if ((PRAppDelegate).countClick == 1) {
            (PRAppDelegate).countClick = (PRAppDelegate).mcpeNum;
        }
        if ((PRAppDelegate).countClick%(PRAppDelegate).mcpeNum==0){
            [PRAppDelegate loadInterstitial];
        }
    }
    
    if ([_mcpeType isEqualToString:kMcpeTypeMap]) {
        MapViewController *mapVC = [[MapViewController alloc] initWithNibName:@"MapViewController" bundle:nil];
        mapVC.strTitle = _mcpeTitle;
        mapVC.strThumbUrl = _stringThumbURL;
        mapVC.strDownloadUrl = _stringDownloadURL;
        mapVC.strMcpeDescription = _mcpeDescription;
        mapVC.mcpeId = _mcpeId;
        [self.parentViewController.navigationController pushViewController:mapVC animated:YES];
    }else if ([_mcpeType isEqualToString:kMcpeTypeAddon]){
        AddonViewController *addonVC = [[AddonViewController alloc] initWithNibName:@"AddonViewController" bundle:nil];
        addonVC.strTitle = _mcpeTitle;
        addonVC.strThumbUrl = _stringThumbURL;
        addonVC.strDownloadUrl = _stringDownloadURL;
        addonVC.strMcpeDescription = _mcpeDescription;
        addonVC.mcpeId = _mcpeId;
        [self.parentViewController.navigationController pushViewController:addonVC animated:YES];
    }else if ([_mcpeType isEqualToString:kMcpeTypeTexture]){
        TextureViewController *textureVC = [[TextureViewController alloc] initWithNibName:@"TextureViewController" bundle:nil];
        textureVC.strTitle = _mcpeTitle;
        textureVC.strThumbUrl = _stringThumbURL;
        textureVC.strDownloadUrl = _stringDownloadURL;
        textureVC.strMcpeDescription = _mcpeDescription;
        textureVC.mcpeId = _mcpeId;
        [self.parentViewController.navigationController pushViewController:textureVC animated:YES];
    }else if ([_mcpeType isEqualToString:kMcpeTypeSkin]){
        if (!skin3dViewController) {
            skin3dViewController = [[OpenGLSkinViewController alloc] initWithNibName:@"OpenGLSkinViewController" bundle:nil];
        }
        skin3dViewController.mcpeId = _mcpeId;
        skin3dViewController.mcpeTitle = _mcpeTitle;
        skin3dViewController.strThumbUrl = _stringThumbURL;
        skin3dViewController.strDownloadUrl = _stringDownloadURL;
        //[skin3dViewController show3DModel];
        [self.parentViewController.navigationController pushViewController:skin3dViewController animated:YES];
    }else if ([_mcpeType isEqualToString:kMcpeTypeSeed]){
        SeedViewController *seedVC = [[SeedViewController alloc] initWithNibName:@"SeedViewController" bundle:nil];
        seedVC.strTitle = _mcpeTitle;
        seedVC.strThumbUrl = _stringThumbURL;
        seedVC.strSeedCode = _seedCode;
        seedVC.strMcpeDescription = _mcpeDescription;
        seedVC.mcpeId = _mcpeId;
        [self.parentViewController.navigationController pushViewController:seedVC animated:YES];
    }else if ([_mcpeType isEqualToString:kMcpeTypeServer]){
        ServerViewController *serverVC = [[ServerViewController alloc] initWithNibName:@"ServerViewController" bundle:nil];
        serverVC.strTitle = _mcpeTitle;
        serverVC.strThumbUrl = _stringThumbURL;
        serverVC.strServerIP = _serverIP;
        serverVC.strServerPort = _serverPort;
        serverVC.strServerDescription = _serverDescription;
        serverVC.mcpeId = _mcpeId;
        [self.parentViewController.navigationController pushViewController:serverVC animated:YES];
    }
}

@end
