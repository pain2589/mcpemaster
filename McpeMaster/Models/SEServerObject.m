/*============================================================================
 PROJECT: SkinEditor
 FILE:    SEServerObject.m
 AUTHOR:  Nguyen Minh Khoai
 DATE:    10/18/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SEServerObject.h"

@interface SEServerObject()
@end

@implementation SEServerObject

+ (instancetype)newObject {
    return [[self alloc] init];
}

@end
