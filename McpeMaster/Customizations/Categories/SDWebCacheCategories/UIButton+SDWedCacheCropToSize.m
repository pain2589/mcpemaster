/*============================================================================
 PROJECT: SportLocker
 FILE:    UIButton+SDWedCacheCropToSize.m
 AUTHOR:  Khoai Nguyen
 DATE:    12/27/14
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "UIButton+SDWedCacheCropToSize.h"
#import "UIImageView+SDWedCacheCropToSize.h"

#import "UIButton+WebCache.h"
#import "SDWebImageCompat.h"
#import "SDWebImageDownloader.h"
#import "SDWebImageManager.h"

#import <CommonCrypto/CommonCryptor.h>
#import <CommonCrypto/CommonDigest.h>

#import "UIImage+FX.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@implementation UIButton (SDWedCacheCropToSize)

#pragma mark - Public Methods

- (void)setImageWithURL:(NSURL *)url
       placeholderImage:(UIImage *)placeholder
              andResize:(CGSize)size
        withContentMode:(UIViewContentMode)mode {

//    UIImage *cachedImage = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:[[url absoluteString] md5]];
//    
//    if(cachedImage) {
//        self.imageView.contentMode = mode;
//        [self setImage:cachedImage forState:UIControlStateNormal];
//    }
//    else {
//        
//        [self sd_setImageWithURL:url
//                        forState:UIControlStateNormal
//                placeholderImage:placeholder
//                         options:SDWebImageCacheMemoryOnly|SDWebImageAllowInvalidSSLCertificates
//                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//                           
//                           @autoreleasepool {
//                               
//                               if(image) {
//                                   
//                                   /* reset mode */
//                                   self.contentMode = mode;
//                                   
//                                   /* update image */
//                                   if(image) {
//                                       if(mode == UIViewContentModeScaleAspectFit) {
//                                           image = [image imageScaledToFitSize:size];
//                                       } else {
//                                           image = [image imageScaledToFillSize:size];
//                                       }
//                                   }
//                                   
//                                   /* cache image */
//                                   [[SDImageCache sharedImageCache] storeImage:image
//                                                                        forKey:[[imageURL absoluteString] md5]
//                                                                        toDisk:YES];
//                                   
//                                   /* update UI */
//                                   [self setImage:image forState:UIControlStateNormal];
//                               }
//                           }
//                       }];
//    }
}

- (void)setImageWithURL:(NSURL *)url
       placeholderImage:(UIImage *)placeholder
        andCropToBounds:(CGRect)bounds {

//    UIImage *cachedImage = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:[[url absoluteString] md5]];
//    
//    if(cachedImage) {
//        [self setImage:cachedImage forState:UIControlStateNormal];
//    }
//    else {
//        
//        [self sd_setImageWithURL:url
//                        forState:UIControlStateNormal
//                placeholderImage:placeholder
//                         options:SDWebImageCacheMemoryOnly|SDWebImageAllowInvalidSSLCertificates
//                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//                           
//                           @autoreleasepool {
//                               
//                               if(image) {
//                                   /* update image */
//                                   if(image) {
//                                       image = [image imageCroppedToRect:bounds];
//                                   }
//                                   
//                                   /* cache image */
//                                   [[SDImageCache sharedImageCache] storeImage:image
//                                                                        forKey:[[imageURL absoluteString] md5]
//                                                                        toDisk:YES];
//                                   
//                                   /* update UI */
//                                   [self setImage:image forState:UIControlStateNormal];
//                                   
//                               }
//                           }
//                       }];
//    }
}

@end
