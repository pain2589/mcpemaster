//
//  DefineConstants.h
//  McpeMaster
//
//  Created by Ngoc Tam Nguyen on 12/28/14.
//  Copyright (c) 2014 Khoai Nguyen. All rights reserved.
//

#ifndef McpeMaster_DefineConstants_h
#define McpeMaster_DefineConstants_h

#import "Macros.h"

#define IPHONE_LANDSCAPE_HIDE                   // comment this define to show menu settings

#ifdef IPHONE_LANDSCAPE_HIDE
#define INCLUDE_LANDSCAPE_SETTINGS          0
#else
#define INCLUDE_LANDSCAPE_SETTINGS          1
#endif

/* Define Notification keys */
#define kNotificationSignInGoogleSuccess                @"kNotificationSignInGoogleSuccess"
#define kNotificationLogoutGoogleSuccess                @"kNotificationLogoutGoogleSuccess"
#define kNotificationLeftMenuShouldRefreshContent       @"kNotificationLeftMenuShouldRefreshContent"
#define kNotificationDidEndPlayVideo                    @"kNotificationDidEndPlayVideo"
#define kNotificationDidMoveVideoToPlaylist             @"kNotificationDidMoveVideoToPlaylist"
#define kNotificationDidDeleteYoutubePlaylist           @"kNotificationDidDeleteYoutubePlaylist"
#define kNotificationDidDeleteVideoInPlaylist           @"kNotificationDidDeleteVideoInPlaylist"

#define O_SCREEN_WIDTH                          [[UIScreen mainScreen] bounds].size.width
#define O_SCREEN_HEIGHT                         [[UIScreen mainScreen] bounds].size.height

#define SCREEN_WIDTH                            fixedScreenRect().size.width
#define SCREEN_HEIGHT                           fixedScreenRect().size.height

#define kMaxResultCount                         20
#define kClipCellHeight                         150

/* Define action sheet Slider View */
#define kActionSheetSliderView                  @"SliderCustomView"
#define kNotificationRemoveMiniClip             @"kNotificationRemoveMiniClip"

#define NOT_NULL_OBJ(o)                         ((id)o == [NSNull null] ? nil : o)
#define NOT_NULL_STR(o)                         ((id)o == [NSNull null] || !o ? @"" : o)
#define NOT_NULL_NUM(o)                         ((id)o == [NSNull null] || !o ? [NSNumber numberWithInteger:0] : o)

static NSString *kLikeKey = @"like";
static NSString *kLinkKey = @"link";
static NSString *kThumbnailKey = @"thumnail";
static NSString *kDurationKey = @"duration";
static NSString *kViewCountKey = @"views";
static NSString *kPosterKey = @"poster";
static NSString *kPublishTimeKey = @"publish time";
static NSString *kTitleKey = @"title";


extern NSDictionary *clipDetailInfo(NSNumber *like,
                                    NSString *link,
                                    NSString *duration,
                                    NSString *thumnail,
                                    NSNumber *viewCount,
                                    NSString *poster,
                                    NSString *publishTime,
                                    NSString *title);

typedef NS_ENUM(short, ClipQueryType) {
    ClipQueryTypeVideos = 0,
    ClipQueryTypeChannels,
    ClipQueryTypePlaylists
};

#define kAnimationDuration  0.25
#define kImageScaleFactor   2.5


/* FIXED SCREEN SIZE */
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-function"

static CGRect fixedScreenRect() {
    
    static dispatch_once_t onceToken;
    static CGRect screenRect;
    dispatch_once(&onceToken, ^{
        CGRect rect = [UIScreen mainScreen].bounds;
        CGFloat width = MIN(rect.size.width, rect.size.height);
        CGFloat height = MAX(rect.size.width, rect.size.height);
        screenRect = (CGRect){.origin={0,0}, .size = {width, height}};
    });
    
    return screenRect;
}

#pragma clang diagnostic pop

/*=============================================================================*/
// NSUserDefault Utility
/*=============================================================================*/

extern BOOL UDIsValidKey(NSString *key);

extern void UDSetValue4Key(id value, NSString *key);

extern void UDSetBool4Key(BOOL value, NSString *key);

extern void UDSetInteger4Key(NSInteger value, NSString *key);

extern void UDSetDouble4Key(double value, NSString *key);

extern id UDValue4Key(NSString *key);

extern BOOL UDBool4Key(NSString *key);

extern NSInteger UDInteger4Key(NSString *key);

extern double UDDouble4Key(NSString *key);

//

#define kAllowSignIn 0

#define kCurrentCountryCode @"kCurrentCountryCode"
#define kCurrentCountryName @"kCurrentCountryName"

//
static NSString *kVideoDuration = @"duration";
static NSString *kVideoViewCount = @"viewCount";
static NSString *kVideoChannelTitle = @"chanelTitle";

/* MCPE Keys */
static NSString *kMcpeId = @"mcpeId";
static NSString *kMcpeType = @"type";
static NSString *kMcpeTitle = @"name";
static NSString *kMcpeThumb = @"thumb";
static NSString *kMcpeFile = @"file";
static NSString *kMcpeServerIP = @"serverIP";
static NSString *kMcpeServerPort = @"serverPort";
static NSString *kMcpeServerDescription = @"serverDescription";
static NSString *kMcpeServerErr = @"error";
static NSString *kMcpeServerMaxPlayers = @"maxPlayers";
static NSString *kMcpeServerPlayers = @"players";
static NSString *kMcpeSeedCode = @"seedCode";
static NSString *kMcpeDescription = @"mcpeDescription";

#define kMcpeTypeMap @"map"
#define kMcpeTypeMod @"mod"
#define kMcpeTypeSkin @"skin"
#define kMcpeTypeTexture @"texture"
#define kMcpeTypeAddon @"addon"
#define kMcpeTypeServer @"server"
#define kMcpeTypeSeed @"seed"

//
#define ADMOB_PUBLISHER_ID          @"pub-2672912825222764"
#define ADMOB_APP_ID                @"ca-app-pub-2672912825222764~3168760937"
#define ADMOB_BANNER_ID             @"ca-app-pub-2672912825222764/4645494132"
#define ADMOB_INTERSTITIAL_ID       @"ca-app-pub-2672912825222764/7356138137"
#define ADMOB_REWARDED              @"ca-app-pub-2672912825222764/9546130187"

//
#define kIsPro @"kIsPro"
#define kSkinUrl @"kSkinUrl"
#define kIsRegistered @"kIsRegistered"

//
#define AppID @"1447583700"
#define IAP_ID @"juan.com.dubattery.gopro"
#define IAP_AUTO_RENEWABLE_SUBSCRIPTION @"len.com.mcpemaster.premium"

//use flag for review case
// Logic:
// mcpeType: 3333 - lach luat mode, 2508 - nothing, 512 - increase review, 333 - free trial
// isConsent: 0 - turn off collect user consent, 1 - turn on collect user consent
#define AppFlag 3333

#endif


