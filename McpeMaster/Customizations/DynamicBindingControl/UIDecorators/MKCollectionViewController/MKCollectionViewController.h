/*============================================================================
 PROJECT: McpeMaster
 FILE:    MKCollectionViewController.h
 AUTHOR:  Khoai Nguyen
 DATE:    3/15/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import <UIKit/UIKit.h>
#import "MKCollectionViewCell.h"
#import "MKCollectionReusableView.h"
#import "MKCollectionViewControllerProtocols.h"
#import "MKListViewControllerLogics.h"
#import "MKDynamicBindingDelegate.h"
#import "MKIndicatorViewProtocols.h"

/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 Interface:   MKCollectionViewController
 =============================================================================*/

@protocol MKCollectionViewControllerDelegate, MKCollectionViewControllerDataSource, MKCollectionViewControllerConfiguration;

@interface MKCollectionViewController : UICollectionViewController
@property(nonatomic, strong) id <MKCollectionViewControllerDataSource> dataSource;
@property(nonatomic, strong) id <MKCollectionViewControllerDelegate> delegate;
@property(nonatomic, strong) id <MKCollectionViewControllerConfiguration> configuration;
@property(nonatomic, strong) id <MKCollectionViewControllerLogicDelegate> logics;
@property(nonatomic, strong) id <MKIndicatorViewProtocols> indicatorDelegate;

@property(nonatomic, assign) NSInteger index;
@property(nonatomic, assign) NSInteger offset;
@property(nonatomic, strong) NSMutableDictionary *ads;
/// List of ads remaining to be preloaded.
@property(nonatomic, strong) NSMutableArray<GADBannerView *> *adsToLoad;
/// Mapping of GADBannerView ads to their load state.
@property(nonatomic, strong) NSMutableDictionary<NSString *, NSNumber *> *loadStateForAds;
/// A banner ad is placed in the UITableView once per adInterval.
@property(nonatomic, assign) NSInteger adInterval;

- (void)loadConfigurations;

- (void)waitForLoadingData;

- (void)stopForLoadingData;

- (void)cleanData;

- (void)fetchData;

- (void)fetchMoreData;

- (void)reloadData;

- (void)pullToReload;

- (void)insertAdView;

@end
