/*============================================================================
 PROJECT: SkinEditor
 FILE:    SEBodyPartViewController.h
 AUTHOR:  Nguyen Minh Khoai
 DATE:    10/19/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SEPartViewController.h"
#import "SESkinObject.h"

/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   SEBodyPartViewController
 =============================================================================*/


@interface SEBodyPartViewController : SEPartViewController

/* UI components */
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

/* Logical components */
@property (nonatomic, copy) NSString *partImagedName;
@property (nonatomic, copy) NSString *partTitle;
@property (nonatomic, strong) SESkinBodyPartObject *partObject;

@end
