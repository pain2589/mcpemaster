/*============================================================================
 PROJECT: McpeMaster
 FILE:    PRListViewController.h
 AUTHOR:  Khoai Nguyen
 DATE:    3/16/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "MKCollectionViewController.h"

/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   PRListViewController
 =============================================================================*/


@interface PRListViewController : MKCollectionViewController
@end

@interface PRSearchVideoViewController : PRListViewController
@end

@interface PRSearchChannelViewController : PRListViewController
@end

@interface PRSearchPlayListViewController : PRListViewController
@end

#pragma mark - Controller for Account's Subscriptions

@interface PRSubscriptionVideoViewController : PRListViewController

@end

@interface PRSubscriptionChannelViewController : PRListViewController

@end
