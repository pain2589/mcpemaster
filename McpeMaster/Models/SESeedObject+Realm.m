/*============================================================================
 PROJECT: SkinEditor
 FILE:    SESeedObject+Realm.m
 AUTHOR:  Khoai Nguyen
 DATE:    11/13/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SESeedObject+Realm.h"
#import "RLMSeedObject.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@implementation SESeedObject (Realm)

- (RLMSeedObject *)realmObject {
    
    RLMSeedObject *object = [[RLMSeedObject alloc] init];
    object.type = @"seed";
    object.logTime = [[NSDate date] timeIntervalSince1970];
    
    return object;
}

+ (SESeedObject *)fromRealmObject:(RLMSeedObject *)realmObject {
    
    if (!realmObject) return nil;
    
    if (realmObject.name.length > 0) {
        SESeedObject *object = [SESeedObject newObject];
        return object;
    }
    
    return nil;
}

@end
