/*============================================================================
 PROJECT: McpeMaster
 FILE:    MKListViewDelegateDecorator.m
 AUTHOR:  Khoai Nguyen
 DATE:    3/16/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "PRListViewLogicDelegate.h"
#import "MKCollectionViewController.h"
#import "UIScrollView+CustomizedPullToRefresh.h"
#import "MKRealmManager.h"
#import "RLMMapObject.h"
#import "SEMapObject+Realm.h"
#import "RLMAddonObject.h"
#import "SEAddonObject+Realm.h"
#import "RLMTextureObject.h"
#import "SETextureObject+Realm.h"
#import "RLMSkinOnlineObject.h"
#import "SESkinOnlineObject+Realm.h"
#import "RLMSeedObject.h"
#import "SESeedObject+Realm.h"
#import "RLMServerObject.h"
#import "SEServerObject+Realm.h"


/*============================================================================
 PRIVATE MACRO
 =============================================================================*/

/*============================================================================
 Interface:   PRListViewDelegate
 =============================================================================*/

@interface PRListViewLogicDelegate ()
@property(nonatomic, readwrite) BOOL hasMoreData;
@property(nonatomic, readwrite) BOOL loadedAllRecords;
@property(nonatomic, assign) BOOL requesting;

@end

@implementation PRListViewLogicDelegate
@synthesize data, isReloadedData;

#pragma mark - Properties

- (NSMutableArray *)data {
    if (!data) {
        data = [[NSMutableArray alloc] init];
    }
    return data;
}

- (void)loadDataInCollectionController:(MKCollectionViewController *)controller
                         completeBlock:(LoadDataCompleteBlock)completeBlock {
    /* will be overridden in subclasses */
}

#pragma mark - Delegates

- (void)clearDataInController:(MKCollectionViewController *)controller {
    [self.data removeAllObjects];
}

- (void)loadDataInCollectionViewControllerAtFirstTime:(MKCollectionViewController *)controller {
    if (self.isReloadedData == NO) {
        self.isReloadedData = YES;

        /* load data here */
        [self reloadDataInCollectionViewController:controller];
    }
}

- (void)reloadDataInCollectionViewController:(MKCollectionViewController *)controller {
    [self forceReloadDataInCollectionViewController:controller];
}

- (void)forceReloadDataInCollectionViewController:(MKCollectionViewController *)controller {

    /* reset all parameters */
    self.nextPageToken = nil;
    self.needToReload = YES;

    /* call webservice to reload data */
    [self loadDataInCollectionViewController:controller];
}

- (void)loadMoreDataInCollectionViewController:(MKCollectionViewController *)controller {
    self.needToReload = NO;
    [self loadDataInCollectionViewController:controller];
}

- (void)updateController:(MKCollectionViewController *)controller
    disablePullToRefresh:(BOOL)disablePullToRefresh
   disableInfiniteScroll:(BOOL)disableInfiniteScroll
             withMessage:(NSString*)message{

    [controller.collectionView stopPullToRefreshAndShowInfiniteView:self.hasMoreData];
    [controller.collectionView disablePullToRefresh:disablePullToRefresh];
    [controller.collectionView disableInfiniteScrolling:disableInfiniteScroll];

    [controller stopForLoadingData];
    [controller reloadData];
    _requesting = NO;
}

- (void)loadDataInCollectionViewController:(MKCollectionViewController *)controller {
    if (_requesting) return;

    _requesting = YES;
    _hasNoConnection = NO;

    if (![PRAppDelegate connected]) {
        _hasNoConnection = YES;
        [self updateController:controller
          disablePullToRefresh:([self.data count] == 0)
         disableInfiniteScroll:(_hasNoConnection & !_hasMoreData)
                   withMessage:nil];
        return;
    } else {
        // Hide no internet conection view
        [controller stopForLoadingData];
    }
    
    if ([self.data count] == 0 || (self.hasNoConnection && [self.data count] == 0)) {
        [controller waitForLoadingData];
    }

    __weak typeof(self) weakSelf = self;
    [self loadDataInCollectionController:controller
                           completeBlock:^(id response, id error) {

                               __strong typeof(self) strongSelf = weakSelf;

                               if (!error) {

                                   if (response && ![response isKindOfClass:[NSNull class]]) {
                                       if (strongSelf.needToReload) {
                                           strongSelf.needToReload = NO;
                                           [strongSelf.data removeAllObjects];
                                       }
                                       [strongSelf.data addObjectsFromArray:response];
                                       
                                       if (![PRAppDelegate isFavoriteView]){
                                           [controller insertAdView];
                                       }

                                       //Tiep remove gioi han max result count de load more
                                       //if (strongSelf.nextPageToken == nil || [response count] < kMaxResultCount) {
                                       if (strongSelf.nextPageToken == nil) {
                                           strongSelf.hasMoreData = NO;
                                       }
                                       else {
                                           strongSelf.hasMoreData = YES;
                                       }

                                       strongSelf.hasNoResult = NO;
                                   }
                                   else {
                                       /* show no result */
                                       strongSelf.hasNoResult = ([strongSelf.data count] == 0);
                                       strongSelf.hasMoreData = NO;
                                   }
                               }else {
                                   self->_errMsg = [error localizedDescription];
                                   self->_hasNoConnection = YES;
                                   [self updateController:controller
                                     disablePullToRefresh:([self.data count] == 0)
                                    disableInfiniteScroll:(self->_hasNoConnection & !self->_hasMoreData)
                                              withMessage:self->_errMsg];
                                   return;
                               }

                               /* adjust loading views */
                               [strongSelf updateController:controller
                                       disablePullToRefresh:strongSelf.hasNoResult
                                      disableInfiniteScroll:(!strongSelf.hasMoreData | strongSelf.hasNoResult)
                                                withMessage:nil];
                           }];
}

- (id)collectionController:(MKCollectionViewController *)controller dataAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.row < [data count]) {
        return data[indexPath.row];
    }
    return nil;
}

@end

/*============================================================================
 Interface:   PRSearchPlayListViewLogicDelegate
 =============================================================================*/
@implementation PRVideoListViewLogicDelegate

- (void)loadDataInCollectionController:(MKCollectionViewController *)controller
                         completeBlock:(LoadDataCompleteBlock)completeBlock {
    if ([_selectedCategory.value isEqualToString:@"FavoriteMap"]){
        [self fetchFavoriteMapFromDB];
        completeBlock(self.favoriteMaps,nil);
    }else if ([_selectedCategory.value isEqualToString:@"FavoriteAddon"]){
        [self fetchFavoriteAddonFromDB];
        completeBlock(self.favoriteAddons,nil);
    }else if ([_selectedCategory.value isEqualToString:@"FavoriteTexture"]){
        [self fetchFavoriteTextureFromDB];
        completeBlock(self.favoriteTextures,nil);
    }else if ([_selectedCategory.value isEqualToString:@"FavoriteServer"]){
        [self fetchFavoriteServerFromDB];
        completeBlock(self.favoriteServers,nil);
    }else if ([_selectedCategory.value isEqualToString:@"FavoriteSeed"]){
        [self fetchFavoriteSeedFromDB];
        completeBlock(self.favoriteSeeds,nil);
    }else if ([_selectedCategory.value isEqualToString:@"FavoriteSkin"]){
        [self fetchFavoriteSkinOnlineFromDB];
        completeBlock(self.favoriteSkins,nil);
    }else if ([_selectedCategory.value isEqualToString:@"SearchMap"] ||
              [_selectedCategory.value isEqualToString:@"SearchAddon"] ||
              [_selectedCategory.value isEqualToString:@"SearchTexture"] ||
              [_selectedCategory.value isEqualToString:@"SearchSkin"] ||
              [_selectedCategory.value isEqualToString:@"SearchSeed"] ||
              [_selectedCategory.value isEqualToString:@"SearchServer"]){
        if (!self.nextPageToken) {
            self.nextPageToken = @"1";
        }
        //Tiep return list of video here
        [self getDataWithKeyword:[PRAppDelegate mcpeSearchKeyword] page:[self.nextPageToken intValue]
                completeBlock:^(NSError *err,NSData* dataResponse,NSString* nextPageToken){
                    if (!err) {
                        self.nextPageToken = nextPageToken;
                        NSError *error = nil;
                        id list = [NSJSONSerialization JSONObjectWithData:dataResponse options:NSJSONReadingAllowFragments error:&error];
                        
                        if (error) {
                            NSLog(@"ERROR --> %@", error.localizedDescription);
                            completeBlock(nil, error);
                        }
                        else {
                            [controller insertAdView];
                            completeBlock(list, error);
                        }
                    }else{
                        NSLog(@"ERROR --> %@", err.localizedDescription);
                        completeBlock(nil, err);
                    }
                }];
    }else{
        if (!self.nextPageToken) {
            self.nextPageToken = @"1";
        }
        //Tiep return list of video here
        [self getDataWithPage:[self.nextPageToken intValue]
                completeBlock:^(NSError *err,NSData* dataResponse,NSString* nextPageToken){
                    if (!err) {
                        self.nextPageToken = nextPageToken;
                        NSError *error = nil;
                        id list = [NSJSONSerialization JSONObjectWithData:dataResponse options:NSJSONReadingAllowFragments error:&error];
                        
                        if (error) {
                            NSLog(@"ERROR --> %@", error.localizedDescription);
                            completeBlock(nil, error);
                        }
                        else {
                            completeBlock(list, error);
                        }
                    }else{
                        NSLog(@"ERROR --> %@", err.localizedDescription);
                        completeBlock(nil, err);
                    }
                }];
    }
}

- (void)getDataWithPage:(int)page
          completeBlock:(void (^)(NSError *err, NSData *dataResponse, NSString *nextPageToken))completeBlock{
    //Tiep Load Data From Server
    NSString *urlString= [NSString stringWithFormat:@"https://mcpemaster.co/mcpehub/getMcpe%@.php?page=%d&v=%@",_selectedCategory.value,page,[PRAppDelegate majorVersion]];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *urlUTF8 = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSURL *url = [NSURL URLWithString:urlUTF8];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSURLResponse *response;
        NSError *err = nil;
        NSData *dataResponse = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            if (err) {
                NSLog(@"ERROR --> %@", err.localizedDescription);
                completeBlock(err,nil,nil);
            }else{
                completeBlock(err,dataResponse,[NSString stringWithFormat:@"%d",page+1]);
            }
        });
    });
}

- (void)getDataWithKeyword:(NSString*)keyword page:(int)page
          completeBlock:(void (^)(NSError *err, NSData *dataResponse, NSString *nextPageToken))completeBlock{
    //Tiep Load Data From Server
    NSString *urlString= [NSString stringWithFormat:@"https://mcpemaster.co/mcpehub/getMcpe%@.php?k=%@&page=%d&v=%@",_selectedCategory.value,keyword,page,[PRAppDelegate majorVersion]];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *urlUTF8 = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSURL *url = [NSURL URLWithString:urlUTF8];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSURLResponse *response;
        NSError *err = nil;
        NSData *dataResponse = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            if (err) {
                NSLog(@"ERROR --> %@", err.localizedDescription);
                completeBlock(err,nil,nil);
            }else{
                completeBlock(err,dataResponse,[NSString stringWithFormat:@"%d",page+1]);
            }
        });
    });
}

//fetch DB

- (void)fetchFavoriteMapFromDB {
    RLMResults<RLMMapObject *> *objects = [[RLMMapObject objectsWhere:@"favorited = YES"] sortedResultsUsingProperty:@"logTime" ascending:NO];
    
    NSMutableArray *favoriteMaps = [NSMutableArray new];
    
    if (objects.count > 0) {
        for (NSUInteger i = 0; i < objects.count; i++) {
            RLMMapObject *object = [objects objectAtIndex:i];
            [favoriteMaps addObject:object];
        }
    }
    
    self.favoriteMaps = [[NSMutableArray alloc] init];
    [self.favoriteMaps addObjectsFromArray:favoriteMaps];
}

- (void)fetchFavoriteAddonFromDB {
    RLMResults<RLMAddonObject *> *objects = [[RLMAddonObject objectsWhere:@"favorited = YES"] sortedResultsUsingProperty:@"logTime" ascending:NO];
    
    NSMutableArray *favoriteAddons = [NSMutableArray new];
    
    if (objects.count > 0) {
        for (NSUInteger i = 0; i < objects.count; i++) {
            RLMAddonObject *object = [objects objectAtIndex:i];
            [favoriteAddons addObject:object];
        }
    }
    
    self.favoriteAddons = [[NSMutableArray alloc] init];
    [self.favoriteAddons addObjectsFromArray:favoriteAddons];
}

- (void)fetchFavoriteTextureFromDB {
    RLMResults<RLMTextureObject *> *objects = [[RLMTextureObject objectsWhere:@"favorited = YES"] sortedResultsUsingProperty:@"logTime" ascending:NO];
    
    NSMutableArray *favoriteTextures = [NSMutableArray new];
    
    if (objects.count > 0) {
        for (NSUInteger i = 0; i < objects.count; i++) {
            RLMTextureObject *object = [objects objectAtIndex:i];
            [favoriteTextures addObject:object];
        }
    }
    
    self.favoriteTextures = [[NSMutableArray alloc] init];
    [self.favoriteTextures addObjectsFromArray:favoriteTextures];
}

- (void)fetchFavoriteSkinOnlineFromDB {
    RLMResults<RLMSkinOnlineObject *> *objects = [[RLMSkinOnlineObject objectsWhere:@"favorited = YES"] sortedResultsUsingProperty:@"logTime" ascending:NO];
    
    NSMutableArray *favoriteSkins = [NSMutableArray new];
    
    if (objects.count > 0) {
        for (NSUInteger i = 0; i < objects.count; i++) {
            RLMSkinOnlineObject *object = [objects objectAtIndex:i];
            [favoriteSkins addObject:object];
        }
    }
    
    self.favoriteSkins = [[NSMutableArray alloc] init];
    [self.favoriteSkins addObjectsFromArray:favoriteSkins];
}

- (void)fetchFavoriteSeedFromDB {
    RLMResults<RLMSeedObject *> *objects = [[RLMSeedObject objectsWhere:@"favorited = YES"] sortedResultsUsingProperty:@"logTime" ascending:NO];
    
    NSMutableArray *favoriteSeeds = [NSMutableArray new];
    
    if (objects.count > 0) {
        for (NSUInteger i = 0; i < objects.count; i++) {
            RLMSeedObject *object = [objects objectAtIndex:i];
            [favoriteSeeds addObject:object];
        }
    }
    
    self.favoriteSeeds = [[NSMutableArray alloc] init];
    [self.favoriteSeeds addObjectsFromArray:favoriteSeeds];
}

- (void)fetchFavoriteServerFromDB {
    RLMResults<RLMServerObject *> *objects = [[RLMServerObject objectsWhere:@"favorited = YES"] sortedResultsUsingProperty:@"logTime" ascending:NO];
    
    NSMutableArray *favoriteServers = [NSMutableArray new];
    
    if (objects.count > 0) {
        for (NSUInteger i = 0; i < objects.count; i++) {
            RLMServerObject *object = [objects objectAtIndex:i];
            [favoriteServers addObject:object];
        }
    }
    
    self.favoriteServers = [[NSMutableArray alloc] init];
    [self.favoriteServers addObjectsFromArray:favoriteServers];
}

@end
