/*============================================================================
 PROJECT: McpeMaster
 FILE:    PRSearchNavigationBarDelegate.m
 AUTHOR:  Khoai Nguyen
 DATE:    4/5/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "PRNavigationBarDelegate.h"
#import "ConcreteNavigationBarDelegates.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/
@interface PRSearchNavigationBarDelegate ()
@end

@implementation PRSearchNavigationBarDelegate

#pragma mark -

- (void)resetSearchController {

}

- (void)setUpNavigationBar {

    [self.delegate setUpNavigationBar];

    //Tiep hide search bar
//    UIBarButtonItem *barButtonItem = createNavigationBarItem(self, @{kBarItemImage : @"icon_search",
//            kBarItemSelector : @"didTouchOnBarButtonItem:"});
//    self.ownedViewController.navigationItem.rightBarButtonItems = @[barButtonItem];
}

- (void)didTouchOnBarButtonItem:(UIButton *)button {

    if (self.onTouchedCallback != nil && self.onTouchedCallback(PRNavigationBarTypeSearch) == NO) {
        return;
    }
}

@end

/*============================================================================
 Interface:   PRMenuNavigationBarDelegate
 =============================================================================*/
@implementation PRMenuNavigationBarDelegate

- (void)didTouchOnBarButtonItem:(UIButton *)button {

    if (self.onTouchedCallback != nil && self.onTouchedCallback(MKNavigationBarButtonTypeMenu) == NO) {
        return;
    }

    ECSlidingViewController *slideVC = self.ownedViewController.slidingViewController;
    BOOL anchorLeftShowing = (slideVC.currentTopViewPosition == ECSlidingViewControllerTopViewPositionAnchoredRight);

    if (anchorLeftShowing) {
        [slideVC resetTopViewAnimated:YES];
    } else {
        [slideVC anchorTopViewToRightAnimated:YES];
    }
}

@end

/*============================================================================
 Interface:   PRChooseCountryNavigationBarDelegate
 =============================================================================*/
@interface PRChooseCountryNavigationBarDelegate ()
@property(strong, nonatomic) NavigationCoutryView *navigationTitleView;
@end

@implementation PRChooseCountryNavigationBarDelegate

- (void)refreshWithTitle:(NSString *)title {
    //remove country picker
//    CountryPickerView *picker = [CountryPickerView sharedCountryPickerView];
//    NSString *countryImage = [NSString stringWithFormat:@"CountryPicker.bundle/%@", picker.currentCountryCode];
//    [_navigationTitleView setupCountryWithImage:[IBHelper loadImage:countryImage]
//                                       category:title];
//    [_navigationTitleView setupCountryWithImage:[IBHelper loadImage:nil]
//                                       category:title];
    
    //self.ownedViewController.navigationItem.title = title;
    self.ownedViewController.navigationController.navigationBar.titleTextAttributes = @{NSFontAttributeName: FONT_GEOMETRIA(18), NSForegroundColorAttributeName: [UIColor darkGrayColor]};
}

#pragma mark -

- (void)setUpNavigationBar {

    [self.delegate setUpNavigationBar];

    if (!_navigationTitleView) {

//        self.navigationTitleView = [IBHelper loadViewNib:@"NavigationCountryView"];
//        self.navigationTitleView.frame = self.ownedViewController.navigationController.navigationBar.bounds;
//        self.navigationTitleView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
//
//        __weak typeof(self) weakSelf = self;
//        self.navigationTitleView.onChooseCountryCallBack = ^{
//
//            /* show picker view */
//            if (weakSelf.onTouchedCallback != nil && weakSelf.onTouchedCallback(PRNavigationBarTypePickCountry) == NO) {
//                return;
//            }
//        };
//
//        /* replace titleView of navigation bar with choosing country view */
//        self.ownedViewController.navigationItem.titleView = self.navigationTitleView;
    }
}

@end

/*============================================================================
 Interface:   PRClipNavigationBarDelegate
 =============================================================================*/
@implementation PRClipNavigationBarDelegate

- (id)initWithOwnViewController:(UIViewController *)ownViewController
                callBackHandler:(OnTouchedNavigationBarCallBack)handler {

    /* create left menu delegate */
    PRMenuNavigationBarDelegate *leftMenuDelegate = [[PRMenuNavigationBarDelegate alloc] initWithOwnViewController:ownViewController];
    leftMenuDelegate.onTouchedCallback = handler;

    /* create search menu delegate */
    PRSearchNavigationBarDelegate *searchBarDelegate = [[PRSearchNavigationBarDelegate alloc] initWithDelegate:leftMenuDelegate andOwnViewController:ownViewController];
    searchBarDelegate.onTouchedCallback = handler;

    /* create title view delegate */
    PRChooseCountryNavigationBarDelegate *titleViewDelegate = [[PRChooseCountryNavigationBarDelegate alloc] initWithDelegate:searchBarDelegate
                                                                                                        andOwnViewController:ownViewController];
    titleViewDelegate.onTouchedCallback = handler;

    return (id) titleViewDelegate;
}

@end

/*============================================================================
 Interface:   PRSettingNavigationBarDelegate
 =============================================================================*/
@implementation PRSettingNavigationBarDelegate

- (id)initWithOwnViewController:(UIViewController *)ownViewController
                callBackHandler:(OnTouchedNavigationBarCallBack)handler {

    /* create left menu delegate */
    PRMenuNavigationBarDelegate *leftMenuDelegate = [[PRMenuNavigationBarDelegate alloc] initWithOwnViewController:ownViewController];
    leftMenuDelegate.onTouchedCallback = handler;

    return (id) leftMenuDelegate;
}

@end

/*============================================================================
 Interface:   PRSearchAndBackNavigationBarDelegate
 =============================================================================*/
@implementation PRSearchAndBackNavigationBarDelegate

- (id)initWithOwnViewController:(UIViewController *)ownViewController
                callBackHandler:(OnTouchedNavigationBarCallBack)handler {

    /* create left menu delegate */
    MKLeftBackNavigationBarDelegate *leftBackDelegate = [[MKLeftBackNavigationBarDelegate alloc] initWithOwnViewController:ownViewController];
    leftBackDelegate.onTouchedCallback = handler;

    /* create search menu delegate */
    PRSearchNavigationBarDelegate *searchBarDelegate = [[PRSearchNavigationBarDelegate alloc] initWithDelegate:leftBackDelegate andOwnViewController:ownViewController];
    searchBarDelegate.onTouchedCallback = handler;

    return (id) searchBarDelegate;
}
@end

/*============================================================================
 Interface:   PRSearchAndMenuNavigationBarDelegate
 =============================================================================*/
@implementation PRSearchAndMenuNavigationBarDelegate

- (id)initWithOwnViewController:(UIViewController *)ownViewController
                callBackHandler:(OnTouchedNavigationBarCallBack)handler {

    /* create left menu delegate */
    PRMenuNavigationBarDelegate *leftMenuDelegate = [[PRMenuNavigationBarDelegate alloc] initWithOwnViewController:ownViewController];
    leftMenuDelegate.onTouchedCallback = handler;

    /* create search menu delegate */
    PRSearchNavigationBarDelegate *searchBarDelegate = [[PRSearchNavigationBarDelegate alloc] initWithDelegate:leftMenuDelegate andOwnViewController:ownViewController];
    searchBarDelegate.onTouchedCallback = handler;

    return (id) searchBarDelegate;
}

@end
