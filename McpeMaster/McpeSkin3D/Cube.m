//
//  Cube.m
//  GirlSkin
//
//  Created by apple on 2016. 7. 12..
//  Copyright (c) 2016년 SWORD. All rights reserved.
//

#import "Cube.h"


@implementation Cube
- (void) initCube: (float) sx
               sy: (float) sy
               sz: (float) sz
          offsetx: (float) offsetx
          offsety: (float) offsety
          offsetz: (float) offsetz
       step_value1: (float) step_value1
       rot_axis_x: (float) rot_axis_x
       rot_axis_y: (float) rot_axis_y
       rot_axis_z: (float) rot_axis_z
        max_anlge1: (float) max_angle1
        min_angle1: (float) min_angle1

{
    // mac init
    [self initCubePure:sx sy:sy sz:sz offsetx:offsetx offsety:offsety offsetz:offsetz];
    
    // second init
    step_value = step_value1;
    angle_axis[0] = rot_axis_x;
    angle_axis[1] = rot_axis_y;
    angle_axis[2] = rot_axis_z;
    max_angle = max_angle1;
    min_angle = min_angle1;
}

- (void) initCubePure: (float) sx
                   sy: (float) sy
                   sz: (float) sz
              offsetx: (float) offsetx
              offsety: (float) offsety
              offsetz: (float) offsetz
{
    // java init
    for(int i = 0; i < 3; i++) mScale[i] = mOffset[i] = angle_axis[i] = 0.0f;
    mAngle = 0.0f;
    step_value = -0.15f;
    max_angle = 3.0f;
    min_angle = -3.0f;
    
    float tmpface_vertices[] = {
        // front face.
        -1.0f, -1.0f,  1.0f, // 0
        -1.0f,  1.0f,  1.0f, // 3
        1.0f,  1.0f,  1.0f, // 2
        1.0f, -1.0f,  1.0f, // 1
        // top face.
        -1.0f,  1.0f,  1.0f, // 3
        -1.0f,  1.0f, -1.0f, // 7
        1.0f,  1.0f, -1.0f, // 6
        1.0f,  1.0f,  1.0f, // 2
        // bottom face.
        1.0f, -1.0f,  1.0f, // 1
        1.0f, -1.0f, -1.0f, // 5
        -1.0f, -1.0f, -1.0f, // 4
        -1.0f, -1.0f,  1.0f, // 0
        // left face
        1.0f, -1.0f,  1.0f, // 1
        1.0f,  1.0f,  1.0f, // 2
        1.0f,  1.0f, -1.0f, // 6
        1.0f, -1.0f, -1.0f, // 5
        // right face
        -1.0f, -1.0f, -1.0f, // 4
        -1.0f,  1.0f, -1.0f, // 7
        -1.0f,  1.0f,  1.0f, // 3
        -1.0f, -1.0f,  1.0f, // 0
        
        // back face
        1.0f, -1.0f, -1.0f, // 5
        1.0f,  1.0f, -1.0f, // 6
        -1.0f,  1.0f, -1.0f, // 7
        -1.0f, -1.0f, -1.0f, // 4
    };
    memcpy(face_vertices, tmpface_vertices, sizeof(tmpface_vertices));
    memcpy(mVertexBuffer, tmpface_vertices, sizeof(tmpface_vertices));
    
    int tmpnormal_vertices[] = {
        // front face.
        0.0f, 0.0f, 1.0f,  // 0
        0.0f, 0.0f, 1.0f, 	// 3
        0.0f, 0.0f, 1.0f, 	// 2
        0.0f, 0.0f, 1.0f, 	// 1
        // top face.
        0.0f, 1.0f, 0.0f, 	// 3
        0.0f, 1.0f, 0.0f, 	// 7
        0.0f, 1.0f, 0.0f, 	// 6
        0.0f, 1.0f, 0.0f, 	// 2
        // bottom face.
        0.0f, -1.0f, 0.0f, // 4
        0.0f, -1.0f, 0.0f, // 0
        0.0f, -1.0f, 0.0f, // 1
        0.0f, -1.0f, 0.0f, // 5
        // left face
        1.0f, 0.0f, 0.0f, 	// 1
        1.0f, 0.0f, 0.0f, 	// 2
        1.0f, 0.0f, 0.0f,	// 6
        1.0f, 0.0f, 0.0f, 	// 5
        // right face
        -1.0f, 0.0f, 0.0f, // 7
        -1.0f, 0.0f, 0.0f, // 3
        -1.0f, 0.0f, 0.0f, // 0
        -1.0f, 0.0f, 0.0f, // 4
        // back face
        0.0f, 0.0f, -1.0f, // 5
        0.0f, 0.0f, -1.0f, // 6
        0.0f, 0.0f, -1.0f, // 7
        0.0f, 0.0f, -1.0f, // 4
    };
    memcpy(normal_vertices, tmpnormal_vertices, sizeof(tmpnormal_vertices));
    memcpy(mNormalVertexBuffer, tmpnormal_vertices, sizeof(tmpnormal_vertices));
  
    mTextureBuffers = [NSMutableArray arrayWithCapacity:10];
    
    // mac init
    mScale[0] = sx; mScale[1] = sy; mScale[2] = sz;
    mOffset[0] = offsetx; mOffset[1] = offsety; mOffset[2] = offsetz;
    
    for( int i = 0; i < 24; i++ ){
        mVertexBuffer[i * 3] 		= mVertexBuffer[i * 3] * mScale[0] / 2.0f;
        mVertexBuffer[i * 3 + 1] 	= mVertexBuffer[i * 3 + 1] * mScale[1] / 2.0f;
        mVertexBuffer[i * 3 + 2] 	= mVertexBuffer[i * 3 + 2] * mScale[2] / 2.0f;
    }
}

- (float *) AddTextures: (float*) texCoords
                  nSize: (int) nSize
{
    float *lpData = calloc(sizeof(float), nSize);
    memcpy(lpData, texCoords, sizeof(float) * nSize);
    
    [mTextureBuffers addObject:[NSNumber numberWithLongLong:(long long)lpData]];
    return lpData;
}

- (void) ClearAllTextures
{
    // all free
    for(int i = 0; i < [mTextureBuffers count]; i++){
        NSNumber *lpNItem = [mTextureBuffers objectAtIndex:i];
        float *lpData = (float*) [lpNItem longLongValue];
        free(lpData);
    }
    // array free
    [mTextureBuffers removeAllObjects];
}

- (void)draw:(BOOL)isRunning
{
    // drawing.
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    // change status.
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, mVertexBuffer);
    glNormalPointer(GL_FLOAT, 0, mNormalVertexBuffer);
    
    glPushMatrix();
    glTranslatef(mOffset[0], mOffset[1], mOffset[2]);
    
    if( isRunning ){
        glRotatef(mAngle, angle_axis[0], angle_axis[1], angle_axis[2]);
        
        mAngle += step_value;
        if( mAngle >= max_angle ){
            step_value = step_value * -1.0f;
            mAngle = max_angle;
        }
        else if( mAngle <= min_angle ){
            step_value = step_value * -1.0f;
            mAngle = min_angle;
        }
    }
    
    // drawing.
    for( int j = 0; j < [mTextureBuffers count]; j++ ){
        float* tex_buf = (float *)[[mTextureBuffers objectAtIndex:j] longLongValue];
        glTexCoordPointer(2, GL_FLOAT,0, tex_buf);

        for(int i = 0; i < 6; i++ ){
            glDrawArrays(GL_TRIANGLE_FAN, i * 4, 4);
        }
    }				
    
    glPopMatrix();
    
    // disable.
    glDisable(GL_BLEND);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);
}
@end
