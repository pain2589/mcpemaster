/*============================================================================
 PROJECT: McpeMaster
 FILE:    OfflineConnectionView.m
 AUTHOR:  Khoai Nguyen
 DATE:    4/17/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "OfflineConnectionView.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@interface OfflineConnectionView ()

@end

@implementation OfflineConnectionView

- (void)awakeFromNib {
    [super awakeFromNib];

    self.noConnectionTitleLabel.text = @"The Internet connection appears to be offline.";
}

- (IBAction)onTouchedReloadButton:(id)sender {
    if (_reloadEventHandler != nil) {
        _reloadEventHandler();
    }
}

@end
