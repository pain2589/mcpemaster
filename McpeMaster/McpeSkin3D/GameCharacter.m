#import "GameCharacter.h"

@implementation GameCharacter

- (void) initCharacter{
    isRunning = _isAnimated;
    float tmpmRotate[] = {0.0f, 0.0f, 0.0f};
    memcpy(mRotate, tmpmRotate, sizeof(tmpmRotate));
    
    float tmprotate_step[] = {5.0f, 5.0f, 5.0f};
    memcpy(rotate_step, tmprotate_step, sizeof(tmprotate_step));
    
    float imageHeight = 64;
    if (_skinImg){
        if (_skinImg.size.width/_skinImg.size.height == 1){
            //for skin template 64x64
            imageHeight = 64;
        }else if (_skinImg.size.width/_skinImg.size.height == 2){
            //for skin template 64x32
            imageHeight = 32;
        }
    }
    mHead = [Head alloc]; [mHead initCube:imageHeight];
    mBody = [Body alloc]; [mBody initCube:imageHeight];
    mLArm = [LArm alloc]; [mLArm initCube:imageHeight];
    mRArm = [RArm alloc]; [mRArm initCube:imageHeight];
    mLLeg = [LLeg alloc]; [mLLeg initCube:imageHeight];
    mRLeg = [RLeg alloc]; [mRLeg initCube:imageHeight];
    mHat =  [Hat alloc];
    [mHat initCube:imageHeight];
}

- (void) draw{
    // translate and rotate object.
    glRotatef(mRotate[0], 1.0f, 0, 0);
    glRotatef(mRotate[1], 0.0f, 1.0f, 0);
    glRotatef(mRotate[2], 0.0f, 0, 1.0f);
    
    // draw parts of character.
    [mBody draw: isRunning];
    [mHead draw: isRunning];
    [mLArm draw: isRunning];
    [mRArm draw: isRunning];
    [mLLeg draw: isRunning];
    [mRLeg draw: isRunning];
    [mHat draw:isRunning];
}

- (void) SetRotate: (float)ax
                  :(float) ay
                  :(float) az
{
    mRotate[0] = ax;
    mRotate[1] = ay;
    mRotate[2] = az;
}

- (void) AddRotate: (float)ax
                  :(float) ay
                  :(float) az
{
    mRotate[0] += ax;
    mRotate[1] += ay;
    mRotate[2] += az;
}

- (void) SetRotateStep: (float)dx
                    :(float) dy
{
    if(fabsf(dx) >= 1.0f){
        mRotate[1] += rotate_step[1] * (dx >= 0.0f? 1.0f:-1.0f);
    }
    if(fabsf(dy) >= 1.0f){
        mRotate[0] += rotate_step[0] * (dy >= 0.0f? 1.0f:-1.0f);
    }
}

- (void) SetRunning: (bool) isRun
{
    isRunning = isRun;
}

- (void)clearTexture{
    [mBody ClearAllTextures];
    [mBody ClearAllTextures];
    [mHead ClearAllTextures];
    [mLArm ClearAllTextures];
    [mRArm ClearAllTextures];
    [mLLeg ClearAllTextures];
    [mRLeg ClearAllTextures];
    [mHat ClearAllTextures];
}

@end