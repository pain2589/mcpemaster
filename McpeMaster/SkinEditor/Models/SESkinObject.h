/*============================================================================
 PROJECT: SkinEditor
 FILE:    SESkinObject.h
 AUTHOR:  Nguyen Minh Khoai
 DATE:    10/18/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SEBodyPartObject.h"

/*============================================================================
 MACRO
 =============================================================================*/

typedef NS_ENUM(short, SESkinBodyPart) {
    SESkinBodyPartHead = 1,
    SESkinBodyPartHat,
    SESkinBodyPartBody,
    SESkinBodyPartLeftArm,
    SESkinBodyPartRightArm,
    SESkinBodyPartLeftLeg,
    SESkinBodyPartRightLeg,
    
    SESkinBodyPartJacket,
    SESkinBodyPartLeftArm2,
    SESkinBodyPartRightArm2,
    SESkinBodyPartLeftLeg2,
    SESkinBodyPartRightLeg2,
    
    /* for count accessing */
    SESkinBodyPartEditCount = 7,
    SESkinBodyPartCount = 12
};

typedef NS_ENUM(short, SEBodyPart) {
    SEBodyPartTop = 1,
    SEBodyPartBottom,
    SEBodyPartLeft,
    SEBodyPartRight,
    SEBodyPartFront,
    SEBodyPartBack,
    
    /* for count accessing */
    SEBodyPartCount = 6
};

/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   SESkinObject
 =============================================================================*/

@interface SESkinBodyPartObject : SEBaseObject
@property (nonatomic) SEBodyPartObject *top;
@property (nonatomic) SEBodyPartObject *bottom;
@property (nonatomic) SEBodyPartObject *left;
@property (nonatomic) SEBodyPartObject *right;
@property (nonatomic) SEBodyPartObject *front;
@property (nonatomic) SEBodyPartObject *back;
@property (nonatomic, readonly) BOOL isModified;

+ (instancetype)head;
+ (instancetype)body;
+ (instancetype)leftArm;
+ (instancetype)rightArm;
+ (instancetype)leftLeg;
+ (instancetype)rightLeg;
+ (instancetype)hat;

+ (instancetype)jacket;
+ (instancetype)leftArm2;
+ (instancetype)rightArm2;
+ (instancetype)leftLeg2;
+ (instancetype)rightLeg2;

+ (instancetype)oldLeftArm;
+ (instancetype)oldRightArm;
+ (instancetype)oldLeftLeg;
+ (instancetype)oldRightLeg;

- (void)fillColorMatrixInContext:(CGContextRef)context;
- (void)getColorPixels:(unsigned char *)pixels
           bytesPerRow:(NSUInteger)bytesPerRow
         bytesPerPixel:(NSUInteger)bytesPerPixel;

- (void)unionPart:(SESkinBodyPartObject *)object;
- (void)copyPart:(SESkinBodyPartObject *)object;

@end

@class RLMSkinObject;
@interface SESkinObject : SEBaseObject
@property (nonatomic, readonly) UIImage *image;
@property (nonatomic, assign) CGSize imageSize;
@property (nonatomic, strong) NSMutableArray *parts;
@property (nonatomic, assign) BOOL isModified;
@property (nonatomic, strong) RLMSkinObject *referencedRealmObject;

+ (instancetype)newObject;
+ (instancetype)objectFromImage:(UIImage *)image;
- (BOOL)isOldSkinFormat;

@end
