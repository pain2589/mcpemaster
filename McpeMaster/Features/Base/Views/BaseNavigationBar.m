/*============================================================================
 PROJECT: McpeMaster
 FILE:    BaseNavigationBar.m
 AUTHOR:  Khoai Nguyen
 DATE:    12/24/14
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "BaseNavigationBar.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/


@implementation BaseNavigationBar

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
