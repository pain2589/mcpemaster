/*============================================================================
 PROJECT: McpeMaster
 FILE:    LeftMenuSectionHeaderView.m
 AUTHOR:  Ngoc Tam Nguyen
 DATE:    12/28/14
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "LeftMenuSectionHeaderView.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/


@implementation LeftMenuSectionHeaderView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)awakeFromNib {
    [super awakeFromNib];
}

@end
