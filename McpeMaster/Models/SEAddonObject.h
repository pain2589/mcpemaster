/*============================================================================
 PROJECT: SkinEditor
 FILE:    SEMapObject.h
 AUTHOR:  Nguyen Minh Khoai
 DATE:    10/18/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SEBaseObject.h"
/*============================================================================
 MACRO
 =============================================================================*/

@class RLMAddonObject;
@interface SEAddonObject : SEBaseObject
@property (nonatomic, strong) RLMAddonObject *referencedRealmObject;

+ (instancetype)newObject;

@end
