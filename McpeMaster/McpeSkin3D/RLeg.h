//
//  RLeg.h
//  GirlSkin
//
//  Created by apple on 2016. 7. 12..
//  Copyright (c) 2016年 SWORD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Cube1.h"
@interface RLeg: Cube1{
    float rleg_texcoords[2 * 4 * 10];
    float rleg2_texcoords[2 * 4 * 10];
}
- (void) initCube:(float)height;
@end
