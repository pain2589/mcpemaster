//
//  Macros.h
//  McpeMaster
//
//  Created by Khoai Nguyen on 12/25/14.
//  Copyright (c) 2014 Khoai Nguyen. All rights reserved.
//

#ifndef McpeMaster_Macros_h
#define McpeMaster_Macros_h

#define iOSVersion(a)   [[[UIDevice currentDevice] systemVersion] floatValue] >= a
#define iOS7Version     iOSVersion(7)
#define iOS8Version     iOSVersion(8)

/*=============================================================================
 MACRO DEFINITION FOR NOTIFICATION
 =============================================================================*/
#define NotifCenter                     [NSNotificationCenter defaultCenter]
#define NotifReg(o, s, n)                 [NotifCenter addObserver:o selector:s name:n object:nil]
#define NotifRegObj(t, s, n, o)            [NotifCenter addObserver:t selector:s name:n object:o]
#define NotifRegMe(o, s, n)               [NotifCenter addObserver:o selector:s name:n object:o]
#define NotifUnreg(o, n)                 [NotifCenter removeObserver:o name:n object:nil]
#define NotifUnregAll(o)                [NotifCenter removeObserver:o]

#define NotifPost2Obj4Info(n, o, i)       [NotifCenter postNotificationName:n object:o userInfo:i]
#define NotifPost2Obj(n, o)              NotifPost2Obj4Info(n,o,nil)
#define NotifPost(n)                    NotifPost2Obj(n,nil)

#define NotifPostNotif(n)               [NotifCenter postNotification:n]

/*=============================================================================
 MACRO DEFINITION FOR NSUSERDEFAULTS
 =============================================================================*/
#define UsrDflt                         [NSUserDefaults standardUserDefaults]
#define UsrDfltSync()                   [UsrDflt synchronize]

#define UsrDfltCleanKey(k)              [UsrDflt removeObjectForKey:k]
#define UsrDfltSetObjKey(o, k)           [UsrDflt setObject:o forKey:k]
#define UsrDfltObj4Key(k)               [UsrDflt objectForKey:k]
#define UsrDfltDict4Key(k)              [UsrDflt dictionaryForKey:k]

#define UsrDfltRmKey(k)                 [UsrDflt removeObjectForKey:k]


#define UsrDfltSetBool4Key(o, k)         [UsrDflt setBool:o forKey:k]
#define UsrDfltGetBool4Key(k)           [UsrDflt boolForKey:k]

#define UsrDfltSetInt4Key(o, k)         [UsrDflt setInteger:o forKey:k]
#define UsrDfltGetInt4Key(k)           [UsrDflt integerForKey:k]

/*=============================================================================
 MACRO DEFINITION FOR UI APPEARANCE
 =============================================================================*/
#define UIColorFromRGB(rgbValue)        [UIColor \
                                        colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
                                        green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
                                        blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define kSizeMainScreen                 [[UIScreen mainScreen] bounds].size

#define NSStringMultiline(...)          [[NSString alloc] initWithCString:#__VA_ARGS__ encoding:NSUTF8StringEncoding]

//Font
#define FONT_GEOMETRIA(s) [UIFont fontWithName:@"Geometria" size:s]
#define FONT_GEOMETRIA_MEDIUM(s) [UIFont fontWithName:@"Geometria-Medium" size:s]
#define FONT_GEOMETRIA_BOLD(s) [UIFont fontWithName:@"Geometria-Bold" size:s]

#endif

