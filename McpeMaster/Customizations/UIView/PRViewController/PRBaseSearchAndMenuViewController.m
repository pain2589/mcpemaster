/*============================================================================
 PROJECT: McpeMaster
 FILE:    PRBaseSearchAndMenuViewController.m
 AUTHOR:  Khoai Nguyen
 DATE:    4/12/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "PRBaseSearchAndMenuViewController.h"
#import "PRNavigationBarDelegate.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@interface PRBaseSearchAndMenuViewController ()

@end

@implementation PRBaseSearchAndMenuViewController

- (id)initSetupNavigationDelegate {

    NSArray *array = self.navigationController.viewControllers;
    if (array.count > 0 && array.firstObject == self) {
        return [(id) [PRSearchAndMenuNavigationBarDelegate alloc] initWithOwnViewController:self
                                                                            callBackHandler:^BOOL(NSUInteger barItemIndex) {
                                                                                return YES;
                                                                            }];
    }
    return [super initSetupNavigationDelegate];
}

@end
