//
//  NSDate+Extension.m
//  McpeMaster
//
//  Created by Tai Huu Ho on 1/7/15.
//  Copyright (c) 2015 Khoai Nguyen. All rights reserved.
//

#import "NSDate+Extension.h"

@implementation NSDate (Extension)

- (NSString *)stringWithFormat:(NSString *)format {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = format;

    return [formatter stringFromDate:self];
}

- (NSString *)stringYouTubeTime {
    NSDate *sourceDate = self;
    NSDate *now = [NSDate date];
    NSTimeInterval interval = [now timeIntervalSinceDate:sourceDate];
    NSInteger ti = (NSInteger) interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600) % 24;
    NSInteger days = (ti / 86400) % 30;
    NSInteger weeks = (ti / 604800) % 48;
    NSInteger months = (ti / 2592000) % 12;
    NSInteger years = (ti / 31104000);

    if (years > 0) {
        if (years > 1) {
            return [NSString stringWithFormat:@"%ld years ago", (long) years];
        }
        return @"1 year ago";
    }
    else if (months > 0) {
        if (months > 1) {
            return [NSString stringWithFormat:@"%ld months ago", (long) months];
        }
        return @"1 month ago";
    }
    else if (weeks > 0) {
        if (weeks > 1) {
            return [NSString stringWithFormat:@"%ld weeks ago", (long) weeks];
        }
        return @"1 week ago";
    }
    else if (days > 0) {
        if (days > 1) {
            return [NSString stringWithFormat:@"%ld days ago", (long) days];
        }
        return @"1 day ago";
    }
    else if (hours > 0) {
        if (hours > 1) {
            return [NSString stringWithFormat:@"%ld hours ago", (long) hours];
        }
        return @"1 hour ago";
    }
    else if (minutes > 0) {
        if (minutes > 1) {
            return [NSString stringWithFormat:@"%ld minutes ago", (long) minutes];
        }
        return @"1 minute ago";
    }
    else if (seconds >= 0) {
        if (seconds > 1) {
            return [NSString stringWithFormat:@"%ld seconds ago", (long) seconds];
        }
        return @"1 second ago";
    }

    return @"";

}
@end
