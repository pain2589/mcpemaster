/*============================================================================
 PROJECT: SkinEditor
 FILE:    RLMSkinObject.m
 AUTHOR:  Nguyen Minh Khoai
 DATE:    11/10/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "RLMSkinObject.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@implementation RLMSkinObject

+ (NSDictionary *)defaultPropertyValues {
    return @{@"name": @"SkinName"};
}
@end
