//
//  LArm.m
//  GirlSkin
//
//  Created by apple on 2016. 7. 12..
//  Copyright (c) 2016年 SWORD. All rights reserved.
//

#import "LArm.h"


@implementation LArm
- (void) initCube:(float)height
{
    if (height == 32.0f) {
        float tmplarm_texcoords[] = {
            // front leftArm
            44.0f/64.0f,	1.0f,
            44.0f/64.0f,	1.0f - 6.0f/32.0f,
            48.0f/64.0f,	1.0f - 6.0f/32.0f,
            48.0f/64.0f,	1.0f,
            // front leftArm
            44.0f/64.0f,	1.0f - 6.0f/32.0f,
            44.0f/64.0f,	1.0f - 12.0f/32.0f,
            48.0f/64.0f,	1.0f - 12.0f/32.0f,
            48.0f/64.0f,	1.0f - 6.0f/32.0f,
            // top leftArm
            44.0f/64.0f,	1.0f - 12.0f/32.0f,
            44.0f/64.0f,	1.0f - 16.0f/32.0f,
            48.0f/64.0f,	1.0f - 16.0f/32.0f,
            48.0f/64.0f,	1.0f - 12.0f/32.0f,
            // bottom leftArm
            48.0f/64.0f,	1.0f - 12.0f/32.0f,
            48.0f/64.0f,	1.0f - 16.0f/32.0f,
            52.0f/64.0f,	1.0f - 16.0f/32.0f,
            52.0f/64.0f,	1.0f - 12.0f/32.0f,
            // left leftArm
            40.0f/64.0f,	1.0f,
            40.0f/64.0f,	1.0f - 6.0f/32.0f,
            44.0f/64.0f,	1.0f - 6.0f/32.0f,
            44.0f/64.0f,	1.0f,
            // left leftArm1
            40.0f/64.0f,	1.0f - 6.0f/32.0f,
            40.0f/64.0f,	1.0f - 12.0f/32.0f,
            44.0f/64.0f,	1.0f - 12.0f/32.0f,
            44.0f/64.0f,	1.0f - 6.0f/32.0f,
            // right leftArm
            52.0f/64.0f,	1.0f,
            52.0f/64.0f,	1.0f - 6.0f/32.0f,
            48.0f/64.0f,	1.0f - 6.0f/32.0f,
            48.0f/64.0f,	1.0f,
            // right leftArm1
            52.0f/64.0f,	1.0f - 6.0f/32.0f,
            52.0f/64.0f,	1.0f - 12.0f/32.0f,
            48.0f/64.0f,	1.0f - 12.0f/32.0f,
            48.0f/64.0f,	1.0f - 6.0f/32.0f,
            // back leftArm
            52.0f/64.0f,	1.0f,
            52.0f/64.0f,	1.0f - 6.0f/32.0f,
            56.0f/64.0f,	1.0f - 6.0f/32.0f,
            56.0f/64.0f,	1.0f,
            // back leftArm1
            52.0f/64.0f,	1.0f - 6.0f/32.0f,
            52.0f/64.0f,	1.0f - 12.0f/32.0f,
            56.0f/64.0f,	1.0f - 12.0f/32.0f,
            56.0f/64.0f,	1.0f - 6.0f/32.0f,
        };
        
        memcpy(larm_texcoords, tmplarm_texcoords, sizeof(tmplarm_texcoords));
        [super initCube:4.0f sy:12.0f sz:4.0f offsetx:6.0f offsety:2.0f offsetz:0.0f step_value:-20.0f/40.f rot_axis_x:1.0f rot_axis_y:0.0f rot_axis_z:0.0f max_anlge:10.0f min_angle:-10.0f step_value1:40.0f/40.0f max_angle1:20.0f min_angle:-20.0f isFixAngle:true fixDir:-1.0f];
        
        [super AddTextures:larm_texcoords nSize:sizeof(larm_texcoords) / sizeof(float)];
    }else if (height == 64.0f){
        float tmplarm_texcoords[] = {
            // front leftArm
            36.0f/64.0f,	1.0f,                   // 0
            36.0f/64.0f,	1.0f - 6.0f/64.0f,      // 8
            40.0f/64.0f,	1.0f - 6.0f/64.0f,      // 9
            40.0f/64.0f,	1.0f,                   // 1
            // front leftArm1
            36.0f/64.0f,	1.0f - 6.0f/64.0f,      // 0
            36.0f/64.0f,	1.0f - 12.0f/64.0f,     // 8
            40.0f/64.0f,	1.0f - 12.0f/64.0f,     // 9
            40.0f/64.0f,	1.0f - 6.0f/64.0f,
            // top leftArm
            36.0f/64.0f,	1.0f - 12.0f/64.0f,     // 3
            36.0f/64.0f,	1.0f - 16.0f/64.0f,     // 7
            40.0f/64.0f,	1.0f - 16.0f/64.0f,     // 6
            40.0f/64.0f,	1.0f - 12.0f/64.0f,     // 2
            // bottom leftArm
            40.0f/64.0f,	1.0f - 12.0f/64.0f,     // 4
            40.0f/64.0f,	1.0f - 16.0f/64.0f,     // 0
            44.0f/64.0f,	1.0f - 16.0f/64.0f,     // 1
            44.0f/64.0f,	1.0f - 12.0f/64.0f,     // 5
            // left leftArm
            40.0f/64.0f,	1.0f,                   // 1
            40.0f/64.0f,	1.0f - 6.0f/64.0f,      // 9
            44.0f/64.0f,	1.0f - 6.0f/64.0f,      // 10
            44.0f/64.0f,	1.0f,                   // 5
            // left leftArm1
            40.0f/64.0f,	1.0f - 6.0f/64.0f,      // 1
            40.0f/64.0f,	1.0f - 12.0f/64.0f,     // 9
            44.0f/64.0f,	1.0f - 12.0f/64.0f,     // 10
            44.0f/64.0f,	1.0f - 6.0f/64.0f,      // 5
            // right leftArm
            40.0f/64.0f,	1.0f,                   // 1
            40.0f/64.0f,	1.0f - 6.0f/64.0f,      // 9
            44.0f/64.0f,	1.0f - 6.0f/64.0f,      // 10
            44.0f/64.0f,	1.0f,                   // 5
            // right leftArm1
            40.0f/64.0f,	1.0f - 6.0f/64.0f,      // 1
            40.0f/64.0f,	1.0f - 12.0f/64.0f,     // 9
            44.0f/64.0f,	1.0f - 12.0f/64.0f,     // 10
            44.0f/64.0f,	1.0f - 6.0f/64.0f,      // 5
            // back leftArm
            44.0f/64.0f,	1.0f,                   // 5
            44.0f/64.0f,	1.0f - 6.0f/64.0f,      // 10
            48.0f/64.0f,	1.0f - 6.0f/64.0f,      // 11
            48.0f/64.0f,	1.0f,                   // 4
            // back leftArm1
            44.0f/64.0f,	1.0f - 6.0f/64.0f,      // 5
            44.0f/64.0f,	1.0f - 12.0f/64.0f,     // 10
            48.0f/64.0f,	1.0f - 12.0f/64.0f,     // 11
            48.0f/64.0f,	1.0f - 6.0f/64.0f,
        };
        
        memcpy(larm_texcoords, tmplarm_texcoords, sizeof(tmplarm_texcoords));
        
        float tmplarm2_texcoords[] = {
            // front leftArm
            16.0f/64.0f + 36.0f/64.0f,	1.0f, 				// 0
            16.0f/64.0f + 36.0f/64.0f,	1.0f - 6.0f/64.0f, 	// 8
            16.0f/64.0f + 40.0f/64.0f,	1.0f - 6.0f/64.0f,	// 9
            16.0f/64.0f + 40.0f/64.0f,	1.0f,				// 1
            // front leftArm1
            16.0f/64.0f + 36.0f/64.0f,	1.0f - 6.0f/64.0f, 	// 8
            16.0f/64.0f + 36.0f/64.0f,	1.0f - 12.0f/64.0f, // 3
            16.0f/64.0f + 40.0f/64.0f,	1.0f - 12.0f/64.0f,	// 2
            16.0f/64.0f + 40.0f/64.0f,	1.0f - 6.0f/64.0f,	// 9
            // top leftArm
            16.0f/64.0f + 36.0f/64.0f,	1.0f - 12.0f/64.0f,	// 3
            16.0f/64.0f + 36.0f/64.0f,	1.0f - 16.0f/64.0f,	// 7
            16.0f/64.0f + 40.0f/64.0f,	1.0f - 16.0f/64.0f,	// 6
            16.0f/64.0f + 40.0f/64.0f,	1.0f - 12.0f/64.0f, // 2
            // bottom leftArm
            16.0f/64.0f + 40.0f/64.0f,	1.0f - 12.0f/64.0f,	// 4
            16.0f/64.0f + 40.0f/64.0f,	1.0f - 16.0f/64.0f,	// 0
            16.0f/64.0f + 44.0f/64.0f,	1.0f - 16.0f/64.0f,	// 1
            16.0f/64.0f + 44.0f/64.0f,	1.0f - 12.0f/64.0f, // 5
            // left leftArm
            16.0f/64.0f + 40.0f/64.0f,	1.0f,				// 1
            16.0f/64.0f + 40.0f/64.0f,	1.0f - 6.0f/64.0f,	// 9
            16.0f/64.0f + 44.0f/64.0f,	1.0f - 6.0f/64.0f,	// 10
            16.0f/64.0f + 44.0f/64.0f,	1.0f,				// 5
            // left leftArm1
            16.0f/64.0f + 40.0f/64.0f,	1.0f - 6.0f/64.0f,	// 9
            16.0f/64.0f + 40.0f/64.0f,	1.0f - 12.0f/64.0f,	// 2
            16.0f/64.0f + 44.0f/64.0f,	1.0f - 12.0f/64.0f,	// 6
            16.0f/64.0f + 44.0f/64.0f,	1.0f - 6.0f/64.0f,	// 10
            // right leftArm
            16.0f/64.0f + 32.0f/64.0f,	1.0f,	   			// 4
            16.0f/64.0f + 32.0f/64.0f,	1.0f - 6.0f/64.0f,	// 11
            16.0f/64.0f + 36.0f/64.0f,	1.0f - 6.0f/64.0f,	// 8
            16.0f/64.0f + 36.0f/64.0f,	1.0f,				// 0
            // right leftArm1
            16.0f/64.0f + 32.0f/64.0f,	1.0f - 6.0f/64.0f,	// 11
            16.0f/64.0f + 32.0f/64.0f,	1.0f - 12.0f/64.0f,	// 7
            16.0f/64.0f + 36.0f/64.0f,	1.0f - 12.0f/64.0f,	// 3
            16.0f/64.0f + 36.0f/64.0f,	1.0f - 6.0f/64.0f,	// 8
            // back leftArm
            16.0f/64.0f + 44.0f/64.0f,	1.0f,   			// 5
            16.0f/64.0f + 44.0f/64.0f,	1.0f - 6.0f/64.0f,	// 10
            16.0f/64.0f + 48.0f/64.0f,	1.0f - 6.0f/64.0f,	// 11
            16.0f/64.0f + 48.0f/64.0f,	1.0f,				// 4
            // back leftArm1
            16.0f/64.0f + 44.0f/64.0f,	1.0f - 6.0f/64.0f,  // 10
            16.0f/64.0f + 44.0f/64.0f,	1.0f - 12.0f/64.0f,	// 6
            16.0f/64.0f + 48.0f/64.0f,	1.0f - 12.0f/64.0f,	// 7
            16.0f/64.0f + 48.0f/64.0f,	1.0f - 6.0f/64.0f,	// 11
        };
        memcpy(larm2_texcoords, tmplarm2_texcoords, sizeof(tmplarm2_texcoords));
        
        
        [super initCube:4.0f sy:12.0f sz:4.0f offsetx:6.0f offsety:2.0f offsetz:0.0f step_value:-20.0f/40.f rot_axis_x:1.0f rot_axis_y:0.0f rot_axis_z:0.0f max_anlge:10.0f min_angle:-10.0f step_value1:40.0f/40.0f max_angle1:20.0f min_angle:-20.0f isFixAngle:true fixDir:-1.0f];
        
        [super AddTextures:larm_texcoords nSize:sizeof(larm_texcoords) / sizeof(float)];
        [super AddTextures:larm2_texcoords nSize:sizeof(larm2_texcoords) / sizeof(float)];
    }
}
@end
