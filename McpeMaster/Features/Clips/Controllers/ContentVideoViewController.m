//
//  ContentVideoViewController.m
//  Protube
//
//  Created by Thanh. Nguyen Tan (8) on 1/6/15.
//  Copyright (c) 2015 Khoai Nguyen. All rights reserved.
//

#import "ContentVideoViewController.h"

@interface ContentVideoViewController ()

@end

@implementation ContentVideoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
