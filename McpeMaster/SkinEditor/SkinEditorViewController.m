//
//  ViewController.m
//  SkinEditor
//
//  Created by Nguyen Minh Khoai on 10/4/16.
//  Copyright © 2016 Nguyen Minh Khoai. All rights reserved.
//

#import "SkinEditorViewController.h"
#import "UIColor+Extensions.h"
#import "UIView+SEQuartzCore.h"
#import "UIViewController+NavigatingControl.h"

#import "QBImagePickerController.h"
#import "MKPhotoAccessor.h"
#import "SESkinObject.h"

#import "SESKinCell.h"
#import "RLMSkinObject.h"
#import "SESkinObject+Realm.h"
#import "UIAlertController+Utils.h"
#import "MKRealmManager.h"
#import "ADUFileManager.h"
#import "Firebase.h"

@interface SkinEditorViewController ()<QBImagePickerControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UIButton *createNewButton;
@property (weak, nonatomic) IBOutlet UIButton *favoriteButton;
@property (weak, nonatomic) IBOutlet UIButton *importButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;

@property (weak, nonatomic) IBOutlet UIView *buttonsView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) NSMutableArray <RLMSkinObject *> *skins;

@property (nonatomic, assign) BOOL isFavorites;

@end

@implementation SkinEditorViewController

- (BOOL)backButtonHidden {
    return YES;
}

- (NSMutableArray<RLMSkinObject *> *)skins {
    if (!_skins) {
        _skins = [NSMutableArray new];
    }
    return _skins;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //
//    if ((PRAppDelegate).mcpeNum != 0 && (PRAppDelegate).countClick == 0) {
//        (PRAppDelegate).countClick++;
//        if ((PRAppDelegate).countClick == 1) {
//            (PRAppDelegate).countClick = (PRAppDelegate).mcpeNum;
//        }
//        if ((PRAppDelegate).countClick%(PRAppDelegate).mcpeNum==0){
//            [PRAppDelegate loadInterstitial];
//        }
//    }
    
    //
    [self.collectionView setBackgroundColor:UIColorFromRGB(0xE6E6E6)];
    [self.buttonsView setBackgroundColor:UIColorFromRGB(0xE6E6E6)];
    
    //
    UIBarButtonItem *addNewSkinBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addNewSkin)];
    [addNewSkinBtn setTintColor:UIColor.blackColor];
    [self.navigationItem setRightBarButtonItem:addNewSkinBtn];
    
    //
    [_createNewButton makeDefaultGreenBorder];
    [_importButton makeDefaultGreenBorder];
    [_favoriteButton makeDefaultGreenBorder];
    
    //
    _createNewButton.selected = YES;
    
    /* update collection view */
    self.collectionView.contentInset = UIEdgeInsetsMake(10, 10, 10, 10);
    
    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
    layout.minimumLineSpacing = 10;
    layout.minimumInteritemSpacing = 10;
    
    //Can chinh item size cho tung thiet bi de dc iPad 6 items per row, iPhone 3 items per row
    CGRect rect = [UIScreen mainScreen].bounds;
    CGFloat sizeValue;
    if (IS_IPAD){
        //Man Hinh iPad
        sizeValue = (CGRectGetWidth(rect) - 536) / 2;
    }else if (IS_IPHONE_6_7_8_P){
        //Man Hinh iPhone 6 Plus
        sizeValue = (CGRectGetWidth(rect) - 166) / 2;
    }else if (IS_IPHONE_6_7_8){
        //Man Hinh iPhone 6
        sizeValue = (CGRectGetWidth(rect) - 152) / 2;
    }else{
        //Man hinh iPad 4,5
        sizeValue = (CGRectGetWidth(rect) - 134) / 2;
    }
    layout.itemSize = CGSizeMake(sizeValue, sizeValue);
    
    [FIRAnalytics setScreenName:@"Skin_Editor" screenClass:@"Skin_Editor"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addNewSkin{
    id controller = [UIViewController controllerInDefaultStoryBoardByID:@"SESkinViewController" withArguments:@{@"isCreateNew":@YES}];
    [self presentViewControllerEmbeddedInNavigationController:controller animated:YES];
}

- (IBAction)onTouchedActionButton:(id)sender {
    
    if (sender == _createNewButton) {
        if (!_isFavorites) {
//            id controller = [UIViewController controllerInDefaultStoryBoardByID:@"SESkinViewController" withArguments:@{@"isCreateNew":@YES}];
//            [self presentViewControllerEmbeddedInNavigationController:controller animated:YES];
        }else{
            _isFavorites = NO;
            [self swapMode];
            [self fetchSkinsFromDB];
        }
    }else if (sender == _favoriteButton){
        _isFavorites = YES;
        [self swapMode];
        [self fetchFavoriteSkinsFromDB];
    }else {
        QBImagePickerController *imagePickerController = [QBImagePickerController new];
        imagePickerController.delegate = self;
        imagePickerController.allowsMultipleSelection = NO;
        imagePickerController.maximumNumberOfSelection = 1;
        imagePickerController.showsNumberOfSelectedAssets = YES;
        
        [self presentViewController:imagePickerController animated:YES completion:NULL];
        //[[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
        [UIApplication sharedApplication].statusBarHidden = YES;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //Tiep disable banner o skin editor vi gay ra loi UI naviagation bar
    //[self initGoogleAdsBanner];
    
    if (_isFavorites) {
        [self fetchFavoriteSkinsFromDB];
    }else{
        [self fetchSkinsFromDB];
    }
}

- (void)swapMode{
    if (_isFavorites){
        _createNewButton.selected = NO;
        _favoriteButton.selected = YES;
        self.title = @"Favorites";
    }else{
        _createNewButton.selected = YES;
        _favoriteButton.selected = NO;
        self.title = @"Skin Editor";
    }
}

#pragma mark -
#pragma mark - QBImagePickerControllerDelegate

- (void)qb_imagePickerControllerDidCancel:(QBImagePickerController *)imagePickerController {
    [imagePickerController dismissViewControllerAnimated:YES completion:nil];
}

- (void)qb_imagePickerController:(QBImagePickerController *)imagePickerController didSelectAsset:(PHAsset *)asset {
    
}

- (void)qb_imagePickerController:(QBImagePickerController *)imagePickerController didFinishPickingAssets:(NSArray *)assets {
    
    if (assets.count > 0) {
        
        id completeHandler = ^(id  _Nonnull data, NSDictionary * _Nonnull info) {
          
            if (data) {
                UIImage *image = [UIImage imageWithData:data];
                SESkinObject *object = [SESkinObject objectFromImage:image];
                object.isModified = YES;
//                [self pushViewControllerWithStoryID:@"SESkinViewController"
//                                          arguments:@{@"skinObject": object}
//                                           animated:YES];
                id controller = [UIViewController controllerInDefaultStoryBoardByID:@"SESkinViewController"
                                                                      withArguments:@{@"skinObject": object,@"isImport":@YES}];
                
                [self presentViewControllerEmbeddedInNavigationController:controller animated:YES];
            }
        };
        
        [[MKPhotoAccessor sharedAccessor] requestImageDataForAsset:assets.firstObject
                                                     completeBlock:completeHandler];
    }
    
    [imagePickerController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark - UICollectionViewDelegate, UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.skins.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    SESKinCell *cell = (SESKinCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"SESKinCell"
                                                                               forIndexPath:indexPath];
    
    cell.deleteButton.tag = indexPath.row;
    
    if (_isFavorites) {
        [cell.deleteButton setImage:[UIImage imageNamed:@"icon_favorite"] forState:UIControlStateNormal];
        [cell.deleteButton removeTarget:self
                                 action:@selector(onTouchedDeleteFavoriteButton:)
                       forControlEvents:UIControlEventTouchUpInside];
        
        [cell.deleteButton addTarget:self
                              action:@selector(onTouchedDeleteFavoriteButton:)
                    forControlEvents:UIControlEventTouchUpInside];
    }else{
        [cell.deleteButton setImage:[UIImage imageNamed:@"btnDelete"] forState:UIControlStateNormal];
        [cell.deleteButton removeTarget:self
                                 action:@selector(onTouchedDeleteButton:)
                       forControlEvents:UIControlEventTouchUpInside];
        
        [cell.deleteButton addTarget:self
                              action:@selector(onTouchedDeleteButton:)
                    forControlEvents:UIControlEventTouchUpInside];
    }
    
    RLMSkinObject *object = self.skins[indexPath.row];
    NSString *imagePath = [[[ADUFileManager sharedManager] documentDirectoryPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"skins/%@",object.thumbnailPath]];
    cell.skinView.image = [UIImage imageWithContentsOfFile:imagePath];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:NO];
    
    SESkinObject *object = [SESkinObject fromRealmObject:self.skins[indexPath.row]];
    id controller = [UIViewController controllerInDefaultStoryBoardByID:@"SESkinViewController"
                                                          withArguments:@{@"skinObject": object}];
    
    [self presentViewControllerEmbeddedInNavigationController:controller animated:YES];
}

//Tiep De ra 1 khoang trong duoi bottom de show banner
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    //Tiep xoa Phan nay danh cho admob banner cũ
    //    if (IS_IPAD) {
    //        return UIEdgeInsetsMake(0, 0, 90, 0);
    //    }else{
    //        return UIEdgeInsetsMake(0, 0, 50, 0);
    //    }
    
    //Tiep them Phan nay danh cho admob native express banner
    if (IS_IPAD) {
        return UIEdgeInsetsMake(0, 0, 132, 0);
    }else{
        return UIEdgeInsetsMake(0, 0, 132, 0);
    }
}

#pragma mark -
#pragma mark - DB Support

- (void)fetchFavoriteSkinsFromDB {
    
    RLMResults<RLMSkinObject *> *objects = [[RLMSkinObject objectsWhere:@"favorited = YES"] sortedResultsUsingProperty:@"logTime" ascending:NO];
    
    NSMutableArray *skins = [NSMutableArray new];
    
    if (objects.count > 0) {
        for (NSUInteger i = 0; i < objects.count; i++) {
            RLMSkinObject *object = [objects objectAtIndex:i];
            [skins addObject:object];
        }
    }
    
    [self.skins removeAllObjects];
    [self.skins addObjectsFromArray:skins];
    
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    [_collectionView reloadData];
}

- (void)fetchSkinsFromDB {
    
    RLMResults<RLMSkinObject *> *objects = [[RLMSkinObject allObjects] sortedResultsUsingProperty:@"logTime" ascending:NO];
    
    NSMutableArray *skins = [NSMutableArray new];
    if (objects.count > 0) {
        for (NSUInteger i = 0; i < objects.count; i++) {
            RLMSkinObject *object = [objects objectAtIndex:i];
            [skins addObject:object];
        }
    }
    
    [self.skins removeAllObjects];
    [self.skins addObjectsFromArray:skins];
    
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    [_collectionView reloadData];
}

- (void)onTouchedDeleteButton:(UIButton *)sender {
    
    AlertActionCompleteBlock completeBlock = ^(NSUInteger buttonIndex) {
        
        if (buttonIndex == 0) {
            
            /* create new one */
            RLMSkinObject *object = self.skins[sender.tag];
            
            /* remove existing realm object */
            if (object) {
                //Delete file in device
                NSString *skinsDirectoryPath = [[[ADUFileManager sharedManager] documentDirectoryPath] stringByAppendingString:@"/skins/"];
                NSString *thumbnailPath = [skinsDirectoryPath stringByAppendingPathComponent:object.thumbnailPath];
                NSString *originalPath = [skinsDirectoryPath stringByAppendingPathComponent:object.originalPath];
                
                NSFileManager *fileManager = [NSFileManager defaultManager];
                
                if ([fileManager fileExistsAtPath:thumbnailPath]) {
                    NSError *error;
                    BOOL success = [fileManager removeItemAtPath:thumbnailPath error:&error];
                    if (success) {
                    }else
                    {
                        NSLog(@"Could not delete Skin Thumbnail file -:%@ ",[error localizedDescription]);
                    }
                }
                
                if ([fileManager fileExistsAtPath:originalPath]) {
                    NSError *error;
                    BOOL success = [fileManager removeItemAtPath:originalPath error:&error];
                    if (success) {
                    }else
                    {
                        NSLog(@"Could not delete Skin Original file -:%@ ",[error localizedDescription]);
                    }
                }
                [MKRealmManager transactionWithBlock:^(RLMRealm *realm) {
                    [self.skins removeObject:object];
                    [realm deleteObject:(RLMObject *)object];
                    [self.collectionView reloadData];
                }];
            }
        }
    };
    
    [UIAlertController showConfirmPopUpWithTitle:nil
                                         message:@"Are you sure want to delete this Skin from your Device?"
                                    buttonTitles:@[@"OK", @"Cancel"]
                                   completeBlock:completeBlock];
}

- (void)onTouchedDeleteFavoriteButton:(UIButton *)sender {
    
    AlertActionCompleteBlock completeBlock = ^(NSUInteger buttonIndex) {
        
        if (buttonIndex == 0) {
            
            /* create new one */
            RLMSkinObject *object = self.skins[sender.tag];
            
            /* remove existing realm object */
            if (object) {
                [MKRealmManager transactionWithBlock:^(RLMRealm *realm) {
                    object.favorited = NO;
                    
                    [self.skins removeObject:object];
                    [self.collectionView reloadData];
                }];
            }
        }
    };
    
    [UIAlertController showConfirmPopUpWithTitle:nil
                                         message:@"Are you sure you want to remove this Skin from Favorites?"
                                    buttonTitles:@[@"OK", @"Cancel"]
                                   completeBlock:completeBlock];
}

#pragma mark - GAD Banner Ads
- (void)initGoogleAdsBanner{
    if (!self.bannerView)
    {
        self.bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
        [self addBannerViewToView:self.bannerView];
        
        CGRect frame = self.bannerView.frame;
        //frame.origin.y = self.view.frame.size.height - [[[self tabBarController] tabBar] bounds].size.height;
        
        if (IS_IPAD) {
            frame.origin.x = 20;
        }
        
        self.bannerView.frame = frame;
        
        self.bannerView.adUnitID = ADMOB_BANNER_ID;
        self.bannerView.rootViewController = self;
        self.bannerView.delegate = self;
        self.bannerView.hidden = TRUE;
        [self.view addSubview:self.bannerView];
        
        GADRequest *request = [GADRequest request];
        //request.testDevices = @[kGADSimulatorID];
        
        //Forward user consent choice
        if (PACConsentInformation.sharedInstance.requestLocationInEEAOrUnknown &&
            PACConsentInformation.sharedInstance.consentStatus == PACConsentStatusNonPersonalized) {
            GADExtras *extras = [[GADExtras alloc] init];
            extras.additionalParameters = @{@"npa": @"1"};
            [request registerAdNetworkExtras:extras];
            NSLog(@"Ads is non-personalized now!!!");
        }
        
        [self.bannerView loadRequest:request];
    }
}

- (void)addBannerViewToView:(UIView *)bannerView {
    bannerView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:bannerView];
    if (@available(iOS 11.0, *)) {
        // In iOS 11, we need to constrain the view to the safe area.
        [self positionBannerViewFullWidthAtBottomOfSafeArea:bannerView];
    } else {
        // In lower iOS versions, safe area is not available so we use
        // bottom layout guide and view edges.
        [self positionBannerViewFullWidthAtBottomOfView:bannerView];
    }
}

#pragma mark - view positioning

- (void)positionBannerViewFullWidthAtBottomOfSafeArea:(UIView *_Nonnull)bannerView NS_AVAILABLE_IOS(11.0) {
    // Position the banner. Stick it to the bottom of the Safe Area.
    // Make it constrained to the edges of the safe area.
    UILayoutGuide *guide = self.view.safeAreaLayoutGuide;
    
    [NSLayoutConstraint activateConstraints:@[
                                              [guide.leftAnchor constraintEqualToAnchor:bannerView.leftAnchor],
                                              [guide.rightAnchor constraintEqualToAnchor:bannerView.rightAnchor],
                                              [guide.bottomAnchor constraintEqualToAnchor:bannerView.bottomAnchor]
                                              ]];
}

- (void)positionBannerViewFullWidthAtBottomOfView:(UIView *_Nonnull)bannerView {
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:bannerView
                                                          attribute:NSLayoutAttributeLeading
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeLeading
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:bannerView
                                                          attribute:NSLayoutAttributeTrailing
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeTrailing
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:bannerView
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view.safeAreaLayoutGuide.bottomAnchor
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1
                                                           constant:0]];
}

#pragma mark - Banner hide and show -

// Hide the banner by sliding down
-(void)hideBanner:(UIView*)banner
{
    banner.hidden = TRUE;
}

// Show the banner by sliding up
-(void)showBanner:(UIView*)banner
{
    banner.hidden = FALSE;
}

#pragma mark - GADBanner delegate methods -

-(void)showAdmobBanner{
    self.bannerView.hidden = FALSE;
}

-(void)hideAdmobBanner{
    self.bannerView.hidden = TRUE;
}

// Called before ad is shown, good time to show the add
- (void)adViewDidReceiveAd:(GADBannerView *)view
{
    NSLog(@"Admob load");
    [self showBanner:self.bannerView];
}

// An error occured
- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error
{
    NSLog(@"Admob error: %@", error);
    [self hideBanner:self.bannerView];
}

@end

