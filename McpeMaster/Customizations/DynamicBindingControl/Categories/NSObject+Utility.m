//
//  NSObject+Utility.m
//  BrandMate
//
//  Created by vu bui on 1/22/15.
//  Copyright (c) 2015 Nguyen Minh Khoai. All rights reserved.
//

#import "NSObject+Utility.h"
#import "NSString+MKAdditions.h"
#import "Invocation.h"

@implementation NSObject (Utility)

- (void)bindDataIntoSelf:(NSDictionary *)data {
    if (data) {
        id target = nil;
        NSString *method = nil;
        BOOL useInvocation = FALSE;
        for (NSString *key in data.allKeys) {
            target = nil;
            method = nil;
            useInvocation = FALSE;
            id value = [data objectForKey:key];
            if (value) {
                if ([self isKeyPath:key]) {
                    if ([self hasMethodInKeyPath:key]) {
                        useInvocation = TRUE;
                        NSMutableArray *components = [NSMutableArray arrayWithArray:[key componentsSeparatedByString:@"."]];
                        method = [components lastObject];
                        [components removeLastObject];
                        if (components.count == 1)
                            target = [self valueForKey:[components objectAtIndex:0]];
                        else {
                            NSString *tempKeyPath = [components componentsJoinedByString:@"."];
                            target = [self valueForKeyPath:tempKeyPath];
                        }
                    }
                    else
                        [self setValue:value forKeyPath:key];
                }
                else if ([self hasProperty:key inInstance:self])
                    [self setValue:value forKey:key];
                else {
                    target = self;
                    method = key;
                    useInvocation = TRUE;
                }

                if (useInvocation && target && method) {
                    NSInvocation *invocation = [[Invocation sharedInstance] createInvocationWithMethodName:method
                                                                                                    target:target
                                                                                                parameters:value];
                    [[Invocation sharedInstance] invoke:invocation];
                }
            }
        }
    }
}


- (BOOL)isKeyPath:(NSString *)keyPath {
    BOOL result = FALSE;
    if (keyPath) {
        NSRange range = [keyPath rangeOfString:@"."];
        if (range.location != NSNotFound)
            result = TRUE;
    }
    return result;
}

- (BOOL)hasMethodInKeyPath:(NSString *)keyPath {
    BOOL result = FALSE;
    if (keyPath) {
        NSRange range = [keyPath rangeOfString:@":"];
        if (range.location != NSNotFound)
            result = TRUE;
    }
    return result;
}

- (BOOL)hasMethod:(NSString *)method {
    return [self hasMethod:method inDelegate:self];
}

- (BOOL)hasMethod:(NSString *)method inDelegate:(id)delegate {
    BOOL result = FALSE;
    if (method && delegate) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"
        if ([delegate respondsToSelector:NSSelectorFromString(method)])
#pragma clang diagnostic pop
            result = TRUE;
    }
    return result;
}

- (BOOL)hasProperty:(NSString *)property {
    return [self hasProperty:property inInstance:self];
}

- (BOOL)hasProperty:(NSString *)property inInstance:(id)instance {
    BOOL isOK = FALSE;
    if (![property isBlank] && instance) {
        if ([instance respondsToSelector:NSSelectorFromString(property)])
            isOK |= TRUE;

        if ([instance respondsToSelector:NSSelectorFromString([NSString stringWithFormat:@"set%@", property])])
            isOK |= TRUE;
    }

    return isOK;
}

- (id)executeMethod:(NSString *)method withParams:(NSMutableArray *)params {

    id returnValue;
    if ([self hasMethod:method]) {
        NSInvocation *invocation = [[Invocation sharedInstance] createInvocationWithMethodName:method
                                                                                        target:self
                                                                                    parameters:params];
        [[Invocation sharedInstance] invoke:invocation returnData:&returnValue];
    }

    return returnValue;
}

@end
