/*============================================================================
 PROJECT: McpeMaster
 FILE:    PRListViewDataSource.m
 AUTHOR:  Khoai Nguyen
 DATE:    3/18/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "PRListViewDataSource.h"
#import "MKCollectionViewController.h"
#import "ClipListCell.h"
#import "OfflineConnectionView.h"
#import "PRListViewLogicDelegate.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@implementation PRListViewDataSource

#pragma mark - DataSource

- (NSInteger)collectionController:(MKCollectionViewController *)controller numberOfRowInSection:(NSInteger)section {
    if (_logics) {
        return [[_logics data] count];
    }
    return 0;
}

- (MKCollectionViewCell *)collectionController:(MKCollectionViewController *)controller
                        cellForItemAtIndexPath:(NSIndexPath *)indexPath {

    NSString *identifier = [self.configs cellIdentifierForCollectionController:controller
                                                                   atIndexPath:(NSIndexPath *) indexPath];

    MKCollectionViewCell *cell = (MKCollectionViewCell *) [controller.collectionView dequeueReusableCellWithReuseIdentifier:identifier
                                                                                                               forIndexPath:indexPath];
    cell.data = [self.logics collectionController:controller dataAtIndexPath:indexPath];

    return cell;
}


- (CGSize)collectionController:(MKCollectionViewController *)controller
                        layout:(UICollectionViewLayout *)collectionViewLayout
        sizeForItemAtIndexPath:(NSIndexPath *)indexPath {


    CGFloat width = SCREEN_WIDTH;
    
    NSString *mcpeType = [self.logics collectionController:controller dataAtIndexPath:indexPath][kMcpeType];
    if ([mcpeType isEqualToString:kMcpeTypeSkin]) {
        if (IS_IPAD) {
            width = (SCREEN_WIDTH / 4) - 1;
        }else{
            width = (SCREEN_WIDTH / 2) - 1;
        }
        return CGSizeMake(width, width);
    }else{
        if (IS_IPAD) {
            width = (SCREEN_WIDTH / 2) - 1;
        }
        CGFloat scaleFactor = kImageScaleFactor;
        return CGSizeMake(width, width / scaleFactor);
    }
}

#pragma mark - Delegate

- (void)collectionController:(MKCollectionViewController *)controller dataAtSelectedIndex:(id)data {
    /* will be overridden in subclasses */
}

- (void)collectionController:(MKCollectionViewController *)controller didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    /* get data at indexPath */
    id data = [self.logics collectionController:controller dataAtIndexPath:indexPath];
    
    /* control data to update UI/move to another controller */
    [self collectionController:controller dataAtSelectedIndex:data];
}

- (UICollectionReusableView *)footerViewInCollectionViewController:(MKCollectionViewController *)controller
                                                       atIndexPath:(NSIndexPath *)indexPath {

    NSString *identifier = [self.configs footerIDInController:controller atIndexPath:indexPath];
    id view = [controller.collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter
                                                            withReuseIdentifier:identifier
                                                                   forIndexPath:indexPath];
    
    OfflineConnectionView *offlineView = (OfflineConnectionView *) view;
    NSString *errMsg = ((PRListViewLogicDelegate*)_logics).errMsg;
    if (errMsg) {
        [offlineView.noConnectionTitleLabel setText:errMsg];
    }
    
    [offlineView setReloadEventHandler:^{
        [controller.logics loadDataInCollectionViewController:controller];
        [controller.collectionView reloadData];
        
        [controller fetchMoreData];
    }];

    return view;
}

- (CGSize)collectionController:(MKCollectionViewController *)controller
                         layout:(UICollectionViewLayout *)collectionViewLayout
referenceSizeForFooterInSection:(NSInteger)section {
    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *) collectionViewLayout;
    return layout.footerReferenceSize;
}

@end

/*=============================================================================*/
// PRListVideoViewDataSource
/*=============================================================================*/
@implementation PRListVideoViewDataSource

#pragma mark - DataSource

- (void)collectionController:(MKCollectionViewController *)controller
               configureCell:(MKCollectionViewCell *)cell
                 atIndexPath:(NSIndexPath *)indexPath {


    ClipListCell *clipCell = (ClipListCell *) cell;

    if (clipCell.touchedOnOptionsBlock == nil || clipCell.touchedOnOptionsBlock == NULL) {
        [clipCell setTouchedOnOptionsBlock:^(id sender) {
            
        }];
    }
    
    clipCell.parentViewController = controller;
}

- (void)collectionController:(MKCollectionViewController *)controller dataAtSelectedIndex:(id)data {
    [controller pushToControllerName:@"ClipDetailViewController" withData:@{@"video" : data}];
}

@end

