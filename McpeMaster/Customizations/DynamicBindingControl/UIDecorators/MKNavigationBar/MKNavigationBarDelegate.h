/*============================================================================
 PROJECT: McpeMaster
 FILE:    MKNavigationBarDelegate.h
 AUTHOR:  Khoai Nguyen
 DATE:    2/8/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import <Foundation/Foundation.h>

/*============================================================================
 MACRO
 =============================================================================*/
typedef NS_ENUM(short, MKNavigationBarButtonType) {
    MKNavigationBarButtonTypeBack = 0,
    MKNavigationBarButtonTypeMenu,
    MKNavigationBarButtonTypeOther
};

/*============================================================================
 PROTOCOL: MKNavigationBarDelegate
 =============================================================================*/

@protocol MKViewDelegate <NSObject>

@optional
@property(nonatomic, weak, readonly) UIViewController *ownedViewController;

@end

@protocol MKNavigationBarDelegate <NSObject>

@required
- (void)setUpNavigationBar;

@end
