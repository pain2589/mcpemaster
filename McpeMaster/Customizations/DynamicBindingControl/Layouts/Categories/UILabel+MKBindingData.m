/*============================================================================
 PROJECT: McpeMaster
 FILE:    UILabel+MKBindingData.m
 AUTHOR:  Khoai Nguyen
 DATE:    3/10/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "UILabel+MKBindingData.h"
#import "NSObject+CheckTypes.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@implementation UILabel (MKBindingData)

- (void)bindData:(id)data {

    /* only accept string type */
    if (!data || ![data isString]) return;

    self.text = data;
}

@end
