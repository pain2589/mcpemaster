/*============================================================================
 PROJECT: McpeMaster
 FILE:    MKListViewControllerLogics.h
 AUTHOR:  Khoai Nguyen
 DATE:    3/15/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import <Foundation/Foundation.h>
#import "MKDynamicBindingDelegate.h"

/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 PROTOCOL: MKListViewControllerLogics
 =============================================================================*/

#pragma mark - MKCollectionViewControllerLogicDelegate

@class MKCollectionViewController;

@protocol MKCollectionViewControllerLogicDelegate <MKBindingComponentDelegate>

@optional
@property(nonatomic, readonly) BOOL hasMoreData;
@property(nonatomic) BOOL isReloadedData;

@optional
- (void)clearDataInController:(MKCollectionViewController *)controller;

- (void)loadDataInCollectionViewControllerAtFirstTime:(MKCollectionViewController *)controller;

- (void)reloadDataInCollectionViewController:(MKCollectionViewController *)controller;

- (void)loadDataInCollectionViewController:(MKCollectionViewController *)controller;

- (void)loadMoreDataInCollectionViewController:(MKCollectionViewController *)controller;

- (void)forceReloadDataInCollectionViewController:(MKCollectionViewController *)controller;

- (id)collectionController:(MKCollectionViewController *)controller dataAtIndexPath:(NSIndexPath *)indexPath;

@end
