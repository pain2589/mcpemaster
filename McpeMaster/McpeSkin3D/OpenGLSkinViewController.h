//
//  OpenGLSkinViewController.h
//  mcpehub
//
//  Created by Kaka on 7/17/16.
//  Copyright © 2016 Kaka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OpenGLView.h"
#import "MKRealmManager.h"
#import "RLMSkinOnlineObject.h"
#import "SESkinOnlineObject+Realm.h"
#import "UIAlertController+Utils.h"
#import "MKPhotoAccessor.h"

@import GoogleMobileAds;
//<UIActionSheetDelegate,SKPaymentTransactionObserver,SKProductsRequestDelegate,GADBannerViewDelegate>
@interface OpenGLSkinViewController : UIViewController<UIActionSheetDelegate,GADBannerViewDelegate,GADRewardBasedVideoAdDelegate>

@property (strong,nonatomic) GADBannerView *bannerView;

@property (strong, nonatomic) OpenGLView *glView32;

@property (nonatomic, strong) UIImage *skinImg32;
@property (nonatomic, strong) UIImage *skinImg;
@property (nonatomic, weak) IBOutlet UIImageView *bgImgView;

@property(strong, nonatomic) NSString *mcpeId;
@property (nonatomic, strong) NSString *mcpeTitle;

@property(strong, nonatomic) NSString *strDownloadUrl;
@property(strong, nonatomic) NSString *strThumbUrl;

@property(assign, nonatomic) BOOL isOpenFromSkinEditor;

@property (nonatomic, strong) SESkinOnlineObject *skinOnlineObject;
@property (nonatomic, strong) RLMSkinOnlineObject *realmSkinOnlineObject;

- (void)show3DModel;

@end
