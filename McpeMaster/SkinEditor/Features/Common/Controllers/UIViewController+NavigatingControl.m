/*============================================================================
 FILE:    UIViewController+NavigatingControl.m
 AUTHOR:  Khoai Nguyen
 DATE:    8/7/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "UIViewController+NavigatingControl.h"
#import "MKNavigationController.h"
#import "AppDelegate.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@implementation UIViewController (NavigatingControl)

#pragma mark -
#pragma mark - Navigating Control

- (void)pushViewController:(UIViewController *)controller animated:(BOOL)animated {
    [self.navigationController pushViewController:controller animated:animated];
}

- (void)popViewControllerAnimated:(BOOL)animated {
    [self.navigationController popViewControllerAnimated:animated];
}

- (void)popToRootViewControllerAnimated:(BOOL)animated {
    [self.navigationController popToRootViewControllerAnimated:animated];
}

- (void)presentViewControllerEmbeddedInNavigationController:(UIViewController *)controller animated:(BOOL)animated {
    
    MKNavigationController *nav = [[MKNavigationController alloc] initWithRootViewController:controller];
    [self presentViewController:nav animated:animated completion:nil];
}

- (void)presentViewController:(UIViewController *)controller animated:(BOOL)animated {
    [self presentViewController:controller animated:animated completion:nil];
}

#pragma mark -
#pragma mark - StoryBoard

- (void)pushViewControllerWithStoryID:(NSString *)storyID
                         inStoryBoard:(NSString *)storyBoard
                            arguments:(NSDictionary *)arguments
                             animated:(BOOL)animated {
    
    UIViewController *viewController = [UIViewController controllerByStoryID:storyID
                                                                inStoryBoard:storyBoard
                                                               withArguments:arguments];
    if (viewController) {
        [self pushViewController:viewController animated:animated];
    }
}

- (void)pushViewControllerWithStoryID:(NSString *)storyID
                         inStoryBoard:(NSString *)storyBoard
                             animated:(BOOL)animated {
   
    [self pushViewControllerWithStoryID:storyID
                            inStoryBoard:storyBoard
                               arguments:nil
                                animated:animated];
}

- (void)pushViewControllerWithStoryID:(NSString *)storyID arguments:(NSDictionary *)arguments animated:(BOOL)animated {

    UIViewController *viewController = [UIViewController controllerInDefaultStoryBoardByID:storyID
                                                                             withArguments:arguments];
    if (viewController) {
        [self pushViewController:viewController animated:animated];
    }
}

- (void)pushViewControllerWithStoryID:(NSString *)storyID animated:(BOOL)animated {
    [self pushViewControllerWithStoryID:storyID arguments:nil animated:animated];
}

#pragma mark -
#pragma mark - Nibs

- (void)pushViewControllerWithNibName:(NSString *)nibName
                            arguments:(NSDictionary *)arguments
                             animated:(BOOL)animated {
    
    
    Class classType = NSClassFromString(nibName);
    UIViewController *viewController = [classType viewControllerWithNibName:nibName
                                                                  arguments:arguments];
    if (viewController) {
        [self pushViewController:viewController animated:animated];
    }
}

@end
