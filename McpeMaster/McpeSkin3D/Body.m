//
//  Body.m
//  GirlSkin
//
//  Created by apple on 2016. 7. 12..
//  Copyright (c) 2016年 SWORD. All rights reserved.
//

#import "Body.h"


@implementation Body
- (void) initCube:(float)height
{
   float tmpbody_texcoords[] = {
			// front body
			20.0f/64.0f,	32.0f/height, 		// 0
			20.0f/64.0f,	20.0f/height,	    // 3
			28.0f/64.0f,	20.0f/height, 	  	// 2
			28.0f/64.0f,	32.0f/height, 	  	// 1
			// top body
			20.0f/64.0f,	20.0f/height,    	// 3
			20.0f/64.0f,	16.0f/height, 		// 7
			28.0f/64.0f,	16.0f/height,		// 6
			28.0f/64.0f,	20.0f/height,		// 2
			// bottom body
			28.0f/64.0f,	20.0f/height,		// 4
			28.0f/64.0f,	16.0f/height,		// 0
			36.0f/64.0f,	16.0f/height,		// 1
			36.0f/64.0f,	20.0f/height,	    // 5
			// left body
			28.0f/64.0f,	32.0f/height,		// 1
			28.0f/64.0f,	20.0f/height,		// 2
			32.0f/64.0f,	20.0f/height,     	// 6
			32.0f/64.0f,	32.0f/height,    	// 5
			// right body
			16.0f/64.0f,	32.0f/height,	   	// 7
			16.0f/64.0f,	20.0f/height,		// 3
			20.0f/64.0f,	20.0f/height,   	// 0
			20.0f/64.0f,	32.0f/height,   	// 4
			// back body
			32.0f/64.0f,	32.0f/height,   	// 5
			32.0f/64.0f,	20.0f/height,   	// 6
			40.0f/64.0f,	20.0f/height,		// 7
			40.0f/64.0f,	32.0f/height,   	// 4
			};
	memcpy(body_texcoords, tmpbody_texcoords, sizeof(tmpbody_texcoords));
    
    [super initCube:8.0f sy:12.0f sz:4.0f offsetx:0.0f offsety:2.0f offsetz:0.0f step_value1:-0.15f rot_axis_x:0.0f rot_axis_y:1.0f rot_axis_z:0.0f max_anlge1:3.0f min_angle1:-3.0f];
    [super AddTextures:body_texcoords nSize:sizeof(body_texcoords) / sizeof(float)];
    
    if (height == 64.0f) {
        float tmpjacket_texcoords[] = {
            // front body
            20.0f/64.0f,	48.0f/64.0f, 		// 0
            20.0f/64.0f,	36.0f/64.0f,	    // 3
            28.0f/64.0f,	36.0f/64.0f, 	  	// 2
            28.0f/64.0f,	48.0f/64.0f, 	  	// 1
            // top body
            20.0f/64.0f,	36.0f/64.0f,    	// 3
            20.0f/64.0f,	32.0f/64.0f, 		// 7
            28.0f/64.0f,	32.0f/64.0f,		// 6
            28.0f/64.0f,	36.0f/64.0f,		// 2
            // bottom body
            28.0f/64.0f,	36.0f/64.0f,		// 4
            28.0f/64.0f,	32.0f/64.0f,		// 0
            36.0f/64.0f,	32.0f/64.0f,		// 1
            36.0f/64.0f,	36.0f/64.0f,	    // 5
            // left body
            28.0f/64.0f,	48.0f/64.0f,		// 1
            28.0f/64.0f,	36.0f/64.0f,		// 2
            32.0f/64.0f,	36.0f/64.0f,     	// 6
            32.0f/64.0f,	48.0f/64.0f,    	// 5
            // right body
            16.0f/64.0f,	48.0f/64.0f,	   	// 7
            16.0f/64.0f,	36.0f/64.0f,		// 3
            20.0f/64.0f,	36.0f/64.0f,   		// 0
            20.0f/64.0f,	48.0f/64.0f,   		// 4
            // back body
            32.0f/64.0f,	48.0f/64.0f,   		// 5
            32.0f/64.0f,	36.0f/64.0f,   		// 6
            40.0f/64.0f,	36.0f/64.0f,		// 7
            40.0f/64.0f,	48.0f/64.0f,   		// 4
        };
        
        memcpy(jacket_texcoords, tmpjacket_texcoords, sizeof(tmpjacket_texcoords));
        [super AddTextures:jacket_texcoords nSize:sizeof(jacket_texcoords) / sizeof(float)];
    }
}
@end
