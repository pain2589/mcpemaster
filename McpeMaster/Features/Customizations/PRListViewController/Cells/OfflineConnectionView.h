/*============================================================================
 PROJECT: McpeMaster
 FILE:    OfflineConnectionView.h
 AUTHOR:  Khoai Nguyen
 DATE:    4/17/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "MKCollectionReusableView.h"


/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 PROTOCOL
 =============================================================================*/
typedef void(^OnReloadEventHandler)();

/*============================================================================
 Interface:   OfflineConnectionView
 =============================================================================*/

@interface OfflineConnectionView : MKCollectionReusableView
@property(nonatomic, copy) OnReloadEventHandler reloadEventHandler;
@property(weak, nonatomic) IBOutlet UILabel *noConnectionTitleLabel;

- (void)setReloadEventHandler:(OnReloadEventHandler)handler;
@end
