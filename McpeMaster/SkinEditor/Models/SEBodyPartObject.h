/*============================================================================
 PROJECT: SkinEditor
 FILE:    SEBodyPartObject.h
 AUTHOR:  Nguyen Minh Khoai
 DATE:    10/4/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SEBaseObject.h"
#import <UIKit/UIColor.h>

/*============================================================================
 MACRO
 =============================================================================*/
#define SEClearColor [UIColor colorWithRed:0 green:0 blue:0 alpha:0]

/*============================================================================
 RGBAColor
 =============================================================================*/
typedef unsigned char BYTE;

typedef struct RGBA RGBAColor;
typedef struct MatrixDimension MatrixDimension;
typedef struct MatrixIndex MatrixIndex;

struct RGBA {
    BYTE red;
    BYTE green;
    BYTE blue;
    BYTE alpha;
};

inline static RGBAColor RGBColorMake(BYTE red, BYTE green, BYTE blue, BYTE alpha) {
    RGBAColor color;
    color.red   = red;
    color.blue  = blue;
    color.green = green;
    color.alpha = alpha;
    return color;
}

extern const RGBAColor RGBATransparentColor;

/*============================================================================
 MatrixDimension
 =============================================================================*/
struct MatrixDimension {
    NSUInteger rowSize;
    NSUInteger columnSize;
};

inline static MatrixDimension MatrixDimensionMake(NSUInteger rowSize, NSUInteger columnSize) {
    MatrixDimension dimension;
    dimension.rowSize       = rowSize;
    dimension.columnSize    = columnSize;
    return dimension;
}

inline static CGSize MatrixDimensionToCGSize(MatrixDimension dimension) {
    return CGSizeMake(dimension.columnSize, dimension.rowSize);
}

inline static MatrixDimension CGSizeToMatrixDimension(CGSize size) {
    return MatrixDimensionMake(size.height, size.width);
}

extern const MatrixDimension MatrixDimensionZero;

/*============================================================================
 MatrixIndex
 =============================================================================*/
struct MatrixIndex {
    NSUInteger row;
    NSUInteger column;
};

inline static MatrixIndex MatrixIndexMake(NSUInteger row, NSUInteger column) {
    MatrixIndex index;
    index.row       = row;
    index.column    = column;
    return index;
}

/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   SEBYTEMatrix
 =============================================================================*/
@interface SEBYTEMatrix : SEBaseObject
@property (nonatomic, readonly) MatrixDimension dimension;

+ (SEBYTEMatrix *)matrixOfTransparentColorsWithDimension:(MatrixDimension)dimension;
- (void)setColor:(UIColor *)color forMaxtrixIndex:(MatrixIndex)matrixIndex;
- (void)setColor:(UIColor *)color forIndex:(NSUInteger)index;
- (void)fillColor:(UIColor *)color;
- (UIColor *)colorAtMatrixIndex:(MatrixIndex)index;
- (UIColor *)colorAtIndex:(NSUInteger)index;
- (NSComparisonResult)isSquareDimension;
- (instancetype)clonedMatrix;
- (void)unionMatrix:(SEBYTEMatrix *)matrix;

@end

/*============================================================================
 Interface:   SEBodyPartObject
 =============================================================================*/
@interface SEBodyPartObject : SEBaseObject
@property (nonatomic, assign)   CGPoint startPoint;
@property (nonatomic, readonly) SEBYTEMatrix *colorMatrix;
@property (nonatomic, assign) BOOL isReversed;
@property (nonatomic, assign) BOOL isModified;

- (void)updateColorMatrix:(SEBYTEMatrix *)matrix;
- (void)fillColorMatrixInContext:(CGContextRef)context;
- (void)getColorPixels:(unsigned char *)pixels
           bytesPerRow:(NSUInteger)bytesPerRow
         bytesPerPixel:(NSUInteger)bytesPerPixel;
- (void)parseImagePixelsToBodyPart:(UIImage *)image;

+ (SEBodyPartObject *)bodyPartWithDimension:(MatrixDimension)dimension startPoint:(CGPoint)startPoint;
+ (SEBodyPartObject *)reversedBodyPartWithDimension:(MatrixDimension)dimension startPoint:(CGPoint)startPoint;

@end
