/*============================================================================
 PROJECT: McpeMaster
 FILE:    ClipViewController.h
 AUTHOR:  Khoai Nguyen
 DATE:    12/23/14
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import <UIKit/UIKit.h>
#import "PRBaseListViewController.h"

/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   ClipViewController
 =============================================================================*/

@interface ClipViewController : PRBaseListViewController <UISearchBarDelegate,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>
@property(strong, nonatomic) PRServiceCategory *selectedCategory;
@property(assign, nonatomic) int indexCategory;

@end
