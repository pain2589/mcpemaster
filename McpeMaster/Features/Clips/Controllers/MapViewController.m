//
//  MapViewController.m
//  McpeMaster
//
//  Created by kaka on 2/5/17.
//
//

#import "MapViewController.h"
#import "iRate.h"
#import "Firebase.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface MapViewController ()

@end

@implementation MapViewController{
    MBProgressHUD *HUD;
    NSURLSessionDownloadTask *fileDownloadTask;
    BOOL isReadyRewardVideo;
    BOOL hasJustLoginFB;
    BOOL loginErr;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    //Admob Rewarded
    [GADRewardBasedVideoAd sharedInstance].delegate = self;
    if (![[GADRewardBasedVideoAd sharedInstance] isReady]) {
        [self requestRewardedVideo];
    }
    
    //Admob Banner
    [self initGoogleAdsBanner];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (loginErr) {
        loginErr = NO;
        AlertActionCompleteBlock completeBlock = ^(NSUInteger buttonIndex) {
            if (buttonIndex == 1) {
                [self fbLogin];
            }
        };
        [UIAlertController showConfirmPopUpWithTitle:nil
                                             message:@"Login Failed. Please try again!"
                                        buttonTitles:@[@"Cancel", @"Login"]
                                       completeBlock:completeBlock];
    }
    
    if (hasJustLoginFB) {
        self.realmMapObject = [self fetchMapFromDB];
        [self updateUI];
        hasJustLoginFB = NO;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //
    self.title = _strTitle;
    [self.view setBackgroundColor:UIColorFromRGB(0xE6E6E6)];
    [self.textViewDescription setBackgroundColor:UIColorFromRGB(0xE6E6E6)];
    //Add BarButton
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithImage:[IBHelper loadImage:@"icon_back"]
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(didTouchOnBackButton:)];
    self.navigationItem.leftBarButtonItem = leftButton;
    
    //
    if (_strThumbUrl) {
        [self.thumbImg sd_setImageWithURL:[NSURL URLWithString:_strThumbUrl]
                         placeholderImage:[UIImage imageNamed:@"store_background"]
                                completed:^(UIImage *img, NSError *err,SDImageCacheType type,NSURL *imgurl){
                                    
                                }];
    }
    
    if (_strMcpeDescription == nil){
        _strMcpeDescription = @"";
    }
    NSData *data = [[NSData alloc] initWithBase64EncodedString:_strMcpeDescription options:0];
    NSString *htmlString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSAttributedString *attributedString = [[NSAttributedString alloc]
                                                initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                                options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                documentAttributes: nil
                                                error: nil
                                                ];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            self.textViewDescription.attributedText = attributedString;
        });
    });
    //
    self.realmMapObject = [self fetchMapFromDB];
    //
    [self updateUI];
    
    //
    [FIRAnalytics setScreenName:@"Map_View" screenClass:@"Map_View"];
}

- (void)updateUI{
    //
    if ([self isMapDownloaded]){
        self.btnViewDownload.hidden = YES;
        self.btnViewImport.hidden = NO;
    }else{
        self.btnViewDownload.hidden = NO;
        self.btnViewImport.hidden = YES;
    }
    
    //
    if ([self isMapFavorite]){
        [self.btnFavorite1 setBackgroundImage:[UIImage imageNamed:@"icon_favorite"] forState:UIControlStateNormal];
        [self.btnFavorite2 setBackgroundImage:[UIImage imageNamed:@"icon_favorite"] forState:UIControlStateNormal];
    }else{
        [self.btnFavorite1 setBackgroundImage:[UIImage imageNamed:@"icon_unfavoriteBlack"] forState:UIControlStateNormal];
        [self.btnFavorite2 setBackgroundImage:[UIImage imageNamed:@"icon_unfavoriteBlack"] forState:UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)didTouchOnBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)counterMCPE{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *urlString= [NSString stringWithFormat:@"https://mcpemaster.co/mcpehub/counterMcpe.php?type=map&id=%@",self->_mcpeId];
        NSString *urlUTF8 = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSURL *url = [NSURL URLWithString:urlUTF8];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSURLResponse *response;
        NSError *err = nil;
        [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            if (err) {
                NSLog(@"ERROR --> %@", err.localizedDescription);
            }else{
                NSLog(@"Counter Succeffully id = %@",self->_mcpeId);
            }
        });
    });
}

//Download
- (IBAction)downloadMcpe:(id)sender{
    if ([self isMapDownloaded]){
        //Install Map
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            NSString *fileName = self->_strDownloadUrl.lastPathComponent;
            NSString *filePath = [NSString stringWithFormat:@"%@/maps/%@",[[ADUFileManager sharedManager] documentDirectoryPath],fileName];
            NSArray *activityItems = @[[NSURL fileURLWithPath:filePath]];
            
            UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
            activityViewController.excludedActivityTypes = @[UIActivityTypePostToFacebook,
                                                             UIActivityTypePostToTwitter,
                                                             UIActivityTypePostToWeibo,
                                                             UIActivityTypePrint,
                                                             UIActivityTypeCopyToPasteboard,
                                                             UIActivityTypeAssignToContact,
                                                             UIActivityTypeSaveToCameraRoll,
                                                             UIActivityTypeAddToReadingList,
                                                             UIActivityTypePostToFlickr,
                                                             UIActivityTypePostToVimeo,
                                                             UIActivityTypePostToTencentWeibo,
                                                             UIActivityTypeOpenInIBooks,
                                                             UIActivityTypeMessage,
                                                             UIActivityTypeMail];
            
            activityViewController.modalPresentationStyle = UIModalPresentationPopover;
            activityViewController.popoverPresentationController.sourceView = self->_btnViewImport;
            [self presentViewController:activityViewController animated:activityViewController completion:nil];
        });
    }else{
        //Show Alert when install after nth downloads
        int dlTimesInNewVersion = [UsrDfltObj4Key([PRAppDelegate majorVersion]) intValue];
        if ([AppDelegate sharedDelegate].mcpeType == 2508){
//            if (!UDBool4Key(kIsPro)){
//                if (dlTimesInNewVersion >= [PRAppDelegate mcpeNum] && [PRAppDelegate mcpeNum] != 0){
//                    AlertActionCompleteBlock completeBlock = ^(NSUInteger buttonIndex) {
//                        if (buttonIndex == 0) {
//                            if (self->isReadyRewardVideo) {
//                                [self showRewardedVideo];
//                            } else {
//                                //Cancel
//                            }
//                        } else {
//                            [[AppDelegate sharedDelegate].storeService presentAvailablePurchasesFrom:self];
//                        }
//                    };
//                    isReadyRewardVideo = [[GADRewardBasedVideoAd sharedInstance] isReady];
//
//                    if (isReadyRewardVideo) {
//                        [UIAlertController showConfirmPopUpWithTitle:@"Premium Membership"
//                                                             message:@"Upgrade Premium Membership for for Remove Ads, Unlimited Maps, Add-ons, Textures and Skins."
//                                                        buttonTitles:@[@"Watch Ads for 1 FREE download",@"Go Pro"]
//                                                       completeBlock:completeBlock];
//                    } else {
//                        [UIAlertController showConfirmPopUpWithTitle:@"Premium Membership"
//                                                             message:@"Upgrade Premium Membership for for Remove Ads, Unlimited Maps, Add-ons, Textures and Skins."
//                                                        buttonTitles:@[@"Cancel",@"Go Pro"]
//                                                       completeBlock:completeBlock];
//                    }
//
//                    return;
//                } else {
//                    [self downloadOrInstall];
//                }
//            } else {
//                [self downloadOrInstall];
//            }
            [self downloadOrInstall];
        }
        //Show message needed in 512 case
        else if ([AppDelegate sharedDelegate].mcpeType == 512) {
            if (![iRate sharedInstance].ratedAnyVersion) {
                if (dlTimesInNewVersion >= [PRAppDelegate mcpeNum]  && (PRAppDelegate).mcpeNum != 0){
                    if (![[AppDelegate sharedDelegate].mcpeMess isEqualToString:@""]) {
                        AlertActionCompleteBlock completeBlock = ^(NSUInteger buttonIndex) {
                            if (buttonIndex == 0) {
                                //cancel
                            } else {
                                [iRate sharedInstance].ratedThisVersion = YES;
                                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[AppDelegate sharedDelegate].mcpeTitle] options:@{} completionHandler:nil];
                            }
                        };
                        
                        [UIAlertController showConfirmPopUpWithTitle:@"Important!"
                                                             message:[AppDelegate sharedDelegate].mcpeMess
                                                        buttonTitles:@[@"No, Thanks",@"OK"]
                                                       completeBlock:completeBlock];
                        return;
                    }
                }
            }
        } else {
            // Other cases
            [self downloadOrInstall];
        }
    }
}

-(void)downloadOrInstall{
    if (_strDownloadUrl && ![_strDownloadUrl isKindOfClass:[NSNull class]]) {
        if (![_strDownloadUrl isEqualToString:@""]){
            [self counterMCPE];
            HUD = [[MBProgressHUD alloc] initWithView:self.parentViewController.view];
            [self.parentViewController.view addSubview:HUD];
            
            // Set the determinate mode to show task progress.
            HUD.mode = MBProgressHUDModeDeterminate;
            HUD.label.text = @"Downloading...";
            
            HUD.mode = MBProgressHUDModeAnnularDeterminate;
            // Configure a cancel button.
            [HUD.button setTitle:@"Cancel" forState:UIControlStateNormal];
            [HUD.button addTarget:self action:@selector(cancelDownload) forControlEvents:UIControlEventTouchUpInside];
            
            [HUD removeFromSuperViewOnHide];
            [HUD showAnimated:YES];
            
            NSString *mapsDirectoryPath = [[[ADUFileManager sharedManager] documentDirectoryPath] stringByAppendingString:@"/maps"];
            
            // Create the folder if necessary
            BOOL isDir = NO;
            NSFileManager *fileManager = [[NSFileManager alloc] init];
            if (![fileManager fileExistsAtPath:mapsDirectoryPath
                                   isDirectory:&isDir] && isDir == NO) {
                [fileManager createDirectoryAtPath:mapsDirectoryPath
                       withIntermediateDirectories:NO
                                        attributes:nil
                                             error:nil];
            }
            
            NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
            NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
            AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
            NSURLRequest *fileRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:_strDownloadUrl]];
            fileDownloadTask = [manager downloadTaskWithRequest:fileRequest progress:^(NSProgress *progress){
                self->HUD.progressObject = progress;
            }destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
                NSURL *fileUrl = [documentsDirectoryURL URLByAppendingPathComponent:@"maps"];
                fileUrl = [fileUrl URLByAppendingPathComponent:[response suggestedFilename]];
                return fileUrl;
            } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
                if (self.realmMapObject){
                    [MKRealmManager transactionWithBlock:^(RLMRealm *realm) {
                        RLMMapObject *object = [self.mapObject realmObject];
                        object.mcpeId = self.realmMapObject.mcpeId;
                        object.name = self.realmMapObject.name;
                        object.localFile = self.realmMapObject.file;
                        object.thumb = self.realmMapObject.thumb;
                        object.file = self.realmMapObject.file;
                        object.link = self.realmMapObject.link;
                        object.favorited = self.realmMapObject.favorited;
                        object.downloaded = YES;
                        object.mcpeDescription = self.realmMapObject.mcpeDescription;
                        /* remove existing realm object */
                        if (self.realmMapObject) {
                            object.logTime = self.realmMapObject.logTime;
                            [realm deleteObject:(RLMObject *)self.realmMapObject];
                        }
                        
                        [realm addObject:(RLMObject *)object];
                        self.realmMapObject = object;
                        [self updateUI];
                    }];
                }else{
                    [MKRealmManager transactionWithBlock:^(RLMRealm *realm) {
                        RLMMapObject *object = [self.mapObject realmObject];
                        object.mcpeId = self->_mcpeId;
                        object.name = self->_strTitle;
                        object.localFile = [NSURL URLWithString:self->_strDownloadUrl].lastPathComponent;
                        object.thumb = self->_strThumbUrl;
                        object.file = self->_strDownloadUrl;
                        object.link = [NSURL URLWithString:self->_strDownloadUrl].lastPathComponent.stringByDeletingPathExtension;
                        object.downloaded = YES;
                        object.mcpeDescription = self->_strMcpeDescription;
                        /* remove existing realm object */
                        if (self.realmMapObject) {
                            object.logTime = self.realmMapObject.logTime;
                            [realm deleteObject:(RLMObject *)self.realmMapObject];
                        }
                        
                        [realm addObject:(RLMObject *)object];
                        self.realmMapObject = object;
                        [self updateUI];
                    }];
                }
                
                int dlTimesInNewVersion = [UsrDfltObj4Key([PRAppDelegate majorVersion]) intValue];
                NSString *dlTimes = [NSString stringWithFormat:@"%d",dlTimesInNewVersion+1];
                UsrDfltSetObjKey(dlTimes, [PRAppDelegate majorVersion]);
                
                [self->HUD hideAnimated:YES];
            }];
            [fileDownloadTask resume];
        }
    }else{
        [UIAlertController showConfirmPopUpWithTitle:@"Invalid URL"
                                             message:@"Download Url is invalid. We'll fix it soon!"
                                        buttonTitles:@[@"OK"]
                                       completeBlock:nil];
    }
}

- (void)cancelDownload{
    [fileDownloadTask suspend];
    [self deleteDownloadedFile];
    [HUD hideAnimated:YES];
}

- (void)deleteDownloadedFile{
    if (self.realmMapObject) {
        [MKRealmManager transactionWithBlock:^(RLMRealm *realm) {
            RLMMapObject *object = [self.mapObject realmObject];
            object.mcpeId = self.realmMapObject.mcpeId;
            object.name = self.realmMapObject.name;
            object.localFile = self.realmMapObject.file;
            object.thumb = self.realmMapObject.thumb;
            object.file = self.realmMapObject.file;
            object.link = self.realmMapObject.link;
            object.favorited = self.realmMapObject.favorited;
            object.downloaded = NO;
            object.mcpeDescription = self.realmMapObject.mcpeDescription;
            
            /* remove existing realm object */
            if (self.realmMapObject) {
                object.logTime = self.realmMapObject.logTime;
                [realm deleteObject:(RLMObject *)self.realmMapObject];
            }
            
            [realm addObject:(RLMObject *)object];
            self.realmMapObject = object;
            
            //Delete file in device
            NSString *fileName = self->_strDownloadUrl.lastPathComponent;
            NSString *mapsDirectoryPath = [[[ADUFileManager sharedManager] documentDirectoryPath] stringByAppendingString:@"/maps/"];
            NSString *filePath = [mapsDirectoryPath stringByAppendingPathComponent:fileName];
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            if ([fileManager fileExistsAtPath:filePath]) {
                NSError *error;
                BOOL success = [fileManager removeItemAtPath:filePath error:&error];
                if (success) {
                    [self updateUI];
                }else
                {
                    NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
                }
            }
        }];
    }else{
        //Delete file in device
        NSString *fileName = _strDownloadUrl.lastPathComponent;
        NSString *mapsDirectoryPath = [[[ADUFileManager sharedManager] documentDirectoryPath] stringByAppendingString:@"/maps/"];
        NSString *filePath = [mapsDirectoryPath stringByAppendingPathComponent:fileName];
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        if ([fileManager fileExistsAtPath:filePath]) {
            NSError *error;
            BOOL success = [fileManager removeItemAtPath:filePath error:&error];
            if (success) {
                [self updateUI];
            }else
            {
                NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
            }
        }
        
    }
}

//
- (IBAction)onTouchedDeleteButton:(id)sender {
    AlertActionCompleteBlock completeBlock = ^(NSUInteger buttonIndex) {
        
        if (buttonIndex == 1) {
            [self deleteDownloadedFile];
        }
    };
    
    [UIAlertController showConfirmPopUpWithTitle:nil
                                         message:@"Are you sure want to delete this Map from your Device?"
                                    buttonTitles:@[@"Cancel", @"OK"]
                                   completeBlock:completeBlock];
}

//
- (IBAction)onFavoriteButton:(id)sender {
    if ([FBSDKAccessToken currentAccessToken]) {
        // User is logged in
        [FBSDKProfile loadCurrentProfileWithCompletion:
         ^(FBSDKProfile *profile, NSError *error) {
             if (profile) {
                 [self addFavorite];
                 if ([self isMapFavorite]) {
                     [self syncFavorite:profile.userID action:@"add"];
                 } else {
                     [self syncFavorite:profile.userID action:@"remove"];
                 }
             }
         }];
    } else {
        AlertActionCompleteBlock completeBlock = ^(NSUInteger buttonIndex) {
            if (buttonIndex == 1) {
                [self fbLogin];
            }
        };

        [UIAlertController showConfirmPopUpWithTitle:nil
                                             message:@"Please login Facebook to use Favorite function!"
                                        buttonTitles:@[@"Cancel", @"Login"]
                                       completeBlock:completeBlock];
    }
}

- (void)addFavorite {
    self.realmMapObject = [self fetchMapFromDB];
    if (self.realmMapObject){
        [MKRealmManager transactionWithBlock:^(RLMRealm *realm) {
            RLMMapObject *object = [self.mapObject realmObject];
            object.name = self.realmMapObject.name;
            object.localFile = self.realmMapObject.file;
            object.thumb = self.realmMapObject.thumb;
            object.file = self.realmMapObject.file;
            object.link = self.realmMapObject.link;
            object.downloaded = self.realmMapObject.downloaded;
            object.mcpeDescription = self.realmMapObject.mcpeDescription;
            /* add new one */
            if (object) {
                if ([self isMapFavorite]){
                    object.favorited = NO;
                }else{
                    object.favorited = YES;
                }
            }
            
            object.logTime = self.realmMapObject.logTime;
            [realm deleteObject:(RLMObject *)self.realmMapObject];
            
            [realm addObject:(RLMObject *)object];
            self.realmMapObject = object;
            [self updateUI];
        }];
    }else{
        [MKRealmManager transactionWithBlock:^(RLMRealm *realm) {
            RLMMapObject *object = [self.mapObject realmObject];
            object.mcpeId = self->_mcpeId;
            object.name = self->_strTitle;
            object.localFile = [NSURL URLWithString:self->_strDownloadUrl].lastPathComponent;
            object.thumb = self->_strThumbUrl;
            object.file = self->_strDownloadUrl;
            object.link = [NSURL URLWithString:self->_strDownloadUrl].lastPathComponent.stringByDeletingPathExtension;
            object.mcpeDescription = self->_strMcpeDescription;
            /* add new one */
            if (object) {
                if ([self isMapFavorite]){
                    object.favorited = NO;
                }else{
                    object.favorited = YES;
                }
            }
            
            object.logTime = self.realmMapObject.logTime;
            
            [realm addObject:(RLMObject *)object];
            self.realmMapObject = object;
            [self updateUI];
        }];
    }
}

- (void)syncFavorite:(NSString*)userId action:(NSString*)action {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *urlString= [NSString stringWithFormat:@"https://mcpemaster.co/mcpehub/addFavoriteMcpe.php?type=map&userId=%@&id=%@&action=%@",userId,self->_mcpeId,action];
        NSString *urlUTF8 = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSURL *url = [NSURL URLWithString:urlUTF8];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSURLResponse *response;
        NSError *err = nil;
        [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            if (err) {
                NSLog(@"ERROR --> %@", err.localizedDescription);
            }else{
                NSLog(@"Report Map Succeffully id = %@",self->_mcpeId);
            }
        });
    });
}

- (void)fbLogin {
    FBSDKLoginManager *fbLogin = [[FBSDKLoginManager alloc] init];
    [fbLogin logInWithReadPermissions:@[@"email"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error != nil) {
            [fbLogin logOut];
            self->loginErr = YES;
        } else if (result.isCancelled) {
            //cancelled
            [fbLogin logOut];
        } else {
            //success
            self->hasJustLoginFB = YES;
        }
    }];
}

- (IBAction)onReportButton:(id)sender {
    AlertActionCompleteBlock completeBlock = ^(NSUInteger buttonIndex) {
        if (buttonIndex == 0) {
            //cancel
        } else {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                NSString *urlString= [NSString stringWithFormat:@"https://mcpemaster.co/mcpehub/reportMcpe.php?type=map&id=%@",self->_mcpeId];
                NSString *urlUTF8 = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
                NSURL *url = [NSURL URLWithString:urlUTF8];
                NSURLRequest *request = [NSURLRequest requestWithURL:url];
                NSURLResponse *response;
                NSError *err = nil;
                [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    if (err) {
                        NSLog(@"ERROR --> %@", err.localizedDescription);
                    }else{
                        NSLog(@"Report Map Succeffully id = %@",self->_mcpeId);
                    }
                });
            });
        }
    };
    
    [UIAlertController showConfirmPopUpWithTitle:@"Report this Map"
                                         message:@"You should report this map only when it does not work. We will review reported Maps and fix asap."
                                    buttonTitles:@[@"Cancel",@"Report"]
                                   completeBlock:completeBlock];
}

// Then implement the delegate method
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark - DB Support

- (SEMapObject *)mapObject {
    
    if (!_mapObject) {
        _mapObject = [SEMapObject newObject];
    }
    return _mapObject;
}

- (BOOL)isMapFavorite{
    return self.realmMapObject.favorited;
}

- (BOOL)isMapDownloaded{
    return self.realmMapObject.downloaded;
}

- (RLMMapObject*)fetchMapFromDB {
    RLMResults<RLMMapObject *> *objects = [[RLMMapObject objectsWhere:[NSString stringWithFormat:@"link = '%@'",[NSURL URLWithString:_strDownloadUrl].lastPathComponent.stringByDeletingPathExtension]] sortedResultsUsingProperty:@"logTime" ascending:NO];
    return objects.firstObject;
}

#pragma mark - GAD Banner Ads
- (void)initGoogleAdsBanner{
    if (!self.bannerView)
    {
        self.bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerPortrait];
        [self addBannerViewToView:self.bannerView];
        
        if (IS_IPAD) {
            CGRect frame = self.bannerView.frame;
            frame.origin.x = 20;
            self.bannerView.frame = frame;
        }
        
        self.bannerView.adUnitID = ADMOB_BANNER_ID;
        self.bannerView.rootViewController = self;
        self.bannerView.delegate = self;
        self.bannerView.hidden = TRUE;
        [self.view addSubview:self.bannerView];
        
        GADRequest *request = [GADRequest request];
        //request.testDevices = @[kGADSimulatorID];
        
        //Forward user consent choice
        if (PACConsentInformation.sharedInstance.requestLocationInEEAOrUnknown &&
            PACConsentInformation.sharedInstance.consentStatus == PACConsentStatusNonPersonalized) {
            GADExtras *extras = [[GADExtras alloc] init];
            extras.additionalParameters = @{@"npa": @"1"};
            [request registerAdNetworkExtras:extras];
            NSLog(@"Ads is non-personalized now!!!");
        }
        
        [self.bannerView loadRequest:request];
    }
}

- (void)addBannerViewToView:(UIView *)bannerView {
    bannerView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:bannerView];
    if (@available(iOS 11.0, *)) {
        // In iOS 11, we need to constrain the view to the safe area.
        [self positionBannerViewFullWidthAtBottomOfSafeArea:bannerView];
    } else {
        // In lower iOS versions, safe area is not available so we use
        // bottom layout guide and view edges.
        [self positionBannerViewFullWidthAtBottomOfView:bannerView];
    }
}

#pragma mark - view positioning

- (void)positionBannerViewFullWidthAtBottomOfSafeArea:(UIView *_Nonnull)bannerView NS_AVAILABLE_IOS(11.0) {
    // Position the banner. Stick it to the bottom of the Safe Area.
    // Make it constrained to the edges of the safe area.
    UILayoutGuide *guide = self.view.safeAreaLayoutGuide;
    
    [NSLayoutConstraint activateConstraints:@[
                                              [guide.leftAnchor constraintEqualToAnchor:bannerView.leftAnchor],
                                              [guide.rightAnchor constraintEqualToAnchor:bannerView.rightAnchor],
                                              [guide.bottomAnchor constraintEqualToAnchor:bannerView.bottomAnchor]
                                              ]];
}

- (void)positionBannerViewFullWidthAtBottomOfView:(UIView *_Nonnull)bannerView {
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:bannerView
                                                          attribute:NSLayoutAttributeLeading
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeLeading
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:bannerView
                                                          attribute:NSLayoutAttributeTrailing
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeTrailing
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:bannerView
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view.safeAreaLayoutGuide.bottomAnchor
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1
                                                           constant:0]];
}

#pragma mark - Banner hide and show -

// Hide the banner by sliding down
-(void)hideBanner:(UIView*)banner
{
    banner.hidden = TRUE;
}

// Show the banner by sliding up
-(void)showBanner:(UIView*)banner
{
    banner.hidden = FALSE;
}

#pragma mark - GADBanner delegate methods -

-(void)showAdmobBanner{
    self.bannerView.hidden = FALSE;
}

-(void)hideAdmobBanner{
    self.bannerView.hidden = TRUE;
}

// Called before ad is shown, good time to show the add
- (void)adViewDidReceiveAd:(GADBannerView *)view
{
    NSLog(@"Admob load");
    [self showBanner:self.bannerView];
}

// An error occured
- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error
{
    NSLog(@"Admob error: %@", error);
    [self hideBanner:self.bannerView];
}


//Admob Rewared
- (void)requestRewardedVideo {
    GADRequest *request = [GADRequest request];
    
    //Forward user consent choice
    if (PACConsentInformation.sharedInstance.requestLocationInEEAOrUnknown &&
        PACConsentInformation.sharedInstance.consentStatus == PACConsentStatusNonPersonalized) {
        GADExtras *extras = [[GADExtras alloc] init];
        extras.additionalParameters = @{@"npa": @"1"};
        [request registerAdNetworkExtras:extras];
        NSLog(@"Ads is non-personalized now!!!");
    }
    
    [[GADRewardBasedVideoAd sharedInstance] loadRequest:request
                                           withAdUnitID:ADMOB_REWARDED];
}

- (void)showRewardedVideo {
    [[GADRewardBasedVideoAd sharedInstance] presentFromRootViewController:self];
}

#pragma mark GADRewardBasedVideoAdDelegate implementation

- (void)rewardBasedVideoAdDidReceiveAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad is received.");
}

- (void)rewardBasedVideoAdDidOpen:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Opened reward based video ad.");
}

- (void)rewardBasedVideoAdDidStartPlaying:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad started playing.");
}

- (void)rewardBasedVideoAdDidClose:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad is closed.");
    //self.showVideoButton.hidden = YES;
}

- (void)rewardBasedVideoAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd
   didRewardUserWithReward:(GADAdReward *)reward {
    //NSString *rewardMessage =
    //[NSString stringWithFormat:@"Reward received with currency %@ , amount %lf", reward.type,
     //[reward.amount doubleValue]];
    //NSLog(@"%@", rewardMessage);
    // Reward the user for watching the video.
    //[self earnCoins:[reward.amount integerValue]];
    //self.showVideoButton.hidden = YES;
    
    [self requestRewardedVideo];
    NSLog(@"Rewarded successfully.");
    [self downloadOrInstall];
}

- (void)rewardBasedVideoAdWillLeaveApplication:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad will leave application.");
}

- (void)rewardBasedVideoAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd
    didFailToLoadWithError:(NSError *)error {
    //NSLog(@"Reward based video ad failed to load.");
    //[self requestRewardedVideo];
}


@end
