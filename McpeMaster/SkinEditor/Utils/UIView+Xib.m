/*============================================================================
 PROJECT: SkinEditor
 FILE:    UIView+Xib.m
 AUTHOR:  Nguyen Minh Khoai
 DATE:    10/19/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "UIView+Xib.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@implementation UIView (Xib)

+ (instancetype)viewFromNib:(NSString *)nibName {
    
    // Instantiate the nib content without any reference to it.
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];

    // Find the view among nib contents (not too hard assuming there is only one view in it).
    return [nibContents lastObject];
}

+ (instancetype)nibView {
    return [self viewFromNib:NSStringFromClass(self)];
}

@end
