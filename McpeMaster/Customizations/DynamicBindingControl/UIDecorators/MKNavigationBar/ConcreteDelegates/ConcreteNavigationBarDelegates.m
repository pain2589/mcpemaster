/*============================================================================
 PROJECT: McpeMaster
 FILE:    MenuAndSearchNavigationBarView.m
 AUTHOR:  Khoai Nguyen
 DATE:    2/8/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "ConcreteNavigationBarDelegates.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/

/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@implementation MKLeftMenuNavigationBarDelegate

- (void)setUpNavigationBar {

    [self.delegate setUpNavigationBar];
    
        UIBarButtonItem *barButtonItem = createNavigationBarItem(self, @{kBarItemImage : @"icon_menu",
                                                                         kBarItemSelector : @"didTouchOnBarButtonItem:"});
        self.ownedViewController.navigationItem.leftBarButtonItems = @[barButtonItem];
//    }
}

@end

@implementation MKLeftBackNavigationBarDelegate

- (void)setUpNavigationBar {

    [self.delegate setUpNavigationBar];

    UIBarButtonItem *barButtonItem = createNavigationBarItem(self, @{kBarItemImage : @"icon_back",
            kBarItemSelector : @"didTouchOnBarButtonItem:"});
    self.ownedViewController.navigationItem.leftBarButtonItems = @[barButtonItem];
}

- (void)didTouchOnBarButtonItem:(UIButton *)button {

    if (self.onTouchedCallback != nil && self.onTouchedCallback(MKNavigationBarButtonTypeBack) == NO) {
        return;
    }
    
    [self.ownedViewController.navigationController popViewControllerAnimated:YES];
}

@end
