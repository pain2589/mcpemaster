/*============================================================================
 PROJECT: McpeMaster
 FILE:    UIViewController+Utility.m
 AUTHOR:  Khoai Nguyen
 DATE:    2/8/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "UIViewController+Utility.h"
#import "ViewControllerFactory.h"
#import "Invocation.h"
#import "NSObject+Utility.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@implementation UIViewController (Utility)

- (void)pushToControllerName:(NSString *)name {
    [self pushToControllerName:name withData:nil];
}

- (void)pushToControllerName:(NSString *)name withData:(NSDictionary *)data {

    UINavigationController *navController = nil;
    if ([self hasProperty:@"navigationController"])
        navController = [self valueForKey:@"navigationController"];

    if (!navController) {
        UIViewController *topViewController = [self topViewController];
        if (topViewController)
            navController = topViewController.navigationController;
    }

    if (navController) {
        UIViewController *controller = [[ViewControllerFactory sharedInstance] controllerForName:name
                                                                         andBindDataIntoInstance:data];
        if (controller)
            [navController pushViewController:controller animated:TRUE];
    }
}

- (UIViewController *)viewControllerByIdentifier:(NSString *)identifier {
    return [[ViewControllerFactory sharedInstance] controllerForName:identifier];
}

- (UIViewController *)topViewController {
    return [self topViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

#pragma mark - private function -

- (UIViewController *)topViewControllerWithRootViewController:(UIViewController *)rootViewController {

    if ([rootViewController isKindOfClass:[UITabBarController class]]) {

        UITabBarController *tabBarController = (UITabBarController *) rootViewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];

    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {

        UINavigationController *navigationController = (UINavigationController *) rootViewController;
        return [self topViewControllerWithRootViewController:navigationController.visibleViewController];

    } else if (rootViewController.presentedViewController) {

        UIViewController *presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];

    } else if ([rootViewController isKindOfClass:NSClassFromString(@"PKRevealController")]) {

        if ([self hasMethod:@"frontViewController" inDelegate:rootViewController]) {

            UIViewController *frontViewController = nil;

            Invocation *invocationManager = [Invocation sharedInstance];
            NSInvocation *invocation = [invocationManager createInvocationWithMethodName:@"frontViewController"
                                                                                  target:rootViewController
                                                                              parameters:nil];
            [invocationManager invoke:invocation returnData:&frontViewController];

            return [self topViewControllerWithRootViewController:frontViewController];
        }

        return nil; // do nothing with exceptions

    } else {

        for (UIView *view in [rootViewController.view subviews]) {
            id subViewController = [view nextResponder];
            if (subViewController && [subViewController isKindOfClass:[UIViewController class]]) {
                return [self topViewControllerWithRootViewController:subViewController];
            }
        }
        return rootViewController;
    }
}

@end
