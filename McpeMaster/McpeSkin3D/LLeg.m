//
//  LLeg.m
//  GirlSkin
//
//  Created by apple on 2016. 7. 12..
//  Copyright (c) 2016年 SWORD. All rights reserved.
//

#import "LLeg.h"


@implementation LLeg
- (void) initCube:(float)height
{
    if (height == 32.0f) {
        float tmplleg_texcoords[] = {
            // front leftLeg
            0.0f/64.0f + 8.0f/64.0f,	1.0f,
            0.0f/64.0f + 8.0f/64.0f,	1.0f - 6.0f/32.0f,
            0.0f/64.0f + 4.0f/64.0f,	1.0f - 6.0f/32.0f,
            0.0f/64.0f + 4.0f/64.0f,	1.0f,
            // front leftLeg
            0.0f/64.0f + 8.0f/64.0f,	1.0f - 6.0f/32.0f,
            0.0f/64.0f + 8.0f/64.0f,	1.0f - 12.0f/32.0f,
            0.0f/64.0f + 4.0f/64.0f,	1.0f - 12.0f/32.0f,
            0.0f/64.0f + 4.0f/64.0f,	1.0f - 6.0f/32.0f,
            // top leftLeg
            0.0f/64.0f + 4.0f/64.0f,	1.0f - 12.0f/32.0f,
            0.0f/64.0f + 4.0f/64.0f,	1.0f - 16.0f/32.0f,
            0.0f/64.0f + 8.0f/64.0f,	1.0f - 16.0f/32.0f,
            0.0f/64.0f + 8.0f/64.0f,	1.0f - 12.0f/32.0f,
            // bottom leftLeg
            0.0f/64.0f + 8.0f/64.0f,	1.0f - 12.0f/32.0f,
            0.0f/64.0f + 8.0f/64.0f,	1.0f - 16.0f/32.0f,
            0.0f/64.0f + 12.0f/64.0f,	1.0f - 16.0f/32.0f,
            0.0f/64.0f + 12.0f/64.0f,	1.0f - 12.0f/32.0f,
            // left leftLeg
            -8.0f/64.0f + 8.0f/64.0f,	1.0f,
            -8.0f/64.0f + 8.0f/64.0f,	1.0f - 6.0f/32.0f,
            -8.0f/64.0f + 12.0f/64.0f,	1.0f - 6.0f/32.0f,
            -8.0f/64.0f + 12.0f/64.0f,	1.0f,
            // left leftLeg
            -8.0f/64.0f + 8.0f/64.0f,	1.0f - 6.0f/32.0f,
            -8.0f/64.0f + 8.0f/64.0f,	1.0f - 12.0f/32.0f,
            -8.0f/64.0f + 12.0f/64.0f,	1.0f - 12.0f/32.0f,
            -8.0f/64.0f + 12.0f/64.0f,	1.0f - 6.0f/32.0f,
            // right leftLeg
            0.0f/64.0f + 8.0f/64.0f,	1.0f,
            0.0f/64.0f + 8.0f/64.0f,	1.0f - 6.0f/32.0f,
            0.0f/64.0f + 12.0f/64.0f,	1.0f - 6.0f/32.0f,
            0.0f/64.0f + 12.0f/64.0f,	1.0f,
            // right leftLeg
            0.0f/64.0f + 8.0f/64.0f,	1.0f - 6.0f/32.0f,
            0.0f/64.0f + 8.0f/64.0f,	1.0f - 12.0f/32.0f,
            0.0f/64.0f + 12.0f/64.0f,	1.0f - 12.0f/32.0f,
            0.0f/64.0f + 12.0f/64.0f,	1.0f - 6.0f/32.0f,
            // back leftLeg
            0.0f/64.0f + 16.0f/64.0f,	1.0f,
            0.0f/64.0f + 16.0f/64.0f,	1.0f - 6.0f/32.0f,
            0.0f/64.0f + 12.0f/64.0f,	1.0f - 6.0f/32.0f,
            0.0f/64.0f + 12.0f/64.0f,	1.0f,
            // back leftLeg
            0.0f/64.0f + 16.0f/64.0f,	1.0f - 6.0f/32.0f,
            0.0f/64.0f + 16.0f/64.0f,	1.0f - 12.0f/32.0f,
            0.0f/64.0f + 12.0f/64.0f,	1.0f - 12.0f/32.0f,
            0.0f/64.0f + 12.0f/64.0f,	1.0f - 6.0f/32.0f,
        };
        
        memcpy(lleg_texcoords, tmplleg_texcoords, sizeof(lleg_texcoords));

        [super initCube:4.0f sy:12.0f sz:4.0f offsetx:2.0f offsety:-10.0f offsetz:0.0f step_value:60.f/40.0f rot_axis_x:1.0f rot_axis_y:0.0f rot_axis_z:0.0f max_anlge:30.0f min_angle:-30.0f step_value1:-60.0f/40.0f max_angle1:30.0f min_angle:-30.0f isFixAngle:true fixDir:-1.0f];
        [super AddTextures:lleg_texcoords nSize:sizeof(lleg_texcoords) / sizeof(float)];
    }else if (height == 64.0f){
        float tmplleg_texcoords[] = {
            // front leftLeg
            20.0f/64.0f,	1.0f,
            20.0f/64.0f,	1.0f - 6.0f/64.0f,
            24.0f/64.0f,	1.0f - 6.0f/64.0f,
            24.0f/64.0f,	1.0f,
            // front leftLeg1
            20.0f/64.0f,	1.0f - 6.0f/64.0f,
            20.0f/64.0f,	1.0f - 12.0f/64.0f,
            24.0f/64.0f,	1.0f - 12.0f/64.0f,
            24.0f/64.0f,	1.0f - 6.0f/64.0f,
            // top leftLeg
            20.0f/64.0f,	1.0f - 12.0f/64.0f,
            20.0f/64.0f,	1.0f - 16.0f/64.0f,
            24.0f/64.0f,	1.0f - 16.0f/64.0f,
            24.0f/64.0f,	1.0f - 12.0f/64.0f,
            // bottom leftLeg
            24.0f/64.0f,	1.0f - 12.0f/64.0f,
            24.0f/64.0f,	1.0f - 16.0f/64.0f,
            28.0f/64.0f,	1.0f - 16.0f/64.0f,
            28.0f/64.0f,	1.0f - 12.0f/64.0f,
            // left leftLeg
            24.0f/64.0f,	1.0f,
            24.0f/64.0f,	1.0f - 6.0f/64.0f,
            28.0f/64.0f,	1.0f - 6.0f/64.0f,
            28.0f/64.0f,	1.0f,
            // left leftLeg1
            24.0f/64.0f,	1.0f - 6.0f/64.0f,
            24.0f/64.0f,	1.0f - 12.0f/64.0f,
            28.0f/64.0f,	1.0f - 12.0f/64.0f,
            28.0f/64.0f,	1.0f - 6.0f/64.0f,
            // right leftLeg
            24.0f/64.0f,	1.0f,
            24.0f/64.0f,	1.0f - 6.0f/64.0f,
            28.0f/64.0f,	1.0f - 6.0f/64.0f,
            28.0f/64.0f,	1.0f,
            // right leftLeg1
            24.0f/64.0f,	1.0f - 6.0f/64.0f,
            24.0f/64.0f,	1.0f - 12.0f/64.0f,
            28.0f/64.0f,	1.0f - 12.0f/64.0f,
            28.0f/64.0f,	1.0f - 6.0f/64.0f,
            // back leftLeg
            28.0f/64.0f,	1.0f,
            28.0f/64.0f,	1.0f - 6.0f/64.0f,
            32.0f/64.0f,	1.0f - 6.0f/64.0f,
            32.0f/64.0f,	1.0f,
            // back leftLeg1
            28.0f/64.0f,	1.0f - 6.0f/64.0f,
            28.0f/64.0f,	1.0f - 12.0f/64.0f,
            32.0f/64.0f,	1.0f - 12.0f/64.0f,
            32.0f/64.0f,	1.0f - 6.0f/64.0f,
        };
        
        memcpy(lleg_texcoords, tmplleg_texcoords, sizeof(lleg_texcoords));
        
        float tmplleg2_texcoords[] = {
            // front leftLeg
            4.0f/64.0f,     1.0f,                       // 0
            4.0f/64.0f,     1.0f - 6.0f/64.0f,		    // 3
            8.0f/64.0f,     1.0f - 6.0f/64.0f, 	  		// 2
            8.0f/64.0f,     1.0f, 					  	// 1
            // front leftLeg1
            4.0f/64.0f,     1.0f - 6.0f/64.0f,			// 0
            4.0f/64.0f,     1.0f - 12.0f/64.0f,		    // 3
            8.0f/64.0f,     1.0f - 12.0f/64.0f, 	  	// 2
            8.0f/64.0f,     1.0f - 6.0f/64.0f, 			// 1
            // top face.
            4.0f/64.0f,     1.0f - 12.0f/64.0f,	    	// 3
            4.0f/64.0f,     1.0f - 16.0f/64.0f, 		// 7
            8.0f/64.0f,     1.0f - 16.0f/64.0f,			// 6
            8.0f/64.0f,     1.0f - 12.0f/64.0f,			// 2
            // bottom leftLeg
            8.0f/64.0f,     1.0f - 12.0f/64.0f,			// 4
            8.0f/64.0f,     1.0f - 16.0f/64.0f,			// 0
            12.0f/64.0f,	1.0f - 16.0f/64.0f,         // 1
            12.0f/64.0f,	1.0f - 12.0f/64.0f,         // 5
            // left leftLeg
            8.0f/64.0f,     1.0f,						// 1
            8.0f/64.0f,     1.0f - 6.0f/64.0f,			// 2
            12.0f/64.0f,	1.0f - 6.0f/64.0f,          // 6
            12.0f/64.0f,	1.0f,                       // 5
            // left fleftLeg1
            8.0f/64.0f,     1.0f - 6.0f/64.0f,			// 1
            8.0f/64.0f,     1.0f - 12.0f/64.0f,			// 2
            12.0f/64.0f,	1.0f - 12.0f/64.0f,         // 6
            12.0f/64.0f,	1.0f - 6.0f/64.0f,          // 5
            // right leftLeg
            0.0f,           1.0f,		   				// 7
            0.0f,           1.0f - 6.0f/64.0f,			// 3
            4.0f/64.0f,     1.0f - 6.0f/64.0f,   		// 0
            4.0f/64.0f,     1.0f,   					// 4
            // right leftLeg1
            0.0f,           1.0f - 6.0f/64.0f,		   	// 7
            0.0f,           1.0f - 12.0f/64.0f,			// 3
            4.0f/64.0f,     1.0f - 12.0f/64.0f,   		// 0
            4.0f/64.0f,     1.0f - 6.0f/64.0f,   		// 4
            // back leftLeg
            12.0f/64.0f,	1.0f,                       // 5
            12.0f/64.0f,	1.0f - 6.0f/64.0f,          // 6
            16.0f/64.0f,	1.0f - 6.0f/64.0f,          // 7
            16.0f/64.0f,	1.0f,                       // 4
            // back leftLeg1
            12.0f/64.0f,	1.0f - 6.0f/64.0f,          // 5
            12.0f/64.0f,	1.0f - 12.0f/64.0f,         // 6
            16.0f/64.0f,	1.0f - 12.0f/64.0f,         // 7
            16.0f/64.0f,	1.0f - 6.0f/64.0f,          // 4
        };
        
        memcpy(lleg2_texcoords, tmplleg2_texcoords, sizeof(lleg2_texcoords));
        [super initCube:4.0f sy:12.0f sz:4.0f offsetx:2.0f offsety:-10.0f offsetz:0.0f step_value:60.f/40.0f rot_axis_x:1.0f rot_axis_y:0.0f rot_axis_z:0.0f max_anlge:30.0f min_angle:-30.0f step_value1:-60.0f/40.0f max_angle1:30.0f min_angle:-30.0f isFixAngle:true fixDir:-1.0f];
        [super AddTextures:lleg_texcoords nSize:sizeof(lleg_texcoords) / sizeof(float)];
        [super AddTextures:lleg2_texcoords nSize:sizeof(lleg2_texcoords) / sizeof(float)];
    }
}
@end
