//
//  GameCharacter..
//  GirlSkin
//
//  Created by apple on 2016. 7. 12..
//  Copyright (c) 2016년 SWORD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "Head.h"
#import "Body.h"
#import "LArm.h"
#import "RArm.h"
#import "LLeg.h"
#import "RLeg.h"
#import "Hat.h"

@interface GameCharacter : NSObject{
    bool isRunning;
    Head *mHead;
    LArm *mLArm;
    Body *mBody;
    RArm *mRArm;
    LLeg *mLLeg;
    RLeg *mRLeg;
    Hat  *mHat;
    
    float mRotate[3];
    float rotate_step[3];
}

@property (nonatomic,strong) UIImage *skinImg;
@property (nonatomic,assign) BOOL isAnimated;

- (void) initCharacter;
- (void) draw;
- (void) SetRotate: (float) ax
                  : (float) ay
                  :(float) az;
- (void) AddRotate: (float) ax
                  : (float) ay
                  :(float) az;
- (void) SetRotateStep: (float) dx
                      : (float) dy;
- (void) SetRunning: (bool) isRun;

- (void)clearTexture;

@end