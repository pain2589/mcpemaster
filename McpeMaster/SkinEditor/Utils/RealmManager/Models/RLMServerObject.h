/*============================================================================
 PROJECT: SkinEditor
 FILE:    RLMServerObject.h
 AUTHOR:  Nguyen Van Tiep
 DATE:    11/10/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import <Realm/Realm.h>

/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   RLMServerObject
 =============================================================================*/
@interface RLMServerObject : RLMObject
@property NSString *mcpeId;
@property NSString *name;
@property NSString *type;
@property NSString *file;
@property NSString *thumb;
@property NSString *serverIP;
@property NSString *serverPort;
@property NSString *serverDescription;
@property NSString *link;
@property double logTime;
@property BOOL favorited;
@end
