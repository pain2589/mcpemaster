/*============================================================================
 PROJECT: SkinEditor
 FILE:    NSMutableArray+SESkinObject.m
 AUTHOR:  Khoai Nguyen
 DATE:    11/7/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "NSMutableArray+SESkinObject.h"
#import "SESkinObject.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@implementation NSMutableArray (SESkinObject)


- (void)insertPart:(SESkinBodyPartObject *)part atIndex:(NSUInteger)index {
    
    if (index <= 0 || index > self.count) {
        return;
    }
    
   self[index - 1] = part;
}

- (SESkinBodyPartObject *)partAtIndex:(NSUInteger)index {
    
    if (index <= 0 || index > self.count) {
        return nil;
    }
    return self[index - 1];
}

@end
