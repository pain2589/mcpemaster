/*============================================================================
 PROJECT: McpeMaster
 FILE:    BaseNavigationViewController.m
 AUTHOR:  Khoai Nguyen
 DATE:    12/23/14
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "BaseNavigationViewController.h"
/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@interface BaseNavigationViewController ()

@end

@implementation BaseNavigationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    /* change color */
    [[UINavigationBar appearance] setBarTintColor:UIColorFromRGB(0xE6E6E6)];
    self.navigationBar.translucent = NO;
    
    //Tiep Xóa bo tròn góc ở view
//    if (self.cornerRadius) {
//        self.view.layer.cornerRadius = self.cornerRadius.integerValue;
//    } else {
//        self.view.layer.cornerRadius = 10;
//    }
//    self.view.clipsToBounds = YES;

    NSLog(@"[TOP VIEW]: %@", NSStringFromClass([[self topViewController] class]));
    
    //show native express admob ads
    if ([NSStringFromClass([[self topViewController] class]) isEqualToString:@"ServerViewController"]){
        //Don't show banner at ServerViewController
    }else{
        //[self initGoogleAdsBanner];
    }
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    if (IS_IPHONE_X_XS || IS_IPHONE_XS_MAX | IS_IPHONE_XR){
        //Tiep fix bug iphone X navigation bar Phuong an 1
//        CGRect rect = self.navigationBar.frame;
//        rect.origin.y = 0;
//        self.navigationBar.frame = rect;
//        rect.origin.y += 10;
//        self.navigationBar.frame = rect;
    }else{
        /* hot fix or wordaround to force navigation bar without padding space 20px on top */
        CGRect rect = self.navigationBar.frame;
        rect.origin.y = 0;
        self.navigationBar.frame = rect;
        
        /* special layout for search view controller options */
        if (self.topMargin.floatValue > 0) {
            rect.origin.x = 0;
            rect.origin.y = self.topMargin.floatValue;
            rect.size.height = O_SCREEN_HEIGHT - self.topMargin.floatValue;
            rect.size.width = O_SCREEN_WIDTH;
            self.view.frame = rect;
        }
    }

    NSLog(@"[viewDidLayoutSubviews]: %@", NSStringFromClass([[self topViewController] class]));
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (BOOL)shouldAutorotate {
#ifdef IPHONE_LANDSCAPE_HIDE
    return YES;
#else
    return [PRSettings defaultSettings].isLandScapeMode;
#endif
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < 90000
- (NSUInteger)supportedInterfaceOrientations
#else
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
#endif
{
    //Remove
//    if (IS_IPAD) {
//        return UIInterfaceOrientationMaskLandscape;
//    }
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - GAD Banner Ads
- (void)initGoogleAdsBanner{
    if (!self.bannerView)
    {
        self.bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerPortrait];
        [self addBannerViewToView:self.bannerView];
        
        if (IS_IPAD) {
            CGRect frame = self.bannerView.frame;
            frame.origin.x = 20;
            self.bannerView.frame = frame;
        }
        
        self.bannerView.adUnitID = ADMOB_BANNER_ID;
        self.bannerView.rootViewController = self;
        self.bannerView.delegate = self;
        self.bannerView.hidden = TRUE;
        [self.view addSubview:self.bannerView];
        
        GADRequest *request = [GADRequest request];
        //request.testDevices = @[kGADSimulatorID];
        
        //Forward user consent choice
        if (PACConsentInformation.sharedInstance.requestLocationInEEAOrUnknown &&
            PACConsentInformation.sharedInstance.consentStatus == PACConsentStatusNonPersonalized) {
            GADExtras *extras = [[GADExtras alloc] init];
            extras.additionalParameters = @{@"npa": @"1"};
            [request registerAdNetworkExtras:extras];
            NSLog(@"Ads is non-personalized now!!!");
        }
        
        [self.bannerView loadRequest:request];
    }
}

- (void)addBannerViewToView:(UIView *)bannerView {
    bannerView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:bannerView];
    if (@available(iOS 11.0, *)) {
        // In iOS 11, we need to constrain the view to the safe area.
        [self positionBannerViewFullWidthAtBottomOfSafeArea:bannerView];
    } else {
        // In lower iOS versions, safe area is not available so we use
        // bottom layout guide and view edges.
        [self positionBannerViewFullWidthAtBottomOfView:bannerView];
    }
}

#pragma mark - view positioning

- (void)positionBannerViewFullWidthAtBottomOfSafeArea:(UIView *_Nonnull)bannerView NS_AVAILABLE_IOS(11.0) {
    // Position the banner. Stick it to the bottom of the Safe Area.
    // Make it constrained to the edges of the safe area.
    UILayoutGuide *guide = self.view.safeAreaLayoutGuide;
    
    [NSLayoutConstraint activateConstraints:@[
                                              [guide.leftAnchor constraintEqualToAnchor:bannerView.leftAnchor],
                                              [guide.rightAnchor constraintEqualToAnchor:bannerView.rightAnchor],
                                              [guide.bottomAnchor constraintEqualToAnchor:bannerView.bottomAnchor]
                                              ]];
}

- (void)positionBannerViewFullWidthAtBottomOfView:(UIView *_Nonnull)bannerView {
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:bannerView
                                                          attribute:NSLayoutAttributeLeading
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeLeading
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:bannerView
                                                          attribute:NSLayoutAttributeTrailing
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeTrailing
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:bannerView
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view.safeAreaLayoutGuide.bottomAnchor
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1
                                                           constant:0]];
}

#pragma mark - Banner hide and show -

// Hide the banner by sliding down
-(void)hideBanner:(UIView*)banner
{
    banner.hidden = TRUE;
}

// Show the banner by sliding up
-(void)showBanner:(UIView*)banner
{
    banner.hidden = FALSE;
}

#pragma mark - GADBanner delegate methods -

-(void)showAdmobBanner{
    self.bannerView.hidden = FALSE;
}

-(void)hideAdmobBanner{
    self.bannerView.hidden = TRUE;
}

// Called before ad is shown, good time to show the add
- (void)adViewDidReceiveAd:(GADBannerView *)view
{
    NSLog(@"Admob load");
    [self showBanner:self.bannerView];
}

// An error occured
- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error
{
    NSLog(@"Admob error: %@", error);
    [self hideBanner:self.bannerView];
}

@end
