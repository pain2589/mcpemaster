/*============================================================================
 PROJECT: McpeMaster
 FILE:    MKNavigationBarDelegateDecorator.h
 AUTHOR:  Khoai Nguyen
 DATE:    2/8/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import <Foundation/Foundation.h>
#import "MKNavigationBarDelegate.h"

/*============================================================================
 MACRO
 =============================================================================*/

static NSString *kBarItemImage = @"kBarItemImage";
static NSString *kBarItemSelector = @"kBarItemSelector";

UIBarButtonItem *createNavigationBarItem(id target, NSDictionary *attributes);

/*============================================================================
 PROTOCOL
 =============================================================================*/
typedef BOOL(^OnTouchedNavigationBarCallBack)(NSUInteger barItemIndex);

/*============================================================================
 Interface:   MKNavigationBarDelegateDecorator
 =============================================================================*/

@interface MKNavigationBarDelegateDecorator : NSObject <MKNavigationBarDelegate, MKViewDelegate>
@property(nonatomic, strong) id <MKNavigationBarDelegate> delegate;
@property(nonatomic, copy) OnTouchedNavigationBarCallBack onTouchedCallback;

- (void)didTouchOnBarButtonItem:(UIButton *)button;

- (id)initWithOwnViewController:(UIViewController *)ownViewController;

- (id)initWithDelegate:(id <MKNavigationBarDelegate>)delegate
  andOwnViewController:(UIViewController *)ownViewController;

- (id)initWithOwnViewController:(UIViewController *)ownViewController
                callBackHandler:(OnTouchedNavigationBarCallBack)handler;
@end
