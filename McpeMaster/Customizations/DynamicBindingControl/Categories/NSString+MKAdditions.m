/*============================================================================
 PROJECT: McpeMaster
 FILE:    NSString+MKAdditions.m
 AUTHOR:  Khoai Nguyen
 DATE:    2/8/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "NSString+MKAdditions.h"
#import <CommonCrypto/CommonDigest.h>

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@implementation NSString (MKAdditions)

- (BOOL)isBlank {
    BOOL blank = YES;
    NSString *trimString = [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ([trimString length] > 0) {
        blank = NO;
    }
    return blank;
}

- (NSString *)md5Encrypted {
    const char *cStr = [self UTF8String];
    unsigned char digest[16];
    CC_MD5(cStr, (CC_LONG) strlen(cStr), digest); // This is the md5 call

    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];

    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];

    return output;
}

- (NSString *)sha1Encrypted {

    const char *cstr = [self cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:self.length];

    uint8_t digest[CC_SHA1_DIGEST_LENGTH];

    CC_SHA1(data.bytes, (CC_LONG) data.length, digest);

    NSMutableString *output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];

    for (int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];

    return output;
}

@end
