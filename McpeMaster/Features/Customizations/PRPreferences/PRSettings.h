//
//  PRSettings.h
//  McpeMaster
//
//  Created by Le Nguyen Nam Phong on 10/02/2015.
//  Copyright (c) Năm 2015 Khoai Nguyen. All rights reserved.
//

#import <Foundation/Foundation.h>

/*=============================================================================*/
// Youtube stream links
/*=============================================================================*/
typedef NS_ENUM(NSUInteger, MKYoutubeQuality) {
    MKYoutubeQuality240p_MP4 = 36,
    MKYoutubeQuality360p_MP4 = 18,
//    MKYoutubeQuality480p_MP4 = 135,
            MKYoutubeQuality720p_MP4 = 22,
    MKYoutubeQuality1080p_MP4 = 137,
    MKYoutubeQuality128Kb_MP4 = 140
};

@class PRServiceCategory, OptionalValue;

@interface PRSettings : NSObject
@property(copy, nonatomic) NSString *homePage;
@property(copy, nonatomic) NSString *safeSearch;
@property(assign, nonatomic) NSUInteger playBack;
@property(nonatomic) BOOL isLandScapeMode;
@property(nonatomic) BOOL isAutoPlay;
@property(nonatomic) BOOL isAutoFullScreen;
@property(nonatomic) BOOL isBackGroundPlayBack;

@property(assign, nonatomic) BOOL playlistShuffleMode;
@property(assign, nonatomic) BOOL playlistLoopMode;

+ (PRSettings *)defaultSettings;

- (PRServiceCategory *)category;

@end
