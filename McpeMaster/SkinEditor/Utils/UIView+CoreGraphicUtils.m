/*============================================================================
 PROJECT: SkinEditor
 FILE:    UIView+CoreGraphicUtils.m
 AUTHOR:  Nguyen Minh Khoai
 DATE:    10/5/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "UIView+CoreGraphicUtils.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@implementation UIView (CoreGraphicUtils)

- (void)drawGridLinesInRect:(CGRect)rect color:(UIColor *)color matrixDimension:(MatrixDimension)matrixDimension {
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // set line Color
    CGContextSetStrokeColorWithColor(context, color.CGColor);
    CGContextSetLineWidth(context, 1.0);
    
    CGFloat x = CGRectGetMinX(rect);
    CGFloat y = CGRectGetMinY(rect);
    CGFloat maxX = CGRectGetMaxX(rect);
    CGFloat maxY = CGRectGetMaxY(rect);
    
    CGFloat verticalDistance = CGRectGetHeight(rect) / matrixDimension.rowSize;
    CGFloat horizontalDistance = CGRectGetWidth(rect) / matrixDimension.columnSize;
    
    // draw horizontal lines
    CGFloat linePosition = 0;
    while ( linePosition <= rect.size.height ) {
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, x, linePosition + y);
        CGContextAddLineToPoint(context, maxX, linePosition + y);
        CGContextStrokePath(context);
        linePosition += verticalDistance;
    }
    
    // draw vertical lines
    linePosition = 0;
    while ( linePosition <= rect.size.width ) {
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, linePosition + x, y);
        CGContextAddLineToPoint(context, linePosition + x, maxY);
        CGContextStrokePath(context);
        linePosition += horizontalDistance;
    }
}

- (void)fillColorsOfMatrix:(SEBYTEMatrix *)matrix unitRect:(CGRect)unitRect {
    
    for (NSUInteger i = 0; i < matrix.dimension.rowSize; i++) {
        for (NSUInteger j = 0; j < matrix.dimension.columnSize; j++) {
            
            CGFloat x = unitRect.size.width * j;
            CGFloat y = unitRect.size.height * i;
            CGRect rect = (CGRect){.origin = {x, y}, .size = unitRect.size};
            UIColor *color = [matrix colorAtMatrixIndex:(MatrixIndex){.row = i, .column = j}];
            [self drawColor:color inRect:rect];
        }
    }
}

- (void)drawColor:(UIColor *)color inRect:(CGRect)rect {
    [color setFill];
    UIRectFill(rect);
}

@end
