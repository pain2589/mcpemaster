//
//  OfflineModeTableViewCell.h
//  McpeMaster
//
//  Created by Le Nguyen Nam Phong on 13/04/2015.
//  Copyright (c) Năm 2015 Khoai Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OfflineModeTableViewCellDelegatae <NSObject>

- (void)reloadButtonClick;

@end

@interface OfflineModeTableViewCell : UITableViewCell
@property(weak, nonatomic) id <OfflineModeTableViewCellDelegatae> delegate;
@end
