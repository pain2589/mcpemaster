/*============================================================================
 PROJECT: SkinEditor
 FILE:    SEMapObject.h
 AUTHOR:  Nguyen Minh Khoai
 DATE:    10/18/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SEBaseObject.h"
/*============================================================================
 MACRO
 =============================================================================*/

@class RLMSkinOnlineObject;
@interface SESkinOnlineObject : SEBaseObject
@property (nonatomic, strong) RLMSkinOnlineObject *referencedRealmObject;

+ (instancetype)newObject;

@end
