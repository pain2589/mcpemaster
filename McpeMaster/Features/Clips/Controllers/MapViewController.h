//
//  MapViewController.h
//  McpeMaster
//
//  Created by kaka on 2/5/17.
//
//

#import <UIKit/UIKit.h>
#import "ADUFileManager.h"
#import "AFNetworking.h"
#import "MKRealmManager.h"
#import "RLMMapObject.h"
#import "SEMapObject+Realm.h"
#import "UIAlertController+Utils.h"

#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@import GoogleMobileAds;
@interface MapViewController : UIViewController<MFMailComposeViewControllerDelegate,GADBannerViewDelegate,GADRewardBasedVideoAdDelegate>

@property (strong,nonatomic) GADBannerView *bannerView;

@property(weak, nonatomic) IBOutlet UIImageView *thumbImg;
@property(weak, nonatomic) IBOutlet UIButton *btnFavorite1;
@property(weak, nonatomic) IBOutlet UIButton *btnFavorite2;
@property(weak, nonatomic) IBOutlet UITextView *textViewDescription;
@property(weak, nonatomic) IBOutlet UIView *btnViewDownload;
@property(weak, nonatomic) IBOutlet UIView *btnViewImport;

@property(strong, nonatomic) NSString *mcpeId;
@property(strong, nonatomic) NSString *strTitle;
@property(strong, nonatomic) NSString *strDownloadUrl;
@property(strong, nonatomic) NSString *strThumbUrl;
@property(strong, nonatomic) NSString *strMcpeDescription;

@property (nonatomic, strong) SEMapObject *mapObject;
@property (nonatomic, strong) RLMMapObject *realmMapObject;

@end
