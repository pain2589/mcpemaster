/*============================================================================
 PROJECT: McpeMaster
 FILE:    UIScrollView+CustomizedPullToRefresh.m
 AUTHOR:  Khoai Nguyen
 DATE:    3/26/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "UIScrollView+CustomizedPullToRefresh.h"
#import "SVPullToRefresh.h"
#import "TYMActivityIndicatorView.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@implementation UIScrollView (CustomizedPullToRefresh)

- (void)addCustomizedPullToRefreshWithActionHandler:(void (^)(void))actionHandler {

    /* add pull to refresh */
    TYMActivityIndicatorView *indicator = [[TYMActivityIndicatorView alloc] init];
    indicator.activityIndicatorViewStyle = TYMActivityIndicatorViewStyleNormal;
    [indicator startAnimating];
    indicator.hidesWhenStopped = YES;

    /* add pull to refresh handler */
    [self addPullToRefreshWithActionHandler:actionHandler];

    /* customize indicator view */
    [self.pullToRefreshView setCustomView:indicator forState:SVPullToRefreshStateAll];
}

- (void)addCustomizedInfiniteScrollingWithActionHandler:(void (^)(void))actionHandler {

    /* add pull to refresh */
    TYMActivityIndicatorView *indicator = [[TYMActivityIndicatorView alloc] init];
    indicator.activityIndicatorViewStyle = TYMActivityIndicatorViewStyleNormal;
    [indicator startAnimating];
    indicator.hidesWhenStopped = YES;

    /* add pull to refresh handler */
    [self addInfiniteScrollingWithActionHandler:actionHandler];

    /* customize indicator view */
    [self.infiniteScrollingView setCustomView:indicator forState:SVPullToRefreshStateAll];
}

- (void)disableInfiniteScrolling:(BOOL)disable {
    self.showsInfiniteScrolling = !disable;
}

- (void)disablePullToRefresh:(BOOL)disable {
    self.showsPullToRefresh = !disable;
}

- (void)stopPullToRefreshAndShowInfiniteView:(BOOL)showInfiniteView {
    [self.pullToRefreshView stopAnimating];
    [self.infiniteScrollingView stopAnimating];
    [self disableInfiniteScrolling:showInfiniteView];
}

- (void)disablePullToRefreshAndInfiniteScrolling:(BOOL)disable {
    [self disablePullToRefresh:disable];
    [self disableInfiniteScrolling:disable];
}

@end
