/*============================================================================
 PROJECT: McpeMaster
 FILE:    GlobalUtils.h
 AUTHOR:  Khoai Nguyen
 DATE:    2/17/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "JSONModel.h"
#import <Foundation/Foundation.h>

/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   Optional Values
 =============================================================================*/

@interface OptionalValue : JSONModel
@property(nonatomic, copy) NSString *name;
@property(nonatomic, copy) NSString *value;
@property(nonatomic, copy) NSString *extra;
@end

/*============================================================================
 Interface:   GlobalUtils
 =============================================================================*/

@interface GlobalUtils : NSObject
+ (instancetype)sharedUtils;

+ (id)objectForKey:(NSString *)key value:(id)value inArray:(NSArray *)array;

+ (id)objectForValue:(id)value inArray:(NSArray *)array;

- (id)loadConfigurationsFromFile:(const NSString *)file parseToClass:(NSString *)className;
@end
