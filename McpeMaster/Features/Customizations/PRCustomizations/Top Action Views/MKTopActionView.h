/*============================================================================
 PROJECT: McpeMaster
 FILE:    MKTopActionView.h
 AUTHOR:  Khoai Nguyen
 DATE:    3/23/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import <UIKit/UIKit.h>

/*============================================================================
 MACRO
 =============================================================================*/
#define kFilterActions      @[\
NSLocalizedString(@"Videos", nil),\
NSLocalizedString(@"Playlists", nil),\
NSLocalizedString(@"Channels", nil)\
]

#define kAnimationDuration  0.25

/*============================================================================
 PROTOCOL
 =============================================================================*/
typedef void(^TopActionViewCompleteHandler)(NSInteger index, id object);

/*============================================================================
 Interface:   MKTopActionView
 =============================================================================*/


@interface MKTopActionView : UIView
@property(nonatomic, strong) NSArray *data;
@property(nonatomic, readonly) BOOL isShowing;

+ (id)sharedMKTopActionView;

- (void)showAtPoint:(CGPoint)point completeHandler:(TopActionViewCompleteHandler)completeHandler;

- (void)showAtPoint:(CGPoint)point onView:(UIView *)view completeHandler:(TopActionViewCompleteHandler)completeHandler;

- (void)hide:(BOOL)animated;
@end
