//
//  Cube1.h
//  GirlSkin
//
//  Created by apple on 2016. 7. 12..
//  Copyright (c) 2016년 SWORD. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <OpenGLES/ES1/gl.h>
#include <OpenGLES/ES1/glext.h>

#define PI M_PI

@interface Cube1 : NSObject{
    float mScale[3];
    float mOffset[3];
    // Angle values for main rotate.
    float mMainAngle;
    float main_step_value;
    float main_max_angle;
    float main_min_angle;
    float main_angle_axis[3];
    
    // Angle values for sub rotate.
    float mSubAngle;
    float sub_step_value;
    float sub_max_angle;
    float sub_min_angle;
    float sub_angle_axis[3];
    
    bool isFixOneAxis;
    float fixedDir;
    
    // vertex and normal buffers for cubic.
    float mVertexBuffer[4 * 10 * 3];
    float mNormalVertexBuffer[4 * 10 * 3];
    
    float vertices[3 * 12];
    int face_indecies[4 * 10];
    
    float face_vertices[4 * 10 * 3];
    float normal_vertices[3 * 4 * 10];
    
    NSMutableArray *mTextureBuffers;
}
- (void) initCube: (float) sx
               sy: (float) sy
               sz: (float) sz
          offsetx: (float) offsetx
          offsety: (float) offsety
          offsetz: (float) offsetz
       step_value: (float) step_value
       rot_axis_x: (float) rot_axis_x
       rot_axis_y: (float) rot_axis_y
       rot_axis_z: (float) rot_axis_z
        max_anlge: (float) max_angle
        min_angle: (float) min_angle
      step_value1: (float) step_value1
       max_angle1: (float) max_angle1
        min_angle: (float) min_angle1
       isFixAngle: (bool) isFixAngle
           fixDir: (float) fixDir;
- (float *) AddTextures: (float*) texCoords
                  nSize: (int) nSize;
- (void) draw: (bool) isRunning;
- (void) ClearAllTextures;
@end
