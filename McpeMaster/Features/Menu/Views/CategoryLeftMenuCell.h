/*============================================================================
 PROJECT: McpeMaster
 FILE:    CategoryLeftMenuCell.h
 AUTHOR:  Ngoc Tam Nguyen
 DATE:    12/28/14
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import <UIKit/UIKit.h>


/*============================================================================
 MACRO
 =============================================================================*/

#define kCategoryLeftMenuCellHeight     44

/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   CategoryLeftMenuCell
 =============================================================================*/

@interface CategoryLeftMenuCell : UITableViewCell

@property(weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property(weak, nonatomic) IBOutlet UILabel *categoryTitleLabel;

/* YES if this is selected */
@property(assign, nonatomic) BOOL isSelected;

@end
