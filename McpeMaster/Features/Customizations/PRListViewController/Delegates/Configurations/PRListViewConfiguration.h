/*============================================================================
 PROJECT: McpeMaster
 FILE:    PRListViewConfiguration.h
 AUTHOR:  Khoai Nguyen
 DATE:    3/19/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import <Foundation/Foundation.h>
#import "MKCollectionViewControllerProtocols.h"

#import "UIScrollView+CustomizedPullToRefresh.h"
#import "TYMActivityIndicatorView.h"

/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   PRListViewConfiguration
 =============================================================================*/


@interface PRListViewConfiguration : NSObject <MKCollectionViewControllerConfiguration>

@end

@interface PRListVideoViewConfiguration : PRListViewConfiguration
@end

@interface PRListChannelViewConfiguration : PRListViewConfiguration
@end
