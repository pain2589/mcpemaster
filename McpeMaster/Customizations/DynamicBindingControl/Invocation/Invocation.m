//
//  UInvocation.m
//  UtilWithoutReference
//
//  Created by Soc Bui on 12/10/12.
//
//

#import "Invocation.h"
#import "PrimitiveConverter.h"

#define kUInvocationCObjectType @"@"
#define kUInvocationCVoidType @"v"

@interface Invocation ()

- (void)bindParameterIntoInvocation:(NSInvocation *)pInvocation parameters:(NSArray *)pParams;

@end


@implementation Invocation

+ (Invocation *)sharedInstance {
    static Invocation *_Instance;
    if (!_Instance)
        _Instance = [[[self class] alloc] init];

    return _Instance;
}

#pragma mark - public functions -

- (NSInvocation *)createInvocationWithSelector:(SEL)selector target:(id)target parameters:(NSMutableArray *)params {
    NSInvocation *invocation = nil;
    if (target && selector) {
        NSMethodSignature *signature = [target methodSignatureForSelector:selector];

        if (signature) {
            invocation = [NSInvocation invocationWithMethodSignature:signature];
            invocation.target = target;
            invocation.selector = selector;
            params = [self generateDataTypeForParameters:params forMethod:signature];
            [self bindParameterIntoInvocation:invocation parameters:params];
        }
        else
            NSLog(@"%@", [NSString stringWithFormat:@"Internal Error 232-0843 No method signature for selector %@ in %@", NSStringFromSelector(selector), target]);
    }
    return invocation;
}

- (NSInvocation *)createInvocationWithMethodName:(NSString *)methodName target:(id)target parameters:(NSMutableArray *)params {
    return [self createInvocationWithSelector:NSSelectorFromString(methodName) target:target parameters:params];
}

- (id)invoke:(NSInvocation *)invocation {
    return [self invoke:invocation wrapWithNSValue:FALSE];
}

- (id)invoke:(NSInvocation *)invocation wrapWithNSValue:(BOOL)wrapWithNSValue {
    id result = nil;
    @try {
        if (invocation) {
            NSInteger length = invocation.methodSignature.methodReturnLength;
            void *buffer;
            if (length > 0)
                buffer = (void *) malloc(invocation.methodSignature.methodReturnLength);

            [self invoke:invocation returnData:buffer];
            if (length > 0) {
                NSValue *value = [NSValue valueWithBytes:buffer objCType:@encode(void *)];

                if (!wrapWithNSValue)
                    [value getValue:&result];
                else {
                    result = value;
                    free(buffer);
                }
            }
        }
    }
    @catch (NSException *exception) {
        result = nil;
    }
    @finally {

    }
    return result;
}

- (void)invoke:(NSInvocation *)invocation returnData:(void *)returnData {
    @try {
        if (invocation) {
            [invocation invoke];
            NSInteger length = invocation.methodSignature.methodReturnLength;
            if (length > 0)
                [invocation getReturnValue:returnData];
        }
    }
    @catch (NSException *exception) {
    }
    @finally {

    }
}

- (void)invoke:(NSInvocation *)invocation retainValue:(id *)retainData {
    @try {
        if (invocation) {
            [invocation invoke];
            NSInteger length = invocation.methodSignature.methodReturnLength;
            if (length > 0) {
                void *buffer;
                [invocation getReturnValue:&buffer];
                *retainData = (__bridge id) buffer;
//                __unsafe_unretained id result;
//                [invocation getReturnValue:&result];
//                *retainData =   result;
            }
        }
    }
    @catch (NSException *exception) {
    }
    @finally {

    }
}

- (NSUInteger)getSizeOfValue:(NSValue *)value {
    NSUInteger size;
    NSGetSizeAndAlignment([value objCType], &size, NULL);
    return size;
}

#pragma mark - private functions -

/**
bind parameters into invocation.
@Param NSInvocation    invocation
@Param NSArray         params
*/
- (void)bindParameterIntoInvocation:(NSInvocation *)invocation parameters:(NSArray *)params {
    if (invocation && params && params.count > 0) {
        BOOL isUseBuffer = FALSE;
        void *buffer = NULL;
        id param = nil;
        for (int i = 0; i < params.count; i++) {
            param = [params objectAtIndex:i];
            if ([param isEqual:[NSNull null]]) {
                param = nil;
            }
            else if ([param isKindOfClass:[NSData class]]) {
                NSData *data = param;
                buffer = malloc(data.length);
                [data getBytes:buffer length:data.length];
                isUseBuffer = TRUE;
            }
            else if ([param isKindOfClass:[NSValue class]]) {
                NSValue *value = param;
                buffer = malloc([self getSizeOfValue:value]);
                [value getValue:buffer];
                isUseBuffer = TRUE;
            }

            if (!isUseBuffer)
                [invocation setArgument:&param atIndex:2 + i];
            else {
                [invocation setArgument:buffer atIndex:2 + i];
                free(buffer);
            }

            isUseBuffer = FALSE;
        }
    }
}

/**
convert primary parameters to NSValue or NSData.
@Param NSArray             params
@Param NSMethodSignature   method
*/
- (NSMutableArray *)generateDataTypeForParameters:(NSArray *)params forMethod:(NSMethodSignature *)method {
    NSMutableArray *parameters = nil;
    if (params && method) {
        NSUInteger numberOfArgument = method.numberOfArguments - 2;
        if (params.count > 0 && (params.count == numberOfArgument)) {
            id instance = nil;
            parameters = [[NSMutableArray alloc] init];
            for (int i = 0; i < params.count; i++) {
                instance = [params objectAtIndex:i];
                const char *type = [method getArgumentTypeAtIndex:i + 2];
                if ([instance isKindOfClass:[NSString class]])
                    instance = [PrimitiveConverter valueFromText:instance withType:type];

                if (instance)
                    [parameters addObject:instance];
            }
        }
    }
    return parameters;
}

@end
