/*============================================================================
 PROJECT: McpeMaster
 FILE:    MKNavigationBarDelegateDecorator.m
 AUTHOR:  Khoai Nguyen
 DATE:    2/8/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "MKNavigationBarDelegateDecorator.h"
#import "NSObject+Utility.h"

UIBarButtonItem *createNavigationBarItem(id target, NSDictionary *attributes) {

    UIImage *image = [IBHelper loadImage:attributes[kBarItemImage]];

    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = (CGRect) {.origin = CGPointZero, .size = image.size};
    [button setImage:image forState:UIControlStateNormal];
    [button addTarget:target
               action:NSSelectorFromString(attributes[kBarItemSelector])
     forControlEvents:UIControlEventTouchUpInside];

    return [[UIBarButtonItem alloc] initWithCustomView:button];
}

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/
@interface MKNavigationBarDelegateDecorator ()
@property(nonatomic, weak, readwrite) UIViewController *ownedViewController;
@end

@implementation MKNavigationBarDelegateDecorator

- (void)dealloc {
    self.delegate = nil;
}

- (id)initWithDelegate:(id <MKNavigationBarDelegate>)delegate
  andOwnViewController:(UIViewController *)ownViewController {

    self = [super init];
    if (self) {
        self.delegate = delegate;
        self.ownedViewController = ownViewController;
    }
    return self;
}

- (id)initWithOwnViewController:(UIViewController *)ownViewController {
    return [self initWithDelegate:nil andOwnViewController:ownViewController];
}

- (id)initWithOwnViewController:(UIViewController *)ownViewController
                callBackHandler:(OnTouchedNavigationBarCallBack)handler {
    self = [self initWithOwnViewController:ownViewController];
    if (self) {
        self.onTouchedCallback = handler;
    }
    return self;
}

- (void)setUpNavigationBar {
    [self.delegate setUpNavigationBar];
}

- (void)didTouchOnBarButtonItem:(UIButton *)button {

}

@end

