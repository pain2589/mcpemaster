/*============================================================================
 PROJECT: SkinEditor
 FILE:    RLMMapObject.h
 AUTHOR:  Nguyen Van Tiep
 DATE:    11/10/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import <Realm/Realm.h>

/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   RLMTextureObject
 =============================================================================*/
@interface RLMTextureObject : RLMObject
@property NSString *mcpeId;
@property NSString *name;
@property NSString *type;
@property NSString *file;
@property NSString *thumb;
@property NSString *localFile;
@property NSString *link;
@property NSString *mcpeDescription;
@property double logTime;
@property BOOL favorited;
@property BOOL downloaded;
@end
