/*============================================================================
 PROJECT: SuperCleaner
 FILE:    MKPhotoAccessor.h
 AUTHOR:  Khoai Nguyen
 DATE:    8/20/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import <Foundation/Foundation.h>
#import <CoreGraphics/CGGeometry.h>
#import <Photos/Photos.h>
//#import "MKAssetCollection.h"

/*============================================================================
 MACRO
 =============================================================================*/
typedef void(^FetchAssetBlock)(id _Nonnull asset, NSDictionary *_Nullable info, NSUInteger index, CGFloat progress);

typedef void(^FetchAssetCompleteBlock)(id _Nonnull asset, NSDictionary *_Nonnull info);

typedef void(^FetchResultGroupsCompleteBlock)(NSArray<PHFetchResult *> *_Nullable results, BOOL success);

typedef void(^FetchResultCompleteBlock)(PHFetchResult *_Nullable result, BOOL success);

//typedef void(^FetchAssetCollectionsCompleteBlock)(MKAssetCollection *_Nullable assetCollection, BOOL success);

typedef void(^DeleteAssetsCompleteBlock)(BOOL success);

/*============================================================================
 PROTOCOL
 =============================================================================*/


/*============================================================================
 Interface:   MKPhotoAccessor
 =============================================================================*/

@interface MKPhotoAccessor : NSObject

+ (instancetype _Nonnull)sharedAccessor;

- (void)requestImageForAsset:(PHAsset *_Nonnull)asset
                  targetSize:(CGSize)size
                 contentMode:(PHImageContentMode)contentMode
               completeBlock:(FetchAssetCompleteBlock _Nonnull)completeBlock;

- (void)requestImageDataForAsset:(PHAsset *_Nonnull)asset
                   completeBlock:(FetchAssetCompleteBlock _Nonnull)completeBlock;

- (void)requestVideoForAsset:(PHAsset *_Nonnull)asset
               completeBlock:(FetchAssetCompleteBlock _Nonnull)completeBlock;

- (void)fetchPhotosAssetsCompleteBlock:(FetchResultCompleteBlock _Nonnull)completeBlock;

- (void)fetchPhotosAssetsWithoutScreenShotsCompleteBlock:(FetchResultCompleteBlock _Nonnull)completeBlock;

- (void)fetchScreenShotPhotosAssetsCompleteBlock:(FetchResultCompleteBlock _Nonnull)completeBlock;

- (void)fetchVideosAssetsCompleteBlock:(FetchResultCompleteBlock _Nonnull)completeBlock;

- (void)requestPhotoAuthorization:(nullable void (^)(BOOL granted))granted;

- (void)deleteAssets:(NSArray *_Nonnull)assets completeBlock:(DeleteAssetsCompleteBlock _Nonnull)completeBlock;

- (void)saveImage:(nonnull UIImage *)image
  completeHandler:(nullable void(^)(BOOL success, NSError * _Nullable error))completeHandler;

- (void)saveImage:(nonnull UIImage *)image toAlbum:(nonnull NSString*)albumName
  completeHandler:(nullable void(^)(BOOL success, NSError * _Nullable error))completeHandler;

@end
