/*============================================================================
 PROJECT: GetThisNotThat
 FILE:    NSObject+CheckTypes.m
 AUTHOR:  Khoai Nguyen
 DATE:    1/22/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "NSObject+CheckTypes.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@implementation NSObject (CheckTypes)

- (BOOL)isNumber {
    return IS_KIND_CLASS(self, [NSNumber class]);
}

- (BOOL)isString {
    return IS_KIND_CLASS(self, [NSString class]);
}

- (BOOL)isDictionary {
    return IS_KIND_CLASS(self, [NSDictionary class]);
}

- (BOOL)isArray {
    return IS_KIND_CLASS(self, [NSArray class]);
}

- (BOOL)isImage {
    return IS_KIND_CLASS(self, [UIImage class]);
}

@end
