/*============================================================================
 PROJECT: SkinEditor
 FILE:    SETextureObject+Realm.m
 AUTHOR:  Khoai Nguyen
 DATE:    11/13/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SEAddonObject+Realm.h"
#import "RLMAddonObject.h"
/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@implementation SEAddonObject (Realm)

- (RLMAddonObject *)realmObject {
    
    RLMAddonObject *object = [[RLMAddonObject alloc] init];
    object.type = @"addon";
    object.logTime = [[NSDate date] timeIntervalSince1970];
    
    return object;
}

+ (SEAddonObject *)fromRealmObject:(RLMAddonObject *)realmObject {
    
    if (!realmObject) return nil;
    
    if (realmObject.name.length > 0) {
        SEAddonObject *object = [SEAddonObject newObject];
        return object;
    }
    
    return nil;
}

@end
