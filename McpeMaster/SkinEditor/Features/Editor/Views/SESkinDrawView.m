/*============================================================================
 PROJECT: SkinEditor
 FILE:    SESkinDrawView.m
 AUTHOR:  Nguyen Minh Khoai
 DATE:    11/10/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SESkinDrawView.h"
#import "UIView+SEQuartzCore.h"
#import "UIView+Xib.h"
#import "SEDrawView.h"
#import "SESkinObject.h"
#import "SEDescriptionPartView.h"
#import "NSMutableArray+SESkinObject.h"
#import "SEPartViewController.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@interface SESkinDrawView()
@property (nonatomic, weak) UIView *gridView;
@property (nonatomic, assign) CGFloat partSize;
@end

@implementation SESkinDrawView

- (void)initComponents {
    UIView *gridView = [[UIView alloc] initWithFrame:self.bounds];
    gridView.backgroundColor = [UIColor clearColor];
    gridView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    [self addSubview:gridView];
    self.gridView = gridView;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initComponents];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initComponents];
}

- (CGFloat)onePartSize {
    CGRect rect = self.gridView.bounds;
    _partSize = CGRectGetWidth(rect) / (partCount + 2);
    return _partSize;
}

- (void)addPartViewsIfNeeded {
    
    /* add subviews */
    if (self.gridView.subviews.count <= 1) {
        for (NSUInteger i = 0; i < SESkinBodyPartEditCount; i++) {
            
            /* front part view */
            SEDescriptionPartView *frontPartView = [SEDescriptionPartView nibView];
            frontPartView.backgroundColor = [UIColor clearColor];
            frontPartView.drawView.lineColor = [UIColor clearColor];
            frontPartView.tag = (i + 1);
            frontPartView.drawView.tag = (i + 1);
            [self.gridView addSubview:frontPartView];
            
            /* back part view */
            SEDescriptionPartView *backPartView = [SEDescriptionPartView nibView];
            backPartView.backgroundColor = [UIColor clearColor];
            backPartView.drawView.lineColor = [UIColor clearColor];
            backPartView.tag = (i + 1 + SESkinBodyPartEditCount);
            backPartView.drawView.tag = (i + 1 + SESkinBodyPartEditCount);
            [self.gridView addSubview:backPartView];
        }
    }
}

- (void)layoutSkinFrontDirection:(BOOL)status {
    
    /* layout subviews */
    CGFloat partSize = self.onePartSize;
    
    CGSize viewSize1 = CGSizeMake(2 * partSize, 2 * partSize);
    CGSize viewSize2 = CGSizeMake(2 * partSize, 3 * partSize);
    CGSize viewSize3 = CGSizeMake(partSize, 3 * partSize);
    
    CGFloat space = 0;
    CGFloat moreIndex = status ? 0 : SESkinBodyPartEditCount;
    CGFloat y1          = partSize;
    CGFloat y2          = y1 + viewSize1.height + space;
    CGFloat y3          = y2 + viewSize2.height + space;
    CGFloat centerX     = 1.5 * partSize;
    CGFloat xPaddingSpace = status ? 0 : (partSize * 5);
    
    /* head part view */
    UIView *view = [self.gridView viewWithTag:SESkinBodyPartHead + moreIndex];
    view.frame = (CGRect){.origin = {centerX + xPaddingSpace, y1}, .size = viewSize1};
    
    /* hat */
    view = [self.gridView viewWithTag:SESkinBodyPartHat + moreIndex];
    view.frame = (CGRect){.origin = {centerX + xPaddingSpace, y1}, .size = viewSize1};
    
    /* BODY */
    view = [self.gridView viewWithTag:SESkinBodyPartBody + moreIndex];
    view.frame = (CGRect){.origin = {centerX + xPaddingSpace, y2}, .size = viewSize2};
    
    /* RIGHT HAND */
    view = [self.gridView viewWithTag:(status ? SESkinBodyPartRightArm: SESkinBodyPartLeftArm) + moreIndex];
    view.frame = (CGRect){.origin = {centerX + xPaddingSpace - space - viewSize3.width, y2}, .size = viewSize3};
    
    /* LEFT HAND */
    view = [self.gridView viewWithTag:(status ? SESkinBodyPartLeftArm : SESkinBodyPartRightArm) + moreIndex];
    view.frame = (CGRect){.origin = {centerX + xPaddingSpace + space + viewSize2.width, y2}, .size = viewSize3};
    
    /* RIGHT LEG */
    view = [self.gridView viewWithTag:(status ? SESkinBodyPartRightLeg : SESkinBodyPartLeftLeg) + moreIndex];
    view.frame = (CGRect){.origin = {centerX + xPaddingSpace - space, y3}, .size = viewSize3};
    
    /* LEFT LEG */
    view = [self.gridView viewWithTag:(status ? SESkinBodyPartLeftLeg : SESkinBodyPartRightLeg) + moreIndex];
    view.frame = (CGRect){.origin = {centerX + xPaddingSpace + viewSize3.width + space, y3}, .size = viewSize3};
}

- (void)layoutPartSubviews {
    [self layoutSkinFrontDirection:YES];
    [self layoutSkinFrontDirection:NO];
}

- (void)drawSkin {
    
    self.gridView.frame = CGRectInset(self.bounds, 5, 5);
    
    /* add subviews */
    [self addPartViewsIfNeeded];
    
    /* layout subviews */
    [self layoutPartSubviews];
    
    /* update all draw views */
    for (NSUInteger i = 1; i <= SESkinBodyPartEditCount; i++) {
     
        SESkinBodyPartObject *object = [self.skinObject.parts partAtIndex:i];
        
        /* front part */
        SEDescriptionPartView *frontView = [self.gridView viewWithTag:i];
        frontView.drawView.object = object.front;
        [frontView.drawView setNeedsDisplay];
        
        /* back part */
        SEDescriptionPartView *backView = [self.gridView viewWithTag:(i + SESkinBodyPartEditCount)];
        backView.drawView.object = object.back;
        [backView.drawView setNeedsDisplay];
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self drawSkin];
}

- (CGRect)suggestedFrame {
    
    UIView *leftLegView = [self.gridView viewWithTag:SESkinBodyPartLeftLeg];
    CGRect rectForLeftLegView = leftLegView.frame;
    CGRect rect = self.frame;
    
    return (CGRect){.origin = rect.origin, .size = {CGRectGetWidth(rect), CGRectGetMaxY(rectForLeftLegView) + self.partSize}};
}

@end
