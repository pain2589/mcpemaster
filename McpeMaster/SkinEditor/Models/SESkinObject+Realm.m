/*============================================================================
 PROJECT: SkinEditor
 FILE:    SESkinObject+Realm.m
 AUTHOR:  Khoai Nguyen
 DATE:    11/13/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SESkinObject+Realm.h"
#import "RLMSkinObject.h"
#import <UIKit/UIKit.h>
#import "ADUFileManager.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@implementation SESkinObject (Realm)

- (RLMSkinObject *)realmObject {
    
    RLMSkinObject *object = [[RLMSkinObject alloc] init];
    object.logTime = [[NSDate date] timeIntervalSince1970];
    
    return object;
}

+ (SESkinObject *)fromRealmObject:(RLMSkinObject *)realmObject {
    
    if (!realmObject) return nil;
    
    /* get image from path */
    if (realmObject.originalPath.length > 0) {
        
        NSString *imagePath = [[[ADUFileManager sharedManager] documentDirectoryPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"skins/%@",realmObject.originalPath]];
        UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
        SESkinObject *object = [SESkinObject objectFromImage:image];
        object.referencedRealmObject = realmObject;
        return object;
    }
    
    return nil;
}

@end
