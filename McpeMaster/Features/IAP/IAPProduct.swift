//
//  IAPProducts.swift
//  McpeMaster
//
//  Created by Van Tiep Nguyen on 10/10/18.
//

import StoreKit
import IAP

// (1) Define your app-specific product type:

enum IAPProduct: String {
    case weekly = "len.com.mcpemaster.weekly" // Auto renew in every week
    case life = "len.com.mcpemaster.life" // Life time plan
    
    static let allIdentifiers: [IAPProduct] = [.weekly, .life]
}

// (2) Extend it to conform to Purchaseable:

extension IAPProduct: Purchaseable {
    
    static var relevantProductIdentifiers: Set<ProductIdentifier> {
        return Set(IAPProduct.allIdentifiers.map{ $0.rawValue })
    }
    
    static var accentColorForStore: IAPColor {
        return UIColor(red:0.40, green:0.32, blue:0.81, alpha:1.00)
    }
    
    var lifetime: ProductLifetime {
        switch self {
        case .weekly: return .subscription(period: Days(7))
        case .life: return .unlimited
        }
    }
    
    var productIdentifier: ProductIdentifier {
        return rawValue
    }
    
    var marketingMessage: String {
        switch self {
        case .weekly:
            return "Weekly Premium Membership for Remove Fullscreen Ads, Unlimited download for $0.99 only.\nGet a FREE TRIAL for the first time using app."
        case .life:
            return "Lifetime Premium Membership for Remove Fullscreen Ads, Unlimited download for $29.99 only.\nPay only 1 time to get Premium forever."
        }
    }
    
    var callToActionButtonText: String {
        switch self {
        case .weekly:
            return "Try Now"
        case .life:
            return "Go Pro Now"
        }
    }
    
    var marketingTitle: String {
        switch self {
        case .weekly:
            return "Weekly Premium"
        case .life:
            return "Lifetime Premium"
        }
    }
    
    init?(productIdentifier: ProductIdentifier) {
        self.init(rawValue: productIdentifier)
    }
}

// (3) Extend it to conform to Comparable (required by Purchaseable):

extension IAPProduct: Comparable {
    static func <(lhs: IAPProduct, rhs: IAPProduct) -> Bool {
        return lhs.sortIndex < rhs.sortIndex
    }
    
    private var sortIndex: Int {
        switch self {
        case .weekly: return 1
        case .life: return 2
        }
    }
}

// (4) Extend it to conform to Equatable (required by Purchaseable):

extension IAPProduct: Equatable {
    static func ==(lhs: IAPProduct, rhs: IAPProduct) -> Bool {
        switch (lhs, rhs) {
        case (.weekly, .weekly):
            return true
        case (.life, .life):
            return true
        case (.weekly, _),
             (.life, _):
            return false
        }
    }
}

// Conveniences


fileprivate func Days(_ days: Int) -> TimeInterval {
    return TimeInterval(days * 24 * 60 * 60)
}
