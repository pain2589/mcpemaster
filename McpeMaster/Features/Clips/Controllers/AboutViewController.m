//
//  AboutViewController.m
//  McpeMaster
//
//  Created by kaka on 2/5/17.
//
//

#import "AboutViewController.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface AboutViewController ()<MFMailComposeViewControllerDelegate>

@end

@implementation AboutViewController{
    MBProgressHUD *HUD;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //
    [self.view setBackgroundColor:UIColorFromRGB(0xE6E6E6)];
    [self.textViewDescription setBackgroundColor:UIColorFromRGB(0xE6E6E6)];
    /* change color */
    [[UINavigationBar appearance] setBarTintColor:UIColorFromRGB(0xE6E6E6)];
    self.navigationController.navigationBar.translucent = NO;
//    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
//    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    //
    self.title = @"About Us";
    self.navigationController.navigationBar.titleTextAttributes = @{NSFontAttributeName: FONT_GEOMETRIA(18), NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    [[UINavigationBar appearance] setBarTintColor:UIColorFromRGB(0xE6E6E6)];
    self.navigationController.navigationBar.translucent = NO;
    //
    UIButton *saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [saveButton setImage:[UIImage imageNamed:@"icon_menu"] forState:UIControlStateNormal];
    [saveButton addTarget:self action:@selector(openCloseLeftMenu:) forControlEvents:UIControlEventTouchUpInside];
    saveButton.frame = (CGRect){.origin = {0, 0}, .size = {22, 22}};
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:saveButton];
    
    //
//    NSString *htmlString = @"<strong>Who are we?</strong><br>We are big fans of Minecraft Pocket Edition and we hope MCPE Master will make game more interesting. MCPE Master is a FREE assisting tool for playing Minecraft Pocket Edition. It will help users easily download, import and enjoy tons of interesting maps, add-ons, textures, skins, seeds, servers.<br><br><strong>Contact</strong><br>You can send any feedback or submit new maps, add-ons, textures, skins, servers at Email: mcpemaster.co@gmail.com<br>We will test your submits before bring them to MCPE Master.<br><br><strong>Thank you for using app</strong><br>If you love this app, please Rate it & Share with Friends! Your feedback keeps the MCPE Master engine running.";
    NSString *htmlString = @"<strong>Who are we?</strong><br>We are big fans of Minecraft Pocket Edition and we hope MCPE Master will make game more interesting. MCPE Master is a FREE assisting tool for playing Minecraft Pocket Edition. Our revenue is from advertisement, so that please support us if you can.<br><br><strong>Contact</strong><br>Contact us at <a href='https://mcpemaster.co'>https://mcpemaster.co</a><br><br><strong>Thank you for using MCPE Master!</strong>";
//    if (IS_IPHONE_4_OR_LESS){
//        htmlString = @"<strong>Who are we?</strong><br>We are big fans of Minecraft Pocket Edition and we hope MCPE Master will make game more interesting. MCPE Master is a FREE assisting tool for playing Minecraft Pocket Edition.<br><strong>Contact</strong><br>Email: mcpemaster.co@gmail.com or <a href='https://mcpemaster.co'>https://mcpemaster.co</a><br><strong>Thank you for using app</strong><br>If you love this app, please Rate it & Share with Friends! Your feedback keeps the MCPE Master engine running.";
//    }
    NSAttributedString *attributedString = [[NSAttributedString alloc]
                                            initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType}
                                            documentAttributes: nil
                                            error: nil
                                            ];
    self.textViewDescription.attributedText = attributedString;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initGoogleAdsBanner];
}

- (void)openCloseLeftMenu:(id)sender{
    ECSlidingViewController *slideVC = self.slidingViewController;
    BOOL anchorLeftShowing = (slideVC.currentTopViewPosition == ECSlidingViewControllerTopViewPositionAnchoredRight);
    
    if (anchorLeftShowing) {
        [slideVC resetTopViewAnimated:YES];
    } else {
        [slideVC anchorTopViewToRightAnimated:YES];
    }
}

-(IBAction)onTouchFeedback:(id)sender{
    // From within your active view controller
    if([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
        mailCont.mailComposeDelegate = self;
        
        [mailCont setSubject:@"MCPE Master Feedback"];
        [mailCont setToRecipients:[NSArray arrayWithObject:@"mcpemaster.co@gmail.com"]];
        [mailCont setMessageBody:@"" isHTML:NO];
        
        [self presentViewController:mailCont animated:YES completion:nil];
    }
}

// Then implement the delegate method
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)onTouchShare:(id)sender{
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.label.text = @"";
    [HUD removeFromSuperViewOnHide];
    [HUD showAnimated:YES];
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSString *shareMsg = [@"Best tool for Minecraft PE. Download now! http://itunes.apple.com/app/id" stringByAppendingString:AppID];
        NSArray *activityItems = @[shareMsg];
        
        UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems
                                                                                             applicationActivities:nil];
        
        activityViewController.modalPresentationStyle = UIModalPresentationPopover;
        activityViewController.popoverPresentationController.sourceView = self->_btnShare;
        [self presentViewController:activityViewController animated:activityViewController completion:nil];
        
        [self->HUD hideAnimated:YES];
    });
}

-(IBAction)onTouchRate:(id)sender{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[AppDelegate sharedDelegate].mcpeTitle] options:@{} completionHandler:nil];
}

#pragma mark - GAD Banner Ads
- (void)initGoogleAdsBanner{
    if (!self.bannerView)
    {
        self.bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerPortrait];
        [self addBannerViewToView:self.bannerView];
        
        if (IS_IPAD) {
            CGRect frame = self.bannerView.frame;
            frame.origin.x = 20;
            self.bannerView.frame = frame;
        }
        
        self.bannerView.adUnitID = ADMOB_BANNER_ID;
        self.bannerView.rootViewController = self;
        self.bannerView.delegate = self;
        self.bannerView.hidden = TRUE;
        [self.view addSubview:self.bannerView];
        
        GADRequest *request = [GADRequest request];
        //request.testDevices = @[kGADSimulatorID];
        
        //Forward user consent choice
        if (PACConsentInformation.sharedInstance.requestLocationInEEAOrUnknown &&
            PACConsentInformation.sharedInstance.consentStatus == PACConsentStatusNonPersonalized) {
            GADExtras *extras = [[GADExtras alloc] init];
            extras.additionalParameters = @{@"npa": @"1"};
            [request registerAdNetworkExtras:extras];
            NSLog(@"Ads is non-personalized now!!!");
        }
        
        [self.bannerView loadRequest:request];
    }
}

- (void)addBannerViewToView:(UIView *)bannerView {
    bannerView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:bannerView];
    if (@available(iOS 11.0, *)) {
        // In iOS 11, we need to constrain the view to the safe area.
        [self positionBannerViewFullWidthAtBottomOfSafeArea:bannerView];
    } else {
        // In lower iOS versions, safe area is not available so we use
        // bottom layout guide and view edges.
        [self positionBannerViewFullWidthAtBottomOfView:bannerView];
    }
}

#pragma mark - view positioning

- (void)positionBannerViewFullWidthAtBottomOfSafeArea:(UIView *_Nonnull)bannerView NS_AVAILABLE_IOS(11.0) {
    // Position the banner. Stick it to the bottom of the Safe Area.
    // Make it constrained to the edges of the safe area.
    UILayoutGuide *guide = self.view.safeAreaLayoutGuide;
    
    [NSLayoutConstraint activateConstraints:@[
                                              [guide.leftAnchor constraintEqualToAnchor:bannerView.leftAnchor],
                                              [guide.rightAnchor constraintEqualToAnchor:bannerView.rightAnchor],
                                              [guide.bottomAnchor constraintEqualToAnchor:bannerView.bottomAnchor]
                                              ]];
}

- (void)positionBannerViewFullWidthAtBottomOfView:(UIView *_Nonnull)bannerView {
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:bannerView
                                                          attribute:NSLayoutAttributeLeading
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeLeading
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:bannerView
                                                          attribute:NSLayoutAttributeTrailing
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeTrailing
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:bannerView
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view.safeAreaLayoutGuide.bottomAnchor
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1
                                                           constant:0]];
}

#pragma mark - Banner hide and show -

// Hide the banner by sliding down
-(void)hideBanner:(UIView*)banner
{
    banner.hidden = TRUE;
}

// Show the banner by sliding up
-(void)showBanner:(UIView*)banner
{
    banner.hidden = FALSE;
}

#pragma mark - GADBanner delegate methods -

-(void)showAdmobBanner{
    self.bannerView.hidden = FALSE;
}

-(void)hideAdmobBanner{
    self.bannerView.hidden = TRUE;
}

// Called before ad is shown, good time to show the add
- (void)adViewDidReceiveAd:(GADBannerView *)view
{
    NSLog(@"Admob load");
    [self showBanner:self.bannerView];
}

// An error occured
- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error
{
    NSLog(@"Admob error: %@", error);
    [self hideBanner:self.bannerView];
}

@end
