/*============================================================================
 PROJECT: SkinEditor
 FILE:    SEMapObject.h
 AUTHOR:  Nguyen Minh Khoai
 DATE:    10/18/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SEBaseObject.h"
/*============================================================================
 MACRO
 =============================================================================*/

@class RLMTextureObject;
@interface SETextureObject : SEBaseObject
@property (nonatomic, strong) RLMTextureObject *referencedRealmObject;

+ (instancetype)newObject;

@end
