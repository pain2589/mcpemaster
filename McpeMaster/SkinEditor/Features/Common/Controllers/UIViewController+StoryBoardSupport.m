/*============================================================================
 PROJECT: SuperCleaner
 FILE:    UIViewController+StoryBoardSupport.m
 AUTHOR:  Khoai Nguyen
 DATE:    8/7/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "UIViewController+StoryBoardSupport.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@implementation UIViewController (StoryBoardSupport)

+ (instancetype)controllerByStoryID:(NSString *)storyID
                       inStoryBoard:(NSString *)storyName
                      withArguments:(NSDictionary *)arguments {
    
    if (storyName.length == 0 || storyID.length == 0) return nil;
    
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:storyName bundle:[NSBundle mainBundle]];
    UIViewController *controller = [storyBoard instantiateViewControllerWithIdentifier:storyID];
    
    /* binding data */
    if (arguments && controller) {
        
        NSArray *propertyKeys = arguments.allKeys;
        for (NSString *key in propertyKeys) {
            if ([controller respondsToSelector:NSSelectorFromString(key)] ||
                [UIViewController instancesRespondToSelector:NSSelectorFromString(key)]) {
                [controller setValue:arguments[key] forKey:key];
            }
        }
    }
    
    return controller;
}

+ (instancetype)controllerInDefaultStoryBoardByID:(NSString *)storyID
                                    withArguments:(NSDictionary *)arguments {
    return [self controllerByStoryID:storyID
                        inStoryBoard:DefaultStoryBoard
                       withArguments:arguments];
}

+ (instancetype)controllerInDefaultStoryBoardByID:(NSString *)storyID {
    return [self controllerInDefaultStoryBoardByID:storyID withArguments:nil];
}

@end
