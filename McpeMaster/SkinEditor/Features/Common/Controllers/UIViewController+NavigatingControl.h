/*============================================================================
 FILE:    SCBaseViewController+NavigatingControl.h
 AUTHOR:  Khoai Nguyen
 DATE:    8/7/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import <UIKit/UIKit.h>
#import "UIViewController+StoryBoardSupport.h"
#import "UIViewController+NibSupport.h"

/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   SCBaseViewController_NavigatingControl
 =============================================================================*/

@interface UIViewController (NavigatingControl)

- (void)pushViewController:(UIViewController *)controller animated:(BOOL)animated;

- (void)popViewControllerAnimated:(BOOL)animated;

- (void)popToRootViewControllerAnimated:(BOOL)animated;

- (void)presentViewControllerEmbeddedInNavigationController:(UIViewController *)controller animated:(BOOL)animated;

- (void)presentViewController:(UIViewController *)controller animated:(BOOL)animated;

#pragma mark -
#pragma mark - StoryBoard

- (void)pushViewControllerWithStoryID:(NSString *)storyID
                         inStoryBoard:(NSString *)storyBoard
                            arguments:(NSDictionary *)arguments
                             animated:(BOOL)animated;

- (void)pushViewControllerWithStoryID:(NSString *)storyID
                         inStoryBoard:(NSString *)storyBoard
                             animated:(BOOL)animated;

- (void)pushViewControllerWithStoryID:(NSString *)storyID
                            arguments:(NSDictionary *)arguments
                             animated:(BOOL)animated;

- (void)pushViewControllerWithStoryID:(NSString *)storyID animated:(BOOL)animated;

#pragma mark -
#pragma mark - Nibs

- (void)pushViewControllerWithNibName:(NSString *)nibName
                            arguments:(NSDictionary *)arguments
                             animated:(BOOL)animated;
@end
