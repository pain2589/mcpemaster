/*============================================================================
 PROJECT: SkinEditor
 FILE:    SEMapObject.m
 AUTHOR:  Nguyen Minh Khoai
 DATE:    10/18/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SESkinOnlineObject.h"

@interface SESkinOnlineObject()
@end

@implementation SESkinOnlineObject

+ (instancetype)newObject {
    return [[self alloc] init];
}

@end
