//
//  UInvocation.h
//  UtilWithoutReference
//
//  Created by Soc Bui on 12/10/12.
//
//

#import <Foundation/Foundation.h>

@interface Invocation : NSObject

/**
get an instance of itself. It is singleton pattern.
*/
+ (Invocation *)sharedInstance;

/**
create a NSInvocation.
@Param SEL     pSelector
@Param id      pTarget
@Param NSArray pParams
*/
- (NSInvocation *)createInvocationWithSelector:(SEL)selector target:(id)target parameters:(NSMutableArray *)params;

/**
create a NSInvocation.
@Param NSString    pMethodName
@Param id          pTarget
@Param NSArray     pParams
*/
- (NSInvocation *)createInvocationWithMethodName:(NSString *)methodName target:(id)target parameters:(NSMutableArray *)params;

- (id)invoke:(NSInvocation *)invocation;

/**
invoke an invocation.
@Param NSInvocation    pInvocation
@Param BOOL            pWrapWithNSValue    If value is TRUE, it will wrap return value is a NSValue instance.
*/
- (id)invoke:(NSInvocation *)invocation wrapWithNSValue:(BOOL)wrapWithNSValue;

/**
invoke an invocation.
@Param NSInvocation    pInvocation
@Param void *            pReturnData    It is used to contain return value.
*/
- (void)invoke:(NSInvocation *)invocation returnData:(void *)returnData;

- (void)invoke:(NSInvocation *)invocation retainValue:(id *)retainData;

/**
get content's size of NSValue instance.
@Param NSValue pValue  NSValue instance.
*/
- (NSUInteger)getSizeOfValue:(NSValue *)value;

@end
