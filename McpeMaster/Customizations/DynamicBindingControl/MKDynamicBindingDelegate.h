/*============================================================================
 PROJECT: McpeMaster
 FILE:    MKDynamicBindingDelegate.h
 AUTHOR:  Khoai Nguyen
 DATE:    3/15/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import <Foundation/Foundation.h>

/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 PROTOCOL: MKDynamicBindingDelegate
 =============================================================================*/

/*=============================================================================*/
// MKBindingComponentDelegate
/*=============================================================================*/
@protocol MKBindingComponentDelegate <NSObject>

@property(strong, nonatomic) id data; /* data is any object which the subclass of NSObject */

@optional
@property(strong, nonatomic) NSMutableDictionary *metaData; /* property name or keyPath from data object */

@optional
- (void)bindData:(id)data;
@end
