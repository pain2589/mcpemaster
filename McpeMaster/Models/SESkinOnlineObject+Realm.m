/*============================================================================
 PROJECT: SkinEditor
 FILE:    SEMapObject+Realm.m
 AUTHOR:  Khoai Nguyen
 DATE:    11/13/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SESkinOnlineObject+Realm.h"
#import "RLMSkinOnlineObject.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@implementation SESkinOnlineObject (Realm)

- (RLMSkinOnlineObject *)realmObject {
    
    RLMSkinOnlineObject *object = [[RLMSkinOnlineObject alloc] init];
    object.type = @"skin";
    object.logTime = [[NSDate date] timeIntervalSince1970];
    
    return object;
}

+ (SESkinOnlineObject *)fromRealmObject:(RLMSkinOnlineObject *)realmObject {
    
    if (!realmObject) return nil;
    
    if (realmObject.name.length > 0) {
        SESkinOnlineObject *object = [SESkinOnlineObject newObject];
        return object;
    }
    
    return nil;
}

@end
