//
//  IBHelper.h
//  Halalgems
//
//  Created by Nguyen Minh Khoai on 12/20/12.

#import <Foundation/Foundation.h>

#define IS_EXIST_NIB_FILE(fileName) [[NSBundle mainBundle] pathForResource:fileName ofType:@"nib"] != nil)
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_NATIVE_HEIGHT (UIScreen.mainScreen.nativeBounds.size.height)

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_NATIVE_HEIGHT < 1136.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_NATIVE_HEIGHT == 1136.0)
#define IS_IPHONE_6_7_8 (IS_IPHONE && SCREEN_NATIVE_HEIGHT == 1334.0)
#define IS_IPHONE_6_7_8_P (IS_IPHONE && (SCREEN_NATIVE_HEIGHT == 1920.0 || SCREEN_NATIVE_HEIGHT == 2208.0))
#define IS_IPHONE_X_XS (IS_IPHONE && SCREEN_NATIVE_HEIGHT == 2436.0)
#define IS_IPHONE_XS_MAX (IS_IPHONE && SCREEN_NATIVE_HEIGHT == 2688.0)
#define IS_IPHONE_XR (IS_IPHONE && SCREEN_NATIVE_HEIGHT == 1792.0)


@interface IBHelper : NSObject

+ (IBHelper *)sharedUIHelper;

+ (UINib *)nibWithNibName:(NSString *)name;

+ (id)loadViewNib:(NSString *)nibName;

+ (id)loadViewControllerNib:(NSString *)nibView;

+ (id)loadStoryBoard:(NSString *)storyName;

+ (id)loadViewController:(NSString *)name inStory:(NSString *)story;

/* check iPad device */
+ (UIImage *)loadImage:(NSString *)name;

+ (UIImage *)middleStretchImage:(NSString *)name;

+ (UIImage *)horizontalStretchImage:(NSString *)name;

+ (UIImage *)verticalStretchImage:(NSString *)name;

+ (UIImage *)tiledImageName:(NSString *)imageName edgeInset:(UIEdgeInsets)inset;

+ (UIImage *)stretchImagedName:(NSString *)imageName topPadding:(CGFloat)padding;

/* no check iPad device */
+ (NSString *)imageCommonName:(NSString *)name;

+ (UIImage *)loadCommonImage:(NSString *)name;

+ (UIImage *)middleStretchCommonImage:(NSString *)name;

@end
