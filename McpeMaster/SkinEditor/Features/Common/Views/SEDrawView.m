/*============================================================================
 PROJECT: SkinEditor
 FILE:    SEDrawView.m
 AUTHOR:  Nguyen Minh Khoai
 DATE:    10/5/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SEDrawView.h"
#import "UIView+CoreGraphicUtils.h"
#import "SEDrawView+Mathematics.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@interface SEDrawView()
@property (nonatomic, assign) BOOL hasAddedGesture;
@property (nonatomic, weak) UIView *drawBackgroundView;

@end

@implementation SEDrawView

- (void)initComponents {
    
    self.clearsContextBeforeDrawing = YES;
    [self addTapGestureIfNeeded];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initComponents];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initComponents];
}

- (void)addTapGestureIfNeeded {
    if (_tapEnabled && !_hasAddedGesture) {
        UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                  action:@selector(onTouchedUpInside:)];
        [self addGestureRecognizer:gesture];
        _hasAddedGesture = YES;
    }
}

- (void)setTapEnabled:(BOOL)tapEnabled {
    _tapEnabled = tapEnabled;
    [self addTapGestureIfNeeded];
}

- (CGRect)unitRect {
    
    if ((_unitRect.size.width == 0 || _unitRect.size.height == 0) || (isinf(_unitRect.size.width) || isinf(_unitRect.size.height))) {
        _unitRect = [SEDrawView calculateUnitRectWithRect:self.bounds colorMatrix:self.object.colorMatrix];
//        NSLog(@"unitRect -> %@", NSStringFromCGRect(_unitRect));
    }
    return _unitRect;
}

- (UIColor *)fillColor {
    
    if (!_fillColor) {
        return [UIColor clearColor];
    }
    return _fillColor;
}

- (UIColor *)lineColor {
    if (!_lineColor) {
        return [UIColor whiteColor];
    }
    return _lineColor;
}

- (EditorToolBarButtonType)actionType {
    if (self.delegate && [self.delegate respondsToSelector:@selector(actionForDrawView:)]) {
        return [self.delegate actionForDrawView:self];
    }
    return EditorToolBarButtonTypePencil;
}

- (void)onTouchedUpInside:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapOnDrawView:)]) {
        [self.delegate didTapOnDrawView:self];
    }
}

#pragma mark -
#pragma mark - Support

- (MatrixIndex)detectMatrixIndexOfTouchedPoint:(CGPoint)point {
    
    CGFloat unitWidth = CGRectGetWidth(self.unitRect);
    CGFloat unitHeight = CGRectGetHeight(self.unitRect);
    
    NSUInteger row = point.y / unitHeight;
    NSUInteger column = point.x / unitWidth;
    
    return (MatrixIndex){.row = row, .column = column};
}

#pragma mark -
#pragma mark - Touch Event

- (MatrixIndex *)detectMatrixIndexesAtTouches:(NSArray *)touches {
    
    MatrixIndex *indexes = (MatrixIndex *)malloc(touches.count * sizeof(MatrixIndex));
    
    for(NSUInteger i = 0; i < touches.count; i++) {
        UITouch *touch = (UITouch *)[touches objectAtIndex:i];
        CGPoint point = [touch locationInView:self];
        MatrixIndex maxtrixIndex = [self detectMatrixIndexOfTouchedPoint:point];
        indexes[i] = maxtrixIndex;
    }
    
    return indexes;
}

- (void)fillColorInMatrixAtIndexes:(MatrixIndex *)indexes count:(NSUInteger)count {
    
    for (NSUInteger i = 0; i < count; i++) {
        [self.object.colorMatrix setColor:self.fillColor forMaxtrixIndex:indexes[i]];
    }
}

- (void)triggerActionOfTouches:(NSSet<UITouch *> *)touches {
    
    NSArray *touchArray = [touches allObjects];
    MatrixIndex *indexes = [self detectMatrixIndexesAtTouches:touchArray];
    
    EditorToolBarButtonType actionType = [self actionType];
    if (actionType == EditorToolBarButtonTypePicker) {
        UIColor *color = [self.object.colorMatrix colorAtMatrixIndex:indexes[0]];
        [self.delegate drawView:self didPickColor:color];
    }
    else if (actionType == EditorToolBarButtonTypeFill) {
        [self.object.colorMatrix fillColor:self.fillColor];
    }
    else {
        [self fillColorInMatrixAtIndexes:indexes count:touchArray.count];
    }
    
    [self setNeedsDisplay];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    if (_tapEnabled) return;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didChangedOnDrawView:)]) {
        [self.delegate didChangedOnDrawView:self];
    }
    
    [self triggerActionOfTouches:touches];
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if (_tapEnabled) return;
    [self triggerActionOfTouches:touches];
}

#pragma mark -
#pragma mark - drawRect

// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    [self fillColorsOfMatrix:self.object.colorMatrix unitRect:self.unitRect];
    [self drawGridLinesInRect:rect color:self.lineColor matrixDimension:self.object.colorMatrix.dimension];
}

@end
