/*============================================================================
 PROJECT: McpeMaster
 FILE:    ContainerViewController.m
 AUTHOR:  Khoai Nguyen
 DATE:    12/28/14
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "ContainerViewController.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@interface ContainerViewController ()

@property(strong, nonatomic) NSString *currentSegueIdentifier;
@property(assign, nonatomic) BOOL transitionInProgress;

@end

@implementation ContainerViewController

- (id)currentViewController {
    return [self.childViewControllers lastObject];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.transitionInProgress = NO;
    [self showViewControllerWithSegueIdentifier:[self.segueIdentifiers firstObject]];
}

- (void)showViewControllerAtIndex:(NSInteger)index {
    NSString *identifier = self.segueIdentifiers[index];
    [self showViewControllerWithSegueIdentifier:identifier];
}

- (void)showViewControllerWithSegueIdentifier:(NSString *)segueIdentifier {

    if (self.currentSegueIdentifier != segueIdentifier) {
        self.currentSegueIdentifier = segueIdentifier;
    }

    [self performSegueWithIdentifier:segueIdentifier sender:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    /* get index of destination view controller */
    NSInteger index = [self.segueIdentifiers indexOfObject:segue.identifier];

    /* transition to destination view controller */
    if (index != NSNotFound) {
        if (index < self.childViewControllers.count) {
            [self swapFromViewController:[self.childViewControllers firstObject] toViewController:segue.destinationViewController];
        } else {

            // If this is the very first time we're loading this we need to do
            // an initial load and not a swap.
            [self addChildViewController:segue.destinationViewController];
            UIView *destView = ((UIViewController *) segue.destinationViewController).view;

            destView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            destView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);

            [self.view addSubview:destView];
            [segue.destinationViewController didMoveToParentViewController:self];
        }
    }
}

- (void)swapFromViewController:(UIViewController *)fromViewController
              toViewController:(UIViewController *)toViewController {

    toViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);

    [fromViewController willMoveToParentViewController:nil];

    [self addChildViewController:toViewController];

    [self transitionFromViewController:fromViewController
                      toViewController:toViewController
                              duration:1.0
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:nil
                            completion:^(BOOL finished) {
                                [fromViewController removeFromParentViewController];
                                [toViewController didMoveToParentViewController:self];
                                self.transitionInProgress = NO;
                            }];
}

@end
