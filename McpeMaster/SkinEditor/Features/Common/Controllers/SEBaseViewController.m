/*============================================================================
 PROJECT: SkinEditor
 FILE:    SEBaseViewController.m
 AUTHOR:  Khoai Nguyen
 DATE:    10/4/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SEBaseViewController.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@interface SEBaseViewController ()

@end

@implementation SEBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNavigationBar];
}

//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
//    return (interfaceOrientation == UIInterfaceOrientationPortrait);
//}

- (BOOL)shouldAutorotate {
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    return (orientation == UIInterfaceOrientationPortrait);
}

- (BOOL)backButtonHidden {
    return NO;
}

- (void)setupNavigationBar {
    self.navigationController.navigationBar.titleTextAttributes = @{NSFontAttributeName: FONT_GEOMETRIA(18), NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    [[UINavigationBar appearance] setBarTintColor:UIColorFromRGB(0xE6E6E6)];
    self.navigationController.navigationBar.translucent = NO;
    
    //Tiep xoa bo tron view
//    if (self.cornerRadius) {
//        self.navigationController.view.layer.cornerRadius = self.cornerRadius.integerValue;
//    } else {
//        self.navigationController.view.layer.cornerRadius = 10;
//    }
    self.navigationController.view.clipsToBounds = YES;
    
    if (!self.backButtonHidden) {
        /* create navigation bar item */
        UIButton *saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [saveButton setImage:[UIImage imageNamed:@"icon_back"] forState:UIControlStateNormal];
        [saveButton addTarget:self action:@selector(onTouchBackButton:) forControlEvents:UIControlEventTouchUpInside];
        saveButton.frame = (CGRect){.origin = {0, 0}, .size = {22, 22}};
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:saveButton];
    }else{
        //If hide back button, that means we need show left menu button.
        UIButton *saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [saveButton setImage:[UIImage imageNamed:@"icon_menu"] forState:UIControlStateNormal];
        [saveButton addTarget:self action:@selector(openCloseLeftMenu:) forControlEvents:UIControlEventTouchUpInside];
        saveButton.frame = (CGRect){.origin = {0, 0}, .size = {22, 22}};
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:saveButton];
    }
    /* title for navigation bar */
    self.navigationItem.title = @"Skin Editor";
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    if (!self.backButtonHidden) {
    
    }else{
        /* hot fix or wordaround to force navigation bar without padding space 20px on top */
        CGRect rect = self.navigationController.navigationBar.frame;
        rect.origin.y = 0;
        self.navigationController.navigationBar.frame = rect;
        
        /* special layout for search view controller options */
        if (self.topMargin.floatValue > 0) {
            rect.origin.x = 0;
            rect.origin.y = self.topMargin.floatValue;
            rect.size.height = O_SCREEN_HEIGHT - self.topMargin.floatValue;
            rect.size.width = O_SCREEN_WIDTH;
            self.view.frame = rect;
        }
    }
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    //[[UIApplication sharedApplication] setStatusBarHidden:!self.backButtonHidden withAnimation:UIStatusBarAnimationSlide];
    [UIApplication sharedApplication].statusBarHidden = !self.backButtonHidden;
}

- (void)onTouchBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)openCloseLeftMenu:(id)sender{
    ECSlidingViewController *slideVC = self.slidingViewController;
    BOOL anchorLeftShowing = (slideVC.currentTopViewPosition == ECSlidingViewControllerTopViewPositionAnchoredRight);
    
    if (anchorLeftShowing) {
        [slideVC resetTopViewAnimated:YES];
    } else {
        [slideVC anchorTopViewToRightAnimated:YES];
    }
}

@end
