/*============================================================================
 PROJECT: SkinEditor
 FILE:    SEMapObject+Realm.m
 AUTHOR:  Khoai Nguyen
 DATE:    11/13/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "SEMapObject+Realm.h"
#import "RLMMapObject.h"
#import "ADUFileManager.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@implementation SEMapObject (Realm)

- (RLMMapObject *)realmObject {
    
    RLMMapObject *object = [[RLMMapObject alloc] init];
    object.type = @"map";
    object.logTime = [[NSDate date] timeIntervalSince1970];
    
    return object;
}

+ (SEMapObject *)fromRealmObject:(RLMMapObject *)realmObject {
    
    if (!realmObject) return nil;
    
    if (realmObject.name.length > 0) {
        SEMapObject *object = [SEMapObject newObject];
        return object;
    }
    
    return nil;
}

@end
