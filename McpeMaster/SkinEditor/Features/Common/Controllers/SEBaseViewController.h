/*============================================================================
 PROJECT: SkinEditor
 FILE:    SEBaseViewController.h
 AUTHOR:  Khoai Nguyen
 DATE:    10/4/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import <UIKit/UIKit.h>
#import "UIViewController+NavigatingControl.h"
#import "UIColor+Extensions.h"

/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   SEBaseViewController
 =============================================================================*/


@interface SEBaseViewController : UIViewController
@property (nonatomic, readonly) BOOL backButtonHidden;
@property(nonatomic, strong) NSNumber *topMargin;
@property(nonatomic, assign) NSNumber *cornerRadius;

- (void)setupNavigationBar;
- (void)onTouchBackButton:(id)sender;
- (void)openCloseLeftMenu:(id)sender;

@end
