/*============================================================================
 PROJECT: McpeMaster
 FILE:    UIScrollView+CustomizedPullToRefresh.h
 AUTHOR:  Khoai Nguyen
 DATE:    3/26/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import <UIKit/UIKit.h>

/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   UIScrollView_CustomizedPullToRefresh
 =============================================================================*/

@interface UIScrollView (CustomizedPullToRefresh)
- (void)addCustomizedPullToRefreshWithActionHandler:(void (^)(void))actionHandler;

- (void)addCustomizedInfiniteScrollingWithActionHandler:(void (^)(void))actionHandler;

- (void)stopPullToRefreshAndShowInfiniteView:(BOOL)showInfiniteView;

- (void)disablePullToRefreshAndInfiniteScrolling:(BOOL)disable;

- (void)disableInfiniteScrolling:(BOOL)disable;

- (void)disablePullToRefresh:(BOOL)disable;

@end
