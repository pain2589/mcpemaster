/*============================================================================
 PROJECT: GetThisNotThat
 FILE:    CALayer+XibConfiguration.m
 AUTHOR:  Khoai Nguyen
 DATE:    3/4/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "CALayer+XibConfiguration.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@implementation CALayer (XibConfiguration)

- (void)setBorderXibColor:(UIColor *)borderXibColor {
    self.borderColor = borderXibColor.CGColor;
}

- (UIColor *)borderXibColor {
    return [UIColor colorWithCGColor:self.borderColor];
}

@end
