//
//  OpenGLView.m
//  Tutorial01
//
//  Created by  on 12-11-24.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "OpenGLView.h"

@interface OpenGLView()

- (void)setupRenderBuffer;
- (void)destoryRenderAndFrameBuffer;
- (void)render;

@end

@implementation OpenGLView

+ (Class)layerClass {
    return [CAEAGLLayer class];
}

- (void)setupRenderBuffer {
    // Generate IDs for a framebuffer object and a color renderbuffer
    glGenFramebuffersOES(1, &_frameBuffer);
    glGenRenderbuffersOES(1, &_colorRenderBuffer);
    
    glBindFramebufferOES(GL_FRAMEBUFFER_OES, _frameBuffer);
    glBindRenderbufferOES(GL_RENDERBUFFER_OES, _colorRenderBuffer);
    
    [_context renderbufferStorage:GL_RENDERBUFFER_OES fromDrawable:(id<EAGLDrawable>)self.layer];
    
    glFramebufferRenderbufferOES(GL_FRAMEBUFFER_OES, GL_COLOR_ATTACHMENT0_OES, GL_RENDERBUFFER_OES, _colorRenderBuffer);
    
    glGetRenderbufferParameterivOES(GL_RENDERBUFFER_OES, GL_RENDERBUFFER_WIDTH_OES, &backingWidth);
    glGetRenderbufferParameterivOES(GL_RENDERBUFFER_OES, GL_RENDERBUFFER_HEIGHT_OES, &backingHeight);
    
    // For this sample, we also need a depth buffer, so we'll create and attach one via another renderbuffer.
    glGenRenderbuffersOES(1, &depthRenderbuffer);
    glBindRenderbufferOES(GL_RENDERBUFFER_OES, depthRenderbuffer);
    glRenderbufferStorageOES(GL_RENDERBUFFER_OES, GL_DEPTH_COMPONENT16_OES, backingWidth, backingHeight);
    
    glFramebufferRenderbufferOES(GL_FRAMEBUFFER_OES, GL_DEPTH_ATTACHMENT_OES, GL_RENDERBUFFER_OES, depthRenderbuffer);
    
    if(glCheckFramebufferStatusOES(GL_FRAMEBUFFER_OES) != GL_FRAMEBUFFER_COMPLETE_OES)
    {
        NSLog(@"failed to make complete framebuffer object %x", glCheckFramebufferStatusOES(GL_FRAMEBUFFER_OES));
        return;
    }
    
    
    return;

}

- (void)render {
    
    //Tiep - Customize Animation speed of Skin
    if (_isAnimated) {
        if(animationTimer == nil)
            [self setAnimationInterval:1.0f / 50.0f];
    }
    
    [EAGLContext setCurrentContext:_context];

    glBindFramebufferOES(GL_FRAMEBUFFER_OES, _frameBuffer);
   
    // draw background
    
    glLoadIdentity();
    
    //Tiep set OpenGL background color to transparent
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    // draw background plane.
    glPushMatrix();
    
    //Tiep Disable Draw Background "earth.png"
//    glBindTexture(GL_TEXTURE_2D, mBackTexData);
    
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    
    //Tiep Disable Draw Background "earth.png"
//    glVertexPointer(3, GL_FLOAT, 0, mPlaneBuffer);
//    glTexCoordPointer(2, GL_FLOAT,0, mPlaneTextureBuffer);
//    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    
    glPopMatrix();
    
    
    glTranslatef(-6.0f, -9.0f, -30.0f);
    
    //Tiep Edit size 3d model
    if (SCREEN_MAX_LENGTH >= 736.0) {
        //iPhone 6+, iPhone 7+, iPhone 8+, iPhone X or above
        glScalef(0.25f, 0.25f, 0.25f);
    }else{
        glScalef(0.35f, 0.35f, 0.35f);
    }
    
    glPushMatrix();
    
    // Set the active texture unit to texture unit 0.
    // Bind the texture to this unit.
    glBindTexture(GL_TEXTURE_2D, mCharacterTexData);

    [_mCharacter draw];
    
    glPopMatrix();
    glBindRenderbufferOES(GL_RENDERBUFFER_OES, _colorRenderBuffer);
    
    [_context presentRenderbuffer:GL_RENDERBUFFER_OES];
    
    GLenum err = glGetError();
    
    if(err)
        NSLog(@"%x error", err);
}

- (id)initWithFrame:(CGRect)frame skinImage:(UIImage*)skinImg animated:(BOOL)animated
{
    self = [super initWithFrame:frame];
    if (self) {
        _isAnimated = animated;
        _mCharacter = [GameCharacter alloc];
        _mCharacter.isAnimated = animated;
        _mCharacter.skinImg = skinImg;
        [_mCharacter initCharacter];
        
        [self initMincraft];
    }
    
    return self;
}

- (void)startAnimation
{
    animationTimer = [NSTimer scheduledTimerWithTimeInterval:animationInterval target:self selector:@selector(render) userInfo:nil repeats:YES];
}

- (void)stopAnimation
{
    [animationTimer invalidate];
    animationTimer = nil;
}

- (void)clearTexture{
    [_mCharacter clearTexture];
}

- (void)setAnimationInterval:(NSTimeInterval)interval
{
    animationInterval = interval;
    
    if(animationTimer)
    {
        [self stopAnimation];
        
    }
    [self startAnimation];
}

-(void)pan:(UIPanGestureRecognizer *)p
{
    [_mCharacter AddRotate:([p velocityInView:self].y / 180): ([p velocityInView:self].x / 180): 0];
}


#define DEGREES_TO_RADIANS(__ANGLE__) ((__ANGLE__) / 180.0 * PI)

- (void) initMincraft
{
    _eaglLayer = (CAEAGLLayer*) self.layer;
    
    //Tiep Make OpenGL background can transparent
    _eaglLayer.opaque = NO;
    
//    _eaglLayer.opaque = YES;
    
    _eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithBool:NO], kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
    
    EAGLRenderingAPI api = kEAGLRenderingAPIOpenGLES1;
    _context = [[EAGLContext alloc] initWithAPI:api];
    if (!_context || ![EAGLContext setCurrentContext:_context]) {
        NSLog(@"Failed to initialize OpenGLES 1.0 context");
        exit(1);
    }
    
    [self setupRenderBuffer];
    
    
    changeSkinImage = false;
    
    float tmpplane_vertices[] = {
        -200.0f, -100.0f, -100.0f,
        -200.0f, 100.0f, -100.0f,
        200.0f, 100.0f, -100.0f,
        200.0f, -100.0f, -100.0f
    };
    

    memcpy(plane_vertices, tmpplane_vertices, sizeof(tmpplane_vertices));
    memcpy(mPlaneBuffer, tmpplane_vertices, sizeof(tmpplane_vertices));
    
    float tmpplane_texcords[] = {
        0.0f, 1.0f,
        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f
    };
    memcpy(plane_texcords, tmpplane_texcords, sizeof(tmpplane_texcords));
    memcpy(mPlaneTextureBuffer, tmpplane_texcords, sizeof(tmpplane_texcords));


    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pan:)];
    [self addGestureRecognizer:pan];
    
    light0Position[0] = 0.0f; light0Position[1] = 0.0f; light0Position[2] = 100.0f; light0Position[3] = 0.0f;
    
    //Tiep Disable Draw Background "earth.png"
//    NSString *path1 = [[NSBundle mainBundle] pathForResource:@"earth" ofType:@"png"];
//    UIImage *backgroundImg = [[UIImage alloc] initWithContentsOfFile:path1];
//    [self loadTexture:backgroundImg forID: &mBackTexData];
    
    [self loadTexture:_mCharacter.skinImg forID: &mCharacterTexData];

    const GLfloat zNear = 0.1, zFar = 1000.0, fieldOfView = 45.0;
    GLfloat size;
    glEnable(GL_DEPTH_TEST);
    glMatrixMode(GL_PROJECTION);
    size = zNear * tanf(fieldOfView * M_PI / 180 / 2.0);
    CGRect rect = self.bounds;
    
    rect.size.width *= [UIScreen mainScreen].scale;
    rect.size.height *= [UIScreen mainScreen].scale;
    
    glFrustumf(-size, size, -size / (rect.size.width / rect.size.height), size / (rect.size.width / rect.size.height), zNear, zFar);
    
    //Tiep fix loi position 3D model
    if (IS_IPAD){
        glViewport(0, 50, rect.size.width, rect.size.height);
    }else if (IS_IPHONE){
        if (IS_IPHONE_4_OR_LESS){
            glViewport(0, 50, rect.size.width, rect.size.height);
        } else if (IS_IPHONE_6_7_8_P) {
            //iPhone 6+, iPhone 7+, iPhone 8+
            glViewport(-100, -230, rect.size.width, rect.size.height);
        } else if (IS_IPHONE_X_XS || IS_IPHONE_XS_MAX) {
            //iPhone X or above
            glViewport(-100, -300, rect.size.width, rect.size.height);
        } else if (IS_IPHONE_XR) {
            glViewport(0, 0, rect.size.width, rect.size.height);
        } else{
            glViewport(0, 0, rect.size.width, rect.size.height);
        }
    }
    
    glMatrixMode(GL_MODELVIEW);
    glShadeModel(GL_SMOOTH);
    
    
    //Code cua Wang, bi disappear texture when rotate
    //glEnable(GL_CULL_FACE);
    //glCullFace(GL_FRONT);
    
    //Tiep sua loi disappear texture when rotate
    glDisable(GL_CULL_FACE);
    
    glClearDepthf(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glShadeModel(GL_SMOOTH);
    
    glLoadIdentity();
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    
    animationTimer = nil;
}

- (void)destoryRenderAndFrameBuffer{
    if (_frameBuffer)
    {
        glDeleteFramebuffersOES(1, &_frameBuffer);
        _frameBuffer = 0;
    }
    
    if (_colorRenderBuffer)
    {
        glDeleteRenderbuffersOES(1, &_colorRenderBuffer);
        _colorRenderBuffer = 0;
    }
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffersOES(1, &depthRenderbuffer);
        depthRenderbuffer = 0;
    }

}

-(void)layoutSubviews
{
    [EAGLContext setCurrentContext:_context];
    [self destoryRenderAndFrameBuffer];
    [self setupRenderBuffer];
    [self render];
}

- (void)loadTextureFromImg:(UIImage*)img{
    [self loadTexture:img forID: &mCharacterTexData];
}

-(void)loadTexture:(UIImage *)skinImg forID:(GLuint *)tex
{
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_SRC_COLOR);
    
    if (skinImg == nil)
        NSLog(@"Do real error checking here");
    
    GLuint width = (GLuint)CGImageGetWidth(skinImg.CGImage);
    GLuint height = (GLuint)CGImageGetHeight(skinImg.CGImage);
    CGColorSpaceRef colorSpace;
    void *imageData;
    
    // get image info
    
    typedef enum {
        kTexture2DPixelFormat_Automatic = 0,
        kTexture2DPixelFormat_RGBA8888,
        kTexture2DPixelFormat_RGBA4444,
        kTexture2DPixelFormat_RGBA5551,
        kTexture2DPixelFormat_RGB565,
        kTexture2DPixelFormat_RGB888,
        kTexture2DPixelFormat_L8,
        kTexture2DPixelFormat_A8,
        kTexture2DPixelFormat_LA88,
    } Texture2DPixelFormat;
    
    Texture2DPixelFormat pixelFormat = kTexture2DPixelFormat_Automatic;
    
    
    if(pixelFormat == kTexture2DPixelFormat_Automatic){
        CGImageAlphaInfo info = CGImageGetAlphaInfo([skinImg CGImage]);
        BOOL hasAlpha = ((info == kCGImageAlphaPremultipliedLast) || (info == kCGImageAlphaPremultipliedFirst) || (info == kCGImageAlphaLast) || (info == kCGImageAlphaFirst) ? YES : NO);
        if (CGImageGetColorSpace([skinImg CGImage])) {
            if (CGColorSpaceGetModel(CGImageGetColorSpace([skinImg CGImage])) == kCGColorSpaceModelMonochrome) {
                if (hasAlpha) {
                    pixelFormat = kTexture2DPixelFormat_LA88;
                }
                else {
                    pixelFormat = kTexture2DPixelFormat_L8;
                }
            }
            else {
                if((CGImageGetBitsPerPixel([skinImg CGImage]) == 16) && !hasAlpha)
                    pixelFormat = kTexture2DPixelFormat_RGBA5551;
                else {
                    if(hasAlpha)
                        pixelFormat = kTexture2DPixelFormat_RGBA8888;
                    else {
                        pixelFormat = kTexture2DPixelFormat_RGB565;
                    }
                }
            }
        }
        else { //NOTE: No colorspace means a mask image
            pixelFormat = kTexture2DPixelFormat_A8;
        }
    }
    
    
    CGContextRef context;
    switch (pixelFormat) {
        case kTexture2DPixelFormat_RGBA8888:
        case kTexture2DPixelFormat_RGBA4444:
            colorSpace = CGColorSpaceCreateDeviceRGB();
            imageData = malloc(height * width * 4);
            context = CGBitmapContextCreate(imageData, width, height, 8, 4 * width, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
            CGColorSpaceRelease(colorSpace);
            break;
            
        case kTexture2DPixelFormat_RGBA5551:
            colorSpace = CGColorSpaceCreateDeviceRGB();
            imageData = malloc(height * width * 2);
            context = CGBitmapContextCreate(imageData, width, height, 5, 2 * width, colorSpace, kCGImageAlphaNoneSkipFirst | kCGBitmapByteOrder16Little);
            CGColorSpaceRelease(colorSpace);
            break;
            
        case kTexture2DPixelFormat_RGB888:
        case kTexture2DPixelFormat_RGB565:
            colorSpace = CGColorSpaceCreateDeviceRGB();
            imageData = malloc(height * width * 4);
            context = CGBitmapContextCreate(imageData, width, height, 8, 4 * width, colorSpace, kCGImageAlphaNoneSkipLast | kCGBitmapByteOrder32Big);
            CGColorSpaceRelease(colorSpace);
            break;
        case kTexture2DPixelFormat_LA88:
            colorSpace = CGColorSpaceCreateDeviceRGB();
            imageData = malloc(height * width * 4);
            context = CGBitmapContextCreate(imageData, width, height, 8, 4 * width, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
            CGColorSpaceRelease(colorSpace);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid pixel format"];
            
    }
    
    // Flip the Y-axis
    CGContextTranslateCTM (context, 0, 0);
    CGContextScaleCTM (context, 1, 1);
    CGContextClearRect(context, CGRectMake(0, 0, width, height));
    CGContextDrawImage( context, CGRectMake( 0, 0, width, height ), skinImg.CGImage );
    
    
    void*					tempData;
    unsigned char*			inPixel8;
    unsigned int*			inPixel32;
    unsigned char*			outPixel8;
    unsigned short*			outPixel16;
    
    //Convert "-RRRRRGGGGGBBBBB" to "RRRRRGGGGGBBBBBA"
    if (pixelFormat == kTexture2DPixelFormat_RGBA5551) {
        outPixel16 = (unsigned short*)imageData;
        for (int i = 0; i < width * height; ++i, ++outPixel16) {
            *outPixel16 = *outPixel16 << 1 | 0x0001;
        }
    }
    //Convert "RRRRRRRRRGGGGGGGGBBBBBBBBAAAAAAAA" to "RRRRRRRRRGGGGGGGGBBBBBBBB"
    else if (pixelFormat == kTexture2DPixelFormat_RGB888) {
        tempData = malloc(height * width * 3);
        inPixel8 = (unsigned char*)imageData;
        outPixel8 = (unsigned char*)tempData;
        for(int i = 0; i < width * height; ++i) {
            *outPixel8++ = *inPixel8++;
            *outPixel8++ = *inPixel8++;
            *outPixel8++ = *inPixel8++;
            inPixel8++;
        }
        free(imageData);
        imageData = tempData;
    }
    //Convert "RRRRRRRRRGGGGGGGGBBBBBBBBAAAAAAAA" to "RRRRRGGGGGGBBBBB"
    else if (pixelFormat == kTexture2DPixelFormat_RGB565) {
        tempData = malloc(height * width * 2);
        inPixel32 = (unsigned int*)imageData;
        outPixel16 = (unsigned short*)tempData;
        for(int i = 0; i < width * height; ++i, ++inPixel32) {
            *outPixel16++ = ((((*inPixel32 >> 0) & 0xFF) >> 3) << 11) | ((((*inPixel32 >> 8) & 0xFF) >> 2) << 5) | ((((*inPixel32 >> 16) & 0xFF) >> 3) << 0);
        }
        free(imageData);
        imageData = tempData;
    }
    //Convert "RRRRRRRRRGGGGGGGGBBBBBBBBAAAAAAAA" to "RRRRRGGGGBBBBAAAA"
    else if(pixelFormat == kTexture2DPixelFormat_RGBA4444) {
        tempData = malloc(height * width * 2);
        inPixel32 = (unsigned int*)imageData;
        outPixel16 = (unsigned short*)tempData;
        for (int i = 0; i < width * height; ++i, ++inPixel32) {
            *outPixel16++ = ((((*inPixel32 >> 0) & 0xFF) >> 4) << 12) | ((((*inPixel32 >> 8) & 0xFF) >> 4) << 8) | ((((*inPixel32 >> 16) & 0xFF) >> 4) << 4) | ((((*inPixel32 >> 24) & 0xFF) >> 4) << 0);
        }
        free(imageData);
        imageData = tempData;
    }
    //Convert "RRRRRRRRRGGGGGGGGBBBBBBBBAAAAAAAA" to "LLLLLLLLAAAAAAAA"
    else if (pixelFormat == kTexture2DPixelFormat_LA88) {
        tempData = malloc(height * width * 3);
        inPixel8 = (unsigned char*)imageData;
        outPixel8 = (unsigned char*)tempData;
        for (int i = 0; i < width * height; i++) {
            *outPixel8++ = *inPixel8++;
            inPixel8 += 2;
            *outPixel8++ = *inPixel8++;
        }
        free(imageData);
        imageData = tempData;
    }
    else if (pixelFormat == kTexture2DPixelFormat_RGBA8888) {
        inPixel8 = (unsigned char *)imageData;
        unsigned char max = 0;
        for (int i = 0; i < width * height; i++) {
            float alpha = (float)*(inPixel8+3) / 0xff;
            if (alpha > 0.0f && alpha < 1.0f) {
                *(inPixel8) = (unsigned char)((float)*(inPixel8) / alpha);
                if (*(inPixel8) > max) {
                    max = *(inPixel8);
                }
                inPixel8++;
                *(inPixel8) = (unsigned char)((float)*(inPixel8) / alpha);
                if (*(inPixel8) > max) {
                    max = *(inPixel8);
                }
                inPixel8++;
                *(inPixel8) = (unsigned char)((float)*(inPixel8) / alpha);
                if (*(inPixel8) > max) {
                    max = *(inPixel8);
                }
                inPixel8 += 2;
            } else {
                inPixel8 += 4;
            }
        }
    }
    
    
    GLuint  _name;
    
    glGenTextures(1, &_name);
    glBindTexture(GL_TEXTURE_2D, _name);
    
    //Code cũ của Wang, bị lỗi hiển thị border màu đen
//    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
//    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
//    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
//    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
    
    //Tiep fix black border
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
    
    switch (pixelFormat) {
        case kTexture2DPixelFormat_RGBA8888:
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageData);
            break;
            
        case kTexture2DPixelFormat_RGBA4444:
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_SHORT_4_4_4_4, imageData);
            break;
            
        case kTexture2DPixelFormat_RGBA5551:
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_SHORT_5_5_5_1, imageData);
            break;
            
        case kTexture2DPixelFormat_RGB565:
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_SHORT_5_6_5, imageData);
            break;
            
        case kTexture2DPixelFormat_RGB888:
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, imageData);
            break;
            
        case kTexture2DPixelFormat_L8:
            glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, width, height, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, imageData);
            break;
            
        case kTexture2DPixelFormat_A8:
            glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, width, height, 0, GL_ALPHA, GL_UNSIGNED_BYTE, imageData);
            break;
            
        case kTexture2DPixelFormat_LA88:
            glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE_ALPHA, width, height, 0, GL_LUMINANCE_ALPHA, GL_UNSIGNED_BYTE, imageData);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@""];
            
    }
    CGContextRelease(context);
    *tex = _name;
    free(imageData);
    
}

@end
