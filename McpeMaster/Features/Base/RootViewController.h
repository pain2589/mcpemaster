/*============================================================================
 PROJECT: McpeMaster
 FILE:    RootViewController.h
 AUTHOR:  Khoai Nguyen
 DATE:    12/23/14
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "PRECSlidingViewController.h"

/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   RootViewController
 =============================================================================*/

@class PRServiceCategory;

@interface RootViewController : PRECSlidingViewController

- (void)disableSwipeAndTapGesture:(BOOL)status;

- (void)showClipViewControllerWithSelectedCategory:(PRServiceCategory *)category isFavoriteView:(BOOL)isFavorite;

- (void)showSkinEditorViewController;

- (void)showAboutViewController;

@end
