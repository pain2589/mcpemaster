/*============================================================================
 PROJECT: McpeMaster
 FILE:    DefineConstants.m
 AUTHOR:  Khoai Nguyen
 DATE:    2/14/15
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "DefineConstants.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

NSDictionary *clipDetailInfo(NSNumber *like,
        NSString *link,
        NSString *duration,
        NSString *thumnail,
        NSNumber *viewCount,
        NSString *poster,
        NSString *publishTime,
        NSString *title) {
    return @{
            kLikeKey : NOT_NULL_NUM(like),
            kLinkKey : NOT_NULL_STR(link),
            kDurationKey : NOT_NULL_STR(duration),
            kThumbnailKey : NOT_NULL_STR(thumnail),
            kViewCountKey : NOT_NULL_NUM(viewCount),
            kPosterKey : NOT_NULL_STR(poster),
            kPublishTimeKey : NOT_NULL_STR(publishTime),
            kTitleKey : NOT_NULL_STR(title)
    };
}

/*=============================================================================*/
// NSUserDefault Utility
/*=============================================================================*/
BOOL UDIsValidKey(NSString *key) {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (!key) return NO;
    return ([[[defaults dictionaryRepresentation] allKeys] containsObject:key]);
}

void UDSetValue4Key(id value, NSString *key) {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (value && key) {
        [defaults setValue:value forKey:key];
    }
    [defaults synchronize];
}

void UDSetBool4Key(BOOL value, NSString *key) {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (key) {
        [defaults setBool:value forKey:key];
    }
    [defaults synchronize];
}

void UDSetInteger4Key(NSInteger value, NSString *key) {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (key) {
        [defaults setInteger:value forKey:key];
    }
    [defaults synchronize];
}

void UDSetDouble4Key(double value, NSString *key) {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (key) {
        [defaults setDouble:value forKey:key];
    }
    [defaults synchronize];
}

id UDValue4Key(NSString *key) {

    if (!key) return nil;

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults valueForKey:key];
}

BOOL UDBool4Key(NSString *key) {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults boolForKey:key];
}

NSInteger UDInteger4Key(NSString *key) {

    if (!key) return 0;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults integerForKey:key];

}

double UDDouble4Key(NSString *key) {

    if (!key) return 0;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults doubleForKey:key];

}
