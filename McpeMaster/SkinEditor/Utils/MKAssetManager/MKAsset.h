/*============================================================================
 PROJECT: SuperCleaner
 FILE:    MKAsset.h
 AUTHOR:  Khoai Nguyen
 DATE:    8/21/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import <Foundation/Foundation.h>

/*============================================================================
 MACRO
 =============================================================================*/
typedef NS_ENUM(short, MKAssetType) {
    MKAssetTypeSimilarPhoto,
    MKAssetTypeScreenShotPhoto,
    MKAssetTypeVideo
};
/*============================================================================
 PROTOCOL
 =============================================================================*/

/*============================================================================
 Interface:   MKAsset
 =============================================================================*/

@class PHAsset;

@interface MKAsset : NSObject
@property(nonatomic, strong) PHAsset *source;
@property(nonatomic, strong) id data;
@property(nonatomic, assign) BOOL selected;
@property(nonatomic, assign) MKAssetType type;

/* for size */
@property(nonatomic, assign) unsigned long long byteSize;

+ (instancetype)assetWithAsset:(PHAsset *)asset data:(id)data type:(MKAssetType)type;

@end
