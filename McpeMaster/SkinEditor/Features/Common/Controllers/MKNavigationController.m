/*============================================================================
 FILE:    MKNavigationController.m
 AUTHOR:  Khoai Nguyen
 DATE:    8/16/16
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "MKNavigationController.h"
#import "UIColor+Extensions.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@interface MKNavigationController ()

@end

@implementation MKNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.interactivePopGestureRecognizer.enabled = NO;
    
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor darkGrayColor],
                                 NSFontAttributeName: FONT_GEOMETRIA(18)};
    [UINavigationBar appearance].titleTextAttributes = attributes;
    
    [[UINavigationBar appearance] setBarTintColor:UIColorFromRGB(0xE6E6E6)];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.view.clipsToBounds = YES;
}

//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
//    return (interfaceOrientation == UIInterfaceOrientationPortrait);
//}

- (BOOL)shouldAutorotate {
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    return (orientation == UIInterfaceOrientationPortrait);
}

@end
