//
//  NavigationCoutryView.h
//  McpeMaster
//
//  Created by Le Nguyen Nam Phong on 14/01/2015.
//  Copyright (c) Năm 2015 Khoai Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^NavigationBarOnChooseCountryCallBack)();

@interface NavigationCoutryView : UIView

@property(nonatomic, copy) NavigationBarOnChooseCountryCallBack onChooseCountryCallBack;

- (void)setupCountryWithImage:(UIImage *)image category:(NSString *)category;
@end
